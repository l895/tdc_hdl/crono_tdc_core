.. GitLab Pages with Sphinx documentation master file, created by
   sphinx-quickstart on Thu Jan  9 10:28:38 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Crono TDC Core documentation!
====================================================

This pages describes all the necessary info about Crono TDC project.
Crono TDC core is an open source Time To Digital converter based on a multi-phase clock architecture 
that can be easily synthethized to be used in a FPGA.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   introduction.rst
   modules/core_logic.rst
   modules/hdlc_lite.rst
   modules/tdc_input.rst
   modules/uart.rst
   modules/common.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

Fork this project
==================

* https://gitlab.com/l895/tdc_hdl/crono_tdc_core

Another interesting links
==========================

* https://gitlab.com/l895/tdc_hdl/crono_tdc_vivado: Repository with different Vivado projects where Crono TDC is used.
* https://gitlab.com/l895/tdc_hdl/crono_tdc_board: Repository with Crono TDC board Altium PCB project.
* https://gitlab.com/l895/tdc_hdl/crono_tdc_py: Python library to command the Crono TDC through a serial port.
