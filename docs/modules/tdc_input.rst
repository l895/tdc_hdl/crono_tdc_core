TDC Input
***********
Here are the circuits of TDC inputs, where the "magic" happens.
This module de-serializes an input signal (probably a high speed signal) with an ISERDESE2 block in oversample mode. 
This let us to sample 4 times in a clock cycle the signal! Making the "effective" sampling freq 4 times bigger. 
Then, the output of the ISERDESE2 is feeded into a pulse decoder block. This block tags the clock cycles where a pulse is starting/stopping or happening (if the pulse is small enough). 
The output of the pulse decoder is stored in a RAM. Each row of the RAM have this format:

+-----------------------------------------------+-------------------------+-----------------------------+
|clock cycle number (bit width set by parameter)|ISERDESE2 output (4 bits)|measure tag  (3 bits)        |
+===============================================+=========================+=============================+
|Is the clock cycle where the event occurs      |ISERDESE2 output itself  |Determines the type of event |
+-----------------------------------------------+-------------------------+-----------------------------+

.. image:: ../img/tdc_input.png


Xilinx ISERDESE2 OV
====================
Xilinx provides a SERializer-DESerializer IP core called ISERDESE2 that coul be used in a configuration that is specially useful for this application.
The configuration name is `oversample mode`. In this mode, this IP core samples a signal four times in a same clock cycle, so, we could sample signals 4 times faster than system clock!.

.. image:: ../img/iserdese2_ov_xilinx_xapp523.png


ISERDESE 4B pulse decoder
==========================
This module analyze the output of an ISERDESE2 core configured in oversample mode and checks if a pulse is traveling through the channel to which the ISERDESE2 is connected.

.. image:: ../img/iserdese2_4b_pulse_decoder.png

This module have a Finite State Machine that process:
* The ISERDESE2 output delayed 1 clock cycle

* The most significant bit of the next ISERDESE2 output (msb of a "future" sample)

With this information the FSM determines if ISERDESE2 must be stored and tag it. The tag makes further processing easier.

The FSM distinguish two types of pulses: `small pulses` and `long pulses`.
* `small pulses` have a width less or equal to a clock cycle and are contained in only one clock pulse.

* `long pulses` have a width greater than a clock cycle or are small pulses that happens in two consecutive clock cycles.

The tag is a mask of 3 bits:

+------+----------------------------------------------------------------------------------------+
|tag   |meaning                                                                                 |
+======+========================================================================================+
|001   | data has a small pulse                                                                 |
+------+----------------------------------------------------------------------------------------+
|010   | data has a long pulse starting                                                         |
+------+----------------------------------------------------------------------------------------+
|011   | data has a long pulse starting and a small pulse happening                             |
+------+----------------------------------------------------------------------------------------+
|100   | data has a long pulse ending                                                           |
+------+----------------------------------------------------------------------------------------+
|101   | data has a long pulse ending and a small pulse happening                               |
+------+----------------------------------------------------------------------------------------+
|110   | data has a long pulse ending and another one starting                                  |
+------+----------------------------------------------------------------------------------------+
|111   | data has a long pulse ending, a small pulse happening and another long pulse starting  |
+------+----------------------------------------------------------------------------------------+

The output in each state of the FSM is:
* The sampled ISERDESE output if there is any pulse data in it or zero otherwise

* The tag that corresponds to ISERDESE output if there is any pulse data in it or zero otherwise


IDLE and SMALL PULSE states
-----------------------------
The FSM starts in `idle` state. And remains in this state meanwhile the channel has no activity.
The FSM is in `small pulse` state when **only** a small pulse is shown in channel.

The transitions from which the FSM can leave this states are equal! According to input, the FSM can advance to:
* `small pulse` state if only a small pulse is detected

* `long pulse starts` state if a long pulse starting is detected

* `long pulse starts small appears` state if an small pulse appears and a long one is starting

* `idle` state if no activity is shown in channel

+------------------------+-------------+------------------------+----------------------------------+
|state                   |iserdese2 out|future iserdese2 out msb|next_state                        |
+========================+=============+========================+==================================+
|`idle` or `small pulse` |0000         |x                       |idle                              |
+------------------------+-------------+------------------------+----------------------------------+
|`idle` or `small pulse` |0001         |0                       |small pulse                       |
+------------------------+-------------+------------------------+----------------------------------+
|`idle` or `small pulse` |0001         |1                       |long pulse starts                 |
+------------------------+-------------+------------------------+----------------------------------+
|`idle` or `small pulse` |0010         |x                       |small pulse                       |
+------------------------+-------------+------------------------+----------------------------------+
|`idle` or `small pulse` |0011         |0                       |small pulse                       |
+------------------------+-------------+------------------------+----------------------------------+
|`idle` or `small pulse` |0011         |1                       |long pulse starts                 |
+------------------------+-------------+------------------------+----------------------------------+
|`idle` or `small pulse` |0100         |x                       |small pulse                       |
+------------------------+-------------+------------------------+----------------------------------+
|`idle` or `small pulse` |0101         |0                       |small pulse                       |
+------------------------+-------------+------------------------+----------------------------------+
|`idle` or `small pulse` |0101         |1                       |long pulse starts small appears   |
+------------------------+-------------+------------------------+----------------------------------+
|`idle` or `small pulse` |0110         |x                       |small pulse                       |
+------------------------+-------------+------------------------+----------------------------------+
|`idle` or `small pulse` |0111         |0                       |small pulse                       |
+------------------------+-------------+------------------------+----------------------------------+
|`idle` or `small pulse` |0111         |1                       |long pulse starts                 |
+------------------------+-------------+------------------------+----------------------------------+
|`idle` or `small pulse` |1000         |x                       |small pulse                       |
+------------------------+-------------+------------------------+----------------------------------+
|`idle` or `small pulse` |1001         |0                       |small pulse                       |
+------------------------+-------------+------------------------+----------------------------------+
|`idle` or `small pulse` |1001         |1                       |long pulse starts small appears   |
+------------------------+-------------+------------------------+----------------------------------+
|`idle` or `small pulse` |1010         |x                       |small pulse                       |
+------------------------+-------------+------------------------+----------------------------------+
|`idle` or `small pulse` |1011         |0                       |small pulse                       |
+------------------------+-------------+------------------------+----------------------------------+
|`idle` or `small pulse` |1011         |1                       |long pulse starts small appears   |
+------------------------+-------------+------------------------+----------------------------------+
|`idle` or `small pulse` |1100         |x                       |small pulse                       |
+------------------------+-------------+------------------------+----------------------------------+
|`idle` or `small pulse` |1101         |0                       |small pulse                       |
+------------------------+-------------+------------------------+----------------------------------+
|`idle` or `small pulse` |1101         |1                       |long pulse starts small appears   |
+------------------------+-------------+------------------------+----------------------------------+
|`idle` or `small pulse` |1110         |x                       |small pulse                       |
+------------------------+-------------+------------------------+----------------------------------+
|`idle` or `small pulse` |1111         |0                       |small pulse                       |
+------------------------+-------------+------------------------+----------------------------------+
|`idle` or `small pulse` |1111         |1                       |long pulse starts                 |
+------------------------+-------------+------------------------+----------------------------------+

LONG PULSE STARTS, LONG PULSE STARTS SMALL APPEARS and LONG PULSE END LONG STARTS states
------------------------------------------------------------------------------------------
The FSM is in one of this states when a long pulse is starting. The output is different in each state because the events are different, but the transitions to another states are equal.
The differences between this states are the following:
* `long pulse starts`: **only** the start of a long pulse is detected in the channel

* `long pulse starts small appears`: the start of a long pulse is detected and a small pulse is detected

* `long pulse end long starts`: a long pulse finished, the start of a long pulse is detected and a small pulse is detected

According to input, the FSM can advance to:
* `long pulse continue` state if the pulse is still active

* `long pulse end` state if the pulse that started is ending

* `long pulse end long starts` state if the pulse that started is ending and another one is starting,

* `long pulse end small appears` state if the pulse that started is ending and a small pulse is detected

+-----------------------------------------------------------------------------------------+-------------+------------------------+----------------------------------------------------------------------+
|state                                                                                    |iserdese2 out|future iserdese2 out msb|next_state                                                            |
+=========================================================================================+=============+========================+======================================================================+
|`long pulse starts` or `long pulse starts small appears` or `long pulse end long starts` |0000         |x                       |long pulse end                                                        |
+-----------------------------------------------------------------------------------------+-------------+------------------------+----------------------------------------------------------------------+
|`long pulse starts` or `long pulse starts small appears` or `long pulse end long starts` |0001         |0                       |long pulse end small appears                                          |
+-----------------------------------------------------------------------------------------+-------------+------------------------+----------------------------------------------------------------------+
|`long pulse starts` or `long pulse starts small appears` or `long pulse end long starts` |0001         |1                       |long pulse end long starts                                            |
+-----------------------------------------------------------------------------------------+-------------+------------------------+----------------------------------------------------------------------+
|`long pulse starts` or `long pulse starts small appears` or `long pulse end long starts` |0010         |x                       |long pulse end small appears                                          |
+-----------------------------------------------------------------------------------------+-------------+------------------------+----------------------------------------------------------------------+
|`long pulse starts` or `long pulse starts small appears` or `long pulse end long starts` |0011         |0                       |long pulse end small appears                                          |
+-----------------------------------------------------------------------------------------+-------------+------------------------+----------------------------------------------------------------------+
|`long pulse starts` or `long pulse starts small appears` or `long pulse end long starts` |0011         |1                       |long pulse end long starts                                            |
+-----------------------------------------------------------------------------------------+-------------+------------------------+----------------------------------------------------------------------+
|`long pulse starts` or `long pulse starts small appears` or `long pulse end long starts` |0100         |x                       |long pulse end small appears                                          |
+-----------------------------------------------------------------------------------------+-------------+------------------------+----------------------------------------------------------------------+
|`long pulse starts` or `long pulse starts small appears` or `long pulse end long starts` |0101         |0                       |long pulse end small appears                                          |
+-----------------------------------------------------------------------------------------+-------------+------------------------+----------------------------------------------------------------------+
|`long pulse starts` or `long pulse starts small appears` or `long pulse end long starts` |0101         |1                       |X (this combination is not possible, and if does, we need to check it)|
+-----------------------------------------------------------------------------------------+-------------+------------------------+----------------------------------------------------------------------+
|`long pulse starts` or `long pulse starts small appears` or `long pulse end long starts` |0110         |x                       |long pulse end small appears                                          |
+-----------------------------------------------------------------------------------------+-------------+------------------------+----------------------------------------------------------------------+
|`long pulse starts` or `long pulse starts small appears` or `long pulse end long starts` |0111         |0                       |long pulse end small appears                                          |
+-----------------------------------------------------------------------------------------+-------------+------------------------+----------------------------------------------------------------------+
|`long pulse starts` or `long pulse starts small appears` or `long pulse end long starts` |0111         |1                       |long pulse end long starts                                            |
+-----------------------------------------------------------------------------------------+-------------+------------------------+----------------------------------------------------------------------+
|`long pulse starts` or `long pulse starts small appears` or `long pulse end long starts` |1000         |x                       |long pulse end                                                        |
+-----------------------------------------------------------------------------------------+-------------+------------------------+----------------------------------------------------------------------+
|`long pulse starts` or `long pulse starts small appears` or `long pulse end long starts` |1001         |0                       |long pulse end small appears                                          |
+-----------------------------------------------------------------------------------------+-------------+------------------------+----------------------------------------------------------------------+
|`long pulse starts` or `long pulse starts small appears` or `long pulse end long starts` |1001         |1                       |long pulse end long starts                                            |
+-----------------------------------------------------------------------------------------+-------------+------------------------+----------------------------------------------------------------------+
|`long pulse starts` or `long pulse starts small appears` or `long pulse end long starts` |1010         |x                       |long pulse end small appears                                          |
+-----------------------------------------------------------------------------------------+-------------+------------------------+----------------------------------------------------------------------+
|`long pulse starts` or `long pulse starts small appears` or `long pulse end long starts` |1011         |0                       |long pulse end small appears                                          |
+-----------------------------------------------------------------------------------------+-------------+------------------------+----------------------------------------------------------------------+
|`long pulse starts` or `long pulse starts small appears` or `long pulse end long starts` |1011         |1                       |long pulse end long starts                                            |
+-----------------------------------------------------------------------------------------+-------------+------------------------+----------------------------------------------------------------------+
|`long pulse starts` or `long pulse starts small appears` or `long pulse end long starts` |1100         |x                       |long pulse end                                                        |
+-----------------------------------------------------------------------------------------+-------------+------------------------+----------------------------------------------------------------------+
|`long pulse starts` or `long pulse starts small appears` or `long pulse end long starts` |1101         |0                       |long pulse end small appears                                          |
+-----------------------------------------------------------------------------------------+-------------+------------------------+----------------------------------------------------------------------+
|`long pulse starts` or `long pulse starts small appears` or `long pulse end long starts` |1101         |1                       |long pulse end long starts                                            |
+-----------------------------------------------------------------------------------------+-------------+------------------------+----------------------------------------------------------------------+
|`long pulse starts` or `long pulse starts small appears` or `long pulse end long starts` |1110         |x                       |long pulse end                                                        |
+-----------------------------------------------------------------------------------------+-------------+------------------------+----------------------------------------------------------------------+
|`long pulse starts` or `long pulse starts small appears` or `long pulse end long starts` |1111         |0                       |long pulse end                                                        |
+-----------------------------------------------------------------------------------------+-------------+------------------------+----------------------------------------------------------------------+
|`long pulse starts` or `long pulse starts small appears` or `long pulse end long starts` |1111         |1                       |long pulse continue                                                   |
+-----------------------------------------------------------------------------------------+-------------+------------------------+----------------------------------------------------------------------+

LONG PULSE CONTINUE state
---------------------------
The FSM is in this state when there is a pulse active in the channel (in fact, the channel is in a logic high state).
According to input, the FSM can advance to:
* `long pulse end` state if the ongoing pulse is ending

* `long pulse end long starts` state if the ongoing pulse is ending and another one is starting,

* `long pulse end small appears` state if the ongoing pulse is ending and a small pulse is detected

+----------------------+-------------+------------------------+-----------------------------------------------------------------------+
|state                 |iserdese2 out|future iserdese2 out msb|next_state                                                             |
+======================+=============+========================+=======================================================================+
|`long pulse continue` |0000         |x                       |long pulse end                                                         |
+----------------------+-------------+------------------------+-----------------------------------------------------------------------+
|`long pulse continue` |0001         |0                       |long pulse end small appears                                           |
+----------------------+-------------+------------------------+-----------------------------------------------------------------------+
|`long pulse continue` |0001         |1                       |long pulse end long starts                                             |
+----------------------+-------------+------------------------+-----------------------------------------------------------------------+
|`long pulse continue` |0010         |x                       |long pulse end small appears                                           |
+----------------------+-------------+------------------------+-----------------------------------------------------------------------+
|`long pulse continue` |0011         |0                       |long pulse end small appears                                           |
+----------------------+-------------+------------------------+-----------------------------------------------------------------------+
|`long pulse continue` |0011         |1                       |long pulse end long starts                                             |
+----------------------+-------------+------------------------+-----------------------------------------------------------------------+
|`long pulse continue` |0100         |x                       |long pulse end small appears                                           |
+----------------------+-------------+------------------------+-----------------------------------------------------------------------+
|`long pulse continue` |0101         |0                       |long pulse end small appears                                           |
+----------------------+-------------+------------------------+-----------------------------------------------------------------------+
|`long pulse continue` |0101         |1                       |X (this combination is not possible, and if does, we need to check it) |
+----------------------+-------------+------------------------+-----------------------------------------------------------------------+
|`long pulse continue` |0110         |x                       |long pulse end small appears                                           |
+----------------------+-------------+------------------------+-----------------------------------------------------------------------+
|`long pulse continue` |0111         |0                       |long pulse end small appears                                           |
+----------------------+-------------+------------------------+-----------------------------------------------------------------------+
|`long pulse continue` |0111         |1                       |long pulse end long starts                                             |
+----------------------+-------------+------------------------+-----------------------------------------------------------------------+
|`long pulse continue` |1000         |x                       |long pulse end                                                         |
+----------------------+-------------+------------------------+-----------------------------------------------------------------------+
|`long pulse continue` |1001         |0                       |long pulse end small appears                                           |
+----------------------+-------------+------------------------+-----------------------------------------------------------------------+
|`long pulse continue` |1001         |1                       |long pulse end long starts                                             |
+----------------------+-------------+------------------------+-----------------------------------------------------------------------+
|`long pulse continue` |1010         |x                       |long pulse end small appears                                           |
+----------------------+-------------+------------------------+-----------------------------------------------------------------------+
|`long pulse continue` |1011         |0                       |long pulse end small appears                                           |
+----------------------+-------------+------------------------+-----------------------------------------------------------------------+
|`long pulse continue` |1011         |1                       |long pulse end long starts                                             |
+----------------------+-------------+------------------------+-----------------------------------------------------------------------+
|`long pulse continue` |1100         |x                       |long pulse end                                                         |
+----------------------+-------------+------------------------+-----------------------------------------------------------------------+
|`long pulse continue` |1101         |0                       |long pulse end small appears                                           |
+----------------------+-------------+------------------------+-----------------------------------------------------------------------+
|`long pulse continue` |1101         |1                       |long pulse end long starts                                             |
+----------------------+-------------+------------------------+-----------------------------------------------------------------------+
|`long pulse continue` |1110         |x                       |long pulse end                                                         |
+----------------------+-------------+------------------------+-----------------------------------------------------------------------+
|`long pulse continue` |1111         |0                       |long pulse end                                                         |
+----------------------+-------------+------------------------+-----------------------------------------------------------------------+
|`long pulse continue` |1111         |1                       |long pulse continue                                                    |
+----------------------+-------------+------------------------+-----------------------------------------------------------------------+

Note: transitions are exactly the same as `long pulse starts`, `long pulse starts small appears` and `long pulse end long starts` states.


LONG PULSE END and LONG PULSE END SMALL APPEARS state
-------------------------------------------------------
The FSM is in `long pulse end` state when a long pulse is ending and no other activity is shown in the channel.
And is in `long pulse small appears` when a long pulse is ending and a small pulse is detected.

The transitions are equal for both states.

According to input, the FSM can advance to:
* `idle` state if no activity is shown in channel

* `small pulse` state if **only** a small pulse is detected in the channel

* `long pulse starts` state if a long pulse is starting

* `long pulse starts small appears` state if an small pulse appears and a long one is starting

+--------------------------------------------------+-------------+------------------------+----------------------------------------+
|state                                             |iserdese2 out|future iserdese2 out msb|next_state                              |
+==================================================+=============+========================+========================================+
|`long pulse end` or `long pulse end small appers` |0000         |x                       |idle                                    |
+--------------------------------------------------+-------------+------------------------+----------------------------------------+
|`long pulse end` or `long pulse end small appers` |0001         |0                       |small pulse                             |
+--------------------------------------------------+-------------+------------------------+----------------------------------------+
|`long pulse end` or `long pulse end small appers` |0001         |1                       |long pulse starts                       |
+--------------------------------------------------+-------------+------------------------+----------------------------------------+
|`long pulse end` or `long pulse end small appers` |0010         |x                       |small pulse                             |
+--------------------------------------------------+-------------+------------------------+----------------------------------------+
|`long pulse end` or `long pulse end small appers` |0011         |0                       |small pulse                             |
+--------------------------------------------------+-------------+------------------------+----------------------------------------+
|`long pulse end` or `long pulse end small appers` |0011         |1                       |long pulse starts                       |
+--------------------------------------------------+-------------+------------------------+----------------------------------------+
|`long pulse end` or `long pulse end small appers` |0100         |x                       |small pulse                             |
+--------------------------------------------------+-------------+------------------------+----------------------------------------+
|`long pulse end` or `long pulse end small appers` |0101         |0                       |small pulse                             |
+--------------------------------------------------+-------------+------------------------+----------------------------------------+
|`long pulse end` or `long pulse end small appers` |0101         |1                       |long pulse starts small appears         |
+--------------------------------------------------+-------------+------------------------+----------------------------------------+
|`long pulse end` or `long pulse end small appers` |0110         |x                       |small pulse                             |
+--------------------------------------------------+-------------+------------------------+----------------------------------------+
|`long pulse end` or `long pulse end small appers` |0111         |0                       |small pulse                             |
+--------------------------------------------------+-------------+------------------------+----------------------------------------+
|`long pulse end` or `long pulse end small appers` |0111         |1                       |long pulse starts                       |
+--------------------------------------------------+-------------+------------------------+----------------------------------------+
|`long pulse end` or `long pulse end small appers` |1000         |x                       |small pulse                             |
+--------------------------------------------------+-------------+------------------------+----------------------------------------+
|`long pulse end` or `long pulse end small appers` |1001         |0                       |small pulse                             |
+--------------------------------------------------+-------------+------------------------+----------------------------------------+
|`long pulse end` or `long pulse end small appers` |1001         |1                       |long pulse starts small appears         |
+--------------------------------------------------+-------------+------------------------+----------------------------------------+
|`long pulse end` or `long pulse end small appers` |1010         |x                       |small pulse                             |
+--------------------------------------------------+-------------+------------------------+----------------------------------------+
|`long pulse end` or `long pulse end small appers` |1011         |0                       |small pulse                             |
+--------------------------------------------------+-------------+------------------------+----------------------------------------+
|`long pulse end` or `long pulse end small appers` |1011         |1                       |long pulse starts small appears         |
+--------------------------------------------------+-------------+------------------------+----------------------------------------+
|`long pulse end` or `long pulse end small appers` |1100         |x                       |small pulse                             |
+--------------------------------------------------+-------------+------------------------+----------------------------------------+
|`long pulse end` or `long pulse end small appers` |1101         |0                       |small pulse                             |
+--------------------------------------------------+-------------+------------------------+----------------------------------------+
|`long pulse end` or `long pulse end small appers` |1101         |1                       |long pulse starts small appears         |
+--------------------------------------------------+-------------+------------------------+----------------------------------------+
|`long pulse end` or `long pulse end small appers` |1110         |x                       |small pulse                             |
+--------------------------------------------------+-------------+------------------------+----------------------------------------+
|`long pulse end` or `long pulse end small appers` |1111         |0                       |small pulse                             |
+--------------------------------------------------+-------------+------------------------+----------------------------------------+
|`long pulse end` or `long pulse end small appers` |1111         |1                       |long pulse starts                       |
+--------------------------------------------------+-------------+------------------------+----------------------------------------+

Output logic
-----------------
The FSM have 2 outputs: the value and the tag of the measurement. This output depends only of the current state (Moore machine):

+-------------------------------------------+-------------------------------+
|state                                      |output                         |
+===========================================+===============================+
|`idle`                                     |Q = 0;             TAG = 0     |
+-------------------------------------------+-------------------------------+
|`small pulse`                              |Q = ISERDESE2_OUT; TAG = 0b001 |
+-------------------------------------------+-------------------------------+
|`long pulse starts`                        |Q = ISERDESE2_OUT; TAG = 0b010 |
+-------------------------------------------+-------------------------------+
|`long pulse starts small appears`          |Q = ISERDESE2_OUT; TAG = 0b011 |
+-------------------------------------------+-------------------------------+
|`long pulse continue`                      |Q = 0;             TAG = 0     |
+-------------------------------------------+-------------------------------+
|`long pulse end`                           |Q = ISERDESE2_OUT; TAG = 0b100 |
+-------------------------------------------+-------------------------------+
|`long pulse end small appears`             |Q = ISERDESE2_OUT; TAG = 0b101 |
+-------------------------------------------+-------------------------------+
|`long pulse end long starts`               |Q = ISERDESE2_OUT; TAG = 0b110 |
+-------------------------------------------+-------------------------------+

TDC Input RAM
==================
This module is composed by a RAM and a RAM controller. The RAM controller writes the input data into RAM when the `enable` signal is high and the data it's valid. Which data is valid? The data that have the bits [6:3] different from zero.
When `enable` signal is low, the RAM could be read from an external logic.

.. image:: ../img/tdc_input_ram.png


References
============
- [1] Xilinx XAPP523: https://docs.xilinx.com/v/u/en-US/xapp523-lvds-4x-asynchronous-oversampling
- [2] Xilinx UG471: https://docs.xilinx.com/v/u/en-US/ug471_7Series_SelectIO