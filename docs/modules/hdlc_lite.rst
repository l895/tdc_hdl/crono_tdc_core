HDLC-Lite processor
********************

HDLC-lite is a protocol that includes framing, byte-stuffing and a CRC.
Data sent and received by TDC have the following frame:

.. image:: ../img/hdlc_lite_frame_format.png

Each frame have a frame delimiter (0x7E) byte as boundary. The receptor can detect a frame in the channel looking for this frame delimiter char.
Also, to include this character in the data, the protocol uses byte-stuffing. With this mechanism, every data character that is equal to:
* the frame delimiter character: 0x7E
* the control character: 0x7D

is escaped, preceding it with a control character 0x7D and xoring the character per se with 0x20. In this manner, no frame delimiter chars will be inside a frame, and if the receiver detects a control character in the frame, will know that must unescape it.

Take in count that ALL the frame is escaped, including the CRC (frame delimiter are not included of course). In fact, the transmitter will compute CRC of data first and **then** escape the set DATA + CRC.

In this manner, if the CRC have a frame delimiter char or a control char, this char is escaped.

.. image:: ../img/tdc_hdlc_frames.png

The module is split in two sub-modules: TX and RX. Each one have it's complexities that are explained in the diagram of the following sub-sections.

HDLC-Lite TX
===============
This is the block that receives a buffer of bits and puts this data into a HDLC-lite frame. Then interacts with a block that handles the serial communication of the device (UART like logic block) to send byte by byte the HDLC-lite frame.

This block:
1. samples input data when `i_tx_dv` is different from zero

2. computes CRC16-CCITT of this data

3. escapes the data *and* the CRC

4. sends start frame delimiter byte

5. sends each byte of the of the frame (data and CRC)

6. sends end frame delimiter byte

.. image:: ../img/hdlc_lite_tx.png


HDLC-Lite RX
===============
This is the block that receives bytes from another serial port transceiver block and decodes this bytes to build a HDLC-lite frame.

.. image:: ../img/hdlc_lite_rx.png