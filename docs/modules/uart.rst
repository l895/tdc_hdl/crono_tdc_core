UART
***********
UART module used by Crono TDC!
This simple module implements a UART with 8 data bits, 1 start bit and 1 stop bit at a given baudrate.
The baudrate could be configured with a parameter.

UART TX
====================
This module implements a UART transmitter.

The circuit is fairly simple, is composed by a counter, a shift register and a simple FSM.
When input data valid signal `i_tx_dv` is set high, the FSM samples the input data byte and the UART frame is composed (start bit + data bits + stop bit).
After that, the counter and the shift register is enabled. The counter will set a flag every `CLK_PER_BITS` clock cycles and when any of this flags is set, the 
shift register will advance to the next bit. The `CLK_PER_BITS` value is the quantity of clock cycles that a bit must be set to respect the baudrate.
When all the bits are sent (10 bits), the FSM set `o_tx_done` flag high during 1 clock cycle and the system is restored to it's initial state.

.. image:: ../img/uart_tx.png


UART RX
====================
*WIP*