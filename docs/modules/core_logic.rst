Core Logic
--------------------

Crono TDC Core Logic acts as the processor of the system. 
It process the data sent by the user and detects if that data corresponds to a command.
If a command is detected, then the core logic launch the action of that command.

To implement this, this module was split in two: a command processor and a command executor.

.. image:: ../img/tdc_core_logic.svg

Commands Processor
*******************

Command processor is a module that parses process a chunk of data and verifies if that data corresponds to a command frame.

A command frame have the following format:

.. image:: ../img/crono_tdc_command_frame.svg


To decode if a chunk of bytes is a command, the following circuit is used:

.. image:: ../img/cmd_processor.svg

In the following Table we can find the description of each port:

.. list-table:: Command Processor Inputs and Outputs Ports
   :widths: 20 20 60
   :header-rows: 1

   * - Port
     - Direction
     - Description

   * - i_rx_data
     - Input
     - Input data signal. Through this port enters the chunk of data that must be processed.

   * - i_rx_dv
     - Input
     - This signal declares the quantity of bits of `i_rx_data` that contains useful information.

   * - o_rx_cmd_data
     - Output
     - It is the data field of the command frame. 

   * - o_cmd_exec_start
     - Output
     - Identifies which action must be done when a known command is received.

First, the DATA and ID fields from the frame are extracted with combinational circuits. 
In particular, the DATA field is extracted with a barrel shifter and the ID is extracted with some AND gates.
The ID is feed into a Look Up Table that matches the commands IDs with another ID called `Executor ID` that is used by the command executor to know which command must be executed.
Also, the command ID is compared with every known ID by the command processor. If the received ID is equal to almost one known ID then the signal `rx_cmd_valid[0]` is set to `1`.
When a rising edge is detected in the `rx_cmd_valid[0]` signal the `rx_cmd_valid_red` signal is set.

`rx_cmd_valid_red` signal is prolonged for the next 2 clock cycles, allowing the pass of the executor ID to the output port `o_cmd_exec_start`.

In the following figure can be seen the simulation of the circuit.

.. image:: ../img/cmd_processor_waveform.png

Commands Executor
*******************
This module is responsible for executing the actions that must be launched when receiving a command.
Every single command have a correspondent command executor. 
As an example, if the user sends a command to modify a configuration register, then there is a command executor that writes this configuration register.

Since there are several commands, and therefore, several command executors, this module manages which executor must be activated at each instant.

The executors trigger their actions when they detect a rising edge on the i_start port. 
Each bit of the i_cmd_exec_start port is connected to the enable port of an executor, therefore, it is through this port that executors are enabled.

If two commands are received at the same time, both could be executed, but since there is only one data output channel, if the channel is in use and another executor wants to send data, it must wait until the channel is free.
The semaphorization of the output buffer was implemented with an arbitrator and a set of multiplexers.

When an executor wants to use the output buffer, it notifies the arbitrator through the o_tx_active port. 
When this happens, the arbitrator manipulates the selection port of the multiplexers, assigning the buffer to said executor and notifies it that it has access to the buffer using its i_tx_allowed port.

In the following Figure and Table we can find the circuit and the description of the ports of this module.

.. image:: ../img/cmd_executor.svg

.. list-table:: Command Executor Inputs and Outputs Ports
   :widths: 20 20 60
   :header-rows: 1

   * - Port
     - Direction
     - Description

   * - `i_dready`
     - Input
     - Signal used by TDC Inputs to notify when the data stored in their internal storage is ready to read.

   * - `o_chn_ena`
     - Output
     - Signal used to enable TDC Inputs. Each bit corresponds to a single channel. Used only by "Measure Command Executor".

   * - `o_read_chn_sel`
     - Output
     - Signal used to select which TDC Input data will be read. Used only by "Read Channels Data Command Executor".

   * - `i_read_chn_lwa`
     - Input
     - This port holds the last RAM written address of the selected TDC Input. Used only by "Read Channels Data Command Executor".

   * - `o_read_chn_addr`
     - Output
     - This is the address that will be read from the selected TDC Input. Used only by "Read Channels Data Command Executor".

   * - `i_read_chn_data`
     - Input
     - Through this port enters the data from the selected TDC Input. Used only by "Read Channels Data Command Executor".

   * - `i_cmd_exec_start`
     - Input
     - This port is used to enable command executors, in fact, each bit of this port correspond to a determined command executor. When a bit of this port is set, then a command executor is enabled.

   * - `i_rx_cmd_data`
     - Input
     - This is the data that the user sent through a command. Some commands needs the user data, i.e. the command the commands that writes a register.

   * - `o_tx_data`
     - Output
     - This is the data sent by the command executors to the user.

   * - `i_tx_data_done`
     - Input
     - When this port is set, the command executor that holds the tx buffer assumes that the data buffer was fully transmitted.

   * - `o_tx_data_valid`
     - Output
     - This port have the quantity of valid bits of o_tx_data port.

In the following figure we can see the command executor working. In this case, READ_TIME_WINDOWS_US command executor was enabled (bit 2 of `i_cmd_exec_start` was set).
This executor takes the output buffer using `w_read_time_windows_us_tx_active` signal and then sends the `w_time_windows_us` value 0xA through the output buffer `o_tx_data`.
Then, when data is fully transferred (and `i_tx_data_done` signal is set) the executor sets low `w_read_time_windows_us_tx_active` signal, releasing the buffer.

.. image:: ../img/cmd_executor_waveform.png

The different executors are described in the following sections.

Command Executor: Read Register
================================
This command executor reads the value of a register and send it through to whatever it is the transmission method, using `o_tx_data` and `o_tx_data_valid` signals.
The design of this model is flexible. The same module can be instantiated several times so that the user can read different registers through commands.

In particular, the same module is instantiated 4 times to read a register which has:
* the currently active channels.

* the bitstream version of the TDC.

* the number of available channels that exist to enable

* the value of the time window to be used in the next measurement.

.. image:: ../img/ce_read_register.svg

.. list-table:: Read Register Command Executor Ports
   :widths: 20 20 60
   :header-rows: 1

   * - Port
     - Direction
     - Description

   * - `i_start`
     - Input
     - The executor is enabled when detects a rising edge on this signal.

   * - `i_tx_data_done`
     - Input
     - When this port is set, the executor assumes that the data it wanted to transfer has already been sent.

   * - `i_reg_val`
     - Input
     - Through this port will be read the value of the register. 

   * - `i_tx_allowed`
     - Input
     - When this port is set, the executor assumes that he has control over the output global buffer.

   * - `o_tx_active`
     - Output
     - The executor will set up this port when there is data in it's output buffer (`o_tx_data`).

   * - `o_tx_data_valid`
     - Output
     - This port have the quantity of valid data bits in the output buffer (`o_tx_data`).

   * - `o_tx_data`
     - Output
     - This is the output buffer of the module. Here will be the register value.


The following figure shows a simulation of this module in which the register value is 0x200F00F1.
One clock cycle after the enable signal reaches the `i_start` port the executor outputs to its `o_tx_data` output port the value of the register and the `o_tx_active` notification signal goes logical high.

It is then granted access to the global output buffer (see `i_tx_allowed` signal) and is notified in clock cycles that the data has been sent (see `i_tx_data_done` signal).

.. image:: ../img/ce_read_register_waveform.png

Command Executor: Write Register
=================================
This command executor holds the actual value of a writable register and when the command issue the device the command data is written into the register.

.. image:: ../img/ce_write_register.svg

.. list-table:: Write Register Command Executor Ports
   :widths: 20 20 60
   :header-rows: 1

   * - Port
     - Direction
     - Description

   * - `i_start`
     - Input
     - The executor is enabled when detects a rising edge on this signal.

   * - `i_rx_cmd_data`
     - Input
     - The value of this port will be written in the register when the executor is enabled.

   * - `o_reg_val`
     - Output
     - Have the value of the register.

In the following figure we can see a simulation of this executor.
The previous value of the register was 0x48 (D character in ASCII) and the new one is 0x49 (E character in ASCII).
When a rising edge is detected in `i_start` port, the executor is enabled and the value of the input port `i_rx_cmd_data` is stored in the register in the next clock cycle.

.. image:: ../img/ce_write_register_waveform.png

Command Executor: Measure
===========================
This command executor starts all the channels that are available. With this, all the channels will be measuring. When a certain time is reached then the channels are stoped.

.. image:: ../img/ce_measure.svg

Command Executor: Read Channels Data
=====================================
This command executor reads RAM data of the enabled (and available) channels and send them through it's tx buffer.

This module is composed by an fsm and 3 stages:

1. `Channel Selection Stage`: this is the circuit used to check if the state is enabled and available. Is composed by a counter that is triggered by the fsm, this counter selects the analyzed channel and then a comparator checks if the selected channel is enabled, and if does a flag is set. Also, if the channel is enabled an available, the selected channel is sampled and the correspondent mask is set as output.

2. `Data Buffer Building Stage`: this circuit reads channels RAMs data and builds data buffers with that data. When the buffer is full or there isn't more data to read from channel, certain flags are set and feeded into fsm. 
The RAM is read with a counter that increments and go address by address (incrementally) and when the last address is read, a flag is set. Also, the positions in the buffer and the quantity of data registers read is tracked with a counter. When the last address is read or the buffer is set, the fsm stops the address counter and reset the buffer counter and data registers counter.

3. `Output Stage`: with this circuit, we sample the output signals of the module (`o_tx_data` and `o_tx_data_valid`). 
Data valid signal is set high during 2 clock cycles and data is latched until the next data buffer is presented by fsm.
The fsm can latch the value of the output buffer with the `r_fsm_present_data` signal.

.. image:: ../img/ce_read_channels_data.png