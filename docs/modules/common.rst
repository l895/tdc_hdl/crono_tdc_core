Common modules!
****************
There are some common modules used among all the design. This modules could be used in other designs too.

CRC16-CCITT Encoder
=====================
This module computes the CRC16-CCITT of chunk of data.
How this works? The data is entered sequentially (bit by bit) to a Linear Feedback Shift Register (LFSR) and this little circuit computes the polynomial division of the data by the characteristic polynomial (CCITT = 0x1021).

.. image:: ../img/crc16_ccitt_encoder.png
