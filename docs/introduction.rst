Crono TDC Core Introduction
=============================

Crono TDC is a Time to Digital Converter open source IP Core.
This core involves the time interval measurement, a logic control unit and a serial communication interface.
A diagram of the system can be visualized in the following Figure:

.. image:: img/crono_tdc_simplified.svg


The TDC in fact uses an architecture based on multi phases of a clock.
In a brief, this architecture can sample the input signal N times in a clock period.
Where N is the quantity of phases of the clock that are being used. 
This sampling in a same clock period is implemented with an ISERDESE block.
This ISERDESE block parallelizes the input signal into a N bits output.
The trick here is that, if the input signal frequency is 1GHz then each of the output signals have a frequency of 1GHz/N.
All of this magic is done by the block called **TDC Input** and the design can have M inputs where M is limited by the FPGA resources.
Also, this block identifies when the input signal changes it's state and stores that information in an internal RAM.
Later, with the information of said memory, the user can recreate the waveform of the input signal.

The core have a logic control unit that is inside the block **TDC Core Logic**.
This unit can process commands received from the serial interface and execute some action in response.
That is, the TDC acts reactively, the user must send a command for him to perform an action.
This block is divided in two:

* A **Command Processor**, which is in charge of processing the data sent by the user and determining if that data contains a command.

* A **Command Executor**, which contains several finite state machines that implements the correspondent actions of each command. Also, contains a logic that determines which FSM will take control of the data transfer interface.

In fact, the Core Logic decides what must be done and which data must be sent through the serial interface at any time instant.
This block also have some configuration registers of the core that can be modified through commands.


Because sending just chunks of bytes through the serial port is not reliable to command the TDC a framing protocol was implemented.
This protocol is an adapted version of the HDLC protocols, commands sent by the user and responses sent by the TDC are inside this frames.
The use of this protocol augment the reliability of the system because determines the format of the data that the user and the TDC must send.
Also, have an error detection mechanism called CRC.
**HDLC-lite Processor** is in charge of:

* Process all the incoming bytes from the communication interface with the user and build the frames. Then, when a frame is built, this block process the CRC and decodes the data.

* Take the data that must be sent to the user and computes the CRC, then codes it inside a frame.
  When the frame is built, sends byte by byte through the communication interface.

The chosen interface to interact with the FPGA exterior is a **UART**. This block is used to:

* Send the bytes of the frames one by one. This bytes comes from the HDLC-lite Processor.

* Receive the bits from the user and build the bytes. Each byte is sent to the HDLC-lite Processor.


Use Case
-----------------------
The TDC will be designed to be operated through commands. The user sends commands and the TDC will respond to them.

.. image:: img/use_case.svg

Before the measurement start, the user can modify some configurations of the TDC through commands (time windows size and channels enabled).

To trigger a measurement, the user must send the MEASURE command and the TDC will measure it's input during a certain time interval (time windows).
When the time window expires, the TDC sends a message to the user. In this moment, all measured data is stored in TDC RAM.

When the user wants to read the data stored in the TDC, he should send a command and the TDC will start sending the data records.
When there is no more data to send, the TDC will send a message indicating it.

If a new measurement is started, the data stored in RAM will be overwritten.

IP Core Configuration
-----------------------
The core was designed in Verilog with some generic statements to choose some characteristics just modifying parameters of the top-level.
This characteristics are:

* The quantity of channels.
  
* The RAM size of each channel (affects quantity of data that each channel can store).

* The length (in bits) of the field of the data register that represents the coarse count (affect the max time windows that can be measured).

* Initial value of some configuration registers that can be modified during run-time.

* UART Baudrate.


Top level variants
-------------------------------

We have two top-level variants:

* **Crono TDC LF (Low Frequency)**: Here we use a modeled ISERDESE2 in input channels. Is useful to debug issues with ISERDESE2 hardware block. If this core works and the real one (HF) do not work then migth be a problem with ISERDESE2 configuration. The constraint of this model is that we need to select the place where the flip-flops of the modeled ISERDESE2 is placed by route & place tool. If we do not care of this las thing, this will work only at low frequencies.

* **Crono TDC**: This is the Crono TDC itself, using Xilinx ISERDESE2 IP core to oversample input data.