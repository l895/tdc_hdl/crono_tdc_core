/*
 * Verilog code of a pattern generator used to generate patterns
 * to validate funcionality of Crono TDC.
 * Caveats of this pattern generator: the first bit generated will be
 * a dummy (a '1b0'), because the fsm takes 1 clock cycle to start
 * generating the pattern
 *
 * Author: Julian Rodriguez <jnrodriguez@estudiantes.unsam.edu.ar>
 */

module pattern_generator(i_start, i_clk, i_reset, o_pattern);
    parameter PATTERN_SIZE = 28;
    parameter [PATTERN_SIZE-1:0] PATTERN = 28'b0001001100100110010011001000;

    input i_start;  // signal that starts the pattern generation
    input i_clk;    // clock
    input i_reset;  // synchronous reset input
    output o_pattern; // port where the pattern will be generated

    // Rising edge detector in start signal
    wire [1:0] w_start;
    wire w_start_red;
    ffd ffd_red0  ( .D(i_start), .clk(i_clk), .sync_reset(i_reset),  .Q(w_start[0])  );
    ffd ffd_red1  ( .D(w_start[0]), .clk(i_clk), .sync_reset(i_reset),  .Q(w_start[1])  );
    assign w_start_red = w_start[0] & (~w_start[1]);

    // Counter that index the pattern register
    wire w_counter_reset;
    reg r_fsm_counter_reset;
    assign w_counter_reset = r_fsm_counter_reset | i_reset;
    
    integer r_counter;
    
    always @(posedge i_clk) begin
        if( w_counter_reset == 1 ) begin
            r_counter <= PATTERN_SIZE-1;
        end else begin
            r_counter <= r_counter - 1;
        end
    end
    
    wire w_pattern_fully_generated;
    assign w_pattern_fully_generated = (r_counter <= 0) ? 1 : 0;



    localparam  state_idle = 1'b0,
                state_generating_pattern = 1'b1;

    reg fsm_state, fsm_next_state;

    // Current state register (sequential logic)
    always @(posedge i_clk) begin
        if( i_reset == 1'b1 ) begin
            // assign idle state in reset
            fsm_state <= state_idle;
        end else begin // otherwise update the states
            fsm_state <= fsm_next_state;
        end
    end

    always @(fsm_state, w_start_red, w_pattern_fully_generated) begin
        // Set undefined as default state, is good to catch bugs if
        // a transitions is not defined!
        fsm_next_state <= 1'bx;

        // Decide next state based on input
        case(fsm_state)
            state_idle:
                begin
                    if( w_start_red == 1 ) begin
                        fsm_next_state <= state_generating_pattern;
                    end else begin
                        fsm_next_state <= state_idle;
                    end
                end

            state_generating_pattern:
                begin
                    if( w_pattern_fully_generated == 1 ) begin
                        fsm_next_state <= state_idle;
                    end else begin
                        fsm_next_state <= state_generating_pattern;
                    end
                end
        endcase
    end

    // Combinational output logic: will change only when change state
    always @(fsm_state) begin
        r_fsm_counter_reset <= 1;

        case(fsm_state)
            state_generating_pattern:
                begin
                    r_fsm_counter_reset <= 0;
                end
        endcase
    end

assign o_pattern = (w_pattern_fully_generated == 1) ? 0 : PATTERN[r_counter];


// the "macro" to dump signals
`ifdef COCOTB_SIM
initial begin
  $dumpfile ("pattern_generator.vcd");
  $dumpvars (0, pattern_generator);
  #1;
end
`endif

endmodule