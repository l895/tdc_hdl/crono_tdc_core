/*
 * Verilog code of the Core logic of the Cronot TDC.
 *
 * The Crono TDC work based in commands. Commands could be send to it and the TDC will
 * execute some actions based in the command received.
 * 
 * Inside it, the core logic have two processors:
 * 1) TDC command processor: takes data from it's input and process it to determine the
 * command ID and the command data inside this data. Then if the command is known, starts
 * the correspondent executor issuing the correct bit in an output port called `o_cmd_exec_start`.
 *
 * 2) TDC command executor: inside have N commands executors (one for each possible command).
 * Have an input port called `i_cmd_exec_start` in which each bit represents a different command executor,
 * if bit 'i' is active then the command executor 'i' starts its processing.
 *
 * Author: Julian Rodriguez <jnrodriguez@estudiantes.unsam.edu.ar>
 */

module tdc_core_logic(  i_clk, i_reset, i_rx_dv, i_rx_data,
                        o_tx_data, i_tx_data_done, o_tx_data_valid,
                        i_chn_dready, o_chn_ena, 
                        o_read_chn_sel_mask, i_read_chn_lwa, i_read_chn_data, o_read_chn_addr,
                        o_measure_ongoing, o_executing_command);
import tdc_commands_pkg::*; 
    parameter TX_DATA_WIDTH_BYTES = 16;
    parameter TDC_CHANNEL_QTY = 3;
    parameter TDC_CHANNEL_RAM_ADDR_WIDTH = 14;
    parameter TDC_CHANNEL_DATA_REG_WIDTH = 20;
    parameter CFG_REG_INIT_VAL_TIME_WINDOWS_US = 1000000;
    parameter CFG_REG_INIT_VAL_ENABLED_CHN = {TDC_CHANNEL_QTY{1'b1}}; // all channels are enabled by default
    parameter CLOCK_FREQUENCY_HZ = 100000000;
    parameter RX_DATA_WIDTH_BYTES = TDC_CMD_DATA_WIDTH_BYTES+TDC_CMD_ID_WIDTH_BYTES;
    
    input       i_clk;              // input clock
    input       i_reset;            // synchronous reset signal
    // Command processor signals
    input [RX_DATA_WIDTH_BYTES*8-1:0]         i_rx_data;  // received data
    input [$clog2(RX_DATA_WIDTH_BYTES*8):0]   i_rx_dv;    // when this is valid, the core logic will take N bits from i_rx_data (where N is the number in this port)
    // Command executor signals
    output reg [TX_DATA_WIDTH_BYTES*8-1:0]          o_tx_data;      // data that will be sent by the TDC
    output reg [$clog2(TX_DATA_WIDTH_BYTES*8):0]    o_tx_data_valid;// is the quantity of bits of `o_tx_data` buffer that are valid to be sent
    input                                           i_tx_data_done; // tdc core logic will use this signal to notify when the data that is in the `o_tx_data` buffer is fully sent.
    input [TDC_CHANNEL_QTY-1:0]                     i_chn_dready;   // signals used by the tdc inputs to notify when the data is ready to be readed after a measure
    output reg [TDC_CHANNEL_QTY-1:0]                o_chn_ena;      // signal used to enable or disable the channels, each bit represent a different channel
    output reg [TDC_CHANNEL_QTY-1:0]                o_read_chn_sel_mask; // signal used to select which channel will be readed at each time (one-hot encoding)
    input [TDC_CHANNEL_RAM_ADDR_WIDTH-1:0]          i_read_chn_lwa; // have the last written RAM address by the selected channel
    output reg [TDC_CHANNEL_RAM_ADDR_WIDTH-1:0]     o_read_chn_addr;// RAM address from which the core logic is reading data from the selected channel
    input [TDC_CHANNEL_DATA_REG_WIDTH-1:0]          i_read_chn_data;// the port that will hold the data of the selected address and channel
    output o_measure_ongoing; // This signal will be '1' when a measure is ongoing
    output o_executing_command; // This signal will be '1' when a command will be executed

    /* Instantiate TDC command processor and create wires to connect it to the command executor. */
    wire [TDC_CMD_DATA_WIDTH_BYTES*8-1:0] w_rx_cmd_data;
    wire [TDC_CMD_EXECUTORS_QTY-1:0] w_cmd_exec_start;
    tdc_cmd_processor cmd_processor (
        .i_clk              (i_clk),
        .i_reset            (i_reset),
        .i_rx_data          (i_rx_data),
        .i_rx_dv            (i_rx_dv),
        .o_rx_cmd_data      (w_rx_cmd_data),
        .o_cmd_exec_start   (w_cmd_exec_start)
    );

    assign o_executing_command = (w_cmd_exec_start != 0) ? 1 : 0;

    /* Instantiate TDC command executor. */
    tdc_cmd_executor #(
        .TX_DATA_WIDTH_BYTES(TX_DATA_WIDTH_BYTES),
        .TDC_CHANNEL_QTY(TDC_CHANNEL_QTY),
        .CFG_REG_INIT_VAL_TIME_WINDOWS_US(CFG_REG_INIT_VAL_TIME_WINDOWS_US),
        .CFG_REG_INIT_VAL_ENABLED_CHN(CFG_REG_INIT_VAL_ENABLED_CHN),
        .TDC_CHANNEL_DATA_REG_WIDTH(TDC_CHANNEL_DATA_REG_WIDTH),
        .CLOCK_FREQUENCY_HZ(CLOCK_FREQUENCY_HZ)
    ) cmd_executor (
        .i_clk               (i_clk),
        .i_reset             (i_reset),
        .i_tx_data_done      (i_tx_data_done),
        .o_tx_data_valid     (o_tx_data_valid),
        .o_tx_data           (o_tx_data),
        .i_cmd_exec_start    (w_cmd_exec_start),
        .i_rx_cmd_data       (w_rx_cmd_data),
        .o_chn_ena           (o_chn_ena),
        .i_dready            (i_chn_dready),
        .i_read_chn_data     (i_read_chn_data),
        .i_read_chn_lwa      (i_read_chn_lwa),
        .o_read_chn_addr     (o_read_chn_addr),
        .o_read_chn_sel_mask (o_read_chn_sel_mask),
        .o_measure_ongoing   (o_measure_ongoing)
    );


// the "macro" to dump signals
`ifdef COCOTB_SIM
initial begin
  $dumpfile ("tdc_core_logic.vcd");
  $dumpvars (0, tdc_core_logic);
  #1;
end

`else

    /* ILA instantiation if we are debugging this module with Vivado */
    `ifdef DEBUG_TDC_CORE_LOGIC
    ila_0 ila_tdc_core_logic (
        .clk(i_clk), // input wire clk
        .probe0(w_cmd_exec_start), // reg[5:0]
        .probe1(w_rx_cmd_data), // reg[487:0]
        .probe2(o_tx_data) // reg[255:0]
    );
    `endif

`endif

endmodule 