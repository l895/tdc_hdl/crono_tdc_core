/*
 * Verilog code of a command executor that only reads a register and sends it data.
 *
 * Author: Julian Rodriguez <jnrodriguez@estudiantes.unsam.edu.ar>
 */

module ce_read_register(i_start, i_reg_val, i_tx_data_done, i_reset, i_clk, o_tx_active, o_tx_data_valid, o_tx_data, i_tx_allowed);
    parameter TX_DATA_WIDTH_BYTES = 32;    // Width of the tx buffer (in bytes)
    parameter REG_WIDTH = 32;                   // Width of the register to read (in bits)

    input       i_start;            // input signal that indicates when must to start to process the executor
    input       i_clk;              // input clock
    input       i_reset;            // input reset
    input       i_tx_data_done;     // signal that notifies to the module when the data is sent
    input       i_tx_allowed;       // signal that notifies to the module that is allowed to send data
    input [REG_WIDTH-1:0] i_reg_val;// will have the value of the register 

    output reg [TX_DATA_WIDTH_BYTES*8-1:0] o_tx_data;               // output data buffer
    output reg [$clog2(TX_DATA_WIDTH_BYTES*8):0] o_tx_data_valid;   // output reg that indicates how many bits of output buffer are valid
    output reg o_tx_active; // signal used to notify when this module wants to send data

    localparam  state_idle = 2'b00,
                state_fill_data_buffer = 2'b01,
                state_wait_until_tx_done = 2'b10;

    reg[1:0] fsm_state, fsm_next_state;

    // Current state register (sequential logic)
    always @(posedge i_clk) begin
        if( i_reset == 1'b1 ) begin
            // assign idle state in reset
            fsm_state <= state_idle;
        end else begin // otherwise update the states
            fsm_state <= fsm_next_state;
        end
    end

    always @(fsm_state, i_start, i_tx_allowed, i_tx_data_done) begin
        // Set undefined as default state, is good to catch bugs if
        // a transitions is not defined!
        fsm_next_state <= 2'bxx;

        // Decide next state based on input
        case(fsm_state)
            state_idle:
                begin
                    if( i_start == 1 ) begin
                        fsm_next_state <= state_fill_data_buffer;
                    end else begin
                        fsm_next_state <= state_idle;
                    end
                end

            state_fill_data_buffer:
                begin
                    if( i_tx_allowed == 1 ) begin
                        fsm_next_state <= state_wait_until_tx_done;
                    end else begin
                        fsm_next_state <= state_fill_data_buffer;
                    end
                end

            state_wait_until_tx_done:
                begin
                    if( i_tx_data_done == 1 ) begin
                        fsm_next_state <= state_idle;
                    end else begin
                        fsm_next_state <= state_wait_until_tx_done;
                    end
                end
        endcase
    end

    // Combinational output logic: will change only when change state
    always @(fsm_state, i_reg_val) begin
        case(fsm_state)
            state_idle:
                begin
                    // clear buffers
                    o_tx_data <= 0;
                    o_tx_data_valid <= 0;
                    o_tx_active <= 0;
                end

            state_fill_data_buffer:
                begin
                    o_tx_active <= 1;
                    o_tx_data <= i_reg_val & {REG_WIDTH{1'b1}};
                    o_tx_data_valid <= REG_WIDTH;
                end
            
            state_wait_until_tx_done:
                begin
                    o_tx_active <= 1;
                    o_tx_data <= i_reg_val & {REG_WIDTH{1'b1}};
                    o_tx_data_valid <= REG_WIDTH;
                end

            default: begin
                o_tx_data <= 0;
                o_tx_data_valid <= 0;
                o_tx_active <= 0;
            end
        endcase
    end

// the "macro" to dump signals
`ifdef COCOTB_SIM
initial begin
  $dumpfile ("ce_read_register.vcd");
  $dumpvars (0, ce_read_register);
  #1;
end
`endif

endmodule