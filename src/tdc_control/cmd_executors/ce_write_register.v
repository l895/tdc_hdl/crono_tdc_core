/*
 * Verilog code of a command executor that only writes a register.
 * This simple circuit assign the value of i_rx_cmd_data masked to the 
 * width of the register to the output when a rising edge is detected
 * on the `i_star` input port.
 *
 * Author: Julian Rodriguez <jnrodriguez@estudiantes.unsam.edu.ar>
 */

module ce_write_register(i_start, i_clk, i_reset, i_rx_cmd_data, o_reg_val);
    parameter REG_WIDTH = 32;                   // Width of the register to read (in bits)
    parameter RESET_VALUE = {REG_WIDTH{1'b1}};  // Value of the register at reset
    parameter RX_CMD_DATA_WIDTH_BYTES = 4; // Size (in bytes) of commands data

    input   i_start;            // input signal that indicates when must to start to process the executor
    input   i_clk;              // input clock
    input   i_reset;            // input reset
    input   [RX_CMD_DATA_WIDTH_BYTES*8-1:0]     i_rx_cmd_data;  // will have the data of the command
    output  [REG_WIDTH-1:0]                     o_reg_val;      // will have the value of the register 
   
    // rising edge detector circuit in start input port
    wire [1:0] w_start;            
    wire w_start_red; // signal that will be high when a rising edge is detected in the start

    ffd start_ffd0 (.D(i_start),.clk(i_clk),.sync_reset(i_reset),.Q(w_start[0]));
    ffd start_ffd1 (.D(w_start[0]),.clk(i_clk),.sync_reset(i_reset),.Q(w_start[1]));
    assign w_start_red = ~w_start[1] & w_start[0];

    wire [REG_WIDTH-1:0] w_register_value, w_register_value_pre_mux;
    assign w_register_value_pre_mux = i_rx_cmd_data & {REG_WIDTH{1'b1}};

    assign w_register_value = (w_start_red == 1) ? w_register_value_pre_mux : o_reg_val;

    reg_n #(.N(REG_WIDTH),.RESET_VALUE(RESET_VALUE)) register (.D(w_register_value),.clk(i_clk),.sync_reset(i_reset),.Q(o_reg_val));


// the "macro" to dump signals
`ifdef COCOTB_SIM
initial begin
  $dumpfile ("ce_write_register.vcd");
  $dumpvars (0, ce_write_register);
  #1;
end

`else

//`define DEBUG_CE_WRITE_REG 1
`ifdef DEBUG_CE_WRITE_REG
ila_0 ila_ce_write_reg (
    .clk(i_clk), // input wire clk
    .probe0(i_rx_cmd_data), // reg[487:0]
    .probe1(o_reg_val), // reg[31:0]
    .probe2(i_start), // 1 bit
    .probe3(i_clk), // 1 bit
    .probe4(w_start), // reg[1:0]
    .probe5(w_start_red) // 1 bit
);
`endif

`endif

endmodule