/*
 * Verilog code of a command executor that reads all the data located in the RAMs
 * of a given set of tdc inputs and send them through a tx buffer.
 * The channels that are read are selected with `i_channels_enabled` port.
 *
 * Design limitations:
 * 1) because we are cleaning `o_tx_data_valid` signal after 2 clock cycles,
 * the transfer time of the data must be greater than this. In effect,
 * `i_tx_data_done` signal must be set (at most) 3 clock cycles after
 * `o_tx_data_valid` is set.
 *
 * Author: Julian Rodriguez <jnrodriguez@estudiantes.unsam.edu.ar>
 */

module ce_read_channels_data(  i_start, i_clk, i_reset, 
                    i_dready, i_channels_enabled, i_channels_available,
                    i_read_chn_data, i_read_chn_lwa, o_read_chn_addr, o_read_chn_sel_mask,
                    i_tx_data_done, o_tx_data_valid, o_tx_data, i_tx_allowed, o_tx_active);
    parameter CHANNELS_QTY = 3;             // Max quantity of channels that we can have
    parameter TX_DATA_WIDTH_BYTES = 16;     // Width of the tx buffer (in bytes)
    parameter TDC_CHANNEL_RAM_ADDR_WIDTH = 14; // Width of tdc inputs RAM address (in bit)
    parameter TDC_CHANNEL_DATA_REG_WIDTH = 20; // Width of data registers of tdc inputs (in bits)

    localparam DATA_BUFFER_SENT_O_TX_VALID = TX_DATA_WIDTH_BYTES*8; // value set in o_tx_data_valid when a data buffer is sent

    input i_start;  // signal that enables the executor
    input i_clk;    // clock used by executor
    input i_reset;  // synchronous reset input
    /* i_dready, i_channels_enabled & i_channels available: each bit of this signals belongs to one tdc channel.
     * When the bit 'i' of this port is high then channel 'i' have it's data ready to be
     * read. 
     * i_channels_enabled: if bit 'i' is high then channel 'i' data will be read.
     * This is passed by a port because we want to configure this value outside of the executor.
     * i_channels_available: if bit 'i' is high then channel 'i' is available to be used.
     */
    input [CHANNELS_QTY-1:0] i_dready; 
    input [CHANNELS_QTY-1:0] i_channels_enabled;
    input [CHANNELS_QTY-1:0] i_channels_available;

    output reg [CHANNELS_QTY-1:0] o_read_chn_sel_mask;
    input [TDC_CHANNEL_DATA_REG_WIDTH-1:0] i_read_chn_data;
    input [TDC_CHANNEL_RAM_ADDR_WIDTH-1:0] i_read_chn_lwa;
    output [TDC_CHANNEL_RAM_ADDR_WIDTH-1:0] o_read_chn_addr;

    /* Signals to interact with arbiter and tx buffer. */
    output [$clog2(TX_DATA_WIDTH_BYTES*8):0] o_tx_data_valid;
    output reg [TX_DATA_WIDTH_BYTES*8-1:0] o_tx_data;

    input i_tx_data_done; // signal that will be high when the buffer is fully sent
    output reg o_tx_active; // this port will be set high when module wants to use tx buffer (shared resource)
    input i_tx_allowed; // the module will use the tx buffer only if this port is set high


    /* The following parameters define the format of the tx buffer:
     *  ________________________ _______________________________ _________________________________________
     * |                        *                               *                                         |
     * |  CHANNEL ID (2 bytes)  *  DATA_REGISTERS_QTY (1 byte)  *  DATA (up to TX_DATA_WIDTH_BYTES*8-24)  |
     * |________________________*_______________________________*_________________________________________|
     */
    localparam  CHANNEL_ID_WIDTH = 16,        // up to 65536 channels can be countered
                DATA_REGISTERS_QTY_WIDTH = 8, // up to 256 data registers in one buffer
                DATA_BUFF_WIDTH = TX_DATA_WIDTH_BYTES*8-CHANNEL_ID_WIDTH-DATA_REGISTERS_QTY_WIDTH;

    /*- Misc ----------------------------------------------------------------*/
    reg [CHANNELS_QTY-1:0] r_true_enabled_channels_mask; // mask with the channels that will be read

    always @(i_channels_available, i_channels_enabled) begin
        r_true_enabled_channels_mask <= i_channels_available & i_channels_enabled;
    end

    /*- Channel selection stage ---------------------------------------------*/
    /* Channel select counter and asociated circuitry. */
    reg r_fsm_chn_sel_counter_reset, r_chn_sel_counter_finished, r_chn_sel_is_enabled, r_chn_sel_counter_delayed_is_smaller, r_fsm_chn_sel_sampled_reset, r_fsm_chn_sel_counter_next;
    wire w_chn_sel_counter_reset, w_chn_sel_sample_channel_selected, w_chn_sel_sampled_reset;
    wire [$clog2(CHANNELS_QTY):0] w_chn_sel_counter_delayed;
    reg [$clog2(CHANNELS_QTY):0] r_chn_sel_counter, r_chn_sel_selected_channel;
    reg [CHANNELS_QTY-1:0] r_chn_sel_mask;

    assign w_chn_sel_counter_reset = i_reset | r_fsm_chn_sel_counter_reset;

    always@(posedge i_clk) begin
        if( w_chn_sel_counter_reset == 1 ) begin
            r_chn_sel_counter <= 0;
            r_chn_sel_counter_finished <= 0;
        end else if ( r_fsm_chn_sel_counter_next == 1 ) begin
            if( r_chn_sel_counter < CHANNELS_QTY ) begin
                r_chn_sel_counter <= r_chn_sel_counter+1;
            end else begin
                r_chn_sel_counter_finished <= 1;
            end
        end
    end

    // build mask with current selected channel, the -1 is because
    // if we are selecting the channel 1, the mask must be 0b001, if we
    // are selecting channel 2, the mask must be 0b010, and so on
    always @(r_chn_sel_counter) begin
        if( r_chn_sel_counter > 0 ) begin
            r_chn_sel_mask = 1 << r_chn_sel_counter-1;
        end else begin
            r_chn_sel_mask = 0;
        end
    end

    always @(r_chn_sel_mask, r_true_enabled_channels_mask) begin
        r_chn_sel_is_enabled = ((r_chn_sel_mask & r_true_enabled_channels_mask) != 0) ? 1 : 0;
    end

    // build the signal that will sample the value of the selected channel comparing the
    // actual value of the counter with the previous one. This allows us to build a signal of 1 clock
    // cycle of width that will hit the clock enable of the sampling registers
    reg_n #(.N($clog2(CHANNELS_QTY)+1)) chn_sel_counter_reg (.D(r_chn_sel_counter), .clk(i_clk),.sync_reset(w_chn_sel_counter_reset),.Q(w_chn_sel_counter_delayed));

    always @(w_chn_sel_counter_delayed, r_chn_sel_counter) begin
        r_chn_sel_counter_delayed_is_smaller = (w_chn_sel_counter_delayed < r_chn_sel_counter) ? 1 : 0;
    end

    assign w_chn_sel_sample_channel_selected = r_chn_sel_counter_delayed_is_smaller & r_chn_sel_is_enabled;

    // build sampling registers
    assign w_chn_sel_sampled_reset = w_chn_sel_counter_reset | r_fsm_chn_sel_sampled_reset;

    always @(posedge i_clk) begin
        if (w_chn_sel_sampled_reset == 1) begin
            r_chn_sel_selected_channel <= 0;
            o_read_chn_sel_mask <= 0;
        end else if( w_chn_sel_sample_channel_selected == 1 ) begin
            r_chn_sel_selected_channel <= r_chn_sel_counter;
            o_read_chn_sel_mask <= r_chn_sel_mask;
        end
    end

    /*- Data buffer building stage ------------------------------------------*/
    /* Here are 2 counters:
     * 1) data_buff_counter counter -> count the location in the buffer of the next data register. 
     * 2) data_registers_qty counter -> count the qty of data registers in the buffer.*/

    /* RAM read address select counter and asociated circuitry. */
    wire w_addr_counter_reset, w_addr_counter_stop, w_addr_counter_finished_delayed, w_data_buff_counter_reset, w_data_buff_counter_stop;
    integer r_addr_counter;
    reg r_addr_counter_finished, r_addr_counter_fsm_reset, r_data_buff_counter_fsm_reset, r_addr_counter_fsm_stop_fsm, r_data_buff_counter_almost_finished, r_data_buff_counter_fsm_stop, r_data_buff_counter_finished;
    reg [TX_DATA_WIDTH_BYTES*8-1:0] r_tx_data_aux;
    reg [DATA_REGISTERS_QTY_WIDTH-1:0] r_data_buff_regs_qty;
    reg [$clog2(DATA_BUFF_WIDTH):0]  r_data_buff_counter;
    reg [DATA_BUFF_WIDTH-1:0] r_data_buff;

    assign w_addr_counter_reset = i_reset | r_addr_counter_fsm_reset; 
    assign w_addr_counter_stop = r_addr_counter_fsm_stop_fsm | r_data_buff_counter_almost_finished;

    always @(posedge i_clk) begin
        if( w_addr_counter_reset == 1 ) begin
            r_addr_counter <= 0;
            r_addr_counter_finished <= 0;
        end else if ( w_addr_counter_stop != 1 ) begin
            if( r_addr_counter < i_read_chn_lwa ) begin
                r_addr_counter <= r_addr_counter+1;
            end else begin
                r_addr_counter_finished <= 1;
            end
        end
    end

    ffd ffd_addr_counter_finished (.D(r_addr_counter_finished),.clk(i_clk),.sync_reset(i_reset),.Q(w_addr_counter_finished_delayed));

    assign o_read_chn_addr = (r_addr_counter_finished == 0) ? r_addr_counter : 0; // connect the counter to the output only if lwa is not written

    /* Data building counter and associated circuitry */
    assign w_data_buff_counter_reset = i_reset | r_data_buff_counter_fsm_reset; 
    assign w_data_buff_counter_stop = r_addr_counter_finished | r_data_buff_counter_fsm_stop;

    // Data buffer counter and data registers qty counter, this counters stops then 
    // the fsm actives their stop signal and when data buffer counter reachs zero
    always@(posedge i_clk) begin
        if( w_data_buff_counter_reset == 1 ) begin
            r_data_buff_counter <= DATA_BUFF_WIDTH-TDC_CHANNEL_DATA_REG_WIDTH;
            r_data_buff_counter_finished <= 0;
            r_data_buff_counter_almost_finished <= 0;
        end else if(w_data_buff_counter_stop == 0) begin
            if(r_data_buff_counter < 2*TDC_CHANNEL_DATA_REG_WIDTH) begin
                r_data_buff_counter_almost_finished <= 1;
            end 

            if(r_data_buff_counter >= TDC_CHANNEL_DATA_REG_WIDTH) begin
                r_data_buff_counter <= r_data_buff_counter-TDC_CHANNEL_DATA_REG_WIDTH;
            end else begin
                r_data_buff_counter_finished <= 1;
            end

        end
    end

    // if data buf is almost finishing or if addr counter finished, stop data regs qty counter
    wire w_data_buff_regs_qty_stop;
    assign w_data_buff_regs_qty_stop = r_data_buff_counter_almost_finished | r_addr_counter_finished;

    always@(posedge i_clk) begin
        if( w_data_buff_counter_reset == 1 ) begin
            r_data_buff_regs_qty <= 1;
        end else if( w_data_buff_regs_qty_stop != 1 ) begin
            r_data_buff_regs_qty <= r_data_buff_regs_qty + 1;
        end
    end

    wire [TDC_CHANNEL_DATA_REG_WIDTH-1:0] w_read_chn_data_delayed;
    wire w_data_buff_counter_reset_delayed, w_data_buff_counter_finished_delayed;
    wire [$clog2(DATA_BUFF_WIDTH):0]  w_data_buff_counter_delayed;
    ffd ffd_data_buff_counter_reset (.D(w_data_buff_counter_reset),.clk(i_clk),.sync_reset(i_reset),.Q(w_data_buff_counter_reset_delayed));
    reg_n #(.N($clog2(DATA_BUFF_WIDTH)+1)) reg_data_buff_counter (.D(r_data_buff_counter),.clk(i_clk),.sync_reset(i_reset),.Q(w_data_buff_counter_delayed));
    ffd ffd_data_buff_counter_finished (.D(r_data_buff_counter_finished),.clk(i_clk),.sync_reset(i_reset),.Q(w_data_buff_counter_finished_delayed));
    reg_n #(.N(TDC_CHANNEL_DATA_REG_WIDTH)) reg_read_chn_data (.D(i_read_chn_data),.clk(i_clk),.sync_reset(i_reset),.Q(w_read_chn_data_delayed));

    always @(posedge i_clk) begin
        if(w_data_buff_counter_reset_delayed == 1) begin
            r_data_buff <= 0;
        end else if( w_data_buff_counter_finished_delayed != 1 ) begin
            r_data_buff <= r_data_buff | (w_read_chn_data_delayed << w_data_buff_counter_delayed);
        end
    end

    always @(r_chn_sel_selected_channel, r_data_buff_regs_qty, r_data_buff) begin
        r_tx_data_aux <= ((r_chn_sel_selected_channel-1) << (TX_DATA_WIDTH_BYTES*8-CHANNEL_ID_WIDTH)) | (r_data_buff_regs_qty << (TX_DATA_WIDTH_BYTES*8-CHANNEL_ID_WIDTH-DATA_REGISTERS_QTY_WIDTH)) | r_data_buff;
    end

    /*- Output stage --------------------------------------------------------*/
    // Clock cycles counter used to set high the sample and select data signals
    integer r_clock_cycles_counter;
    wire w_clock_cycles_counter_reset;
    reg r_fsm_clock_cycles_counter_reset;

    assign w_clock_cycles_counter_reset = r_fsm_clock_cycles_counter_reset | i_reset; 
    always @(posedge i_clk) begin
        if( w_clock_cycles_counter_reset == 1) begin
            r_clock_cycles_counter <= 0;
        end else begin
            r_clock_cycles_counter <= r_clock_cycles_counter +1;
        end
    end

    // Small circuit that mantain high tx_data_valid signal 2 clock cycles
    reg [$clog2(TX_DATA_WIDTH_BYTES*8):0] r_fsm_tx_data_valid;
    wire [$clog2(TX_DATA_WIDTH_BYTES*8):0] w_fsm_tx_data_valid_delayed0, w_fsm_tx_data_valid_delayed1;
    reg_n #(.N($clog2(TX_DATA_WIDTH_BYTES*8)+1)) tx_data_valid_reg0 (.D(r_fsm_tx_data_valid), .clk(i_clk),.sync_reset(i_reset),.Q(w_fsm_tx_data_valid_delayed0));
    reg_n #(.N($clog2(TX_DATA_WIDTH_BYTES*8)+1)) tx_data_valid_reg1 (.D(w_fsm_tx_data_valid_delayed0), .clk(i_clk),.sync_reset(i_reset),.Q(w_fsm_tx_data_valid_delayed1));
    assign o_tx_data_valid = w_fsm_tx_data_valid_delayed1 | w_fsm_tx_data_valid_delayed0;

    reg r_fsm_present_data;
    reg[1:0] r_fsm_present_data_sel;
    localparam  lp_fsm_present_data_sel_nothing = 0,
                lp_fsm_present_data_sel_noena   = 1,
                lp_fsm_present_data_sel_enaread = 2,
                lp_fsm_present_data_sel_buffer  = 3;

    reg [TX_DATA_WIDTH_BYTES*8-1:0] r_pre_o_tx_data;

    always @(r_fsm_present_data_sel, r_tx_data_aux) begin
        case(r_fsm_present_data_sel)
            lp_fsm_present_data_sel_nothing: begin
                r_pre_o_tx_data <= 0;
            end

            lp_fsm_present_data_sel_noena: begin
                r_pre_o_tx_data <= "noena";
            end

            lp_fsm_present_data_sel_enaread: begin
                r_pre_o_tx_data <= "enaread";
            end

            lp_fsm_present_data_sel_buffer: begin
                r_pre_o_tx_data <= r_tx_data_aux;
            end

            default: begin
                r_pre_o_tx_data <= 0;
            end
        endcase
    end

    // register with clock enabled used to present data
    // the clock enabled is set only the first clock cycle that r_fsm_present_data is high
    wire w_fsm_present_data_delayed, w_fsm_present_data_pulse;
    ffd ffd_fsm_present_data (.D(r_fsm_present_data),.clk(i_clk),.sync_reset(i_reset),.Q(w_fsm_present_data_delayed));
    assign w_fsm_present_data_pulse = r_fsm_present_data ^ w_fsm_present_data_delayed & r_fsm_present_data;

    always @(posedge i_clk) begin
        if( i_reset == 1 ) begin
            o_tx_data <= 0;
        end else if( w_fsm_present_data_pulse == 1 ) begin
            o_tx_data <= r_pre_o_tx_data;
        end
    end

    /*- Begin Finite State Machine construction -----------------------------*/
    localparam  state_idle                                      = 4'b0000,
                state_wait_until_channels_ready                 = 4'b0001,
                state_request_tx_buffer                         = 4'b0010,
                state_go_next_channel                           = 4'b0011,
                state_test_if_channel_enabled                   = 4'b0100,
                state_wait_ram_delay                            = 4'b0101,
                state_read_chn_data                             = 4'b0110,
                state_present_buffer_data                       = 4'b0111,
                state_wait_until_buffer_data_is_sent            = 4'b1000,
                state_cleanup_before_reading_another_channel    = 4'b1001,
                state_end_msg                                   = 4'b1010,
                state_no_channels_enabled_msg                   = 4'b1011,
                state_wait_until_end_msg_is_sent                = 4'b1100,
                state_wait_until_noena_msg_is_sent              = 4'b1101;

    reg[3:0] fsm_state, fsm_next_state;

    // Current state register (sequential logic)
    always @(posedge i_clk) begin
        if( i_reset == 1'b1 ) begin
            fsm_state <= state_idle;
        end else begin // otherwise update the states
            fsm_state <= fsm_next_state;
        end
    end

    always @(   fsm_state, i_start, i_dready, i_tx_allowed, r_chn_sel_counter_finished, r_true_enabled_channels_mask,
                r_addr_counter_finished, r_data_buff_counter_finished, r_chn_sel_is_enabled, i_tx_data_done,
                w_addr_counter_finished_delayed, r_clock_cycles_counter) begin
        // Set undefined as default state, is good to catch bugs if
        // a transitions is not defined!
        fsm_next_state <= 4'bxxxx;

        // Decide next state based on input
        case(fsm_state)
            state_idle: begin
                if(i_start == 1) begin
                    if( r_true_enabled_channels_mask != 0 ) begin
                        fsm_next_state <= state_wait_until_channels_ready;
                    end else begin
                        fsm_next_state <= state_no_channels_enabled_msg;
                    end
                end else begin
                    fsm_next_state <= state_idle;
                end
            end

            state_wait_until_channels_ready: begin
                if( (i_dready & r_true_enabled_channels_mask) != 0 ) begin
                    fsm_next_state <= state_request_tx_buffer;
                end else begin
                    fsm_next_state <= state_wait_until_channels_ready;
                end
            end

            state_request_tx_buffer: begin
                if(i_tx_allowed == 1) begin
                    fsm_next_state <= state_go_next_channel;
                end else begin
                    fsm_next_state <= state_request_tx_buffer;
                end
            end

            state_go_next_channel: begin
                fsm_next_state <= state_test_if_channel_enabled;
            end

            state_test_if_channel_enabled: begin
                if(r_chn_sel_counter_finished == 0) begin
                    if(r_chn_sel_is_enabled == 0) begin
                        fsm_next_state <= state_go_next_channel;
                    end else begin
                        fsm_next_state <= state_wait_ram_delay;
                    end
                end else begin
                    fsm_next_state <= state_end_msg;
                end
            end

            state_wait_ram_delay: begin
                // TODO: we need to delay one clock cycle the w_data_buff_counter_reset signal
                // because the RAM uses non-blocking assignments and the data will be presented one clock later!!!
                fsm_next_state <= state_read_chn_data;
            end

            state_read_chn_data: begin
                if( (w_addr_counter_finished_delayed == 1) || (r_data_buff_counter_finished == 1) ) begin
                    fsm_next_state <= state_present_buffer_data;
                end else begin
                    fsm_next_state <= state_read_chn_data;
                end
            end

            state_present_buffer_data: begin
                if( r_clock_cycles_counter > 1 ) begin
                    fsm_next_state <= state_wait_until_buffer_data_is_sent;
                end else begin
                    fsm_next_state <= state_present_buffer_data;
                end
            end

            state_wait_until_buffer_data_is_sent: begin
                if(i_tx_data_done == 1) begin
                    if(r_addr_counter_finished == 1) begin
                        fsm_next_state <= state_cleanup_before_reading_another_channel;
                    end else begin
                        fsm_next_state <= state_wait_ram_delay;
                    end
                end else begin
                    fsm_next_state <= state_wait_until_buffer_data_is_sent;
                end
            end

            state_cleanup_before_reading_another_channel: begin
                fsm_next_state <= state_go_next_channel;
            end

            state_end_msg: begin
                if( r_clock_cycles_counter > 1 ) begin
                    fsm_next_state <= state_wait_until_end_msg_is_sent;
                end else begin
                    fsm_next_state <= state_end_msg;
                end
            end

            state_no_channels_enabled_msg: begin
                if ( (i_tx_allowed == 1) && (r_clock_cycles_counter > 1) ) begin
                    fsm_next_state <= state_wait_until_noena_msg_is_sent;
                end else begin
                    fsm_next_state <= state_no_channels_enabled_msg;
                end
            end

            state_wait_until_end_msg_is_sent: begin
                if(i_tx_data_done == 1) begin
                    fsm_next_state <= state_idle;
                end else begin
                    fsm_next_state <= state_wait_until_end_msg_is_sent;
                end
            end

            state_wait_until_noena_msg_is_sent: begin
                if(i_tx_data_done == 1) begin
                    fsm_next_state <= state_idle;
                end else begin
                    fsm_next_state <= state_wait_until_noena_msg_is_sent;
                end
            end

        endcase
    end

    // Combinational output logic
    always @(fsm_state, r_chn_sel_mask, r_true_enabled_channels_mask) begin
        // Default values of some registers to avoid latches
        // output ports
        o_tx_active <= 0;

        // intermediate registers
        r_fsm_chn_sel_counter_reset <= 1;
        r_data_buff_counter_fsm_reset <= 1;
        r_data_buff_counter_fsm_stop <= 1;
        r_addr_counter_fsm_reset <= 1;
        r_addr_counter_fsm_stop_fsm <= 1;
        r_fsm_tx_data_valid <= 0;
        r_fsm_present_data_sel <= lp_fsm_present_data_sel_nothing;
        r_fsm_chn_sel_sampled_reset <= 0;
        r_fsm_present_data <= 0;
        r_fsm_clock_cycles_counter_reset <= 1;
        r_fsm_chn_sel_counter_next <= 0;

        case(fsm_state)
            state_request_tx_buffer: begin
                o_tx_active <= 1;
                r_fsm_chn_sel_counter_reset <= 0;
            end

            state_go_next_channel: begin
                r_fsm_chn_sel_counter_next <= 1;

                // to prevent latches in synthesis and mantain values of previous state
                o_tx_active <= 1;
                r_fsm_chn_sel_counter_reset <= 0;
            end

            state_test_if_channel_enabled: begin
                r_fsm_chn_sel_counter_next <= 0;

                // to prevent latches in synthesis and mantain values of previous state
                o_tx_active <= 1;
                r_fsm_chn_sel_counter_reset <= 0;
            end


            state_wait_ram_delay: begin
                r_addr_counter_fsm_reset <= 0;
                r_addr_counter_fsm_stop_fsm <= 0; // un-stop addr counter, we will read data from channel
                

                // to prevent latches in synthesis and mantain values of previous state
                o_tx_active <= 1;
                r_fsm_chn_sel_counter_reset <= 0;
            end

            state_read_chn_data: begin
                r_data_buff_counter_fsm_stop <= 0; // un-stop data buffer pointer counter

                // to prevent latches in synthesis and mantain values of previous state
                o_tx_active <= 1;
                r_fsm_chn_sel_counter_reset <= 0;
                r_addr_counter_fsm_reset <= 0;
                r_addr_counter_fsm_stop_fsm <= 0;
                r_data_buff_counter_fsm_reset <= 0;
            end

            state_present_buffer_data: begin
                r_fsm_present_data_sel <= lp_fsm_present_data_sel_buffer;
                r_fsm_tx_data_valid <= DATA_BUFFER_SENT_O_TX_VALID;
                r_fsm_present_data <= 1;
                r_fsm_clock_cycles_counter_reset <= 0;

                // to prevent latches in synthesis and do not assign default value to this signals
                o_tx_active <= 1;
                r_fsm_chn_sel_counter_reset <= 0;
                r_addr_counter_fsm_reset <= 0;
                r_data_buff_counter_fsm_stop <= 0;
            end

            state_wait_until_buffer_data_is_sent: begin

                // to prevent latches in synthesis and do not assign default value to this signals
                o_tx_active <= 1;
                r_fsm_chn_sel_counter_reset <= 0;
                r_addr_counter_fsm_reset <= 0;
                r_fsm_present_data_sel <= lp_fsm_present_data_sel_buffer;
            end

            state_cleanup_before_reading_another_channel: begin
                r_fsm_chn_sel_sampled_reset <= 1;

                // to prevent latches in synthesis and do not assign default value to this signals
                o_tx_active <= 1;
                r_fsm_chn_sel_counter_reset <= 0;
                r_addr_counter_fsm_reset <= 0;
                r_fsm_present_data_sel <= lp_fsm_present_data_sel_buffer;
            end

            state_end_msg: begin
                r_fsm_present_data_sel <= lp_fsm_present_data_sel_enaread;
                r_fsm_tx_data_valid <= 56;
                r_fsm_present_data <= 1;
                r_fsm_clock_cycles_counter_reset <= 0;

                // to prevent latches in synthesis and do not assign default value to this signals
                o_tx_active <= 1;
            end

            state_no_channels_enabled_msg: begin
                r_fsm_present_data_sel <= lp_fsm_present_data_sel_noena;
                r_fsm_tx_data_valid <= 40;
                r_fsm_present_data <= 1;
                r_fsm_clock_cycles_counter_reset <= 0;

                // to prevent latches in synthesis and do not assign default value to this signals
                o_tx_active <= 1;
            end

            state_wait_until_end_msg_is_sent: begin

                // to prevent latches in synthesis and do not assign default value to this signals
                o_tx_active <= 1;
                r_fsm_present_data_sel <= lp_fsm_present_data_sel_enaread;
            end

            state_wait_until_noena_msg_is_sent: begin

                // to prevent latches in synthesis and do not assign default value to this signals
                o_tx_active <= 1;
                r_fsm_present_data_sel <= lp_fsm_present_data_sel_noena;
            end

        endcase
    end


// the "macro" to dump signals
`ifdef COCOTB_SIM
initial begin
  $dumpfile ("ce_read_channels_data.vcd");
  $dumpvars (0, ce_read_channels_data);
  #1;
end

`else

    /* ILA instantiation if we are debugging this module with Vivado */
//    `define DEBUG_CE_READ_CHANNELS_DATA
    `ifdef DEBUG_CE_READ_CHANNELS_DATA
    ila_ce_read_channels_data ila_ce_read_channels_data (
        .clk(i_clk), // input wire clk
        .probe0(i_start),                   // 1bit
        .probe1(r_addr_counter_finished),   // 1bit 
        .probe2(i_dready),                  // reg[2:0]
        .probe3(i_channels_enabled),        // reg[2:0]
        .probe4(i_channels_available),      // reg[2:0]
        .probe5(i_tx_data_done),            // 1bit
        .probe6(fsm_next_state),            // reg[3:0]
        .probe7(fsm_state),                 // reg[3:0]
        .probe8(i_read_chn_data),           // reg[19:0]
        .probe9(i_read_chn_lwa),            // reg[13:0]
        .probe10(o_tx_data_valid),          // reg[8:0]
        .probe11(i_tx_allowed),             // 1bit
        .probe12(o_tx_data),                //reg[103:0]
        .probe13(r_addr_counter),           //reg[31:0]
        .probe14(w_addr_counter_reset),     //1bit
        .probe15(r_chn_sel_counter_finished) // 1bit
//        .probe16(r_chn_sel_is_enabled), //1bit
//        .probe17(r_chn_sel_counter) // reg[1:0]
    );
    `endif

`endif

endmodule