/*
 * Verilog code of a command executor that enables given channels to measure and
 * disable them after a certain time interval. The channels that must be
 * enabled are set with the port `i_channels_enabled` and the time interval
 * is set with the port `i_time_to_count_us`.
 *
 * Author: Julian Rodriguez <jnrodriguez@estudiantes.unsam.edu.ar>
 */

module ce_measure(  i_start, i_clk, i_reset, i_dready, i_channels_enabled, 
                    i_channels_available, i_tx_data_done, i_time_to_count_us, 
                    o_tx_data_valid, o_tx_data, o_chn_ena, i_tx_allowed, o_tx_active,
                    o_measure_ongoing);
    parameter CHANNELS_QTY = 3;            // Max quantity of channels that we can have
    parameter TX_DATA_WIDTH_BYTES = 32;         // Width of the tx buffer (in bytes)
    parameter CLOCK_FREQUENCY_HZ = 100000000;        // System clock frequency in Hz

    input i_start;  // signal that enables the executor
    input i_clk;    // clock used by executor
    input i_reset;  // synchronous reset input
    /* i_dready, i_channels_enabled & o_chn_ena: each bit of this signals belongs to one tdc channel. 
     * i_dready: if bit 'i' is high then channel 'i' is ready and we can read data from it. 
     * i_channels_enabled: if bit 'i' is high then channel 'i' must be measured in the next execution.
     * This is passed by a port because we want to configure this value outside of the executor.
     * i_channels_available: if bit 'i' is high then channel 'i' is available to be used.
     * o_chn_ena: enables the channels to be measured.
     */
    input [CHANNELS_QTY-1:0] i_dready; 
    input [CHANNELS_QTY-1:0] i_channels_enabled;
    input [CHANNELS_QTY-1:0] i_channels_available;
    output reg [CHANNELS_QTY-1:0] o_chn_ena;

    /* Signals to interact with arbiter and tx buffer. */
    output reg [$clog2(TX_DATA_WIDTH_BYTES*8):0] o_tx_data_valid;
    output reg [TX_DATA_WIDTH_BYTES*8-1:0] o_tx_data;
    input i_tx_data_done; // signal that will be high when the buffer is fully sent
    output reg o_tx_active; // this port will be set high when module wants to use tx buffer (shared resource)
    input i_tx_allowed; // the module will use the tx buffer only if this port is set high

    /* Ports that interacts with the timer. */    
    input [31:0] i_time_to_count_us; // the microseconds that the TDC will be measuring

    /* Misc ports. */
    output reg o_measure_ongoing; // This port will be '1' when the executor is measuring the channels

    /* Begin Finite State Machine construction. */
    localparam  state_idle                      = 3'b000,
                state_set_time_window           = 3'b001,
                state_enable_channels           = 3'b010,
                state_wait_until_time_over      = 3'b011,
                state_disable_channels          = 3'b100,
                state_store_data_in_tx_buff     = 3'b101,
                state_no_channels_enabled_msg   = 3'b110,
                state_clean_tx_buffer           = 3'b111;

    reg[2:0] fsm_state, fsm_next_state;

    // Current state register (sequential logic)
    always @(posedge i_clk) begin
        if( i_reset == 1'b1 ) begin
            fsm_state <= state_idle;
        end else begin // otherwise update the states
            fsm_state <= fsm_next_state;
        end
    end

    wire w_timer_done; // signal that will be high when the timer reached its time
    reg [CHANNELS_QTY-1:0] r_channels_enabled_mask; // mask with the chhanels enabled for an execution
    reg r_fsm_controlled_timer_reset; // the fsm controls a signal that can reset the timer
    reg [7:0][7:0] s_channels_enabled; // here we will hold the channels enabled mask in ascii format

    wire w_clean_tx_buffer_flag, w_clean_tx_buffer_counter_reset;
    integer r_clean_tx_buffer_counter;
    reg r_fsm_clean_tx_buffer_counter_reset;

    always @(   fsm_state, i_start, w_timer_done, i_dready, i_tx_allowed,
                i_tx_data_done, i_channels_enabled, i_channels_available,
                r_channels_enabled_mask, w_clean_tx_buffer_flag) begin
        // Set undefined as default state, is good to catch bugs if
        // a transitions is not defined!
        fsm_next_state <= 3'bxxx;

        // Decide next state based on input
        case(fsm_state)
            state_idle: begin
                if(i_start == 1) begin
                    if ((i_channels_enabled & i_channels_available) != 0) begin
                        fsm_next_state <= state_set_time_window;
                    end else begin
                        fsm_next_state <= state_no_channels_enabled_msg;
                    end
                end else begin
                    fsm_next_state <= state_idle;
                end
            end

            state_set_time_window: begin
                fsm_next_state <= state_enable_channels; 
            end

            state_enable_channels: begin
                fsm_next_state <= state_wait_until_time_over;
            end

            state_wait_until_time_over: begin
                if( w_timer_done == 1 ) begin
                    fsm_next_state <= state_disable_channels;
                end else begin 
                    fsm_next_state <= state_wait_until_time_over;
                end
            end

            state_disable_channels: begin
                if( (i_dready & r_channels_enabled_mask) != 0 ) begin
                    fsm_next_state <= state_store_data_in_tx_buff;
                end else begin
                    fsm_next_state <= state_disable_channels;
                end
            end

            state_store_data_in_tx_buff: begin
                if ( w_clean_tx_buffer_flag == 1 ) begin
                    fsm_next_state <= state_clean_tx_buffer;
                end else begin
                    fsm_next_state <= state_store_data_in_tx_buff;
                end
            end

            state_no_channels_enabled_msg: begin
                if ( w_clean_tx_buffer_flag == 1 ) begin
                    fsm_next_state <= state_clean_tx_buffer;
                end else begin
                    fsm_next_state <= state_no_channels_enabled_msg;
                end
            end

            state_clean_tx_buffer: begin
                if ( (i_tx_allowed == 1) && (i_tx_data_done == 1) ) begin
                    fsm_next_state <= state_idle;
                end else begin
                    fsm_next_state <= state_clean_tx_buffer;
                end
            end

        endcase
    end

    always @(fsm_state, s_channels_enabled) begin
        o_tx_active <= 0;
        o_tx_data <= 0;
        o_tx_data_valid <= 0;
        o_chn_ena <= 0;
        o_measure_ongoing <= 0;
        r_fsm_clean_tx_buffer_counter_reset <= 1;
        r_fsm_controlled_timer_reset <= 0;
        r_channels_enabled_mask <= 0;

        case(fsm_state)
            /* Sample i_channels_enabled and i_channels available and in this
             * clock cycle the time interval is set in timer. */
            state_set_time_window: begin
                r_channels_enabled_mask <= i_channels_enabled & i_channels_available; 
            end

            /* Reset timer and enable channels. Only channels that are
             * enabled and available will be used. */
            state_enable_channels: begin
                r_fsm_controlled_timer_reset <= 1;
                o_chn_ena <= r_channels_enabled_mask;
                o_measure_ongoing <= 1;

                // continue holding values in registers to prevent latch 
                r_channels_enabled_mask <= r_channels_enabled_mask;  
            end

            /* Unreset timer. */
            state_wait_until_time_over: begin
                r_fsm_controlled_timer_reset <= 0;

                // continue holding values in registers to prevent latch
                o_chn_ena <= r_channels_enabled_mask;
                r_channels_enabled_mask <= r_channels_enabled_mask;
                o_measure_ongoing <= 1;
            end

            /* Windows time is over, disable channels and reset counters. */
            state_disable_channels: begin
                o_chn_ena <= 0;

                // continue holding values in registers to prevent latch
                r_channels_enabled_mask <= r_channels_enabled_mask; 
            end

            state_store_data_in_tx_buff: begin
                r_fsm_clean_tx_buffer_counter_reset <= 0;
                o_tx_active <= 1;
                o_tx_data <= "ena" << 64 | s_channels_enabled;
                o_tx_data_valid <= 88;

                // continue holding values in registers to prevent latch
                r_channels_enabled_mask <= r_channels_enabled_mask;  
            end

            state_no_channels_enabled_msg: begin
                r_fsm_clean_tx_buffer_counter_reset <= 0;
                o_tx_active <= 1;
                o_tx_data <= "noena";
                o_tx_data_valid <= 40;
            end

            state_clean_tx_buffer: begin
                o_tx_active <= 1;
            end
        endcase
    end
    /* End Finite State Machine construction. */

    always @(r_channels_enabled_mask) begin
        s_channels_enabled[7] <=   (((r_channels_enabled_mask & 32'hF0000000) >> 28) + 48);
        s_channels_enabled[6] <=   (((r_channels_enabled_mask & 32'h0F000000) >> 24) + 48);
        s_channels_enabled[5] <=   (((r_channels_enabled_mask & 32'h00F00000) >> 20) + 48);
        s_channels_enabled[4] <=   (((r_channels_enabled_mask & 32'h000F0000) >> 16) + 48);
        s_channels_enabled[3] <=   (((r_channels_enabled_mask & 32'h0000F000) >> 12) + 48);
        s_channels_enabled[2] <=   (((r_channels_enabled_mask & 32'h00000F00) >> 8) + 48);
        s_channels_enabled[1] <=   (((r_channels_enabled_mask & 32'h000000F0) >> 4) + 48);
        s_channels_enabled[0] <=   (((r_channels_enabled_mask & 32'h0000000F) >> 0) + 48);
    end

    /* Begin timer circuitry. 
     * The timer will be reset when the FSM commanded reset or the reset module port 
     * is active high.
     * The `write_enable` signal of the `time_to_count` value is set in the rising edge
     * of the i_start signal. */
    wire w_timer_reset;
    assign w_timer_reset = r_fsm_controlled_timer_reset | i_reset;
 
    wire [1:0] w_start;
    ffd start_ffd0 (.D(i_start),.clk(i_clk),.sync_reset(i_reset),.Q(w_start[0]));
    ffd start_ffd1 (.D(w_start[0]),.clk(i_clk),.sync_reset(i_reset),.Q(w_start[1]));
    assign w_start_red = ~w_start[1] & w_start[0];

    us_timer #(
        .INITIAL_TIME_TO_COUNT_US(100),
        .CLOCK_FREQUENCY_MHZ(CLOCK_FREQUENCY_HZ/1000000)
    ) timer (
        .i_clk(i_clk), // input wire clk
        .i_reset(w_timer_reset),
        .i_time_to_count_we(w_start_red), // signal that enables when the timer will set the `time to count` value
        .i_time_to_count_us(i_time_to_count_us),    // port used to configure the `time to count` of the timer
        .o_time_reached(w_timer_done)   // port that is set high when the time is over
    );

    /* Counter used to clean the buffer after a few clock cycles. */
    always@(posedge i_clk) begin
        if( w_clean_tx_buffer_counter_reset == 1 ) begin
            r_clean_tx_buffer_counter <= 0;
        end else begin
            r_clean_tx_buffer_counter <= r_clean_tx_buffer_counter+1;
        end
    end

    assign w_clean_tx_buffer_counter_reset = i_reset | r_fsm_clean_tx_buffer_counter_reset;
    assign w_clean_tx_buffer_flag = (r_clean_tx_buffer_counter > 1) ? 1 : 0;


// the "macro" to dump signals
`ifdef COCOTB_SIM
initial begin
  $dumpfile ("ce_measure.vcd");
  $dumpvars (0, ce_measure);
  #1;
end

`else

    /* ILA instantiation if we are debugging this module with Vivado */
    //`define DEBUG_CE_MEASURE
    `ifdef DEBUG_CE_MEASURE
    ila_0 ila_ce_measure (
        .clk(i_clk), // input wire clk
        .probe0(i_start),     // 1bit
        .probe1(i_reset),     // 1bit 
        .probe2(i_dready),    // reg[2:0]
        .probe3(i_channels_enabled),    // reg[2:0]
        .probe4(i_channels_available),  // reg[2:0]
        .probe5(i_tx_data_done),        // 1bit
        .probe6(i_time_to_count_us),    // reg[31:0]
        .probe7(o_tx_data_valid),       // reg[8:0]
        .probe8(o_chn_ena),             // reg[2:0]
        .probe9(i_tx_allowed),      // 1bit
        .probe10(o_tx_active),      // 1bit
        .probe11(w_timer_done),     // 1bit
        .probe12(fsm_state),        // reg[2:0]
        .probe13(fsm_next_state),    // reg[2:0]
        .probe14(w_start_red),       // 1bit
        .probe15(r_channels_enabled_mask) // reg[2:0]
    );
    `endif

`endif

endmodule
