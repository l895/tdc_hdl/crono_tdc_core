/*
 * Verilog code of the Crono TDC command processor developed by Julian Rodriguez.
 * 
 * This module process incoming data and decode the command ID field and 
 * data field from it. The format of a command is this:
 *            ____________________________ __________________
 *           *                            *                  *
 *           *    DATA (up to 61 bytes)   *   ID (3 bytes)   *
 *           *____________________________*__________________*
 *
 * The 3 LSB are the ID of the command and the rest are DATA bytes.
 * If the command is valid the port `o_rx_cmd_data` is enabled.
 * The most of this block is purely combinational and its output is only a function
 * of the input.
 * Also, matchs the command ID to the ID of the correspondet executor. This module
 * uses a LUT that maps this two types of commands IDs. Then, when a valid command is detected
 * at the input, the correspondent executor ID will be set in the `o_cmd_exec_start` output port
 * only during a clock cycle. 
 *
 * Author: Julian Rodriguez <jnrodriguez@estudiantes.unsam.edu.ar>
 */

module tdc_cmd_processor(i_rx_dv, i_rx_data, o_rx_cmd_data, o_cmd_exec_start, i_clk, i_reset);
import tdc_commands_pkg::*;
  localparam RX_DATA_WIDTH_BYTES = TDC_CMD_DATA_WIDTH_BYTES+TDC_CMD_ID_WIDTH_BYTES;

  input i_reset, i_clk;
  input [$clog2(RX_DATA_WIDTH_BYTES*8):0] i_rx_dv; // indicates how many bits of `i_rx_data` port are valid
  input [RX_DATA_WIDTH_BYTES*8-1:0] i_rx_data; // input data 
  output [TDC_CMD_DATA_WIDTH_BYTES*8-1:0] o_rx_cmd_data; // data that holds the command
  output [TDC_CMD_EXECUTORS_QTY-1:0] o_cmd_exec_start; // data that holds the command

  /* Intermediate signals */
  reg [TDC_CMD_DATA_WIDTH_BYTES*8-1:0] r_data_parsed_from_input;
  reg [TDC_CMD_ID_WIDTH_BYTES*8-1:0] r_id_parsed_from_input;
  wire [2:0] w_rx_cmd_valid;
  wire w_rx_cmd_valid_red;
  wire [TDC_CMD_ID_WIDTH_BYTES*8-1:0] w_rx_cmd_id;
  wire [TDC_CMD_EXECUTORS_QTY-1:0] w_rx_exec_id;
  wire [TDC_CMD_EXECUTORS_QTY-1:0] w_rx_exec_id_delayed;

  /* Register with all the commands ID known: */
  reg [TDC_CMD_ID_WIDTH_BYTES*8-1:0] commands_id [TDC_CMDS_KNOWN_QTY-1:0];
  initial begin 
    commands_id[0] = CMD_ID_READ_CRONO_TDC_CORE_VERSION;
    commands_id[1] = CMD_ID_MEASURE;
    commands_id[2] = CMD_ID_READ_TIME_WINDOWS_US;
    commands_id[3] = CMD_ID_WRITE_TIME_WINDOWS_US;
    commands_id[4] = CMD_ID_READ_CHANNELS_ENABLED;
    commands_id[5] = CMD_ID_WRITE_CHANNELS_ENABLED;
    commands_id[6] = CMD_ID_READ_CHANNELS_AVAILABLE;
    commands_id[7] = CMD_ID_READ_CHANNELS_DATA;
  end

  /* Circuit that parses the incoming data and determines the DATA and ID of the command */
//  assign r_id_parsed_from_input = i_rx_data & {TDC_CMD_ID_WIDTH_BYTES*8{1'b1}};
//  assign r_data_parsed_from_input = i_rx_data >> TDC_CMD_ID_WIDTH_BYTES*8;
  always @(i_rx_data, i_reset) begin
    if(i_reset == 1) begin
      r_id_parsed_from_input <= 0;
      r_data_parsed_from_input <= 0;
    end else begin
      r_id_parsed_from_input <= i_rx_data & {TDC_CMD_ID_WIDTH_BYTES*8{1'b1}};
      r_data_parsed_from_input <= i_rx_data >> TDC_CMD_ID_WIDTH_BYTES*8;
    end
  end

  /* Muxes that select if data must be the parsed one or zero depending if there is any
   * input data. Here we also delays 2 clock cycles the rx_cmd_data value to synchronize
   * it with the circuit that drives `o_cmd_exec_start` port. */
  wire [TDC_CMD_DATA_WIDTH_BYTES*8-1:0] w_rx_cmd_data_pre_delay;
  wire [TDC_CMD_DATA_WIDTH_BYTES*8-1:0] w_rx_cmd_data_post_delay;
  assign w_rx_cmd_data_pre_delay = (i_rx_dv != 0) ? r_data_parsed_from_input: 0;

  reg_n #(.N(TDC_CMD_DATA_WIDTH_BYTES*8)) reg_rx_cmd_data(
      .D            (w_rx_cmd_data_pre_delay),
      .clk          (i_clk),
      .sync_reset   (i_reset),
      .Q            (w_rx_cmd_data_post_delay)
  );

  wire [TDC_CMD_DATA_WIDTH_BYTES*8-1:0] w_cmd_data_pre_persist;
  assign w_cmd_data_pre_persist = (w_rx_cmd_valid_red_delayed[0] == 1)? o_rx_cmd_data:w_rx_cmd_data_post_delay;
  reg_n #(.N(TDC_CMD_DATA_WIDTH_BYTES*8)) reg_cmd_data_persist (.D(w_cmd_data_pre_persist),.clk(i_clk),.sync_reset(i_reset),.Q(o_rx_cmd_data));

  /* Instantiate Look Up Table that matchs commands IDs with commands executors ID. */
  assign w_rx_cmd_id = (i_rx_dv != 0) ? r_id_parsed_from_input: 0;
  tdc_cmd_exec_lut cmd_exec_lut (
      .i_cmd_id       (w_rx_cmd_id),
      .o_exec_id      (w_rx_exec_id)
  );
  
  /* Delay the lut output 2 clock cycles to be synchronized with the 
   * circuit that is detecting the rising edge of the i_start signal. */
  reg_n #(.N(TDC_CMD_EXECUTORS_QTY)) delay_reg_exec_id(
        .D          (w_rx_exec_id),
        .clk        (i_clk),      // clock input
        .sync_reset (i_reset),
        .Q          (w_rx_exec_id_delayed)
  );

  /* Circuit that check if the cmd ID is known:
   * The following generate loops generates this with `TDC_CMDS_KNOWN_QTY == 5:
   *
   * assign b[0] = (rx_cmd_id == cmd_id(0)) ? 1 : 0;
   * assign b[1] = (rx_cmd_id == cmd_id(1)) ? 1 : 0;
   * assign b[2] = (rx_cmd_id == cmd_id(2)) ? 1 : 0;
   * assign b[3] = (rx_cmd_id == cmd_id(3)) ? 1 : 0;
   * assign b[4] = (rx_cmd_id == cmd_id(4)) TDC_CMD_EXECUTORS_QTY? 1 : 0;
   *
   * assign a[0] = b[0] | b[1]; // defined outside for loop
   *
   * assign a[1] = a[0] | b[2]; 
   * assign a[2] = a[1] | b[3];
   * assign a[3] = a[2] | b[4];
   *
   * assign rx_cmd_valid = (a[3] == 1)? 1: 0;
   *
   * With this circuit, if the ID of the command match any ID of the commands
   * defined with the function `cmd_id` then `rx_cmd_valid` will be one.
   * The circuit will be the following: 
   *
   *              .-->| == CMDS[4] |--b[4]--------------------------------------->|OR
   *              |                                                               |OR-->a[3]-->
   *              |-->| == CMDS[3] |--b[3]-------------------------->|OR          |OR
   *              |                                                  |OR-->a[2]-->|OR
   *              |-->| == CMDS[2] |--b[2]------------->|OR          |OR
   *              |                                     |OR-->a[1]-->|OR 
   *              |-->| == CMDS[1] |--b[1]->|OR         |OR
   *              |                         |OR--a[0]-->|OR
   *              |-->| == CMDS[0] |--b[0]->|OR
   *  id_parsed --'
   *  from_input
   */
  wire [TDC_CMDS_KNOWN_QTY-2:0] a;
  wire [TDC_CMDS_KNOWN_QTY-1:0] b;

  genvar i;
  generate
    for(i=0; i<TDC_CMDS_KNOWN_QTY; i=i+1) begin
      assign b[i] = (w_rx_cmd_id == commands_id[i]) ? 1: 0;
    end
  endgenerate

  assign a[0] = b[0] | b[1];
  genvar j;
  generate
    for(j=1; j<TDC_CMDS_KNOWN_QTY-1; j=j+1) begin
      assign a[j] = a[j-1] | b[j+1];
    end
  endgenerate

  /* the mux will set to zero this signal when the system is in reset state, this will
   * help to the rigin edge detector circuit to not be in undefined state. */
  assign w_rx_cmd_valid[0] = (i_reset == 0) ? a[TDC_CMDS_KNOWN_QTY-2]: 0;

  /* Rising edge detector. */
  ffd ffd_red0 (.D(w_rx_cmd_valid[0]),.clk(i_clk),.sync_reset(i_reset),.Q(w_rx_cmd_valid[1]));
  ffd ffd_red1 (.D(w_rx_cmd_valid[1]),.clk(i_clk),.sync_reset(i_reset),.Q(w_rx_cmd_valid[2]));
  assign w_rx_cmd_valid_red = ~(w_rx_cmd_valid[2]) & w_rx_cmd_valid[1];

  /* Delay the rising edge detected signal one clock cycle. */
  wire [2:0] w_rx_cmd_valid_red_delayed;
  ffd ffd_red_delay0 (.D(w_rx_cmd_valid_red),.clk(i_clk),.sync_reset(i_reset),.Q(w_rx_cmd_valid_red_delayed[0]));
  ffd ffd_red_delay1 (.D(w_rx_cmd_valid_red_delayed[0]),.clk(i_clk),.sync_reset(i_reset),.Q(w_rx_cmd_valid_red_delayed[1]));
  assign w_rx_cmd_valid_red_delayed[2] = w_rx_cmd_valid_red_delayed[0] | w_rx_cmd_valid_red_delayed[1];

  /* Persist the exec_id signal one clock cycle more with this feedback ffd. */
  wire [TDC_CMD_EXECUTORS_QTY-1:0] w_cmd_exec_id_pre_persist;
  wire [TDC_CMD_EXECUTORS_QTY-1:0] w_cmd_exec_id_post_persistent;
  assign w_cmd_exec_id_pre_persist = (w_rx_cmd_valid_red_delayed[0] == 1)? w_cmd_exec_id_post_persistent:w_rx_exec_id_delayed;
  reg_n #(.N(TDC_CMD_EXECUTORS_QTY)) reg_cmd_exec_id_post_persist (.D(w_cmd_exec_id_pre_persist),.clk(i_clk),.sync_reset(i_reset),.Q(w_cmd_exec_id_post_persistent));

  /* Now filter, the command exec id must be high only during 2 clock cycles. */
  assign o_cmd_exec_start = (w_rx_cmd_valid_red_delayed[2] == 1)? w_cmd_exec_id_post_persistent: 0;

// the "macro" to dump signals
`ifdef COCOTB_SIM
initial begin
  $dumpfile ("tdc_cmd_processor.vcd");
  $dumpvars (0, tdc_cmd_processor);
  #1;
end

`else

  /* ILA instantiation if we are debugging this module with Vivado */
  //`define DEBUG_TDC_CMD_PROCESSOR
  `ifdef DEBUG_TDC_CMD_PROCESSOR
  ila_0 ila_tdc_cmd_processor (
      .clk(i_clk), // input wire clk
      .probe0(i_rx_dv), // reg[8:0]
      .probe1(i_rx_data), // reg[511:0]
      .probe2(o_cmd_exec_start), // reg[5:0]
      .probe3(w_rx_cmd_id), // reg[23:0]
      .probe4(w_rx_cmd_valid_red), // 1 bit
      .probe5(w_rx_exec_id),       // reg[5:0]
      .probe6(w_rx_cmd_valid),  // reg[2:0]
      .probe7(w_rx_cmd_valid_red_delayed)  // reg[2:0]
  );
  `endif

`endif

endmodule
