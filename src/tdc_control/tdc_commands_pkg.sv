package tdc_commands_pkg;
    /* This package have the IDs of the known commands that can receive
     * the Cronos TDC.
     * This commands received by the TDC have the following format:
     *            ____________________________ __________________
     *           *                            *                  *
     *           *    DATA (up to 4 bytes)   *   ID (3 bytes)   *
     *           *____________________________*__________________*
     */
    parameter TDC_CMD_ID_WIDTH_BYTES = 3;       // the width of the ID is 3 bytes
    parameter TDC_CMD_DATA_WIDTH_BYTES = 4;    // a command can have up to 4 bytes of data

    /* Known received commands. This ones are sent
    * by the external logic (outside FPGA) that is
    * commanding the TDC.
    * .i.e.: PC --> TDC 
    */
    parameter TDC_CMDS_KNOWN_QTY = 8;                       /* NOTE: must be update if a command is added in the following list! */
    parameter CMD_ID_READ_CRONO_TDC_CORE_VERSION    =  1;   /* Command used to read the version of the Crono TDC version "installed". */
    parameter CMD_ID_MEASURE                        =  2;   /* Launch a measure with the current enabled channels and time windows. */
    parameter CMD_ID_READ_TIME_WINDOWS_US           =  3;   /* Read the value of the time windows of the measure process (us). */
    parameter CMD_ID_WRITE_TIME_WINDOWS_US          =  4;   /* Set value of the time windows of the measure process (us). */
    parameter CMD_ID_READ_CHANNELS_ENABLED          =  5;   /* Read the value of the register that define the enabled channels. */
    parameter CMD_ID_WRITE_CHANNELS_ENABLED         =  6;   /* Set value of the register that define the enabled channels. */
    parameter CMD_ID_READ_CHANNELS_AVAILABLE        =  7;   /* Read the value of the register that hold the available channels of TDC. */
    parameter CMD_ID_READ_CHANNELS_DATA             =  8;   /* Read the data stored in the channels enabled. */

    /* Command executors ID.*/
    parameter TDC_CMD_EXECUTORS_QTY = 8;                       /* NOTE: must be update if a command is added in the following list! */
    parameter CMD_EXEC_ID_READ_CRONO_TDC_CORE_VERSION    =  1;   /* Command used to read the version of the Crono TDC version "installed". */
    parameter CMD_EXEC_ID_MEASURE                        =  2;   /* Launch a measure with the current enabled channels and time windows. */
    parameter CMD_EXEC_ID_READ_TIME_WINDOWS_US           =  4;   /* Read the value of the time windows of the measure process (us). */
    parameter CMD_EXEC_ID_WRITE_TIME_WINDOWS_US          =  8;   /* Set value of the time windows of the measure process (us). */
    parameter CMD_EXEC_ID_READ_CHANNELS_ENABLED          =  16;  /* Read the value of the register that define the enabled channels. */
    parameter CMD_EXEC_ID_WRITE_CHANNELS_ENABLED         =  32;  /* Set value of the register that define the enabled channels. */
    parameter CMD_EXEC_ID_READ_CHANNELS_AVAILABLE        =  64;  /* Read the value of the register that hold the available channels of TDC. */
    parameter CMD_EXEC_ID_READ_CHANNELS_DATA             =  128; /* Read the data stored in the channels enabled. */

endpackage