/*
 * Verilog code of the Look Up Table used to match between commands
 * and commands executors in Crono TDC. 
 * 
 * Each command must be executed by a determined command executor.
 * This table has the definitions of which executor has to execute each command.
 *
 * Author: Julian Rodriguez <jnrodriguez@estudiantes.unsam.edu.ar>
 */

module tdc_cmd_exec_lut(i_cmd_id, o_exec_id);
import tdc_commands_pkg::*;
  input [TDC_CMD_ID_WIDTH_BYTES*8-1:0] i_cmd_id; // id of the incoming command
  output reg [TDC_CMD_EXECUTORS_QTY-1:0] o_exec_id;      // id of the executor that must execute the command

always @(i_cmd_id)
case(i_cmd_id)
    CMD_ID_READ_CRONO_TDC_CORE_VERSION: o_exec_id = CMD_EXEC_ID_READ_CRONO_TDC_CORE_VERSION;
    CMD_ID_MEASURE:                     o_exec_id = CMD_EXEC_ID_MEASURE;
    CMD_ID_READ_TIME_WINDOWS_US:        o_exec_id = CMD_EXEC_ID_READ_TIME_WINDOWS_US;
    CMD_ID_WRITE_TIME_WINDOWS_US:       o_exec_id = CMD_EXEC_ID_WRITE_TIME_WINDOWS_US;
    CMD_ID_READ_CHANNELS_ENABLED:       o_exec_id = CMD_EXEC_ID_READ_CHANNELS_ENABLED;
    CMD_ID_WRITE_CHANNELS_ENABLED:      o_exec_id = CMD_EXEC_ID_WRITE_CHANNELS_ENABLED;
    CMD_ID_READ_CHANNELS_AVAILABLE:     o_exec_id = CMD_EXEC_ID_READ_CHANNELS_AVAILABLE;
    CMD_ID_READ_CHANNELS_DATA:          o_exec_id = CMD_EXEC_ID_READ_CHANNELS_DATA;
    default:                            o_exec_id = 0;
endcase


// the "macro" to dump signals
`ifdef COCOTB_SIM
initial begin
  $dumpfile ("tdc_cmd_exec_lut.vcd");
  $dumpvars (0, tdc_cmd_exec_lut);
  #1;
end
`endif

endmodule
