/*
 * Verilog code of the Crono TDC command executor developed by Julian Rodriguez.
 *
 * The Crono TDC can be operated through commands. This commands are "executed"
 * by this module, where each command have a "executor" associated.
 *
 * Author: Julian Rodriguez <jnrodriguez@estudiantes.unsam.edu.ar>
 */

`define TX_BUFF_REQUESTERS_QTY          6 // quantity of command executors that will need to use the tx buffer

// Priority of executors that could have access to the tx buffer.
`define READ_CRONO_TDC_VERSION_PRIORITY   0
`define READ_TIME_WINDOWS_US_PRIORITY     1
`define READ_ENABLED_CHANNELS_PRIORITY    2
`define READ_CHANNELS_AVAILABLE_PRIORITY  3
`define MEASURE_PRIORITY                  4
`define READ_CHANNELS_DATA_PRIORITY       5

module tdc_cmd_executor(i_clk, i_reset, i_tx_data_done,
                        o_tx_data_valid, o_tx_data, i_cmd_exec_start,
                        i_rx_cmd_data, i_dready, o_chn_ena,
                        i_read_chn_data, i_read_chn_lwa,
                        o_read_chn_addr, o_read_chn_sel_mask,
                        o_measure_ongoing);
import tdc_commands_pkg::*;
import crono_tdc_core_version_pkg::*;
  parameter TX_DATA_WIDTH_BYTES = 16;
  parameter TDC_CHANNEL_QTY = 3;
  parameter CLOCK_FREQUENCY_HZ = 100000000;
  parameter CFG_REG_INIT_VAL_TIME_WINDOWS_US = 1000000;
  parameter CFG_REG_INIT_VAL_ENABLED_CHN = {TDC_CHANNEL_QTY{1'b1}}; // all channels are enabled by default
  parameter TDC_CHANNEL_RAM_ADDR_WIDTH = 14;  // TDC RAM address width bit 
  parameter TDC_CHANNEL_DATA_REG_WIDTH = 20;  // TDC data registers width bit

  input i_reset, i_clk;
  input i_tx_data_done; // signal used by the block that handles the transmission of data to notify that the data in the tx buffer has been tranmsmitted already
  output reg [$clog2(TX_DATA_WIDTH_BYTES*8):0] o_tx_data_valid; // signal used to indicate how many bits of the tx buffer are valid
  output reg [TX_DATA_WIDTH_BYTES*8-1:0] o_tx_data; // output data buffer

  input [TDC_CMD_EXECUTORS_QTY-1:0] i_cmd_exec_start; // signal used by an external module to indicate which executor must start its processing
  input [TDC_CMD_DATA_WIDTH_BYTES*8-1:0] i_rx_cmd_data; // buffer with the data of the command that is being executed

  /* Ports of measure command executor. */
  input [TDC_CHANNEL_QTY-1:0] i_dready;  // signals asserted by tdc inputs to notify that data is ready in their RAMs
  output [TDC_CHANNEL_QTY-1:0] o_chn_ena; // signals used to enable the tdc inputs (channels)

  /* Ports of read channels data command executor. */
  input [TDC_CHANNEL_DATA_REG_WIDTH-1:0] i_read_chn_data;
  input [TDC_CHANNEL_RAM_ADDR_WIDTH-1:0] i_read_chn_lwa;
  output reg [TDC_CHANNEL_RAM_ADDR_WIDTH-1:0] o_read_chn_addr;
  output [TDC_CHANNEL_QTY-1:0] o_read_chn_sel_mask;

  /* Misc ports. */
  output o_measure_ongoing; // This signal will be '1' when a measure is ongoing

  /* Non-writable registers used by the executors. Here we assign the initial values to the registers. */
  reg [CRONO_TDC_CORE_GITHASH_WIDTH-1:0] r_crono_tdc_core_version;
  reg [31:0] r_channels_available;
  initial begin
    r_crono_tdc_core_version = CRONO_TDC_CORE_GITHASH;
    r_channels_available = {TDC_CHANNEL_QTY{1'b1}}; // this is a mask with '1s' in each available channel
  end

  /* Writable registers used by the executors. This are wires because this registers are implemented
   * inside the `write_register` executors. */
  wire [31:0] w_time_windows_us; // register that will hold the time windows of the measurements (in us)
  wire [31:0] w_channels_enabled; // register that will hold the enabled channels

  /* Instantiate the command executors that reads registers.
   * The position in the `i_cmd_start` buffer is equal to the log2 of the executor ID!. */
  // READ_CRONO_TDC_CORE_VERSION command 
  wire [TX_DATA_WIDTH_BYTES*8-1:0] w_read_crono_tdc_core_version_tx_buff;
  wire [$clog2(TX_DATA_WIDTH_BYTES*8):0] w_read_crono_tdc_core_version_tx_data_valid;
  wire w_read_crono_tdc_core_version_tx_active;
  ce_read_register #(
    .REG_WIDTH(CRONO_TDC_CORE_GITHASH_WIDTH),
    .TX_DATA_WIDTH_BYTES(TX_DATA_WIDTH_BYTES)
  ) ce_read_crono_tdc_core_version (
    .i_start         (i_cmd_exec_start[$clog2(CMD_EXEC_ID_READ_CRONO_TDC_CORE_VERSION)]),
    .i_clk           (i_clk),
    .i_reset         (i_reset),
    .i_tx_data_done  (i_tx_data_done),
    .i_tx_allowed    (tx_buff_grants_unencoded[`READ_CRONO_TDC_VERSION_PRIORITY]),
    .i_reg_val       (r_crono_tdc_core_version),
    .o_tx_data       (w_read_crono_tdc_core_version_tx_buff),
    .o_tx_data_valid (w_read_crono_tdc_core_version_tx_data_valid),
    .o_tx_active     (w_read_crono_tdc_core_version_tx_active)
  );

  // READ_TIME_WINDOWS_US command 
  wire [TX_DATA_WIDTH_BYTES*8-1:0] w_read_time_windows_us_tx_buff;
  wire [$clog2(TX_DATA_WIDTH_BYTES*8):0] w_read_time_windows_us_tx_data_valid;
  wire w_read_time_windows_us_tx_active;
  ce_read_register #(
    .REG_WIDTH(32),
    .TX_DATA_WIDTH_BYTES(TX_DATA_WIDTH_BYTES)
  ) ce_read_time_windows_us (
    .i_start         (i_cmd_exec_start[$clog2(CMD_EXEC_ID_READ_TIME_WINDOWS_US)]),
    .i_clk           (i_clk),
    .i_reset         (i_reset),
    .i_tx_data_done  (i_tx_data_done),
    .i_tx_allowed    (tx_buff_grants_unencoded[`READ_TIME_WINDOWS_US_PRIORITY]),
    .i_reg_val       (w_time_windows_us),
    .o_tx_data       (w_read_time_windows_us_tx_buff),
    .o_tx_data_valid (w_read_time_windows_us_tx_data_valid),
    .o_tx_active     (w_read_time_windows_us_tx_active)
  );

  // READ_CHANNELS_ENABLED command 
  wire [TX_DATA_WIDTH_BYTES*8-1:0] w_read_channels_enabled_tx_buff;
  wire [$clog2(TX_DATA_WIDTH_BYTES*8):0] w_read_channels_enabled_tx_data_valid;
  wire w_read_channels_enabled_tx_active;
  ce_read_register #(
    .REG_WIDTH(32),
    .TX_DATA_WIDTH_BYTES(TX_DATA_WIDTH_BYTES)
  ) ce_read_channels_enabled (
    .i_start         (i_cmd_exec_start[$clog2(CMD_EXEC_ID_READ_CHANNELS_ENABLED)]),
    .i_clk           (i_clk),
    .i_reset         (i_reset),
    .i_tx_data_done  (i_tx_data_done),
    .i_tx_allowed    (tx_buff_grants_unencoded[`READ_ENABLED_CHANNELS_PRIORITY]),
    .i_reg_val       (w_channels_enabled),
    .o_tx_data       (w_read_channels_enabled_tx_buff),
    .o_tx_data_valid (w_read_channels_enabled_tx_data_valid),
    .o_tx_active     (w_read_channels_enabled_tx_active)
  );

  // READ_CHANNELS_AVAILABLE command 
  wire [TX_DATA_WIDTH_BYTES*8-1:0] w_read_channels_available_tx_buff;
  wire [$clog2(TX_DATA_WIDTH_BYTES*8):0] w_read_channels_available_tx_data_valid;
  wire w_read_channels_available_tx_active;
  ce_read_register #(
    .REG_WIDTH(32),
    .TX_DATA_WIDTH_BYTES(TX_DATA_WIDTH_BYTES)
  ) ce_read_channels_available (
    .i_start         (i_cmd_exec_start[$clog2(CMD_EXEC_ID_READ_CHANNELS_AVAILABLE)]),
    .i_clk           (i_clk),
    .i_reset         (i_reset),
    .i_tx_data_done  (i_tx_data_done),
    .i_tx_allowed    (tx_buff_grants_unencoded[`READ_CHANNELS_AVAILABLE_PRIORITY]),
    .i_reg_val       (r_channels_available),
    .o_tx_data       (w_read_channels_available_tx_buff),
    .o_tx_data_valid (w_read_channels_available_tx_data_valid),
    .o_tx_active     (w_read_channels_available_tx_active)
  );

  /* Instantiate the command executors that writes registers.*/
  ce_write_register #(
    .REG_WIDTH(32),
    .RESET_VALUE(CFG_REG_INIT_VAL_TIME_WINDOWS_US),
    .RX_CMD_DATA_WIDTH_BYTES(TDC_CMD_DATA_WIDTH_BYTES)
  ) ce_write_time_windows_us (
    .i_start        (i_cmd_exec_start[$clog2(CMD_EXEC_ID_WRITE_TIME_WINDOWS_US)]),
    .i_clk          (i_clk),
    .i_reset        (i_reset),
    .i_rx_cmd_data  (i_rx_cmd_data),
    .o_reg_val      (w_time_windows_us)
  );

  ce_write_register #(
    .REG_WIDTH(32),
    .RESET_VALUE(CFG_REG_INIT_VAL_ENABLED_CHN),
    .RX_CMD_DATA_WIDTH_BYTES(TDC_CMD_DATA_WIDTH_BYTES)
  ) ce_write_channels_enabled (
    .i_start        (i_cmd_exec_start[$clog2(CMD_EXEC_ID_WRITE_CHANNELS_ENABLED)]),
    .i_clk          (i_clk),
    .i_reset        (i_reset),
    .i_rx_cmd_data  (i_rx_cmd_data),
    .o_reg_val      (w_channels_enabled)
  );

  /* Instantiate the command executor that launchs the measurements with tdc channels.*/
  wire [TX_DATA_WIDTH_BYTES*8-1:0] w_measure_tx_buff;
  wire [$clog2(TX_DATA_WIDTH_BYTES*8):0] w_measure_tx_data_valid;
  wire w_measure_tx_active;
  ce_measure #(
    .CHANNELS_QTY(TDC_CHANNEL_QTY),
    .TX_DATA_WIDTH_BYTES(TX_DATA_WIDTH_BYTES),
    .CLOCK_FREQUENCY_HZ(CLOCK_FREQUENCY_HZ)
  ) ce_measure_channels (
    .i_start              (i_cmd_exec_start[$clog2(CMD_EXEC_ID_MEASURE)]),
    .i_clk                (i_clk),
    .i_reset              (i_reset),
    .i_dready             (i_dready),
    .i_channels_enabled   (w_channels_enabled[TDC_CHANNEL_QTY-1:0]),     // is directly the register: 'channels_enabled'
    .i_time_to_count_us   (w_time_windows_us),      // is directly the register: 'time_to_count_us'
    .i_channels_available ({TDC_CHANNEL_QTY{1'b1}}), // channels available are fixed to the bitstream
    .o_chn_ena            (o_chn_ena),
    .o_tx_data_valid      (w_measure_tx_data_valid),
    .o_tx_data            (w_measure_tx_buff),
    .i_tx_data_done       (i_tx_data_done),
    .o_tx_active          (w_measure_tx_active),
    .i_tx_allowed         (tx_buff_grants_unencoded[`MEASURE_PRIORITY]),
    .o_measure_ongoing    (o_measure_ongoing)
  );

  /* Instantiate the command executor that reads the RAM of the enabled channels and send
   * the data to the output buffer. */
  wire [TX_DATA_WIDTH_BYTES*8-1:0] w_read_channels_data_tx_buff;
  wire [$clog2(TX_DATA_WIDTH_BYTES*8):0] w_read_channels_data_tx_data_valid;
  wire w_read_channels_data_tx_active;
  ce_read_channels_data #(
    .CHANNELS_QTY(TDC_CHANNEL_QTY),
    .TX_DATA_WIDTH_BYTES(TX_DATA_WIDTH_BYTES),
    .TDC_CHANNEL_RAM_ADDR_WIDTH(TDC_CHANNEL_RAM_ADDR_WIDTH),
    .TDC_CHANNEL_DATA_REG_WIDTH(TDC_CHANNEL_DATA_REG_WIDTH)
  ) ce_read_channels_data (
    .i_start                (i_cmd_exec_start[$clog2(CMD_EXEC_ID_READ_CHANNELS_DATA)]),
    .i_clk                  (i_clk),
    .i_reset                (i_reset),
    .i_dready               (i_dready),
    .i_channels_enabled     (w_channels_enabled[TDC_CHANNEL_QTY-1:0]),
    .i_channels_available   ({TDC_CHANNEL_QTY{1'b1}}),
    .i_read_chn_data        (i_read_chn_data),      
    .i_read_chn_lwa         (i_read_chn_lwa),       
    .o_read_chn_addr        (o_read_chn_addr),      
    .o_read_chn_sel_mask    (o_read_chn_sel_mask),  
    .i_tx_data_done         (i_tx_data_done),
    .o_tx_data_valid        (w_read_channels_data_tx_data_valid),
    .o_tx_data              (w_read_channels_data_tx_buff),
    .i_tx_allowed           (tx_buff_grants_unencoded[`READ_CHANNELS_DATA_PRIORITY]),
    .o_tx_active            (w_read_channels_data_tx_active)
  );

  /* Instantiate the arbiter to share the tx buffer between various command executors. */
  wire [`TX_BUFF_REQUESTERS_QTY-1:0] tx_buff_grants_unencoded;
  wire [$clog2(`TX_BUFF_REQUESTERS_QTY)-1:0] tx_buff_grants_encoded;
  wire [`TX_BUFF_REQUESTERS_QTY-1:0] tx_buff_requests;
  lock_arbiter #(
    .REQUESTERS_QTY(`TX_BUFF_REQUESTERS_QTY)
  ) tx_buff_arbiter (
    .i_clk              (i_clk),
    .i_reset            (i_reset),
    .i_requesters       (tx_buff_requests),
    .o_grants_unencoded (tx_buff_grants_unencoded),
    .o_grants_encoded   (tx_buff_grants_encoded)
  );

  // Connect the requests of each cmd executor that will use the tx buffer to the arbiter
  assign tx_buff_requests[`READ_CRONO_TDC_VERSION_PRIORITY] = w_read_crono_tdc_core_version_tx_active;
  assign tx_buff_requests[`READ_TIME_WINDOWS_US_PRIORITY] = w_read_time_windows_us_tx_active;
  assign tx_buff_requests[`READ_ENABLED_CHANNELS_PRIORITY] = w_read_channels_enabled_tx_active;
  assign tx_buff_requests[`READ_CHANNELS_AVAILABLE_PRIORITY] = w_read_channels_available_tx_active;
  assign tx_buff_requests[`MEASURE_PRIORITY] = w_measure_tx_active;
  assign tx_buff_requests[`READ_CHANNELS_DATA_PRIORITY] = w_read_channels_data_tx_active;

  // Multiplexers that select the data that will be in the tx buffer
  always @(*) begin
    case(tx_buff_grants_encoded)
      `READ_CRONO_TDC_VERSION_PRIORITY: begin 
        o_tx_data <= w_read_crono_tdc_core_version_tx_buff;
        o_tx_data_valid <= w_read_crono_tdc_core_version_tx_data_valid;
      end

      `READ_TIME_WINDOWS_US_PRIORITY: begin 
        o_tx_data <= w_read_time_windows_us_tx_buff;
        o_tx_data_valid <= w_read_time_windows_us_tx_data_valid;
      end

      `READ_ENABLED_CHANNELS_PRIORITY: begin 
        o_tx_data <= w_read_channels_enabled_tx_buff;
        o_tx_data_valid <= w_read_channels_enabled_tx_data_valid;
      end

      `READ_CHANNELS_AVAILABLE_PRIORITY: begin 
        o_tx_data <= w_read_channels_available_tx_buff;
        o_tx_data_valid <= w_read_channels_available_tx_data_valid;
      end

      `MEASURE_PRIORITY: begin 
        o_tx_data <= w_measure_tx_buff;
        o_tx_data_valid <= w_measure_tx_data_valid;
      end

      `READ_CHANNELS_DATA_PRIORITY: begin 
        o_tx_data <= w_read_channels_data_tx_buff;
        o_tx_data_valid <= w_read_channels_data_tx_data_valid;
      end

      default: begin
        o_tx_data <= 0;
        o_tx_data_valid <= 0;
      end
    endcase
  end


// the "macro" to dump signals
`ifdef COCOTB_SIM
initial begin
  $dumpfile ("tdc_cmd_executor.vcd");
  $dumpvars (0, tdc_cmd_executor);
  #1;
end

`else

  //`define DEBUG_TDC_CMD_EXECUTOR 1
  `ifdef DEBUG_TDC_CMD_EXECUTOR
  ila_tdc_cmd_executor ila_tdc_cmd_executor (
      .clk(i_clk), // input wire clk
      .probe0(i_cmd_exec_start), // reg[7:0]
      .probe1(w_channels_enabled), // reg[31:0]
      .probe2(w_time_windows_us), // reg[31:0]
      .probe3(i_rx_cmd_data), // reg[103:0]
      .probe4(o_tx_data_valid), // reg[7:0]
      .probe5(o_tx_data), // reg[103:0]
      .probe6(i_tx_data_done), // bit 1
      .probe7(i_dready), // reg[2:0]
      .probe8(o_chn_ena), // reg[2:0]
      .probe9(i_read_chn_data), // reg[19:0]
      .probe10(i_read_chn_lwa), // reg[12:0]
      .probe11(o_read_chn_addr), // reg[12:0]
      .probe12(o_read_chn_sel_mask), // reg[2:0]
      .probe13(o_measure_ongoing) // 1 bit
  );
  `endif

`endif

endmodule
