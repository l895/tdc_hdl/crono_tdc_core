#!/bin/bash

## Script used to write the current version of the code in `crono_tdc_core_version_pkg.v`

CRONO_TDC_CORE_VERSION_PKG=${1:-'crono_tdc_core_version_pkg.sv'}
GIT_HASH=$(git log --pretty=format:'%h' -n 1)

echo "Current git hash: ${GIT_HASH}"

sed -i "s/parameter CRONO_TDC_CORE_GITHASH = .*;/parameter CRONO_TDC_CORE_GITHASH = \"${GIT_HASH}\";/g" ${CRONO_TDC_CORE_VERSION_PKG}

cat ${CRONO_TDC_CORE_VERSION_PKG}

echo "Version updated in ${CRONO_TDC_CORE_VERSION_PKG}: ${GIT_HASH}"