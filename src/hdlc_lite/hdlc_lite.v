/*
 * Verilog code for a HDLC-lite processor.
 *
 * Description:
 * This module implements a HDLC-lite frame decoder an frame encoder.
 * The frames of this protocol have this format:
 * 
 *     1 byte     N bytes    2 bytes  1 byte
 *    | FRAME |    DATA    |  CRC16  | FRAME |
 *    | DELIM |            |         | DELIM |
 *
 * This processor is split in two blocks:
 * 1) HDLC-lite rx: decodes bytes from its inputs and build hdlc-lite frames.
 * Then process it (unescaping characters that would be escaped)
 * and checks the CRC and only presents the data of the frame
 * in an output port.
 *
 * 2) HDLC-lite tx: takes up to 256 bits of data as input and build a hdlc-lite
 * frame with it. The module escapes the caracters that must be escaped and then
 * calculates the CRC of the frame. Then, sends byte by byte to another by
 * its output port.
 *
 * Indeed, this module is just a wrapper of the receptor and transmitter ones.
 *
 * Parameters:
 *  HDLC_LITE_FRAME_MAX_WIDTH_BYTES = is the width in bytes of the HDLC-lite frame (i.e. 68)
 *  HDLC_LITE_FRAME_DELIMITER_CHAR = each HDLC-lite frame is delimited with this character (i.e. 0x7E)
 *  HDLC_LITE_ESCAPE_CHAR = if there is any `frame delimiter` or `escape char` in the DATA field of the 
 *  frame, it is escaped inserting this byte first and XORing the data character in question with 0x20.
 *
 * Author: Julian Rodriguez <jnrodriguez@estudiantes.unsam.edu.ar>
 */

module hdlc_lite(i_clk, i_reset, i_rx_byte, i_rx_dv, o_rx_correct_crc, o_rx_data, o_rx_dv, o_tx_active, o_tx_done, i_tx_done, o_tx_dv, o_tx_byte, i_tx_data, i_tx_dv);
    parameter HDLC_LITE_TX_FRAME_MAX_WIDTH_BYTES = 32;
    parameter HDLC_LITE_RX_FRAME_MAX_WIDTH_BYTES = 32;
    parameter HDLC_LITE_FRAME_DELIMITER_CHAR = 8'h7E;
    parameter HDLC_LITE_ESCAPE_CHAR = 8'h7D;

    /* Useful local parameters (just to not use magic numbers). */
    // frame delimiters could not be escaped, so only rest -2 bytes
    localparam HDLC_LITE_TX_FRAME_DATA_AND_CRC_ESCAPED_MAX_WIDTH_BYTES     = HDLC_LITE_TX_FRAME_MAX_WIDTH_BYTES-2;
    // do not count crc, so only rest -2 bytes
    localparam HDLC_LITE_TX_FRAME_DATA_ESCAPED_MAX_WIDTH_BYTES             = HDLC_LITE_TX_FRAME_DATA_AND_CRC_ESCAPED_MAX_WIDTH_BYTES-2;
    // because every char could be escaped, we accepta data's buffer 2 times smaller than the whole frame (data buffers to send) 
    localparam HDLC_LITE_TX_FRAME_DATA_AND_CRC_UNESCAPED_MAX_WIDTH_BYTES   = HDLC_LITE_TX_FRAME_DATA_AND_CRC_ESCAPED_MAX_WIDTH_BYTES/2;
    // and we accept as input data two bytes less, because in the final buffer we must to store the crc too
    localparam HDLC_LITE_TX_INPUT_DATA_ACCEPTED                            = HDLC_LITE_TX_FRAME_DATA_AND_CRC_UNESCAPED_MAX_WIDTH_BYTES-2;
    // frame delimiters could not be escaped, so only rest -2 bytes
    localparam HDLC_LITE_RX_FRAME_DATA_AND_CRC_ESCAPED_MAX_WIDTH_BYTES     = HDLC_LITE_RX_FRAME_MAX_WIDTH_BYTES-2;
    // because every char could be escaped, we accept data's buffer 2 times smaller than the whole frame (data buffers to send) 
    localparam HDLC_LITE_RX_FRAME_DATA_AND_CRC_UNESCAPED_MAX_WIDTH_BYTES   = HDLC_LITE_RX_FRAME_DATA_AND_CRC_ESCAPED_MAX_WIDTH_BYTES/2;
    // This is the maximum quantity of data bytes that we will proved to external logic.
    localparam HDLC_LITE_RX_OUTPUT_DATA_MAX_WIDTH_BYTES                    = HDLC_LITE_RX_FRAME_DATA_AND_CRC_UNESCAPED_MAX_WIDTH_BYTES-2;


    input       i_clk;              // input clock
    input       i_reset;            // synchronous reset signal
    // RX signals
    input [7:0] i_rx_byte;          // received byte
    input       i_rx_dv;            // when this is valid, the hdlc lite will take a byte from the input as valid
    output      o_rx_correct_crc;   // indicates if the crc of the most recent frame was valid or invalid
    output [HDLC_LITE_RX_OUTPUT_DATA_MAX_WIDTH_BYTES*8-1:0] o_rx_data;
    output [$clog2(HDLC_LITE_RX_OUTPUT_DATA_MAX_WIDTH_BYTES*8):0] o_rx_dv;    // indicates how many bits of o_rx_data are valid
    // TX signals
    input i_tx_done;            // external logic will indicate when a transference over the lower protocol is done (i.e. port handled by UART)
    output o_tx_active;         // module will set this to indicate when a transmission is in progress
    output o_tx_done;           // module will set this to indicate when a transmission is finished
    output o_tx_dv;             // module will set this to indicate to the lower protocol module that a byte is ready to be sent
    output [7:0] o_tx_byte;     // byte that will be sent
    input [HDLC_LITE_TX_INPUT_DATA_ACCEPTED*8-1:0] i_tx_data; // data to send
    input [$clog2(HDLC_LITE_TX_INPUT_DATA_ACCEPTED*8):0] i_tx_dv;    // external logic indicates how many valid bits are stored in i_tx_data

    hdlc_lite_rx #(
        .HDLC_LITE_FRAME_MAX_WIDTH_BYTES(HDLC_LITE_RX_FRAME_MAX_WIDTH_BYTES),
        .HDLC_LITE_FRAME_DELIMITER_CHAR(HDLC_LITE_FRAME_DELIMITER_CHAR),
        .HDLC_LITE_ESCAPE_CHAR(HDLC_LITE_ESCAPE_CHAR)
    ) hdlc_lite_receptor (
        .i_clk          (i_clk),            // input clock
        .i_rst          (i_reset), 
        .i_rx_byte      (i_rx_byte),        // byte to receive
        .i_rx_dv        (i_rx_dv),          // indicates when is valid a byte stored in i_rx_byte
        .o_correct_crc  (o_rx_correct_crc), // indicates if the crc of the most recent frame was valid or invalid
        .o_rx_dv        (o_rx_dv),          // indicates how many bits of o_rx_data are valid
        .o_rx_data      (o_rx_data)         // contains the data of incoming frame
    );

    hdlc_lite_tx #(
        .HDLC_LITE_FRAME_MAX_WIDTH_BYTES(HDLC_LITE_TX_FRAME_MAX_WIDTH_BYTES),
        .HDLC_LITE_FRAME_DELIMITER_CHAR(HDLC_LITE_FRAME_DELIMITER_CHAR),
        .HDLC_LITE_ESCAPE_CHAR(HDLC_LITE_ESCAPE_CHAR)
    ) hdlc_lite_transmitter (
        .i_clk          (i_clk),        // input clock
        .i_rst          (i_reset),
        .o_tx_active    (o_tx_active),  // module will set this to indicate when a transmission is in progress
        .o_tx_done      (o_tx_done),    // module will set this to indicate when a transmission is finished
        .i_tx_done      (i_tx_done),    // external logig will indicate when a transference over the lower protocol is done (i.e. port handled by UART)
        .o_tx_dv        (o_tx_dv),      // module will set this to indicate to the lower protocol module that a byte is ready to be sent
        .o_tx_byte      (o_tx_byte),    // byte that will be sent
        .i_tx_data      (i_tx_data),    // data to send
        .i_tx_dv        (i_tx_dv)       // external logic indicates how many valid bits are stored in i_tx_data
    );

// the "macro" to dump signals
`ifdef COCOTB_SIM
initial begin
  $dumpfile ("hdlc_lite.vcd");
  $dumpvars (0, hdlc_lite);
  #1;
end
`endif

endmodule 