/*
 * Verilog code to implement a transmitter for HDLC-lite proto.
 * Description:
 * This module implements a receptor that can process bytes and
 * build "HDLC-lite" frames.
 * This frames are composed like this:
 * 
 *     1 byte     N bytes    2 bytes  1 byte
 *    | FRAME |    DATA    |  CRC16  | FRAME |
 *    | DELIM |            |         | DELIM |
 *
 * There is two special bytes (control bytes) in this protocol:
 * -> Frame delimiter character: 0x7E
 * -> Escape character: 0x7D
 * 
 * When an escape character or a delimiter appears in the data,
 * the transmitter scapes it with an 0x7D and then send the character
 * in question xor'ed with 0x20.
 * So, if this module encounters this values in the data:
 * 1) 0x7D 0x5D => that means that the transmitter wanted to send an 0x7D
 * 2) 0x7D 0x5E => that means that the transmitter wanted to send and 0x7E
 * This module takes this in count. Whenever the data is fully charged in a 
 * pre-buffer, it checks if there is any char that must be escaped and then
 * XOR this character and pre-prend an 0x7D before it.
 *
 * Author: Julian Rodriguez <jnrodriguez@estudiantes.unsam.edu.ar>
 */

module hdlc_lite_tx (i_clk, i_rst, i_tx_data, o_tx_active, o_tx_done, i_tx_dv, o_tx_byte, i_tx_done, o_tx_dv);
    parameter HDLC_LITE_FRAME_MAX_WIDTH_BYTES = 32;
    parameter HDLC_LITE_FRAME_DELIMITER_CHAR = 8'h7E;
    parameter HDLC_LITE_ESCAPE_CHAR = 8'h7D;

    /* Useful local parameters (just to not use magic numbers). */
    // frame delimiters could not be escaped, so only rest -2 bytes
    localparam HDLC_LITE_TX_FRAME_DATA_AND_CRC_ESCAPED_MAX_WIDTH_BYTES      = HDLC_LITE_FRAME_MAX_WIDTH_BYTES-2;
    // because every char could be escaped, we accept data's buffer 2 times smaller than the whole frame (data buffers to send) 
    localparam HDLC_LITE_TX_FRAME_DATA_AND_CRC_UNESCAPED_MAX_WIDTH_BYTES    = HDLC_LITE_TX_FRAME_DATA_AND_CRC_ESCAPED_MAX_WIDTH_BYTES/2;
    // and we accept as input data two bytes less, because in the final buffer we must to store the crc too
    localparam HDLC_LITE_TX_INPUT_MAX_DATA_ACCEPTED_WIDTH_BYTES             = HDLC_LITE_TX_FRAME_DATA_AND_CRC_UNESCAPED_MAX_WIDTH_BYTES-2;

    input       i_clk;          // input clock
    input       i_rst;
    output reg  o_tx_active;    // module will set this to indicate when a transmission is in progress
    output reg  o_tx_done;      // module will set this to indicate when a transmission is finished
    input i_tx_done;            // external logic will indicate when a transference over the lower protocol is done (i.e. port handled by UART)
    output          o_tx_dv;    // module will set this to indicate to the lower protocol module that a byte is ready to be sent
    output [7:0]    o_tx_byte;  // byte that will be sent

    /* Note 1: Verilog do not support two dimensional arrays as port of modules. This is
     * supported only in SystemVerilog. To use a multi-dimensional port we must to 'flattenize' the port
     * and unpack it inside the module.
     * https://stackoverflow.com/questions/36370438/is-it-possible-to-take-input-port-as-array-in-verilog
     * Another bad thing of 2D is that we can't visualize them in simulators (gtkwave don't do it).
     * I opted to use a 1D array and work a little more indexing it.
     * Note 2: We opted to send only the half of the capacity of data in one transference because
     * in the worst case all the chars will be escaped. In this terrible scenario, we will double
     * the quantity of data sent! So, to no overflow the buffer, we only allow to send the half of the
     * capacity of the buffer at each moment. 
     */
    input [HDLC_LITE_TX_INPUT_MAX_DATA_ACCEPTED_WIDTH_BYTES*8-1:0] i_tx_data;          // data to send
    input [$clog2(HDLC_LITE_TX_INPUT_MAX_DATA_ACCEPTED_WIDTH_BYTES*8):0] i_tx_dv;    // external logic indicates how many valid bits are stored in i_tx_data

    reg [HDLC_LITE_TX_FRAME_DATA_AND_CRC_ESCAPED_MAX_WIDTH_BYTES*8-1:0] w_tx_buffer; // pre-buffer where the data to send will be stored (minus the frame delimiters)

    /*- Input stage ---------------------------------------------------------*/
    wire w_data_at_input_dv_diff_zero;
    assign w_data_at_input_dv_diff_zero = (i_tx_dv != 0) ? 1 : 0;

    // Rising edge detector
    wire [1:0] w_data_at_input_diff_zero;
    wire w_data_at_input;
    ffd ffd_start_red0 (.D(w_data_at_input_dv_diff_zero),.clk(i_clk),.sync_reset(i_rst),.Q(w_data_at_input_diff_zero[0]));
    ffd ffd_start_red1 (.D(w_data_at_input_diff_zero[0]),.clk(i_clk),.sync_reset(i_rst),.Q(w_data_at_input_diff_zero[1]));
    assign w_data_at_input = w_data_at_input_diff_zero[0] & (~w_data_at_input_diff_zero[1]);

    reg [HDLC_LITE_TX_INPUT_MAX_DATA_ACCEPTED_WIDTH_BYTES*8-1:0] r_pre_tx_buffer;
    reg [$clog2(HDLC_LITE_TX_INPUT_MAX_DATA_ACCEPTED_WIDTH_BYTES*8):0] r_total_data_bits;

    always @(posedge i_clk) begin
        if( i_rst == 1 ) begin
            r_pre_tx_buffer <= 0;
            r_total_data_bits <= 0;
        end else if (w_data_at_input == 1) begin
            r_pre_tx_buffer <= i_tx_data;
            r_total_data_bits <= i_tx_dv;
        end
    end

    // the fsm reacts to this signal delayed to take use the sampled values (pre_tx_buffer and total_data_bits)
    wire w_data_at_input_delayed;
    ffd ffd_data_at_input (.D(w_data_at_input),.clk(i_clk),.sync_reset(i_rst),.Q(w_data_at_input_delayed));

    /*- CRC encoding stage --------------------------------------------------*/
    reg [$clog2(HDLC_LITE_TX_INPUT_MAX_DATA_ACCEPTED_WIDTH_BYTES*8):0] r_crc_encoding_buff_counter;
    wire w_crc_encoding_reset_buff_counter;
    reg r_fsm_reset_crc_encoding_buff_counter; //reset signal used by fsm
    assign w_crc_encoding_reset_buff_counter = r_fsm_reset_crc_encoding_buff_counter | i_rst;

    // crc encoder takes 1 clock cycle to analyze bits. So, if we set crc_encoder_valid_input
    // to `1` in clock cycle i, then the first bit is analyzed in clock cycle i+1.
    
    reg r_crc_encoding_buff_counter_finished;
    always @(posedge i_clk) begin
        if( w_crc_encoding_reset_buff_counter == 1 ) begin
            r_crc_encoding_buff_counter <= r_total_data_bits+1; //reset val is the qty of bits of data to send
            r_crc_encoding_buff_counter_finished <= 0;
        end else if( r_crc_encoding_buff_counter <= 0 ) begin
            r_crc_encoding_buff_counter_finished <= 1;
        end else begin
            r_crc_encoding_buff_counter <= r_crc_encoding_buff_counter-1;
        end
    end

    // CRC encoder instantiation and input signals construction
    wire w_crc_encoder_valid_input;
    assign w_crc_encoder_valid_input = (~r_crc_encoding_buff_counter_finished) & (~w_crc_encoding_reset_buff_counter);

    wire w_crc_encoder_input_data;
    assign w_crc_encoder_input_data = (r_pre_tx_buffer >> r_crc_encoding_buff_counter) & 1'b1;

    wire [15:0] w_crc_encoder_output;
    wire w_crc_encoder_output_valid;

    crc16_ccitt_encoder crc_encoder (
        .clk            (i_clk),
        .i_data         (w_crc_encoder_input_data),
        .i_data_valid   (w_crc_encoder_valid_input),
        .i_rst          (w_crc_encoding_reset_buff_counter),
        .o_crc          (w_crc_encoder_output),
        .o_crc_valid    (w_crc_encoder_output_valid)
    );

    // Sample the input data with the CRC appended as a trailer in r_pre_tx_buffer_with_crc
    reg [((HDLC_LITE_TX_INPUT_MAX_DATA_ACCEPTED_WIDTH_BYTES+2)*8)-1:0] r_pre_tx_buffer_with_crc;
    always @(posedge i_clk) begin
        if( i_rst  == 1 ) begin
            r_pre_tx_buffer_with_crc <= 0; 
        end if(w_crc_encoder_output_valid == 1) begin
            r_pre_tx_buffer_with_crc <= ((r_pre_tx_buffer << 16) & ~(16'hFF)) | w_crc_encoder_output;
        end
    end

    /*- Escaping stage ------------------------------------------------------*/
    reg [$clog2(HDLC_LITE_TX_INPUT_MAX_DATA_ACCEPTED_WIDTH_BYTES*8):0] r_escaping_buff_counter;
    wire w_reset_escaping_buff_counter;
    reg r_fsm_reset_escaping_buff_counter, r_escaping_finished; //reset signal used by fsm

    assign w_reset_escaping_buff_counter = r_fsm_reset_escaping_buff_counter | i_rst;

    always @(posedge i_clk) begin
        if( w_reset_escaping_buff_counter == 1 ) begin
            r_escaping_buff_counter <= 0; 
            r_escaping_finished <= 0;
        end else if(r_escaping_buff_counter >= (r_total_data_bits+8)) begin // The +16 is for the CRC
            r_escaping_finished <= 1;
        end else begin
            r_escaping_buff_counter <= r_escaping_buff_counter + 8;
        end
    end

    // barrel shifter that picks the byte that will be analyzed
    wire [7:0] w_escaping_byte_to_analyze;
    assign w_escaping_byte_to_analyze = (r_pre_tx_buffer_with_crc >> r_escaping_buff_counter);

    // comparator that checks if the analyzed byte is a control char
    wire w_escaping_byte_is_ctrl_char;
    assign w_escaping_byte_is_ctrl_char = ( (w_escaping_byte_to_analyze == HDLC_LITE_FRAME_DELIMITER_CHAR) || (w_escaping_byte_to_analyze == HDLC_LITE_ESCAPE_CHAR) ) ? 1 : 0; 

    // characters that will be stored in tx buffer, depending if the char is a control byte or not
    wire [15:0] w_escaping_chars_to_store_in_tx_buff;
    assign w_escaping_chars_to_store_in_tx_buff = (w_escaping_byte_is_ctrl_char == 1) ? ((16'h7D00) | (8'h20 ^ w_escaping_byte_to_analyze)) : w_escaping_byte_to_analyze;

    // quantity of bits that will be stored in tx buffer, depending if the char is a control byte or not
    wire [4:0] w_escaping_bits_qty_to_store_in_tx_buff;
    assign w_escaping_bits_qty_to_store_in_tx_buff = (w_escaping_byte_is_ctrl_char == 1) ? 16 : 8;

    // Count the quantity of bits stored in tx buffer
    reg [$clog2(HDLC_LITE_FRAME_MAX_WIDTH_BYTES*8):0] r_escaping_bits_stored_in_tx_buff_without_flags;
    always @(posedge i_clk) begin
        if( w_reset_escaping_buff_counter == 1 ) begin
            r_escaping_bits_stored_in_tx_buff_without_flags <= 0;
        end else if( r_escaping_finished != 1 ) begin
            r_escaping_bits_stored_in_tx_buff_without_flags <= r_escaping_bits_stored_in_tx_buff_without_flags + w_escaping_bits_qty_to_store_in_tx_buff;
        end
    end

    // Build tx buffer with the chars that must be stored in it (depending if the char must be escaped or not)
    // the chars are shifted N positions to the left, where N is the quantity of bits already stored in buffer
    reg [HDLC_LITE_FRAME_MAX_WIDTH_BYTES*8-1:0] r_tx_buff_without_flags;
    always @(posedge i_clk) begin
        if( w_reset_escaping_buff_counter == 1 ) begin
            r_tx_buff_without_flags <= 0;
        end else if( r_escaping_finished != 1 ) begin
            r_tx_buff_without_flags <= r_tx_buff_without_flags | (w_escaping_chars_to_store_in_tx_buff << r_escaping_bits_stored_in_tx_buff_without_flags);
        end
    end

    // Build frame with frame delimiters
    reg [(HDLC_LITE_FRAME_MAX_WIDTH_BYTES+2)*8-1:0] r_tx_buff_with_flags;
    always @(posedge i_clk) begin
        if( i_rst == 1 ) begin
            r_tx_buff_with_flags <= 0;
        end else begin
            r_tx_buff_with_flags <= (HDLC_LITE_FRAME_DELIMITER_CHAR << (r_escaping_bits_stored_in_tx_buff_without_flags+8)) | (r_tx_buff_without_flags << 8) | HDLC_LITE_FRAME_DELIMITER_CHAR;
        end
    end

    // Store qty of bits in hdlc-lite frame (including frame delimiters)
    reg [$clog2(HDLC_LITE_FRAME_MAX_WIDTH_BYTES*8):0] r_escaping_bits_stored_in_tx_buff_with_flags;
    always @(r_escaping_bits_stored_in_tx_buff_without_flags) begin
        r_escaping_bits_stored_in_tx_buff_with_flags <= r_escaping_bits_stored_in_tx_buff_without_flags + 16;
    end

    /*- Output logic --------------------------------------------------------*/
    // Registers that samples tx buffer and bits qty when fsm triggers a signal
    reg r_fsm_sample_tx_buffer; // signal used by fsm to sample tx buffer and bits qty in buffer
    reg [(HDLC_LITE_FRAME_MAX_WIDTH_BYTES+2)*8-1:0] r_sampled_tx_buff_with_flags;
    reg [$clog2(HDLC_LITE_FRAME_MAX_WIDTH_BYTES*8):0] r_sampled_bits_stored_in_tx_buff_with_flags;
    always @(posedge i_clk) begin
        if( i_rst == 1 ) begin
            r_sampled_tx_buff_with_flags <= 0;
            r_sampled_bits_stored_in_tx_buff_with_flags <= 0;
        end else if( r_fsm_sample_tx_buffer == 1 ) begin
            r_sampled_tx_buff_with_flags <= r_tx_buff_with_flags;
            r_sampled_bits_stored_in_tx_buff_with_flags <= r_escaping_bits_stored_in_tx_buff_with_flags;
        end
    end

    // Counter that increments the tx buffer pointer byte by byte, starting for MSB going to LSB
    reg [$clog2(HDLC_LITE_FRAME_MAX_WIDTH_BYTES*8):0] r_send_frame_counter;
    wire w_reset_send_frame_counter, w_fsm_send_next_byte_delayed;
    reg r_fsm_reset_send_frame_counter, r_fsm_send_next_byte, r_send_frame_counter_finished;

    ffd ffd_send_next_byte (.D(r_fsm_send_next_byte),.clk(i_clk),.sync_reset(i_rst),.Q(w_fsm_send_next_byte_delayed));

    assign w_reset_send_frame_counter = r_fsm_reset_send_frame_counter | i_rst;

    always @(posedge i_clk) begin
        if( w_reset_send_frame_counter == 1 ) begin
            r_send_frame_counter <= 0;
            r_send_frame_counter_finished <= 0;
        end else if (r_send_frame_counter >= r_sampled_bits_stored_in_tx_buff_with_flags) begin
            r_send_frame_counter_finished <= 1;
        end else if( r_fsm_send_next_byte == 1 ) begin 
            r_send_frame_counter <= r_send_frame_counter + 8;
        end
    end

    // Mantain high o_tx_dv two clock cycles when send frame counter advances!
    wire w_tx_dv, w_tx_dv_delayed;
    assign w_tx_dv = (r_send_frame_counter_finished == 0) ? w_fsm_send_next_byte_delayed : 0;
    ffd tx_dv_ffd (.D(w_tx_dv),.clk(i_clk),.sync_reset(i_rst),.Q(w_tx_dv_delayed));

    assign o_tx_dv = (w_tx_dv | w_tx_dv_delayed) & ~w_reset_send_frame_counter; // the ~w_reset_send_frame_counter is to not active o_tx_dv when we do not want

    // build byte that must be sent with the counter and the quantity of bits stored in tx buff
    assign o_tx_byte = (r_sampled_tx_buff_with_flags >> (r_sampled_bits_stored_in_tx_buff_with_flags - r_send_frame_counter)) & 8'hFF;

    /*- Finite State Machine ------------------------------------------------*/
    localparam  state_idle                          = 3'b000,
                state_computing_crc                 = 3'b001,
                state_escaping_frame                = 3'b010,
                state_send_frame                    = 3'b011,
                state_send_next_byte                = 3'b100,
                state_wait_until_byte_is_sent       = 3'b101,
                state_set_tx_done_flag              = 3'b110;

    reg[2:0] fsm_state, fsm_next_state;

    // Current state register (sequential logic)
    always @(posedge i_clk) begin
        if( i_rst == 1'b1 ) begin 
            fsm_state <= state_idle;
        end else begin // otherwise update the states
            fsm_state <= fsm_next_state;
        end
    end

    always @(   fsm_state, w_data_at_input_delayed, w_crc_encoder_output_valid,
                r_escaping_finished, r_send_frame_counter, r_send_frame_counter_finished, i_tx_done) begin
        // Set undefined as default state, is good to catch bugs if
        // a transitions is not defined!
        fsm_next_state <= 3'bxxx;

        // Decide next state based on input
        case(fsm_state)
            state_idle:
                begin
                    if( w_data_at_input_delayed == 1 ) begin
                        fsm_next_state <= state_computing_crc;
                    end else begin
                        fsm_next_state <= state_idle;
                    end
                end

            state_computing_crc:
                begin
                    if( w_crc_encoder_output_valid == 1 ) begin
                        fsm_next_state <= state_escaping_frame;
                    end else begin
                        fsm_next_state <= state_computing_crc;
                    end
                end

            state_escaping_frame:
                begin
                    if( r_escaping_finished == 1 ) begin
                        fsm_next_state <= state_send_frame;
                    end else begin
                        fsm_next_state <= state_escaping_frame;
                    end
                end

            state_send_frame:
                begin
                    fsm_next_state <= state_send_next_byte;
                end

            state_send_next_byte:
                begin
                    fsm_next_state <= state_wait_until_byte_is_sent;
                end

            state_wait_until_byte_is_sent:
                begin
                    if( (i_tx_done == 1) && (r_send_frame_counter_finished == 1) ) begin
                        fsm_next_state <= state_set_tx_done_flag;
                    end else if( (i_tx_done == 1) && (r_send_frame_counter_finished == 0) ) begin
                        fsm_next_state <= state_send_next_byte;
                    end else begin
                        fsm_next_state <= state_wait_until_byte_is_sent;
                    end
                end

            state_set_tx_done_flag:
                begin
                    fsm_next_state <= state_idle;
                end

        endcase
    end

    // Combinational output logic: will run only when fsm changes its state
    always @(fsm_state) begin
        o_tx_active <= 1;
        o_tx_done <= 0;
        r_fsm_reset_crc_encoding_buff_counter <= 1;
        r_fsm_reset_escaping_buff_counter <= 1;
        r_fsm_reset_send_frame_counter <= 1;
        r_fsm_send_next_byte <= 0;
        r_fsm_sample_tx_buffer <= 0;

        case(fsm_state)
            state_idle:
                begin
                    o_tx_active <= 0;
                end

            state_computing_crc:
                begin
                    r_fsm_reset_crc_encoding_buff_counter <= 0;
                end

            state_escaping_frame:
                begin
                    r_fsm_reset_escaping_buff_counter <= 0;
                end

            state_send_frame:
                begin
//                    r_fsm_reset_send_frame_counter <= 0;
                    r_fsm_sample_tx_buffer <= 1;
                end

            state_send_next_byte:
                begin
                    r_fsm_send_next_byte <= 1;

                    // mantain old values and avoid latches
                    r_fsm_reset_send_frame_counter <= 0;
                end

            state_wait_until_byte_is_sent:
                begin
                    // mantain old values and avoid latches
                    r_fsm_reset_send_frame_counter <= 0;
                end

           state_set_tx_done_flag:
                begin
                    o_tx_done <= 1;
                end
        endcase
    end

// the "macro" to dump signals
`ifdef COCOTB_SIM
initial begin
  $dumpfile ("hdlc_lite_tx.vcd");
  $dumpvars (0, hdlc_lite_tx);
  #1;
end

`else

    // FIXME: i do not know why this module do not work if ILA is not instantiated in current project :(
    /* ILA instantiation if we are debugging this module with Vivado */
    //`define DEBUG_HDLC_LITE_TX
    `ifdef DEBUG_HDLC_LITE_TX
    ila_0 ila_hdlc_lite_tx (
        .clk(i_clk), // input wire clk
        .probe0(fsm_state),         // reg[2:0]
        .probe1(fsm_next_state),    // reg[2:0]
        .probe2(o_tx_byte),         //reg[7:0]
        .probe3(o_tx_dv),           //1bit
        .probe4(i_tx_data),         //reg[103:0]
        .probe5(i_tx_dv),           //reg[7:0]
        .probe6(i_tx_done)          //1 bit
    );
    `endif

`endif

endmodule