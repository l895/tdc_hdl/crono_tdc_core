/*
 * Verilog code to implement a receptor for HDLC-lite proto.
 * Description:
 * This module implements a receptor that can process bytes and
 * build "HDLC-lite" frames.
 * This frames are composed like this:
 * 
 *     1 byte     N bytes    2 bytes  1 byte
 *    | FRAME |    DATA    |  CRC16  | FRAME |
 *    | DELIM |            |         | DELIM |
 *
 * There is two special bytes (control bytes) in this protocol:
 * -> Frame delimiter character: 0x7E
 * -> Escape character: 0x7D
 * 
 * When an escape character or a delimiter appears in the data,
 * the transmitter scapes it with an 0x7D and then send the character
 * in question xor'ed with 0x20.
 * So, if this module encounters this values in the data:
 * 1) 0x7D 0x5D => that means that the transmitter wanted to send an 0x7D
 * 2) 0x7D 0x5E => that means that the transmitter wanted to send and 0x7E
 * This module takes this in count, removes the 0x7D character and dis-xor
 * the next character (making two times xor to a number returns the first
 * number in question).
 *
 * Author: Julian Rodriguez <jnrodriguez@estudiantes.unsam.edu.ar>
 */

module hdlc_lite_rx (i_clk, i_rst, i_rx_byte, i_rx_dv, o_correct_crc, o_rx_dv, o_rx_data);
    parameter HDLC_LITE_FRAME_MAX_WIDTH_BYTES = 32;
    parameter HDLC_LITE_FRAME_DELIMITER_CHAR = 8'h7E;
    parameter HDLC_LITE_ESCAPE_CHAR = 8'h7D;

    /* Local parameters (just to not use magic numbers). */    
    // frame delimiters could not be escaped, so only rest -2 bytes
    localparam HDLC_LITE_RX_FRAME_DATA_AND_CRC_ESCAPED_MAX_WIDTH_BYTES      = HDLC_LITE_FRAME_MAX_WIDTH_BYTES-2;
    // because every char could be escaped, we accept data's buffer 2 times smaller than the whole frame (data buffers to send) 
    localparam HDLC_LITE_RX_FRAME_DATA_AND_CRC_UNESCAPED_MAX_WIDTH_BYTES    = HDLC_LITE_RX_FRAME_DATA_AND_CRC_ESCAPED_MAX_WIDTH_BYTES/2;
    // This is the maximum quantity of data bytes that we will proved to external logic.
    localparam HDLC_LITE_RX_OUTPUT_MAX_DATA_WIDTH_BYTES                     = HDLC_LITE_RX_FRAME_DATA_AND_CRC_UNESCAPED_MAX_WIDTH_BYTES-2;

    input       i_clk;                // input clock
    input       i_rst;
    input [7:0] i_rx_byte;          // byte to receive
    input       i_rx_dv;            // indicates when is valid a byte stored in i_rx_byte
    output      o_correct_crc;      // indicates if the crc of the most recent frame was valid or invalid

    /* Notes: Verilog do not support two dimensional arrays as port of modules. This is
     * supported only in SystemVerilog. To use a multi-dimensional port we must to 'flattenize' the port
     * and unpack it inside the module.
     * https://stackoverflow.com/questions/36370438/is-it-possible-to-take-input-port-as-array-in-verilog
     * Another bad thing of 2D is that we can't visualize them in simulators (gtkwave don't do it).
     * I opted to use a 1D array and work a little more indexing it.
     */
    output reg [HDLC_LITE_RX_OUTPUT_MAX_DATA_WIDTH_BYTES*8-1:0] o_rx_data;  // is the data of the received frame, the size is half minus 4 bytes of the total frame width
    output [$clog2(HDLC_LITE_RX_OUTPUT_MAX_DATA_WIDTH_BYTES*8):0] o_rx_dv;  // indicates how many bits of o_rx_data are valid
    
    /*- Input stage ---------------------------------------------------------*/
    // Detect the rising edges of i_rx_dv signal and samples i_rx_byte in this events.
    // Then check if sampled i_rx_byte is equal to frame delimiter and set `w_frame_delimiter_received` flag
    wire [1:0] w_i_rx_dv_red;
    wire w_rx_dv_active;
    ffd ffd_start_red0 (.D(i_rx_dv),.clk(i_clk),.sync_reset(i_rst),.Q(w_i_rx_dv_red[0]));
    ffd ffd_start_red1 (.D(w_i_rx_dv_red[0]),.clk(i_clk),.sync_reset(i_rst),.Q(w_i_rx_dv_red[1]));
    assign w_rx_dv_active = w_i_rx_dv_red[0] & (~w_i_rx_dv_red[1]);
 
    reg [7:0] r_sampled_input_byte;
    always @(posedge i_clk) begin
        if(i_rst == 1) begin
            r_sampled_input_byte <= 0;
        end else if(w_rx_dv_active == 1) begin
            r_sampled_input_byte <= i_rx_byte;
        end
    end

    reg r_byte_received_is_frame_delimiter;
    always @(i_rx_byte) begin
        if(i_rx_byte == HDLC_LITE_FRAME_DELIMITER_CHAR) begin
            r_byte_received_is_frame_delimiter = 1;
        end else begin
            r_byte_received_is_frame_delimiter = 0;
        end
    end

    wire w_frame_delimiter_received;
    assign w_frame_delimiter_received = r_byte_received_is_frame_delimiter & w_rx_dv_active;

    // Count how many frame delimiters was received!
    integer r_frame_delimiter_received_counter; // TODO: change this to an smaller register
    wire w_frame_delimiter_received_counter_reset;
    reg r_fsm_frame_delimiter_received_counter_reset;

    assign w_frame_delimiter_received_counter_reset = i_rst | r_fsm_frame_delimiter_received_counter_reset;

    always @(posedge i_clk) begin
        if(w_frame_delimiter_received_counter_reset == 1) begin
            r_frame_delimiter_received_counter <= 0;
        end else if(w_frame_delimiter_received == 1) begin
            r_frame_delimiter_received_counter <= r_frame_delimiter_received_counter + 1;
        end
    end

    /*- Receving bytes stage ------------------------------------------------*/
    // Here we receive all the incoming bytes, probably escaped.
    reg [$clog2(HDLC_LITE_FRAME_MAX_WIDTH_BYTES*8):0] r_received_bits_counter;
    reg r_fsm_reset_received_bits_counter; //reset signal used by fsm

    wire w_reset_received_bits_counter;
    assign w_reset_received_bits_counter = r_fsm_reset_received_bits_counter | i_rst;

    always @(posedge i_clk) begin
        if( w_reset_received_bits_counter == 1 ) begin
            r_received_bits_counter <= 0; 
        end else if( (w_rx_dv_active == 1) && (w_frame_delimiter_received == 0) ) begin
            r_received_bits_counter <= r_received_bits_counter + 8;
        end
    end

    // build the next byte to store shifted N positions to the left
    reg [HDLC_LITE_FRAME_MAX_WIDTH_BYTES*8-1:0] r_received_bits_next_byte_to_store;
    always @(r_received_bits_counter, r_sampled_input_byte) begin
        r_received_bits_next_byte_to_store <= (r_sampled_input_byte << (HDLC_LITE_FRAME_MAX_WIDTH_BYTES*8-r_received_bits_counter));
    end

    // build the register that holds the stored bytes shifted to the left,
    // this registers does the OR betwen the next byte to store and the actual content of the
    // register when a byte is detected at the input
    reg [HDLC_LITE_FRAME_MAX_WIDTH_BYTES*8-1:0] r_received_bits_buff_escaped_pre_shift;
    wire w_rx_dv_active_delayed;
    ffd ffd_rx_dv_active_delayed (.D(w_rx_dv_active),.clk(i_clk),.sync_reset(i_rst),.Q(w_rx_dv_active_delayed));

    always @(posedge i_clk) begin
        if( w_reset_received_bits_counter == 1 ) begin
            r_received_bits_buff_escaped_pre_shift <= 0; 
        end else if( (w_rx_dv_active_delayed == 1) && (r_frame_delimiter_received_counter < 2) ) begin // stop counter
            r_received_bits_buff_escaped_pre_shift <= r_received_bits_buff_escaped_pre_shift | r_received_bits_next_byte_to_store;
        end
    end

    // shift to right rx_buffer to place the LSB at position 0, also we shift 8 more to delete frame delimiter
    reg [HDLC_LITE_FRAME_MAX_WIDTH_BYTES*8-1:0] r_rx_buff_escaped;
    always @(r_received_bits_buff_escaped_pre_shift, r_received_bits_counter) begin
        r_rx_buff_escaped <= r_received_bits_buff_escaped_pre_shift >> ((HDLC_LITE_FRAME_MAX_WIDTH_BYTES*8-r_received_bits_counter)+0);
    end

    /*- Unescaping stage ----------------------------------------------------*/ 
    // Register the value of received bits counter here to prevent timing issues in implementation
    wire [$clog2(HDLC_LITE_FRAME_MAX_WIDTH_BYTES*8):0] w_received_bits_counter_delay0;
    wire [$clog2(HDLC_LITE_FRAME_MAX_WIDTH_BYTES*8):0] w_received_bits_counter_delay1;
    reg_n #(.N($clog2(HDLC_LITE_FRAME_MAX_WIDTH_BYTES*8)+1)) received_bits_counter_reg0 (.D(r_received_bits_counter),.clk(i_clk),.sync_reset(i_rst),.Q(w_received_bits_counter_delay0));
    reg_n #(.N($clog2(HDLC_LITE_FRAME_MAX_WIDTH_BYTES*8)+1)) received_bits_counter_reg1 (.D(w_received_bits_counter_delay0),.clk(i_clk),.sync_reset(i_rst),.Q(w_received_bits_counter_delay1));

   // Unescaping buff counter
    integer r_unescaping_buff_counter;
    wire w_reset_unescaping_buff_counter;
    reg r_fsm_reset_unescaping_buff_counter; //reset signal used by fsm
    reg r_unescaping_finished;

    // Delay 2 clock cycles the reset signal emited by the fsm to be in sync with the registered received_bits_counter
    wire [1:0] w_fsm_reset_unescaping_buff_counter_delayed;
    ffd ffd_fsm_reset_unescaping_buff_counter0 (.D(r_fsm_reset_unescaping_buff_counter),.clk(i_clk),.sync_reset(i_rst),.Q(w_fsm_reset_unescaping_buff_counter_delayed[0]));
    ffd ffd_fsm_reset_unescaping_buff_counter1 (.D(w_fsm_reset_unescaping_buff_counter_delayed[0]),.clk(i_clk),.sync_reset(i_rst),.Q(w_fsm_reset_unescaping_buff_counter_delayed[1]));

    assign w_reset_unescaping_buff_counter = w_fsm_reset_unescaping_buff_counter_delayed[1] | i_rst;
    always @(posedge i_clk) begin
        if( w_reset_unescaping_buff_counter == 1 ) begin
            r_unescaping_buff_counter <= 0;
            r_unescaping_finished <= 0;
        end else begin
            if(r_unescaping_buff_counter >= w_received_bits_counter_delay1) begin
                r_unescaping_finished <= 1;
            end else begin
                r_unescaping_buff_counter <= r_unescaping_buff_counter + 8;
            end
        end
    end

    wire w_unescaping_finished_delayed;
    ffd ffd_unescaping_finished (.D(r_unescaping_finished),.clk(i_clk),.sync_reset(w_reset_unescaping_buff_counter),.Q(w_unescaping_finished_delayed));

    // Mask the next byte to be analyzed
    reg [7:0] w_unescaping_byte_to_analyze;
    always @(posedge i_clk) begin
        w_unescaping_byte_to_analyze <= (r_rx_buff_escaped >> (w_received_bits_counter_delay1-r_unescaping_buff_counter)) & 8'hFF;
    end

    // Flag that is set when the byte is a control byte
    wire w_unescaping_ctrl_byte_detected, w_unescaping_ctrl_byte_detected_delayed;
    assign w_unescaping_ctrl_byte_detected = (w_unescaping_byte_to_analyze == HDLC_LITE_ESCAPE_CHAR) ? 1 : 0;
    ffd ffd_ctrl_byte_detected (.D(w_unescaping_ctrl_byte_detected),.clk(i_clk),.sync_reset(i_rst),.Q(w_unescaping_ctrl_byte_detected_delayed));

    // XOR the current byte if the previous byte was a control byte
    // Also, if the current byte is a control byte, set a zero to w_unescaping_byte_to_store
    reg [7:0] w_unescaping_byte_to_store;
    always @(w_unescaping_ctrl_byte_detected_delayed, w_unescaping_ctrl_byte_detected, w_unescaping_byte_to_analyze) begin
        if(w_unescaping_ctrl_byte_detected == 1) begin
            w_unescaping_byte_to_store <= 0;
        end else if(w_unescaping_ctrl_byte_detected_delayed == 1) begin
            w_unescaping_byte_to_store <= (8'h20 ^ w_unescaping_byte_to_analyze);
        end else begin
            w_unescaping_byte_to_store <= w_unescaping_byte_to_analyze;
        end
    end

    // Here, we delay the reset one clock cycle to feed the counter that counts the bits stored
    // in the unescaped buffer.
    wire w_reset_unescaping_buff_counter_delayed;
    ffd ffd_reset_unescaping_buff_counter (.D(w_reset_unescaping_buff_counter),.clk(i_clk),.sync_reset(i_rst),.Q(w_reset_unescaping_buff_counter_delayed)); 

    // Count the quantity of control bits detected
    integer r_unescaping_ctrl_bits_qty;
    always @(posedge i_clk) begin
        if( w_reset_unescaping_buff_counter_delayed == 1 ) begin
            r_unescaping_ctrl_bits_qty <= 0;
        end else if(w_unescaping_ctrl_byte_detected_delayed == 1) begin
            r_unescaping_ctrl_bits_qty <= r_unescaping_ctrl_bits_qty + 8;
        end
    end

    // Count the bits stored in unescaped buffer.
    integer r_unescaping_stored_bits_counter;
    always @(posedge i_clk) begin
        if( w_reset_unescaping_buff_counter_delayed == 1 ) begin
            r_unescaping_stored_bits_counter <= 0;
        end else if( (r_unescaping_finished  == 0) && (w_unescaping_ctrl_byte_detected == 0) ) begin // stop counter
            r_unescaping_stored_bits_counter <= r_unescaping_stored_bits_counter + 8;
        end
    end

    // Shift to the left the byte that must be stored in frame
    reg [$clog2(HDLC_LITE_RX_FRAME_DATA_AND_CRC_UNESCAPED_MAX_WIDTH_BYTES*8+1):0] r_unescaping_shift_value;
    always @(w_received_bits_counter_delay1, r_unescaping_stored_bits_counter) begin
        if((w_received_bits_counter_delay1-r_unescaping_stored_bits_counter) >= 0) begin
            r_unescaping_shift_value <= w_received_bits_counter_delay1-r_unescaping_stored_bits_counter; 
        end else begin
            r_unescaping_shift_value <= 0; 
        end
    end

    reg [HDLC_LITE_RX_FRAME_DATA_AND_CRC_UNESCAPED_MAX_WIDTH_BYTES*8-1:0] r_unescaping_byte_to_store_shifted;
    always @(posedge i_clk) begin
        if(w_reset_unescaping_buff_counter_delayed == 1) begin
            r_unescaping_byte_to_store_shifted <= 0;
        end else begin
            r_unescaping_byte_to_store_shifted <= (w_unescaping_byte_to_store << r_unescaping_shift_value);
        end
    end

    // Continously build the frame, making an OR shift to the left the byte that must be stored in frame
    reg [HDLC_LITE_RX_FRAME_DATA_AND_CRC_UNESCAPED_MAX_WIDTH_BYTES*8-1:0] r_rx_buff_unescaped_pre_reg;
    wire [HDLC_LITE_RX_FRAME_DATA_AND_CRC_UNESCAPED_MAX_WIDTH_BYTES*8-1:0] w_rx_frame_unescaped_shifted;
    always @(r_unescaping_byte_to_store_shifted, w_rx_frame_unescaped_shifted) begin
        r_rx_buff_unescaped_pre_reg <= r_unescaping_byte_to_store_shifted | w_rx_frame_unescaped_shifted;
    end 
    reg_n #(.N(HDLC_LITE_RX_FRAME_DATA_AND_CRC_UNESCAPED_MAX_WIDTH_BYTES*8)) rx_frame_data_unescaped_reg (.D(r_rx_buff_unescaped_pre_reg),.clk(i_clk),.sync_reset(w_reset_unescaping_buff_counter),.Q(w_rx_frame_unescaped_shifted));

    reg [15:0] r_rx_frame_crc_unescaped;
    reg [HDLC_LITE_RX_OUTPUT_MAX_DATA_WIDTH_BYTES*8-1:0] r_rx_frame_data_unescaped;
    always @(posedge i_clk) begin
        if(w_reset_unescaping_buff_counter_delayed == 1) begin
            r_rx_frame_crc_unescaped <= 0;
            r_rx_frame_data_unescaped <= 0;
        end else begin
            r_rx_frame_crc_unescaped = (w_rx_frame_unescaped_shifted >> r_unescaping_ctrl_bits_qty) & 16'hFFFF;
            r_rx_frame_data_unescaped = w_rx_frame_unescaped_shifted >> (16+r_unescaping_ctrl_bits_qty);
        end
    end

    /*- CRC encoding stage --------------------------------------------------*/
    integer r_crc_encoding_buff_counter;
    wire w_crc_encoding_reset_buff_counter;
    reg r_fsm_reset_crc_encoding_buff_counter; //reset signal used by fsm
    assign w_crc_encoding_reset_buff_counter = r_fsm_reset_crc_encoding_buff_counter | i_rst;

    always @(posedge i_clk) begin
        if( w_crc_encoding_reset_buff_counter == 1 ) begin
            r_crc_encoding_buff_counter <= (r_unescaping_stored_bits_counter >= 16) ? (r_unescaping_stored_bits_counter-16) : 0; //reset val is the qty of bits of the unescaped frame
        end else begin
            if( r_crc_encoding_buff_counter >= 0 ) begin
                r_crc_encoding_buff_counter <= r_crc_encoding_buff_counter -1;
            end
        end
    end

    // crc encoder takes 1 clock cycle to analyze bits. So, if we set crc_encoder_valid_input
    // to `1` in clock cycle i, then the first bit is analyzed in clock cycle i+1.
    // For this reason, we start crc encoder with w_data_at_input signal too

    wire w_crc_encoder_valid_input;
    assign w_crc_encoder_valid_input = ((r_crc_encoding_buff_counter >= 0) && (w_crc_encoding_reset_buff_counter == 0)) ? 1 : 0;

    wire w_crc_encoder_input_data;
    assign w_crc_encoder_input_data = (r_rx_frame_data_unescaped >> r_crc_encoding_buff_counter) & 1'b1;

    wire [15:0] w_crc_encoder_output;
    wire w_crc_encoder_output_valid;

    crc16_ccitt_encoder crc_encoder (
        .clk            (i_clk),
        .i_data         (w_crc_encoder_input_data),
        .i_data_valid   (w_crc_encoder_valid_input),
        .i_rst          (i_rst),
        .o_crc          (w_crc_encoder_output),
        .o_crc_valid    (w_crc_encoder_output_valid)
    );

    // set o_correct_crc high during 3 clock cycles
    wire [2:0] w_crc_encoding_correct;
    assign w_crc_encoding_correct[0] = ((r_rx_frame_crc_unescaped == w_crc_encoder_output) && (w_crc_encoder_output_valid == 1)) ? 1 : 0; 
    ffd ffd_crc_encoding_correct0 (.D(w_crc_encoding_correct[0]),.clk(i_clk),.sync_reset(w_crc_encoding_reset_buff_counter),.Q(w_crc_encoding_correct[1]));
    ffd ffd_crc_encoding_correct1 (.D(w_crc_encoding_correct[1]),.clk(i_clk),.sync_reset(w_crc_encoding_reset_buff_counter),.Q(w_crc_encoding_correct[2]));

    assign o_correct_crc = w_crc_encoding_correct[0] | w_crc_encoding_correct[1] | w_crc_encoding_correct[2];

    /*- Misc ----------------------------------------------------------------*/
    // Mantain high o_rx_dv two clock cycles!
    reg  [$clog2(HDLC_LITE_RX_OUTPUT_MAX_DATA_WIDTH_BYTES*8):0] r_fsm_rx_dv;
    wire [$clog2(HDLC_LITE_RX_OUTPUT_MAX_DATA_WIDTH_BYTES*8):0] w_fsm_rx_dv_delayed0;
    wire [$clog2(HDLC_LITE_RX_OUTPUT_MAX_DATA_WIDTH_BYTES*8):0] w_fsm_rx_dv_delayed1;
    reg_n #( .N( $clog2(HDLC_LITE_RX_OUTPUT_MAX_DATA_WIDTH_BYTES*8)+1 ) ) fsm_rx_dv_reg0 (.D(r_fsm_rx_dv),.clk(i_clk),.sync_reset(i_rst),.Q(w_fsm_rx_dv_delayed0));
    reg_n #( .N( $clog2(HDLC_LITE_RX_OUTPUT_MAX_DATA_WIDTH_BYTES*8)+1 ) ) fsm_rx_dv_reg1 (.D(w_fsm_rx_dv_delayed0),.clk(i_clk),.sync_reset(i_rst),.Q(w_fsm_rx_dv_delayed1));

    assign o_rx_dv = w_fsm_rx_dv_delayed0 | w_fsm_rx_dv_delayed1;

    reg r_fsm_present_data;
    always @(posedge i_clk) begin
        if(i_rst == 1) begin
            o_rx_data <= 0;
        end else if( r_fsm_present_data == 1 ) begin
            o_rx_data <= r_rx_frame_data_unescaped;
        end
    end

    /*- Finite state machine ------------------------------------------------*/
    localparam  state_idle                  = 3'b000,
                state_waiting_end_frame     = 3'b001,
                state_unescape_frame        = 3'b010,
                state_computing_crc         = 3'b011,
                state_present_data          = 3'b100;

    reg[2:0] fsm_state, fsm_next_state;

    // Current state register (sequential logic)
    always @(posedge i_clk) begin
        if( i_rst == 1'b1 ) begin 
            fsm_state <= state_idle;
        end else begin // otherwise update the states
            fsm_state <= fsm_next_state;
        end
    end

    integer r_fsm_tx_buff_ptr;

    always @(fsm_state, r_frame_delimiter_received_counter, w_unescaping_finished_delayed, w_crc_encoder_output_valid) begin
        // Set undefined as default state, is good to catch bugs if
        // a transitions is not defined!
        fsm_next_state <= 3'bxxx;

        // Decide next state based on input
        case(fsm_state)
            state_idle:
                begin
                    if( r_frame_delimiter_received_counter > 0 ) begin
                        fsm_next_state <= state_waiting_end_frame;
                    end else begin
                        fsm_next_state <= state_idle;
                    end
                end

            state_waiting_end_frame:
                begin
                    if( r_frame_delimiter_received_counter > 1 ) begin
                        fsm_next_state <= state_unescape_frame;
                    end else begin
                        fsm_next_state <= state_waiting_end_frame;
                    end
                end

            state_unescape_frame:
                begin
                    if( w_unescaping_finished_delayed == 1 ) begin
                        fsm_next_state <= state_computing_crc;
                    end else begin
                        fsm_next_state <= state_unescape_frame;
                    end
                end

            state_computing_crc:
                begin
                    if( w_crc_encoder_output_valid == 1 ) begin
                        fsm_next_state <= state_present_data;
                    end else begin
                        fsm_next_state <= state_computing_crc;
                    end
                end

            state_present_data:
                begin
                    fsm_next_state <= state_idle;
                end
        endcase
    end

    // Combinational output logic: will run only when fsm changes its state
    always @(fsm_state, r_unescaping_stored_bits_counter) begin
        r_fsm_reset_crc_encoding_buff_counter <= 1;
        r_fsm_reset_unescaping_buff_counter <= 1;
        r_fsm_reset_received_bits_counter <= 1;
        r_fsm_frame_delimiter_received_counter_reset <= 1;

        r_fsm_present_data <= 0;
        r_fsm_rx_dv <= 0;

        case(fsm_state)

            state_idle:
                begin
                    r_fsm_frame_delimiter_received_counter_reset <= 0;
                end

            state_waiting_end_frame:
                begin
                    r_fsm_reset_received_bits_counter <= 0;
                    r_fsm_frame_delimiter_received_counter_reset <= 0;
                end

            state_unescape_frame:
                begin
                    r_fsm_reset_unescaping_buff_counter <= 0;

                    // mantain old value and avoid unnecesary latches
                    r_fsm_reset_received_bits_counter <= 0;
                end

            state_computing_crc:
                begin
                    r_fsm_reset_crc_encoding_buff_counter <= 0;

                    // mantain old value and avoid unnecesary latches
                    r_fsm_reset_unescaping_buff_counter <= 0;
                    r_fsm_reset_received_bits_counter <= 0;
                end

            state_present_data:
                begin
                    r_fsm_rx_dv <= r_unescaping_stored_bits_counter-16;
                    r_fsm_present_data <= 1;

                    // mantain old value and avoid unnecesary latches
                    r_fsm_reset_unescaping_buff_counter <= 0;
                    r_fsm_reset_received_bits_counter <= 0;
                    r_fsm_reset_crc_encoding_buff_counter <= 0;
                end
        endcase
    end


// the "macro" to dump signals
`ifdef COCOTB_SIM
initial begin
  $dumpfile ("hdlc_lite_rx.vcd");
  $dumpvars (0, hdlc_lite_rx);
  #1;
end

`else

    /* ILA instantiation if we are debugging this module with Vivado */
    //`define DEBUG_HDLC_LITE_RX
    `ifdef DEBUG_HDLC_LITE_RX
    ila_hdlc_lite_rx ila_hdlc_lite_rx (
        .clk(i_clk), // input wire clk
        // probe rx path
        .probe0(i_rx_byte),     // reg[7:0]
        .probe1(i_rx_dv),       // 1bit
        .probe2(fsm_state),     // reg[2:0]
        .probe3(fsm_next_state),// reg[2:0]
        .probe4(w_rx_dv_active), // 1bit
        .probe5(o_rx_dv),       // reg[7:0]
        .probe6(o_correct_crc),  // 1bit
        .probe7(r_fsm_frame_delimiter_received_counter_reset), // 1bit
        .probe8(r_unescaping_stored_bits_counter), // reg[31:0]
        .probe9(r_frame_delimiter_received_counter), // reg[31:0]
        .probe10(w_frame_delimiter_received), // 1bit
        .probe11(r_sampled_input_byte), // reg[7:0]
        .probe12(r_byte_received_is_frame_delimiter), //1bit
        .probe13(o_rx_data) // reg[103:0]
    );
    `endif

`endif

endmodule
