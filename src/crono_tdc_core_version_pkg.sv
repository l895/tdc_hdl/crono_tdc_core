package crono_tdc_core_version_pkg;
    /* Package with the current version of Crono TDC Core code.
     * Can be update with the script `update_crono_tdc_version.sh` and will
     * fill this parameters with the values of the current branch.
     * Crono TDC Core code can consume this values in some way.
     */
    parameter CRONO_TDC_CORE_GITHASH = "318c6a9";
    parameter CRONO_TDC_CORE_GITHASH_WIDTH = 7*8;   /* Width (in bits) of the githash. */
endpackage
