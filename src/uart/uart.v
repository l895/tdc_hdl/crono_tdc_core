/*
 * Verilog code for a UART.
 * Description:
 * This module containes a UART that it's able to receive 8 bits
 * with one start bit, one stop bit and no parity bit. When receive is
 * complete the signal o_rx_dv will be driven high for one clock cycle.
 *
 * Parameters:
 *  BAUDRATE = the baudrate of the serial comm (i.e. 115200)
 *  CLOCK_FREQ_HZ = the frequency of the clock (i_clk) in Hz
 *
 * Author: Julian Rodriguez <jnrodriguez@estudiantes.unsam.edu.ar>
 */

`timescale 1ns / 1ps

module uart
#(  parameter   BAUDRATE      =   115200,
                CLOCK_FREQ_HZ =   10000000
)(
    input        i_clk,          // input clock
    input       i_reset,
    // RX signals
    input        i_rx_serial,    // input serial port (just 1 bit because is serial)
    output       o_rx_dv,        // flag that indicates that there is data in the reception buffer
    output [7:0] o_rx_byte,      // reception buffer
    // TX signals
    input       i_tx_dv,        // signal used to enable a transference of the byte in 'i_tx_byte'
    input [7:0] i_tx_byte,      // register with the byte to send
    output      o_tx_active,    // flag set when the channel is active (sending data)
    output      o_tx_serial,    // channel where the data will be sent
    output      o_tx_done       // flag set for one cycle when a transference fininshed
);


    
    uart_rx #(
        .CLOCK_FREQ_HZ(CLOCK_FREQ_HZ),
        .BAUDRATE(BAUDRATE)
        ) uart_receptor(
        .i_clk          (i_clk),           // input clock         
        .i_rx_serial    (i_rx_serial),     // serial port RX pin 
        .o_rx_dv        (o_rx_dv),         // flag set when the reception buffer have data 
        .o_rx_byte      (o_rx_byte)        // reception buffer 
    );

    uart_tx #(
        .CLOCK_FREQ_HZ(CLOCK_FREQ_HZ),
        .BAUDRATE(BAUDRATE)
    ) uart_transmitter(
        .i_clk          (i_clk),        // input clock
        .i_reset        (i_reset),
        .i_tx_dv        (i_tx_dv),      // signal used to enable a transference of the byte in 'i_tx_byte'
        .i_tx_byte      (i_tx_byte),    // register with the byte to send
        .o_tx_active    (o_tx_active),  // flag set when the channel is active (sending data)
        .o_tx_serial    (o_tx_serial),  // channel where the data will be sent
        .o_tx_done      (o_tx_done)     // flag set for one cycle when a transference fininshed 
    );

// the "macro" to dump signals
`ifdef COCOTB_SIM
initial begin
  $dumpfile ("uart.vcd");
  $dumpvars (0, uart);
  #1;
end
`endif

endmodule 