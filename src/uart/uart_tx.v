/*
 * Verilog code for a UART transmitter.
 * Description:
 * This module containes a UART transmitter that is able to transmit 8 bits
 * with one start bit, one stop bit and no parity bit. When transmit is
 * complete the flag "o_tx_done" will be drive high for a clock cycle.
 *
 * Parameters:
 *  CLKS_PER_BIT = (clock_freq_hz/baudrate)
 *      => i.e.: 10 MHz clock and 115200 baudrate
 *      => CLKS_PER_BIT = 10000000/115200 = 87
 *
 * Author: Julian Rodriguez <jnrodriguez@estudiantes.unsam.edu.ar>
 */

`timescale 1ns / 1ps

module uart_tx(
        input       i_clk,          // input clock
        input       i_reset,        // reset signal
        input       i_tx_dv,        // signal used to enable a transference of the byte in 'i_tx_byte'
        input [7:0] i_tx_byte,      // register with the byte to send
        output reg  o_tx_active,    // flag set when the channel is active (sending data)
        output reg  o_tx_serial,    // channel where the data will be sent
        output reg  o_tx_done       // flag set for one cycle when a transference fininshed
    );

    parameter CLOCK_FREQ_HZ = 100000000;
    parameter BAUDRATE = 115200;

    // The quantity of clocks per bit (must be calculated with baudrate and input clock frequency)
    localparam CLKS_PER_BIT = CLOCK_FREQ_HZ/BAUDRATE;
    localparam START_BIT_FINISHED_CLOCK_COUNT   = CLKS_PER_BIT;
    localparam DATA_BIT0_FINISHED_CLOCK_COUNT   = CLKS_PER_BIT * 2;
    localparam DATA_BIT1_FINISHED_CLOCK_COUNT   = CLKS_PER_BIT * 3;
    localparam DATA_BIT2_FINISHED_CLOCK_COUNT   = CLKS_PER_BIT * 4;
    localparam DATA_BIT3_FINISHED_CLOCK_COUNT   = CLKS_PER_BIT * 5;
    localparam DATA_BIT4_FINISHED_CLOCK_COUNT   = CLKS_PER_BIT * 6;
    localparam DATA_BIT5_FINISHED_CLOCK_COUNT   = CLKS_PER_BIT * 7;
    localparam DATA_BIT6_FINISHED_CLOCK_COUNT   = CLKS_PER_BIT * 8;
    localparam DATA_BIT7_FINISHED_CLOCK_COUNT   = CLKS_PER_BIT * 9;
    localparam STOP_BIT_FINISHED_CLOCK_COUNT    = CLKS_PER_BIT * 10;

    // Clock cycles counter 
    reg r_fsm_clock_cycles_count_reset, r_clock_cycles_count_start_bit_time_finished, r_clock_cycles_count_stop_bit_time_finished;
    wire w_clock_cycles_count_reset;
    reg [$clog2(STOP_BIT_FINISHED_CLOCK_COUNT):0] r_clock_cycles_count;
    reg [9:0] r_clock_cycles_count_uart_frame_time_finished;

    assign w_clock_cycles_count_reset = i_reset | r_fsm_clock_cycles_count_reset;

    always @(posedge i_clk) begin
        if(w_clock_cycles_count_reset == 1) begin
            r_clock_cycles_count <= 0;
            r_clock_cycles_count_uart_frame_time_finished <= 0;

        end else begin
            r_clock_cycles_count <= r_clock_cycles_count + 1;

            if( r_clock_cycles_count > START_BIT_FINISHED_CLOCK_COUNT ) begin
                r_clock_cycles_count_uart_frame_time_finished <= r_clock_cycles_count_uart_frame_time_finished | 10'b0000000001;
            end
            
            if( r_clock_cycles_count > DATA_BIT0_FINISHED_CLOCK_COUNT ) begin
                r_clock_cycles_count_uart_frame_time_finished <= r_clock_cycles_count_uart_frame_time_finished | 10'b0000000010;
            end

            if( r_clock_cycles_count > DATA_BIT1_FINISHED_CLOCK_COUNT ) begin
                r_clock_cycles_count_uart_frame_time_finished <= r_clock_cycles_count_uart_frame_time_finished | 10'b0000000100;
            end

            if( r_clock_cycles_count > DATA_BIT2_FINISHED_CLOCK_COUNT ) begin
                r_clock_cycles_count_uart_frame_time_finished <= r_clock_cycles_count_uart_frame_time_finished | 10'b0000001000;
            end

            if( r_clock_cycles_count > DATA_BIT3_FINISHED_CLOCK_COUNT ) begin
                r_clock_cycles_count_uart_frame_time_finished <= r_clock_cycles_count_uart_frame_time_finished | 10'b0000010000;
            end

            if( r_clock_cycles_count > DATA_BIT4_FINISHED_CLOCK_COUNT ) begin
                r_clock_cycles_count_uart_frame_time_finished <= r_clock_cycles_count_uart_frame_time_finished | 10'b0000100000;
            end

            if( r_clock_cycles_count > DATA_BIT5_FINISHED_CLOCK_COUNT ) begin
                r_clock_cycles_count_uart_frame_time_finished <= r_clock_cycles_count_uart_frame_time_finished | 10'b0001000000;
            end

            if( r_clock_cycles_count > DATA_BIT6_FINISHED_CLOCK_COUNT ) begin
                r_clock_cycles_count_uart_frame_time_finished <= r_clock_cycles_count_uart_frame_time_finished | 10'b0010000000;
            end

            if( r_clock_cycles_count > DATA_BIT7_FINISHED_CLOCK_COUNT ) begin
                r_clock_cycles_count_uart_frame_time_finished <= r_clock_cycles_count_uart_frame_time_finished | 10'b0100000000;
            end

            if( r_clock_cycles_count > STOP_BIT_FINISHED_CLOCK_COUNT ) begin
                r_clock_cycles_count_uart_frame_time_finished <= r_clock_cycles_count_uart_frame_time_finished | 10'b1000000000;
            end

        end
    end

    // Detect if data bits finished counter changed
    wire [9:0] w_clock_cycles_count_uart_frame_time_finished_delayed;
    wire w_clock_cycles_count_uart_frame_time_finished_has_changed;

    reg_n #(.N(10)) clock_cycles_count_data_bits_finished (.D(r_clock_cycles_count_uart_frame_time_finished),.clk(i_clk),.sync_reset(i_reset),.Q(w_clock_cycles_count_uart_frame_time_finished_delayed));
    assign w_clock_cycles_count_uart_frame_time_finished_has_changed = (r_clock_cycles_count_uart_frame_time_finished != w_clock_cycles_count_uart_frame_time_finished_delayed) ? 1 : 0;

    // Sample input byte and build uart frame when fsm says so
    reg [9:0] r_uart_frame;
    reg r_fsm_sample_input_tx_byte;

    always @(posedge i_clk) begin
        if( i_reset == 1 ) begin
            r_uart_frame <= 10'h3FF;
        end else if (r_fsm_sample_input_tx_byte == 1) begin
            r_uart_frame <= (1'b1 << 9) | (i_tx_byte << 1) | 1'b0;
        end
    end 

    // PISO shift register
    wire w_shift_register_reset;
    reg r_fsm_shift_register_reset;
    reg [3:0] r_shift_register_index;
    assign w_shift_register_reset = i_reset | r_fsm_shift_register_reset;

    always @(posedge i_clk) begin
        if(w_shift_register_reset == 1) begin
            o_tx_serial <= 1;
            r_shift_register_index <= 0;
        end else if(w_clock_cycles_count_uart_frame_time_finished_has_changed == 1) begin
            o_tx_serial <= r_uart_frame >> r_shift_register_index;
            r_shift_register_index <= r_shift_register_index + 1;
        end
    end

    // Clock cycles counter to set high 2 clock cycles o_tx_done signal
    reg [2:0] r_tx_done_clock_cycles_counter;
    wire w_tx_done_clock_cycles_counter_reset;
    reg r_fsm_tx_done_clock_cycles_counter_reset;

    assign w_tx_done_clock_cycles_counter_reset = r_fsm_tx_done_clock_cycles_counter_reset | i_reset; 
    always @(posedge i_clk) begin
        if( w_tx_done_clock_cycles_counter_reset == 1) begin
            r_tx_done_clock_cycles_counter <= 0;
        end else begin
            r_tx_done_clock_cycles_counter <= r_tx_done_clock_cycles_counter +1;
        end
    end

    /*- Finite State Machine --------------------------------------------------*/
    // States of the FSM of the UART transmitter
    localparam  state_idle              = 2'b00,
                state_sample_input_byte = 2'b01,
                state_send_uart_frame   = 2'b10,
                state_cleanup           = 2'b11;

    reg [1:0] fsm_state;
    reg [1:0] fsm_next_state;

    // Current state register (sequential logic)
    always @(posedge i_clk) begin
        if( i_reset == 1'b1 ) begin 
            fsm_state <= state_idle;
        end else begin // otherwise update the states
            fsm_state <= fsm_next_state;
        end
    end

    always @(fsm_state, i_tx_dv, r_clock_cycles_count_uart_frame_time_finished, r_tx_done_clock_cycles_counter ) begin
        // Set undefined as default state, is good to catch bugs if
        // a transitions is not defined!
        fsm_next_state <= 2'bxx;

        // Decide next state based on input
        case(fsm_state)

            /* IDLE state: the FSM checks if the enable tranmission
             * signal (i_tx_dv) was drived high. And if this flag
             * is set then go to TX_START_BIT state. */
            state_idle:
                begin
                    if (i_tx_dv == 1'b1) begin
                        fsm_next_state <= state_sample_input_byte;
                    end else begin
                        fsm_next_state <= state_idle;
                    end
                end

            /* STATE_SAMPLE_INPUT_BYTE state: in this state, the module samples the value of the input
             * byte and builds the uart frame that will be sent. */
            state_sample_input_byte:
                begin
                    fsm_next_state <= state_send_uart_frame;
                end

            /* STATE_SEND_UART_FRAME state: in this state, the module enables the counter and shift register
            * that will send the bits of the frame in the correspondent time. */
            state_send_uart_frame:
                begin
                    if (r_clock_cycles_count_uart_frame_time_finished != 10'h3FF) begin
                        fsm_next_state <= state_send_uart_frame;
                    end else begin
                        fsm_next_state <= state_cleanup;
                    end
                end

            /* STATE_CLEANUP state: in this state we set o_tx_done flag, unset o_tx_active flag and reset shift register and counter.  */
            state_cleanup:
                begin
                    if( r_tx_done_clock_cycles_counter >= 1 ) begin
                        fsm_next_state <= state_idle;
                    end else begin
                        fsm_next_state <= state_cleanup;
                    end
                end

        endcase
    end

    // Combinational output logic
    always @(fsm_state) begin
        r_fsm_clock_cycles_count_reset <= 1;
        r_fsm_shift_register_reset <= 1;
        o_tx_active <= 0;
        o_tx_done <= 0;
        r_fsm_sample_input_tx_byte <= 0;
        r_fsm_tx_done_clock_cycles_counter_reset <= 1;

        case (fsm_state)

            state_sample_input_byte:
                begin
                    r_fsm_sample_input_tx_byte <= 1;
                end

            state_send_uart_frame:
                begin
                    o_tx_active <= 1;
                    r_fsm_clock_cycles_count_reset <= 0;
                    r_fsm_shift_register_reset <= 0;
                end

            state_cleanup:
                begin
                    o_tx_done <= 1;
                    r_fsm_tx_done_clock_cycles_counter_reset <= 0;
                end
        endcase
    end

endmodule