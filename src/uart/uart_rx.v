/*
 * Verilog code for a UART receptor.
 * Description:
 * This module contains a UART receiver that is able to receive 8 bits
 * with one start bit, one stop bit and no parity bit. When receive is
 * complete the signal o_rx_dv will be driven high for one clock cycle.
 *
 * Parameters:
 *  CLKS_PER_BIT = (clock_freq_hz/baudrate)
 *      => i.e.: 10 MHz clock and 115200 baudrate
 *      => CLKS_PER_BIT = 10000000/115200 = 87
 *
 * Author: Julian Rodriguez <jnrodriguez@estudiantes.unsam.edu.ar>
 */

`timescale 1ns / 1ps

module uart_rx(
        input        i_clk,         // input clock
        input        i_rx_serial,   // input serial port (just 1 bit because is serial)
        output       o_rx_dv,       // flag that indicates that there is data in the reception buffer
        output [7:0] o_rx_byte      // reception buffer
    );

    parameter CLOCK_FREQ_HZ = 100000000;
    parameter BAUDRATE = 115200;

    // The quantity of clocks per bit (must be calculated with baudrate and input clock frequency)
    localparam CLKS_PER_BIT = CLOCK_FREQ_HZ/BAUDRATE;
    localparam HALF_CLKS_PER_BIT = (CLKS_PER_BIT-1)/2;

// States of the FSM of the UART receiver
    integer s_IDLE         = 3'b000;
    integer s_RX_START_BIT = 3'b001;
    integer s_RX_DATA_BITS = 3'b010;
    integer s_RX_STOP_BIT  = 3'b011;
    integer s_CLEANUP      = 3'b100;

    reg           r_rx_data_r = 1'b1;
    reg           r_rx_data   = 1'b1;

    reg [$clog2(CLKS_PER_BIT)-1:0] r_clock_count = 0;
    reg [2:0]     r_bit_index                    = 0; //8 bits total
    reg [7:0]     r_rx_byte                      = 0;
    reg           r_rx_dv                        = 0;
    reg [2:0]     r_sm_main                      = 0;

    // Purpose: Double-register the incoming data.
    // This allows it to be used in the UART RX Clock Domain.
    // (It removes problems caused by metastability)
    always @(posedge i_clk)
        begin
            r_rx_data_r <= i_rx_serial;
            r_rx_data   <= r_rx_data_r;
        end


    // Purpose: Control RX state machine
    always @(posedge i_clk) begin
        case (r_sm_main)
            
            /* IDLE state: the FSM checks if the start bit is detected. */
            s_IDLE :
                begin
                    r_rx_dv       <= 1'b0;
                    r_clock_count <= 0;
                    r_bit_index   <= 0;

                    if (r_rx_data == 1'b0)
                        r_sm_main <= s_RX_START_BIT;
                    else
                        r_sm_main <= s_IDLE;
                end

            /* RX_START_BIT state: we check the middle of start bit to make 
            * sure it's still low. */
            s_RX_START_BIT :
                begin
                    if (r_clock_count == HALF_CLKS_PER_BIT) begin
                        // We are in the middle of the bit period!
                            if (r_rx_data == 1'b0) begin
                                // The bit is 0 go to the state that receive the bits!
                                r_clock_count <= 0;  // reset counter, found the middle
                                r_sm_main     <= s_RX_DATA_BITS;
                            end else
                                // The bit is not 0 anymore then was a glitch! go to idle
                                r_sm_main <= s_IDLE;
                    end else begin
                        // count clock cycles until we reach the middle of the bit period
                            r_clock_count <= r_clock_count + 1;
                            r_sm_main     <= s_RX_START_BIT;
                    end
                end

            /* RX_DATA_BITS state: wait CLK_PER_BIT-1 clock cycles to sample
            * serial data 8 times (one per bit sampled). */
            s_RX_DATA_BITS :
                begin
                    if (r_clock_count < CLKS_PER_BIT-1) begin
                    // wait CLK_PER_BIT-1 clock cycles
                        r_clock_count <= r_clock_count + 1;
                        r_sm_main     <= s_RX_DATA_BITS;
                    end else begin
                    // we reached CLK_PER_BIT-1 clock cycles! 
                        r_clock_count          <= 0;
                        r_rx_byte[r_bit_index] <= r_rx_data;

                        // check if we have received all bits
                        if (r_bit_index < 7) begin
                                r_bit_index <= r_bit_index + 1;
                                r_sm_main   <= s_RX_DATA_BITS;
                        end
                        else begin
                            // if we received up to 8 bits, then go to the state that samples the stop bit
                                r_bit_index <= 0;
                                r_sm_main   <= s_RX_STOP_BIT;
                        end
                    end
                end

            /* RX_STOP_BIT state: in this state we wat CLKS_PER_BIT-1 clock cycles
            * to finish the transmission. Here we are receiving the stop bit (stop bit = 1). 
            * In conclusion, the buffer full flag will be set after a half bit period of the
            * rising edge of the serial rx line. */
            s_RX_STOP_BIT :
                begin
                    // Wait CLKS_PER_BIT-1 clock cycles for Stop bit to finish
                    if (r_clock_count < CLKS_PER_BIT-1) begin
                            r_clock_count <= r_clock_count + 1;
                            r_sm_main     <= s_RX_STOP_BIT;
                    end
                    else begin
                            r_rx_dv       <= 1'b1;
                            r_clock_count <= 0;
                            r_sm_main     <= s_CLEANUP;
                    end
                end


            /* CLEANUP state: this is just a teardown state to clear the flag
            * that indicates that there is data in the reception buffer. */
            s_CLEANUP :
                begin
                    r_sm_main <= s_IDLE;
                    r_rx_dv   <= 1'b0;
                end


            default :
                r_sm_main <= s_IDLE;

        endcase
    end

    assign o_rx_dv   = r_rx_dv;
    assign o_rx_byte = r_rx_byte;

endmodule // uart_rx