/*
 * Verilog code for a fake ISERDESE2 in oversample mode used in Crono TDC.
 * Description:
 * This module models the ISERDESE2 IP Core in oversample mode provided
 * by Xilinx. The idea is use this model in behavioral simulations only
 * to speed up the development of the overall TDC system.
 * 
 * This core samples a signal four times during a clock period using 4
 * clocks with a phase shift between them:
 * CLK = 0º
 * CLKB = 180º
 * OCLK = 90º
 * OCLKB = 270º
 * 
 * Then, after feeding the ISERDESE2 with a nibble, the core sets this nibble 
 * in the output in a parallel form after two clock cycles. 
 *
 * This ISERDESE2 is fixed to 1:4.
 *
 * Note: See ISERDESE2 in oversample mode in UG471 "7 Series FPGAs 
 * SelectIO Resources".
 *
 * Author: Julian Rodriguez <jnrodriguez@estudiantes.unsam.edu.ar>
 */

`timescale 1ps/1ps

module fake_iserdese2_ov(
        input                       D,
        output [3:0]                Q,
        input                       CLK,        // clk
        input                       CLKB,       // clk 180 
        input                       OCLK,       // clk 90
        input                       OCLKB,      // clk 270
        input                       RST
    );

    wire q11, q12, q21, q22, q31, q32, q41, q42;
    wire out[3:0];

    /* First row:
     * Sample the most significant bit in the clock period,
     * using the clock without phase shift.
     */
    ffd ffd_q11(
        .D          (D),
        .clk        (CLK),
        .sync_reset (RST),
        .Q          (q11)
    );

    ffd ffd_q12(
        .D          (q11),
        .clk        (CLK),
        .sync_reset (RST),
        .Q          (q12)
    );

    ffd ffd_q3(
        .D          (q12),
        .clk        (CLK),
        .sync_reset (RST),
        .Q          (out[3])
    );

    /* Second row:
     * Sample the bit 1 in the clock period,
     * using the clock with a phaseshift of 180 (CLKB).
     */
    ffd ffd_q21(
        .D          (D),
        .clk        (CLKB),
        .sync_reset (RST),
        .Q          (q21)
    );

    ffd ffd_q22(
        .D          (q21),
        .clk        (OCLK),
        .sync_reset (RST),
        .Q          (q22)
    );

    ffd ffd_q2(
        .D          (q22),
        .clk        (CLK),
        .sync_reset (RST),
        .Q          (out[1])
    );

    /* Third row:
     * Sample the bit 2 in the clock period,
     * using the clock with a phaseshift of 90 (OCLK).
     */
    ffd ffd_q31(
        .D          (D),
        .clk        (OCLK),
        .sync_reset (RST),
        .Q          (q31)
    );

    ffd ffd_q32(
        .D          (q31),
        .clk        (CLK),
        .sync_reset (RST),
        .Q          (q32)
    );

    ffd ffd_q1(
        .D          (q32),
        .clk        (CLK),
        .sync_reset (RST),
        .Q          (out[2])
    );

    /* Fourth row:
     * Sample the bit 0 (LSB) in the clock period,
     * using the clock with a phaseshift of 270 (OCLKB).
     */
    ffd ffd_q41(
        .D          (D),
        .clk        (OCLKB),
        .sync_reset (RST),
        .Q          (q41)
    );

    ffd ffd_q42(
        .D          (q41),
        .clk        (OCLK),
        .sync_reset (RST),
        .Q          (q42)
    );

    ffd ffd_q0(
        .D          (q42),
        .clk        (CLK),
        .sync_reset (RST),
        .Q          (out[0])
    );

    assign Q[3] = out[3];
    assign Q[2] = out[2];
    assign Q[1] = out[1];
    assign Q[0] = out[0];

// the "macro" to dump signals
`ifdef COCOTB_SIM
initial begin
  $dumpfile ("fake_iserdese2_ov.vcd");
  $dumpvars (0, fake_iserdese2_ov);
  #1;
end
`endif

endmodule 