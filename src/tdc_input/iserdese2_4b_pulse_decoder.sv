/*
 * Verilog code for a finite state machine that implements a TDC with the
 * output of an iserdes of 4 bits.
 * Description:
 * This core processs the output and the delayed output (1 clock cycle)
 * of an ISERDESE2 to determine when happens a small pulse or a long pulse.
 * When the FSM detects a small pulse or the starting/ending of a long pulse
 * it samples the serdes output and tags it.
 *
 * Input: 4 bits
 *    bits [3:0]: ISERDESE2 output
 *
 * Output: CLK_CYCLE_COUNT_WIDTH+4+3 bits
 *    bits [CLK_CYCLE_COUNT_WIDTH+4+3-1:7]: clock cycle count
 *    bits [5:2]: ISERDESE2 output
 *    bits [2:0]: measurement tag mask, each bit have a different meaning:
 *        bit 0 => small pulse in ISERDESE2 output
 *        bit 1 => long pulse starting in ISERDESE2 output
 *        bit 2 => long pulse ending in ISERDESE2 output
 *
 * And the bits can be combined in the following way, depending on the
 * state of the FSM:
 * `idle` or `long pulse continue`            => TAG = 0     
 * `small pulse`                              => TAG = 0b001 
 * `long pulse starts`                        => TAG = 0b010 
 * `long pulse starts small appears`          => TAG = 0b011 
 * `long pulse end`                           => TAG = 0b100 
 * `long pulse end small appears`             => TAG = 0b101 
 * `long pulse end long starts`               => TAG = 0b110 
 *
 *
 * Author: Julian Rodriguez <jnrodriguez@estudiantes.unsam.edu.ar>
 */

`timescale 1ps/1ps

module iserdese2_4b_pulse_decoder(i_serdes_out, i_reset, i_clk, o_data, i_start, i_stop);
    parameter CLK_CYCLE_COUNT_WIDTH = 13; // Width of the clock cycles counter

    input [3:0]                                 i_serdes_out;
    input                                       i_reset; // used to reset teh fsm, if high the fsm is in reset state
    input                                       i_clk;
    input                                       i_start; // signal used to start the pulse decoder (starts on rising edges of this signal)
    input                                       i_stop; // signal used to stop the pulse decoder (stops on rising edges of this signal)
    output reg [CLK_CYCLE_COUNT_WIDTH+3+4-1:0]  o_data; // [2:0] = measurement type, [6:3] = delayed SerDes value, [22:6] = clock cycles count

    /* Register ISERDESE2 output and build a register with the registered output and the MSB of the actual output. */
    wire [3:0] w_serdes_1delay;
    wire [3:0] w_serdes_2delay;
    reg_n #(.N(4)) reg0_serdese (.D(i_serdes_out),.clk(i_clk),.sync_reset(i_reset),.Q(w_serdes_1delay));
    reg_n #(.N(4)) reg1_serdese (.D(w_serdes_1delay),.clk(i_clk),.sync_reset(i_reset),.Q(w_serdes_2delay));

    wire [4:0] w_fsm_serdes_input;
    assign w_fsm_serdes_input[4:1] = w_serdes_1delay;
    assign w_fsm_serdes_input[0] = i_serdes_out[3];

    localparam  state_off                                       = 4'b0000,
                state_idle                                      = 4'b0001,
                state_small_pulse                               = 4'b0010,
                state_long_pulse_starts                         = 4'b0011,
                state_long_pulse_starts_small_appears           = 4'b0100,
                state_long_pulse_continue                       = 4'b0101,
                state_long_pulse_end                            = 4'b0110,
                state_long_pulse_end_long_starts                = 4'b0111,
                state_long_pulse_end_small_appears              = 4'b1000;

    localparam  tag_small_pulse                               = 3'b001,
                tag_long_pulse_starts                         = 3'b010,
                tag_long_pulse_starts_small_appears           = 3'b011,
                tag_long_pulse_ends                           = 3'b100,
                tag_long_pulse_ends_small_appears             = 3'b101,
                tag_long_pulse_ends_long_starts               = 3'b110;

    reg [3:0] fsm_state, fsm_next_state;
    reg [3:0] r_sampled_data;
    reg [2:0] r_data_tag;
    reg r_fsm_clk_cycles_counter_reset; // register used to reset the clock cycles counter

    // Current state register (sequential logic)
    always @(posedge i_clk)
    begin
        if( i_reset == 1'b1 ) begin // go to idle state if reset is asserted
          fsm_state <= state_off;
        end else begin // otherwise update the states
          fsm_state <= fsm_next_state;
        end
    end

    // Next state logic: define next state of the fsm based in serdes output
    always @(fsm_state, w_fsm_serdes_input, i_start, i_stop) begin
      // Set undefined as default state, is good to catch bugs if
      // a transitions is not defined!
      fsm_next_state <= 4'bxxxx;

      case(fsm_state)
        state_off: begin
          if( i_start == 1 ) begin
            fsm_next_state <= state_idle;
          end else begin
            fsm_next_state <= state_off;
          end
        end

        state_idle, state_small_pulse: begin
          if( i_stop == 1 ) begin
            fsm_next_state <= state_off;
          end else begin
            case(w_fsm_serdes_input)
              5'b00000, 5'b00001: fsm_next_state <= state_idle;
              5'b00010, 5'b00100, 5'b00101, 5'b00110, 5'b01000, 5'b01001, 5'b01010, 5'b01100, 5'b01101, 5'b01110, 5'b10000, 5'b10001, 5'b10010, 5'b10100, 5'b10101, 5'b10110, 5'b11000, 5'b11001, 5'b11010, 5'b11100, 5'b11101, 5'b11110: fsm_next_state <= state_small_pulse;
              5'b00011, 5'b00111, 5'b01111, 5'b11111: fsm_next_state <= state_long_pulse_starts;
              5'b01011, 5'b10011, 5'b10111, 5'b11011: fsm_next_state <= state_long_pulse_starts_small_appears;
              default: fsm_next_state <= 4'bxxxx;
            endcase
          end
        end

        state_long_pulse_starts, state_long_pulse_starts_small_appears, state_long_pulse_end_long_starts, state_long_pulse_continue: begin
          if( i_stop == 1 ) begin
            fsm_next_state <= state_off;
          end else begin
            case(w_fsm_serdes_input)
              5'b00000, 5'b00001, 5'b10000, 5'b10001, 5'b11000, 5'b11001, 5'b11100, 5'b11101, 5'b11110: fsm_next_state <= state_long_pulse_end;
              5'b00010, 5'b00100, 5'b00101, 5'b00110, 5'b01000, 5'b01001, 5'b01010, 5'b01100, 5'b01101, 5'b01110, 5'b10010, 5'b10100, 5'b10101, 5'b10110, 5'b11010: fsm_next_state <= state_long_pulse_end_small_appears;
              5'b00011, 5'b00111, 5'b01111, 5'b10011, 5'b10111, 5'b11011: fsm_next_state <= state_long_pulse_end_long_starts;
              5'b01011: fsm_next_state <= 4'bxxxx; // TODO: check if this state is possible, i think not
              5'b11111: fsm_next_state <= state_long_pulse_continue;
              default: fsm_next_state <= 4'bxxxx;
            endcase
          end
        end

        state_long_pulse_end, state_long_pulse_end_small_appears: begin
          if( i_stop == 1 ) begin
            fsm_next_state <= state_off;
          end else begin
            case(w_fsm_serdes_input)
              5'b00000, 5'b00001: fsm_next_state <= state_idle;
              5'b00010, 5'b00100, 5'b00101, 5'b00110, 5'b01000, 5'b01001, 5'b01010, 5'b01100, 5'b01101, 5'b01110, 5'b10000, 5'b10001, 5'b10010, 5'b10100, 5'b10101, 5'b10110, 5'b11000, 5'b11001, 5'b11010, 5'b11100, 5'b11101, 5'b11110: fsm_next_state <= state_small_pulse;
              5'b00011, 5'b00111, 5'b01111, 5'b11111: fsm_next_state <= state_long_pulse_starts;
              5'b01011, 5'b10011, 5'b10111, 5'b11011: fsm_next_state <= state_long_pulse_starts_small_appears;
              default: fsm_next_state <= 4'bxxxx;
            endcase
          end
        end

      endcase
    end

    // Combinational output logic
    always @(fsm_state, w_serdes_2delay) begin
      r_sampled_data <= 0;
      r_data_tag <= 0;
      r_fsm_clk_cycles_counter_reset <= 0;

      case(fsm_state)
        state_off: begin
          r_fsm_clk_cycles_counter_reset <= 1;
        end

        state_small_pulse: begin
          r_sampled_data <= w_serdes_2delay;
          r_data_tag <= tag_small_pulse;
        end

        state_long_pulse_starts: begin
          r_sampled_data <= w_serdes_2delay;
          r_data_tag <= tag_long_pulse_starts;
        end

        state_long_pulse_starts_small_appears: begin
          r_sampled_data <= w_serdes_2delay;
          r_data_tag <= tag_long_pulse_starts_small_appears;
        end

        state_long_pulse_end: begin
          r_sampled_data <= w_serdes_2delay;
          r_data_tag <= tag_long_pulse_ends;
        end

        state_long_pulse_end_long_starts: begin
          r_sampled_data <= w_serdes_2delay;
          r_data_tag <= tag_long_pulse_ends_long_starts;
        end

        state_long_pulse_end_small_appears: begin
          r_sampled_data <= w_serdes_2delay;
          r_data_tag <= tag_long_pulse_ends_small_appears;
        end

      endcase
    end

  // Clock cycles counter
  wire w_clk_cycles_counter_reset;
  reg [CLK_CYCLE_COUNT_WIDTH-1:0] r_clock_cycles_count; 

  assign w_clk_cycles_counter_reset = (r_fsm_clk_cycles_counter_reset || i_reset); 

  always @(posedge i_clk) begin
      if( w_clk_cycles_counter_reset == 1'b1 ) begin
        r_clock_cycles_count <= 0;
      end else begin
        r_clock_cycles_count <= r_clock_cycles_count + 1;
      end
  end

  /* Delay 2 clock cycles the clock cycles counter. The delayed signal
  * is sampled by the fsm because it take some clock cycles to process the serdes output. */
  reg [CLK_CYCLE_COUNT_WIDTH-1:0] r_clock_cycles_count_1delay;
  reg [CLK_CYCLE_COUNT_WIDTH-1:0] r_clock_cycles_count_2delay;
  reg_n #(.N(CLK_CYCLE_COUNT_WIDTH)) reg0_clock_count (.D(r_clock_cycles_count),.clk(i_clk),.sync_reset(i_reset),.Q(r_clock_cycles_count_1delay));
  reg_n #(.N(CLK_CYCLE_COUNT_WIDTH)) reg1_clock_count (.D(r_clock_cycles_count_1delay),.clk(i_clk),.sync_reset(i_reset),.Q(r_clock_cycles_count_2delay));

  // Build output data register
  wire [CLK_CYCLE_COUNT_WIDTH+4+3-1:0] w_data_reg;
  assign w_data_reg = (r_clock_cycles_count_2delay << 7) | (r_sampled_data << 3) | r_data_tag;

  assign o_data = (r_sampled_data == 0) ? 0 : w_data_reg;

// the "macro" to dump signals
`ifdef COCOTB_SIM
initial begin
  $dumpfile ("iserdese2_4b_pulse_decoder.vcd");
  $dumpvars (0, iserdese2_4b_pulse_decoder);
  #1;
end
`else

  /* ILA instantiation if we are debugging this module with Vivado */
  //`define DEBUG_ISERDESE2_4B_PULSE_DECODER
  `ifdef DEBUG_ISERDESE2_4B_PULSE_DECODER
  ila_0 ila_iserdese2_4b_pulse_decoder (
      .clk(i_clk), // input wire clk
      .probe0(fsm_state),         // reg[3:0]
      .probe1(fsm_next_state),    // reg[3:0]
      .probe2(r_clock_cycles_count), // reg[12:0]
      .probe3(r_clock_cycles_count_1delay), //reg[12:0]
      .probe4(r_clock_cycles_count_2delay), //reg[12:0]
      .probe5(r_sampled_data), //reg[3:0]
      .probe6(r_data_tag),     //reg[2:0]
      .probe7(o_data),            // reg[19:0]
      .probe8(w_clk_cycles_counter_reset), //1bit
      .probe9(r_fsm_clk_cycles_counter_reset), //1bit
      .probe10(i_serdes_out) // reg[3:0]
  );
  `endif

`endif

endmodule