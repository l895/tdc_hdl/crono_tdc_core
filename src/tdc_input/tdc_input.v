/*
 * Verilog code of a fake TDC input using a simulated ISERDESE2 in oversample
 * mode and a fsm that process the output of this ISERDESE2 to tag the events
 * where pulses are starting or ending.
 * This TDC input have a RAM to store the data too. Each valid input will be stored
 * in this RAM and then we will retrieve the data from this RAM reading it.
 *
 * Author: Julian Rodriguez <jnrodriguez@estudiantes.unsam.edu.ar>
 */

`timescale 1ps/1ps

module tdc_input(i_data,o_data,i_clk,i_serdes_clk,i_serdes_clkb,i_serdes_oclk,i_serdes_oclkb,i_sync_reset,i_read_addr,i_ena,o_lwa,o_dready,o_mem_full);
    parameter CLK_CYCLE_COUNT_WIDTH = 13;
    parameter RAM_ADDR_WIDTH = 14;
    parameter RAM_SIZE = 8192;

    input           i_data;          // input
    input           i_clk;          // general clk
    input           i_serdes_clk;   // serdes clk
    input           i_serdes_clkb;  // serdes clk 180 
    input           i_serdes_oclk;  // serdes clk 90
    input           i_serdes_oclkb; // serdes clk 270
    input           i_sync_reset;
    input           i_ena;    // if active, stores data from input into RAM
    input [RAM_ADDR_WIDTH-1:0] i_read_addr;   // address to read from the RAM
    output o_dready;      // data ready signal
    output o_mem_full;    // set when the RAM is full
    output [RAM_ADDR_WIDTH-1:0]             o_lwa;    // last written address 
    output [CLK_CYCLE_COUNT_WIDTH+3+4-1:0]  o_data; // output

    // serdes signals
    wire [3:0] w_serdes_out;

    // iserdes fsm signals
    wire [CLK_CYCLE_COUNT_WIDTH+3+4-1:0] w_pulse_decoder_data;

    // RAM signals
    reg [RAM_ADDR_WIDTH-1:0] addr;
    reg write_enable;
    wire w_ram_enable;

    iserdese2_4b_s7_ov iserdese2(
        .i_data         (i_data),
        .o_data         (w_serdes_out),
        .i_clk          (i_serdes_clk),
        .i_clkb         (i_serdes_clkb),
        .i_oclk         (i_serdes_oclk),
        .i_oclkb        (i_serdes_oclkb),
        .i_reset        (i_sync_reset) 
    );

    /* Enable signal rising/falling edge detectors and delayer. */
    wire [1:0] w_ena_delayed;
    ffd ffd_ena0 (.clk(i_clk),.sync_reset(i_sync_reset),.D(i_ena),.Q(w_ena_delayed[0]));
    ffd ffd_ena1 (.clk(i_clk),.sync_reset(i_sync_reset),.D(w_ena_delayed[0]),.Q(w_ena_delayed[1]));

    // enable signal rising edge detector
    wire w_ena_red;
    assign w_ena_red = w_ena_delayed[0] & (~w_ena_delayed[1]);
    // enable signal falling edge detector
    wire w_ena_fed;
    // falling edge detected signal delay-chain
    wire [4:0] w_ena_fed_delayed;
    assign w_ena_fed = w_ena_delayed[1] & (~w_ena_delayed[0]);
    ffd ffd_fed0 (.clk(i_clk),.sync_reset(i_sync_reset),.D(w_ena_fed),.Q(w_ena_fed_delayed[0]));
    ffd ffd_fed1 (.clk(i_clk),.sync_reset(i_sync_reset),.D(w_ena_fed_delayed[0]),.Q(w_ena_fed_delayed[1]));
    ffd ffd_fed2 (.clk(i_clk),.sync_reset(i_sync_reset),.D(w_ena_fed_delayed[1]),.Q(w_ena_fed_delayed[2]));
    ffd ffd_fed3 (.clk(i_clk),.sync_reset(i_sync_reset),.D(w_ena_fed_delayed[2]),.Q(w_ena_fed_delayed[3]));
    ffd ffd_fed4 (.clk(i_clk),.sync_reset(i_sync_reset),.D(w_ena_fed_delayed[3]),.Q(w_ena_fed_delayed[4]));

    /* Instantiate ISERDESE2 4bit pulse decoder. This 
     * pulse decoder is driven by a start and stop signal. */
    iserdese2_4b_pulse_decoder #(
        .CLK_CYCLE_COUNT_WIDTH(CLK_CYCLE_COUNT_WIDTH)
    )pulse_decoder(
        .i_serdes_out     (w_serdes_out),
        .i_clk            (i_clk),
        .i_reset          (i_sync_reset),
        .o_data           (w_pulse_decoder_data),
        .i_start          (w_ena_red),
        .i_stop           (w_ena_fed_delayed[2])
    );

    /* Instantiate TDC input RAM. */
    tdc_input_ram #(.SIZE(RAM_SIZE),
        .ADDR_WIDTH(RAM_ADDR_WIDTH),
        .COL_WIDTH(CLK_CYCLE_COUNT_WIDTH+3+4)) ram(
        .i_clk            (i_clk),
        .i_read_addr      (i_read_addr),
        .i_reset          (i_sync_reset),
        .i_ena            (w_ram_enable),
        .i_data           (w_pulse_decoder_data),
        .o_data           (o_data),
        .o_lwa            (o_lwa),
        .o_mem_full       (o_mem_full)
    );

    /*
     * The iserdes4b pusle decoder and RAM controller fsm used in this core
     * have sequential logic that delays the system. This delay is characterized
     * and is used to set a 'data ready signal' that could be useful
     * to inform to the external logic when all the input data that appears
     * when the enable signal is active is fully processed.
     * So, the data_ready signal will be low when the RAM is enabled and high
     * when RAM is not enabled.
     *            _________________________________________
     * enable: __|                                         |___________
     *                                                     <------>    
     *                                                  system delay   
     *                                                             ____
     * dready: xxx________________________________________________|    
     */
    assign w_ram_enable = i_ena | w_ena_delayed[0] | w_ena_fed | w_ena_fed_delayed[0] | w_ena_fed_delayed[1] | w_ena_fed_delayed[2] | w_ena_fed_delayed[3];
    assign o_dready = ~(w_ram_enable | w_ena_fed_delayed[4]);

// the "macro" to dump signals
`ifdef COCOTB_SIM
initial begin
  $dumpfile ("tdc_input.vcd");
  $dumpvars (0, tdc_input);
  #1;
end
`else

    /* ILA instantiation if we are debugging this module with Vivado */
   // `define DEBUG_TDC_INPUT
    `ifdef DEBUG_TDC_INPUT
    ila_tdc_input ila_tdc_input (
        .clk(i_clk), // input wire clk
        .probe0(w_serdes_out), // reg[3:0]
        .probe1(i_data), // 1bit
        .probe2(i_ena), // 1bit
        .probe3(w_pulse_decoder_data), // reg[19:0]
        .probe4(o_data),    // reg[19:0]
        .probe5(w_ram_enable), // 1bit
        .probe6(o_dready), // 1bit
        .probe7(o_lwa) // reg[13:0]
    );
    `endif

`endif

endmodule 