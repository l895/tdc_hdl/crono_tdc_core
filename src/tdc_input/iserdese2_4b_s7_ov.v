/*
 * Verilog code of ISERDESE2 instantiation in oversample mode used in Crono TDC.
 * Description:
 * This module models the ISERDESE2 IP Core in oversample mode provided
 * by Xilinx. In this code we are making the instantion of this IP core
 * to be used in oversample mode.
 * 
 * This core samples a signal four times during a clock period using 4
 * clocks with a phase shift between them:
 * CLK = 0º
 * CLKB = 180º
 * OCLK = 90º
 * OCLKB = 270º
 * 
 * Then, after feeding the ISERDESE2 with a nibble, the core sets this nibble 
 * in the output in a parallel form after two clock cycles. 
 *
 * This ISERDESE2 is fixed to 1:4.
 *
 * Note: See ISERDESE2 in oversample mode in UG471 "7 Series FPGAs 
 * SelectIO Resources".
 * This core maybe could not be simulated with open source tools like icarus
 * because the IP core is propietary of Xilinx.
 *
 * See the following post talking about ISERDESE2 output order in Xilinx IP:
 * https://support.xilinx.com/s/question/0D52E00006iHrT2SAK/7series-iserdes-oversample-bit-order-output?language=en_US
 * 
 * Author: Julian Rodriguez <jnrodriguez@estudiantes.unsam.edu.ar>
 */

`timescale 1ps/1ps

module iserdese2_4b_s7_ov(
        input                       i_data,
        output [3:0]                o_data,
        input                       i_clk,   // clk
        input                       i_oclk,  // clk 90
        input                       i_clkb,  // clk 180
        input                       i_oclkb, // clk 270
        input                       i_reset
    );

ISERDESE2 #(
   .DATA_RATE("SDR"),           // DDR, SDR
   .DATA_WIDTH(4),              // Parallel data width (2-8,10,14)
   .DYN_CLKDIV_INV_EN("FALSE"), // Enable DYNCLKDIVINVSEL inversion (FALSE, TRUE)
   .DYN_CLK_INV_EN("FALSE"),    // Enable DYNCLKINVSEL inversion (FALSE, TRUE)
   // INIT_Q1 - INIT_Q4: Initial value on the Q outputs (0/1)
   .INIT_Q1(1'b0),
   .INIT_Q2(1'b0),
   .INIT_Q3(1'b0),
   .INIT_Q4(1'b0),
   .INTERFACE_TYPE("OVERSAMPLE"),   // MEMORY, MEMORY_DDR3, MEMORY_QDR, NETWORKING, OVERSAMPLE
   .IOBDELAY("NONE"),               // NONE, BOTH, IBUF, IFD
   .NUM_CE(1),                      // Number of clock enables (1,2)
   .OFB_USED("FALSE"),              // Select OFB path (FALSE, TRUE)
   .SERDES_MODE("MASTER"),          // MASTER, SLAVE
   // SRVAL_Q1 - SRVAL_Q4: Q output values when SR is used (0/1)
   .SRVAL_Q1(1'b0),
   .SRVAL_Q2(1'b0),
   .SRVAL_Q3(1'b0),
   .SRVAL_Q4(1'b0)
)
ISERDESE2_inst (
   // Combinatorial output: 1-bit output: Combinatorial output, is a pass-through of input data
   // We are not using it here.
   .O(),

   // Q1 - Q4: 1-bit (each) output: Registered data outputs
   .Q1(o_data[3]),
   .Q2(o_data[1]),
   .Q3(o_data[2]),
   .Q4(o_data[0]),
   .Q5(),
   .Q6(),
   .Q7(),
   .Q8(),
   
   // SHIFTOUT1, SHIFTOUT2: 1-bit (each) output: Data width expansion output ports
   .SHIFTOUT1(),
   .SHIFTOUT2(),

   // 1-bit input: The BITSLIP pin performs a Bitslip operation synchronous to
   // CLKDIV when asserted (active High). Subsequently, the data seen on the Q1
   // to Q8 output ports will shift, as in a barrel-shifter operation, one
   // position every time Bitslip is invoked (DDR operation is different from
   // SDR).
   // We will not use this.   
   .BITSLIP(1'b0),          

   // CE1, CE2: 1-bit (each) input: Data register clock enable inputs
   // We are using only one clock enable and setting it high always.
   .CE1(1'b1),
   .CE2(1'b1),
   .CLKDIVP(1'b0),   // 1-bit input: Only supported in MIG tool, we are not using it, so we connect it to gnd.

   // Clocks: 1-bit (each) input: ISERDESE2 clock input ports
   .CLK(i_clk),      // 1-bit input: input clock.
   .OCLK(i_oclk),    // 1-bit input: input clock delayed 90º.
   .CLKB(i_clkb),    // 1-bit input: input clock delayed 180º.
   .OCLKB(i_oclkb),  // 1-bit input: input clock delayed 270º.
   .CLKDIV(1'b0), // 1-bit input: Divided clock. We are not using it here.

   // Dynamic Clock Inversions: 1-bit (each) input: Dynamic clock inversion pins to switch clock polarity
   // We are not using this here.
   .DYNCLKDIVSEL(1'b0), // 1-bit input: Dynamic CLKDIV inversion
   .DYNCLKSEL(1'b0),       // 1-bit input: Dynamic CLK/CLKB inversion

   // Input Data: 1-bit (each) input: ISERDESE2 data input ports
   .D(i_data),          // 1-bit input: Data input
   .DDLY(1'b0),         // 1-bit input: Serial data from IDELAYE2. We are not using it here.
   .OFB(1'b0),          // 1-bit input: Data feedback from OSERDESE2. We are not using it here.
   .RST(i_reset),                   // 1-bit input: Active high asynchronous reset

   // SHIFTIN1, SHIFTIN2: 1-bit (each) input: Data width expansion input ports, not used here
   .SHIFTIN1(1'b0),
   .SHIFTIN2(1'b0)
);

// End of ISERDESE2_inst instantiation

// the "macro" to dump signals
`ifdef COCOTB_SIM
initial begin
  $dumpfile ("iserdese2_s7_ov.vcd");
  $dumpvars (0, iserdese2_s7_ov);
  #1;
end

`else

  /* ILA instantiation if we are debugging this module with Vivado */
  //`define DEBUG_ISERDESE2_4B_S7_OV
  `ifdef DEBUG_ISERDESE2_4B_S7_OV
  ila_iserdese2_4b_s7_ov ila_iserdese2_4b_s7_ov (
      .clk(i_clk), // input wire clk
      .probe0(i_data),         // 1bit
      .probe1(o_data)    // reg[3:0]
  );
  `endif

`endif

endmodule 