/*
 * Verilog code of the RAM used in TDC inputs in Crono TDC.
 * This module stores input data into RAM when it's valid 
 * and if ENABLE signal is active.
 * As an output, the module informs what it was the last written address,
 * with this information, an external logic can know the qty of 
 * data registers stored in TDC RAM.
 *
 * In write mode (`ena` == 1), the memory will be filled until is full.
 * When the memory is full `mem_full` flag will be set and no more data
 * will be stored.
 * 
 * Author: Julian Rodriguez <jnrodriguez@estudiantes.unsam.edu.ar>
 */


module tdc_input_ram(i_clk, i_read_addr, i_reset, i_ena, i_data, o_data, o_lwa, o_mem_full);
    parameter SIZE = 8192;      // RAM: Quantity of rows
    parameter ADDR_WIDTH = 10;  // RAM: Width of the address register
    parameter COL_WIDTH = 6;    // RAM: Width of each column

    input i_clk;
    input [ADDR_WIDTH-1:0] i_read_addr;   // address to read
    input i_reset;
    input i_ena;                      // if active, stores data from input in RAM
    input [COL_WIDTH-1:0] i_data; // data to write into the RAM if enable is active
    output [COL_WIDTH-1:0] o_data; // data to read from RAM
    output reg [ADDR_WIDTH-1:0] o_lwa;    // last written address 
    output reg o_mem_full;    // flag set when the memory is full

    integer r_write_address_counter;

    // Register input data
    wire [COL_WIDTH-1:0] w_data_registered;
    reg_n #(.N(COL_WIDTH)) reg_data  ( .D(i_data), .clk(i_clk), .sync_reset(i_reset),  .Q(w_data_registered));

    // Rising edge detector in enable signal
    wire [1:0] w_ena;
    ffd ffd_red0  ( .D(i_ena), .clk(i_clk), .sync_reset(i_reset),  .Q(w_ena[0]));
    ffd ffd_red1  ( .D(w_ena[0]), .clk(i_clk), .sync_reset(i_reset),  .Q(w_ena[1]));

    wire w_ena_red;
    assign w_ena_red = w_ena[0] & (~w_ena[1]);

    // Input data validator
    wire w_valid_input_data;
    assign w_valid_input_data = ((w_data_registered & 7'h78) != 0) ? 1 : 0;
    
    wire w_ram_write_enable = (o_mem_full == 0) ? w_valid_input_data : 0;

    // Select ram address
    wire [ADDR_WIDTH-1:0] w_ram_address;
    assign w_ram_address = ( (i_ena | w_ena[0]) == 1 ) ? r_write_address_counter : i_read_addr;

    // Instantiate the RAM
    ram_read_first #(
            .SIZE(SIZE),
            .ADDR_WIDTH(ADDR_WIDTH),
            .COL_WIDTH(COL_WIDTH),
            .NB_COL(1)      // the RAM will have only 1 column
        ) ram (
        .clk            (i_clk),
        .we             (w_ram_write_enable),
        .addr           (w_ram_address),
        .di             (w_data_registered),    // the input of the TDC_INPUT_RAM is the RAM input per se
        .dout           (o_data)     // the output of the TDC_INPUT_RAM is the RAM output per se
    );

    // Write address counter
    always @(posedge i_clk) begin
        if ((i_reset == 1) || (w_ena_red == 1)) begin
            r_write_address_counter <= 0;
        end else if (w_valid_input_data == 1) begin
            if( r_write_address_counter < SIZE-1 ) begin
                r_write_address_counter <= r_write_address_counter + 1;
            end
        end
    end

    always @(r_write_address_counter) begin
        if(r_write_address_counter < SIZE-1) begin
            o_mem_full <= 0;
        end else begin
            o_mem_full <= 1;
        end
    end

    always @(r_write_address_counter, o_mem_full) begin
        case(o_mem_full)
            0: begin
                o_lwa = (r_write_address_counter > 0) ? r_write_address_counter-1: 0;
            end

            1: begin
                o_lwa = r_write_address_counter;
            end

            default: begin
                o_lwa = 1'bx;
            end

        endcase
    end

// the "macro" to dump signals
`ifdef COCOTB_SIM
initial begin
  $dumpfile ("tdc_input_ram.vcd");
  $dumpvars (0, tdc_input_ram);
  #1;
end
`endif

endmodule
