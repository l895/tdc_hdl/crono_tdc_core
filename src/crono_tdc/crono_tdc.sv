/*
 * Time to digital converter called Crono TDC.
 * 
 * This module is the top-level of the Crono TDC that can measure high speed
 * signals becase is using ISERDESE2 as input stages.
 * This module do not include the clock generation. 
 *
 * Author: Julian Rodriguez <jnrodriguez@estudiantes.unsam.edu.ar>
 */


module crono_tdc(i_reset,i_clk,i_serdes_clk,i_serdes_oclk,i_serdes_clkb,i_serdes_oclkb,i_rx_serial,o_tx_serial,i_channels_inputs,o_measure_ongoing,o_flags);
import tdc_commands_pkg::*; 
  /* HDLC-lite parameters. */
  parameter HDLC_LITE_TX_FRAME_MAX_WIDTH_BYTES = 32;
  /* TDC inputs parameters. */
  parameter TDC_CHANNEL_QTY = 3;
  parameter MAX_TDC_CHANNELS_QTY = 32;
  parameter TDC_CHANNEL_RAM_ADDR_WIDTH = 14;
  parameter TDC_CHANNEL_CLK_CYCLE_COUNT_WIDTH = 10;
  parameter TDC_CHANNEL_RAM_SIZE = 2048;
  /* TDC Config registers parameters. */
  parameter CFG_REG_INIT_VAL_TIME_WINDOWS_US = 1000000;
  parameter CFG_REG_INIT_VAL_ENABLED_CHN = {TDC_CHANNEL_QTY{1'b1}}; // all channels are enabled by default
  /* UART parameters. */
  parameter UART_BAUDRATE = 115200;
  parameter SYSTEM_CLOCK_FREQUENCY_HZ = 100000000;

  /* Useful local parameters (just to not use magic numbers). */
  localparam TDC_CHANNEL_DATA_REG_WIDTH = TDC_CHANNEL_CLK_CYCLE_COUNT_WIDTH+4+3;
  // do not count frame delimiters, so rest -2 bytes
  localparam HDLC_LITE_TX_FRAME_DATA_AND_CRC_ESCAPED_MAX_WIDTH_BYTES      = HDLC_LITE_TX_FRAME_MAX_WIDTH_BYTES-2;
  // do not count crc, so only rest -2 bytes
  localparam HDLC_LITE_TX_FRAME_DATA_ESCAPED_MAX_WIDTH_BYTES              = HDLC_LITE_TX_FRAME_DATA_AND_CRC_ESCAPED_MAX_WIDTH_BYTES-2;
  // because every char could be escaped, we accepta data's buffer 2 times smaller than the whole frame (data buffers to send) 
  localparam HDLC_LITE_TX_FRAME_DATA_AND_CRC_UNESCAPED_MAX_WIDTH_BYTES    = HDLC_LITE_TX_FRAME_DATA_AND_CRC_ESCAPED_MAX_WIDTH_BYTES/2;
  // and we accept as input data two bytes less, because in the final buffer we must to store the crc too
  localparam HDLC_LITE_TX_INPUT_DATA_ACCEPTED                             = HDLC_LITE_TX_FRAME_DATA_AND_CRC_UNESCAPED_MAX_WIDTH_BYTES-2;
  // compute the quantity of muxes used to handle tdc inputs
  localparam MUXES_QTY  = (TDC_CHANNEL_QTY / 4) + ((TDC_CHANNEL_QTY % 4) != 0);
  // Width (in bytes) of the biggest that we expect from an incoming frame
  localparam HDLC_LITE_RX_FRAME_DATA_MAX_WIDTH_BYTES = TDC_CMD_DATA_WIDTH_BYTES+TDC_CMD_ID_WIDTH_BYTES;
  // Width (in bytes) of the biggest frame received. Is adjusted to commands width. Here we
  // include the CRC and double the quantity of bits because they could come escaped. And then we add the frame delimiters.
  localparam HDLC_LITE_RX_FRAME_MAX_WIDTH_BYTES = (HDLC_LITE_RX_FRAME_DATA_MAX_WIDTH_BYTES+2)*2+2;

  localparam TDC_FLAGS_QTY = 3;

  input i_reset;      // synchronous reset (active high)
  input i_clk;          // input clock
  input i_serdes_clk;   // serdes input clock
  input i_serdes_oclk;  // serdes input clock with a 90º phase-shift
  input i_serdes_clkb;  // serdes input clock with a 180º phase-shift
  input i_serdes_oclkb; // serdes input clock with a 270º phase-shift
  input i_rx_serial;  // UART input serial port
  output o_tx_serial; // UART output serial port
  input [TDC_CHANNEL_QTY-1:0] i_channels_inputs; // TDC inputs
  output o_measure_ongoing; // This signal will be '1' when a measure is ongoing
  output [TDC_FLAGS_QTY-1:0] o_flags;

  /* Instantiate TDC core logic and create wires to connect to another modules. */
  wire [HDLC_LITE_RX_FRAME_DATA_MAX_WIDTH_BYTES*8-1:0] w_core_logic_rx_data;
  wire [$clog2(HDLC_LITE_RX_FRAME_DATA_MAX_WIDTH_BYTES*8):0] w_core_logic_rx_dv;
  wire [HDLC_LITE_TX_INPUT_DATA_ACCEPTED*8-1:0] w_core_logic_tx_data;
  wire [$clog2(HDLC_LITE_TX_INPUT_DATA_ACCEPTED*8):0] w_core_logic_tx_data_valid;
  wire w_core_logic_tx_data_done, w_core_logic_executing_command;
  wire [TDC_CHANNEL_QTY-1:0] w_core_logic_chn_dready;
  wire [TDC_CHANNEL_QTY-1:0] w_core_logic_chn_ena;
  wire [TDC_CHANNEL_QTY-1:0] w_core_logic_read_chn_sel;
  wire [TDC_CHANNEL_RAM_ADDR_WIDTH-1:0] w_core_logic_read_chn_lwa;
  wire [TDC_CHANNEL_RAM_ADDR_WIDTH-1:0] w_core_logic_read_chn_addr;
  wire [TDC_CHANNEL_DATA_REG_WIDTH-1:0] w_core_logic_read_chn_data;

  tdc_core_logic #(
    .TX_DATA_WIDTH_BYTES(HDLC_LITE_TX_INPUT_DATA_ACCEPTED),
    .TDC_CHANNEL_QTY(TDC_CHANNEL_QTY),
    .TDC_CHANNEL_RAM_ADDR_WIDTH(TDC_CHANNEL_RAM_ADDR_WIDTH),
    .TDC_CHANNEL_DATA_REG_WIDTH(TDC_CHANNEL_DATA_REG_WIDTH),
    .CFG_REG_INIT_VAL_TIME_WINDOWS_US(CFG_REG_INIT_VAL_TIME_WINDOWS_US),
    .CFG_REG_INIT_VAL_ENABLED_CHN(CFG_REG_INIT_VAL_ENABLED_CHN),
    .CLOCK_FREQUENCY_HZ(SYSTEM_CLOCK_FREQUENCY_HZ)
  ) core_logic (
    .i_clk            (i_clk),
    .i_reset          (i_reset),
    .i_rx_data        (w_core_logic_rx_data), // TODO: check if we can fix this magic number
    .i_rx_dv          (w_core_logic_rx_dv),

    .o_tx_data        (w_core_logic_tx_data),
    .o_tx_data_valid  (w_core_logic_tx_data_valid),
    .i_tx_data_done   (w_core_logic_tx_data_done),

    .i_chn_dready         (w_core_logic_chn_dready),
    .o_chn_ena            (w_core_logic_chn_ena),
    .o_read_chn_sel_mask  (w_core_logic_read_chn_sel),
    .i_read_chn_lwa       (w_core_logic_read_chn_lwa),
    .o_read_chn_addr      (w_core_logic_read_chn_addr),
    .i_read_chn_data      (w_core_logic_read_chn_data),

    .o_measure_ongoing    (o_measure_ongoing),
    .o_executing_command  (w_core_logic_executing_command)
  );

  /* Instantiate HDLC-lite processor (RX and TX) and create wires to 
   * connect it to another modules. */
  wire [7:0] w_hdlc_lite_rx_byte;
  wire [7:0] w_hdlc_lite_tx_byte;
  wire w_hdlc_lite_rx_dv, w_hdlc_lite_tx_done, w_hdlc_lite_tx_dv, w_hdlc_lite_rx_correct_crc, w_hdlc_lite_tx_active;

  hdlc_lite #(
    .HDLC_LITE_TX_FRAME_MAX_WIDTH_BYTES(HDLC_LITE_TX_FRAME_MAX_WIDTH_BYTES),
    .HDLC_LITE_RX_FRAME_MAX_WIDTH_BYTES(HDLC_LITE_RX_FRAME_MAX_WIDTH_BYTES),
    .HDLC_LITE_FRAME_DELIMITER_CHAR(8'h7E),
    .HDLC_LITE_ESCAPE_CHAR(8'h7D)
  ) hdlc_lite_processor (
    .i_clk              (i_clk),
    .i_reset            (i_reset),

    .i_rx_byte          (w_hdlc_lite_rx_byte),
    .i_rx_dv            (w_hdlc_lite_rx_dv),

    .o_rx_correct_crc   (w_hdlc_lite_rx_correct_crc), // this port is leaved unconnected, we are not doing anything right now if CRC is not correct
    .o_rx_data          (w_core_logic_rx_data),
    .o_rx_dv            (w_core_logic_rx_dv),

    .i_tx_data          (w_core_logic_tx_data),
    .o_tx_active        (w_hdlc_lite_tx_active), // this port is leaved unconnected
    .o_tx_done          (w_core_logic_tx_data_done),
    .i_tx_dv            (w_core_logic_tx_data_valid),

    .o_tx_byte          (w_hdlc_lite_tx_byte),
    .i_tx_done          (w_hdlc_lite_tx_done),
    .o_tx_dv            (w_hdlc_lite_tx_dv)
  );

  /* Instantiate UART and create wires to conect it to another modules. */
  uart #(
    .BAUDRATE(UART_BAUDRATE),
    .CLOCK_FREQ_HZ(SYSTEM_CLOCK_FREQUENCY_HZ) 
    ) uart_comm(
    .i_clk        (i_clk),
    .i_rx_serial  (i_rx_serial),
    .o_rx_dv      (w_hdlc_lite_rx_dv),
    .o_rx_byte    (w_hdlc_lite_rx_byte),
    .i_tx_dv      (w_hdlc_lite_tx_dv),
    .i_tx_byte    (w_hdlc_lite_tx_byte),
    .o_tx_active  (),
    .o_tx_done    (w_hdlc_lite_tx_done),
    .o_tx_serial  (o_tx_serial),
    .i_reset      (i_reset)
  );


  /* We well need some muxes to mux the RAM addresses of the tdc inputs.
   * We are using 4:1 muxes here to do the trick. So, we need at least
   * TDC_CHANNEL_QTY/4 muxes here (rounding up). We will end with
   * multiple muxes[j].select wires. The length of this wires is 2 bit.
   * We can concat this selects in one bigger select to transform this
   * array of muxes into a big mux!
   */
  generate
    genvar j;
    for(j=TDC_CHANNEL_QTY/4; j>=0; j = j - 1) begin: mux
      wire [TDC_CHANNEL_RAM_ADDR_WIDTH-1:0] in_data0_chn_lwa;
      wire [TDC_CHANNEL_RAM_ADDR_WIDTH-1:0] in_data1_chn_lwa;
      wire [TDC_CHANNEL_RAM_ADDR_WIDTH-1:0] in_data2_chn_lwa;
      wire [TDC_CHANNEL_RAM_ADDR_WIDTH-1:0] in_data3_chn_lwa;
      wire [TDC_CHANNEL_RAM_ADDR_WIDTH-1:0] out_data_chn_lwa;
      wire [3:0] select;

      one_hot_mux_41 #(
        .INPUT_WIDTH(TDC_CHANNEL_RAM_ADDR_WIDTH)
      ) chn_lwa (
        .i_data0      (mux[j].in_data0_chn_lwa),
        .i_data1      (mux[j].in_data1_chn_lwa),
        .i_data2      (mux[j].in_data2_chn_lwa),
        .i_data3      (mux[j].in_data3_chn_lwa),
        .o_data       (mux[j].out_data_chn_lwa),
        .i_sel        (mux[j].select)
      );

      wire [TDC_CHANNEL_DATA_REG_WIDTH-1:0] in_data0_chn_out_data;
      wire [TDC_CHANNEL_DATA_REG_WIDTH-1:0] in_data1_chn_out_data;
      wire [TDC_CHANNEL_DATA_REG_WIDTH-1:0] in_data2_chn_out_data;
      wire [TDC_CHANNEL_DATA_REG_WIDTH-1:0] in_data3_chn_out_data;
      wire [TDC_CHANNEL_DATA_REG_WIDTH-1:0] out_data_chn_out_data;

      one_hot_mux_41 #(
        .INPUT_WIDTH(TDC_CHANNEL_DATA_REG_WIDTH)
      ) chn_out_data (
        .i_data0      (mux[j].in_data0_chn_out_data),
        .i_data1      (mux[j].in_data1_chn_out_data),
        .i_data2      (mux[j].in_data2_chn_out_data),
        .i_data3      (mux[j].in_data3_chn_out_data),
        .o_data       (mux[j].out_data_chn_out_data),
        .i_sel        (mux[j].select)
      );

      assign w_core_logic_read_chn_data = mux[j].out_data_chn_out_data;
      assign w_core_logic_read_chn_lwa = mux[j].out_data_chn_lwa;
    end
  endgenerate

  /* Instantiate TDC inputs (channels).
   * Here we will create TDC_CHANNEL_QTY `tdc_inputs`.
   * Also, we will have ceil(TDC_CHANNEL_QTY/4) muxes for 
   * last_written_address ports of tdc_inputs and round(TDC_CHANNEL_QTY/4) muxes for read_address ports
   * of tdc_inputs. We will connect the muxes to the tdc_inputs here too.
   */
  generate
    genvar i;
    for(i = 0; i < TDC_CHANNEL_QTY; i = i + 1) begin: channel
        wire [TDC_CHANNEL_RAM_ADDR_WIDTH-1:0] w_lwa;
        wire [TDC_CHANNEL_RAM_ADDR_WIDTH-1:0] w_read_addr;
        wire [TDC_CHANNEL_DATA_REG_WIDTH-1:0] w_output_data;

        tdc_input #(
          .CLK_CYCLE_COUNT_WIDTH(TDC_CHANNEL_CLK_CYCLE_COUNT_WIDTH),
          .RAM_ADDR_WIDTH(TDC_CHANNEL_RAM_ADDR_WIDTH),
          .RAM_SIZE(TDC_CHANNEL_RAM_SIZE)
        ) tdc_input (
          .i_data       (i_channels_inputs[i]),
          .i_clk        (i_clk),
          .i_serdes_clk   (i_serdes_clk),
          .i_serdes_clkb  (i_serdes_clkb),
          .i_serdes_oclk  (i_serdes_oclk),
          .i_serdes_oclkb (i_serdes_oclkb),
          .i_sync_reset (i_reset),
          .i_ena        (w_core_logic_chn_ena[i]),
          .i_read_addr  (channel[i].w_read_addr),
          .o_lwa        (channel[i].w_lwa),
          .o_data       (channel[i].w_output_data),
          .o_dready     (w_core_logic_chn_dready[i]),
          .o_mem_full   ()
        );
    end
  endgenerate

  /* Make the connections between the channels, the muxes and the core logic. */
  generate
    genvar k;
    for(k = 0; k < TDC_CHANNEL_QTY; k = k + 1) begin
      assign channel[k].w_read_addr = w_core_logic_read_chn_addr;

      /* Connect w_lwa and w_read_addr of the channel to the correct mux input.
       * i/4 will give the number of the mux that must be used:
       * channel 0 = floor(0/4) = 0 => mux 0
       * channel 1 = floor(1/4) = 0 => mux 0
       * channel 2 = floor(2/4) = 0 => mux 0
       * channel 3 = floor(3/4) = 0 => mux 0
       * channel 4 = floor(4/4) = 1 => mux 1
       * channel 5 = floor(5/4) = 1 => mux 1
       * etc ...
       * And modulo 4 will give the correct input:
       * channel 0 = 0 % 4 = 0 = mux[0].lwa.i_data0
       * channel 1 = 1 % 4 = 1 = mux[0].lwa.i_data1
       * channel 2 = 2 % 4 = 2 = mux[0].lwa.i_data2
       * channel 3 = 3 % 4 = 3 = mux[0].lwa.i_data3
       * channel 4 = 4 % 4 = 0 = mux[1].lwa.i_data0  # start over 0
       * channel 5 = 5 % 4 = 1 = mux[1].lwa.i_data1
      */
      if(k % 4 == 0) begin
        assign mux[k/4].in_data0_chn_lwa = channel[k].w_lwa;
        assign mux[k/4].in_data0_chn_out_data = channel[k].w_output_data;
      end else if(k % 4 == 1) begin
        assign mux[k/4].in_data1_chn_lwa = channel[k].w_lwa;
        assign mux[k/4].in_data1_chn_out_data = channel[k].w_output_data;
      end else if(k % 4 == 2) begin
        assign mux[k/4].in_data2_chn_lwa = channel[k].w_lwa;
        assign mux[k/4].in_data2_chn_out_data = channel[k].w_output_data;
      end else if(k % 4 == 3) begin
        assign mux[k/4].in_data3_chn_lwa = channel[k].w_lwa;
        assign mux[k/4].in_data3_chn_out_data = channel[k].w_output_data;
      end
    end
    
    /* Here we build the select signal.
     * TDC core logic have an output that selects the channel that will be read. This 
     * output is in one-hot encoding. For example, with 6 inputs:
     * w_core_logic_read_chn_sel = b000001 -> channel 0 selected
     * w_core_logic_read_chn_sel = b000010 -> channel 1 selected
     * w_core_logic_read_chn_sel = b000100 -> channel 2 selected
     * w_core_logic_read_chn_sel = b001000 -> channel 3 selected
     * w_core_logic_read_chn_sel = b010000 -> channel 4 selected
     * w_core_logic_read_chn_sel = b100000 -> channel 5 selected
     * 
     * The muxes have a select signal of 4 bits and we can have up to N muxes.
     * The select signal of this muxes is also one-hot. So, if we concat all of this signals
     * into a big select signal we will have a 1 to 1 connection between tdc_core_logic
     * output port and the selects of the muxes. For example, with 3 muxes:
     * assign w_core_logic_read_chn_sel = {muxes[2].select, muxes[1].select, muxes[0].select}
     */
    genvar n;
    for(n=0; n<MUXES_QTY*4; n = n +1) begin
      if(n < TDC_CHANNEL_QTY) 
        assign mux[n/4].select[n%4] = w_core_logic_read_chn_sel[n];
      else 
        assign mux[n/4].select[n%4] = 0;
      // TDC_CHANNEL_QTY = 6
      // n = 0: assign mux[0].select[0] = w_core_logic_read_chn_sel[0];
      // n = 1: assign mux[0].select[1] = w_core_logic_read_chn_sel[1];
      // n = 2: assign mux[0].select[2] = w_core_logic_read_chn_sel[2];
      // n = 3: assign mux[0].select[3] = w_core_logic_read_chn_sel[3];
      // n = 4: assign mux[1].select[0] = w_core_logic_read_chn_sel[4];
      // n = 5: assign mux[1].select[1] = w_core_logic_read_chn_sel[5];
      // n = 6: assign mux[1].select[2] = 0
      // n = 7: assign mux[1].select[3] = 0
    end
  endgenerate

  /* Instantiate alert system. */
  wire [TDC_FLAGS_QTY-1:0] w_alert_flags_hits;
  assign w_alert_flags_hits[0] = w_hdlc_lite_rx_correct_crc;  // flag set high when hdlc receptor decodes a correct crc
  assign w_alert_flags_hits[1] = w_hdlc_lite_tx_active; // flag set high when hdlc transmitter is sending things
  assign w_alert_flags_hits[2] = w_core_logic_executing_command; // flag set high when tdc will execute a command

  alert_flags #(
    .FLAGS_QTY(TDC_FLAGS_QTY),
    .FLAGS_SET_HIGH_TIME_US(1000000),
    .SYSTEM_CLOCK_FREQUENCY_HZ(SYSTEM_CLOCK_FREQUENCY_HZ)
  ) alert_flags (
    .i_reset  (i_reset),
    .i_clk  (i_clk),
    .i_hit  (w_alert_flags_hits),
    .o_flag (o_flags)
  );

/* ILA instantiation if we are debugging this module with Vivado */
//`ifdef DEBUG_CRONO_TDC_LF
//ila_1 ila_debug (
//    .clk(i_clk), // input wire clk
//);
//`endif

// the "macro" to dump signals
`ifdef COCOTB_SIM
initial begin
  $dumpfile ("crono_tdc_lf.vcd");
  $dumpvars (0, crono_tdc_lf);
  #1;
end
`endif

endmodule
