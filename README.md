# Welcome to Crono TDC Core main readme!!

Crono TDC Core developed by Julian Rodriguez. 
This core can process multiple LVDS inputs and measure the width and occurrence time of the pulses in this lines.

## Directory structure

```bash
.
├── dockershell.sh
├── README.md
├── src
|   ├── common                          # common cores used during the design (verilog sources)
|   ├── crono_tdc                       # crono tdc top-levels (verilog sources)
|   ├── crono_tdc_core_version_pkg.v    # verilog package where the actual version of the code is stored (updated in each commit)
|   ├── hdlc_lite                       # hdlc-lite processor verilog sources
|   ├── tdc_control                     # tdc control logic verilog sources
|   ├── tdc_input                       # modules used to design the tdc input (iserdese fsm, fake iserdese, tdc_input_ram)
|   ├── uart                            # uart module
|   └── update_crono_tdc_version.sh     # magical script to update the crono_tdc_core code version in `crono_tdc_core_version_pkg.v`
└── tests                           # test benches coded in python using cocotb
    ├── common
    ├── hdlc_lite
    ├── run_tests.sh                # magical script to run the tests automatically
    ├── tdc_control
    ├── tdc_input
    ├── uart
    ├── tb_tools                    # python package with useful functions and tools to make testbenchs!
    └── setup.py                    # pip installater of `tb_tools` package

```

## Testing

We are using `cocotb` to run the testbenchs of Verilog modules. The testbenchs with this framework can be done in Python! yeeeii!

See `cocotb` doc: https://docs.cocotb.org/en/stable/ for more information. 

We make a docker image with cocotb prepared (see `.gci/Dockerfile`). In the CI pipeline, the image is built and then the testbenchs
are executed inside this image. But, we can build this image locally and execute the testbenchs.

### In CI

The gitlab repository has configured (see `gitlab-ci.yml`) a pipeline to run the testbenchs in each commit.

### Locally

We can enter in the `dockershell.sh` and execute the script located in `src/tests/run_tests.sh` to run the testbenchs. Also, this scripts allows to run individual tests.

### Code styling

The code styling standard followed was the one described in SystemVerilog website: https://www.systemverilog.io/styleguide

