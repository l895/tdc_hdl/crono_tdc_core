import cocotb
from cocotb.triggers import Timer, RisingEdge
from cocotb.clock import Clock

from tb_tools.utils import reset_dut

async def setup(dut, clk_period_ns):
    clock = Clock(dut.i_clk, clk_period_ns, units="ns")
    cocotb.start_soon(clock.start())  # Start the clock
    dut.i_ena.value = 0 # disable RAM
    dut.i_read_addr.value = 0
    dut.i_data.value = 0

    await reset_dut(dut.i_reset, clk_period_ns*2, 'ns')
    await Timer(clk_period_ns, 'ns')


def input_data_is_valid(input_data: int) -> bool:
    """ Determine if input data is valid """
    return True if ((input_data & 0x78) != 0) else False


def calculate_expected_lwa(dut, values:list, log=True):
    """
    Calculate the expected last written addres of the RAM if we fill it
    with a given list of values. The RAM controller only writes valid
    data in RAM, the data that is not valid is skipped. For this reason,
    the last written address may be not equal to the amount of data given
    as input to the controller.

    Parameters
    ----------
    dut :
        The Device Under Test.
    
    values: list
        List with the values we will give as input to the RAM controller.

    log: Bool
        Set to True if want to log each iteration over the list of values given.
    """
    dut._log.info('Calculating expected last written address...')
    # calculate expected last written address counting valid values
    # in input data
    expected_lwa = -1
    for val in values:
        if input_data_is_valid(val):
            expected_lwa += 1
            if log:
                dut._log.info(f"val: 0b{val:019b} (0x{val:04X}) is valid (count = {expected_lwa:4d})")
        else:
            if log:
                dut._log.info(f"val: 0b{val:019b} (0x{val:04X}) is not valid (count = {expected_lwa:4d})")

    if expected_lwa < 0:
        return 0
    else:
        return expected_lwa


async def fill_ram_with_pattern(    dut, 
                                    values:list,
                                    clock_period_ns:int):
    """
    Fill the RAM with a given pattern.

    Parameters
    ----------
    dut : 
        The Device Under Test.

    values : list
        List with the values of the pattern.
    """

    expected_lwa = calculate_expected_lwa(dut, values)
    dut._log.info('expected last written address: {}'.format(expected_lwa))

    dut._log.info('start filling the RAM with the pattern!')

    dut.i_ena.value = 1 # first enable the RAM to write
    await RisingEdge(dut.i_clk)

    for dat in values:
        dut.i_data.value = dat
        await RisingEdge(dut.i_clk)

    dut.i_ena.value = 0 # disable RAM
    dut.i_data.value = 0

    # The RAM controller have a delay of 1 clock cycle processing the input data.
    # Also, there is one delay of 1 clock cycle in the setting of the 'last written address' port.
    dut._log.info('waiting until ram controller finishes the writing...')
    await Timer(2*clock_period_ns, 'ns')

    assert dut.o_lwa.value == expected_lwa, 'last written address is not correct! {:d} != {}'.format(dut.o_lwa.value.integer, expected_lwa)

    dut.i_ena.value = 0 # disable RAM


async def verify_pattern_in_ram(    dut, 
                                    values:list,
                                    clock_period_ns:int):
    """
    Verify if RAM have a given pattern.
    Checks from address 0 to len(values)-1 if the RAM have a 
    given pattern. 

    Parameters
    ----------
    dut : 
        The Device Under Test.

    values : list
        List with the values of the pattern.
    """
    dut._log.info('start checking the values of the RAM!')

    dut.i_read_addr.value = 0
    dut.i_ena.value = 0 # disable RAM to read
    await Timer(clock_period_ns, 'ns')

    addr_to_read = 0
    for dat in values:
        if not input_data_is_valid(dat):
            dut._log.info('value 0x{:02X} of the pattern is not valid, skipping...'.format(dat))
            continue
        dut.i_read_addr.value = addr_to_read
        await RisingEdge(dut.i_clk)
        await Timer(1, 'ps')

        assert dut.o_data.value == dat, 'value not corresponds to pattern, 0x{:04X} != 0x{:04X}'.format(dut.o_data.value.integer, dat)
        dut._log.info('addr {:3}: value 0x{:04X} == 0x{:04X} is valid!'.format(
                        dut.i_read_addr.value.integer, dut.o_data.value.integer, dat))
        addr_to_read += 1


async def fill_ram_with_val(dut,val:int,qty:int,clk_period:int,clk_period_time_unit:str):
    """
    Fill the RAM 'qty' times with a given value. 
    The value must be a valid one! if not, the ram will not be filled.

    Parameters
    ----------
    dut : 
        The Device Under Test.

    values : int
        Valid input data for RAM controller (val & 0x3F != 0).

    qty : int
        Quantity of writes to do in RAM.
    """
    assert input_data_is_valid(val), 'value is not valid! exiting test...'
    dut._log.info(f'start writing {qty} times the RAM with 0x{val:X}!')

    dut.i_ena.value = 1 # first enable the RAM to write

    dut.i_data.value = val
    for i in range(qty):
        await Timer(1, "ps")
        await RisingEdge(dut.i_clk)

    dut.i_data.value = 0

    dut._log.info(f'stored the value {val:X} {qty} times to RAM!...')

    dut.i_ena.value = 0 # disable RAM


async def test_ram_overflow(    dut,
                                val_fill:int,
                                val_overflow:int,
                                ram_size:int,
                                clock_period_ns:int):
    """
    Present a lot of data to the RAM and see if the mem_full flag
    is set and the write_enable is disabled.
    Parameters
    ----------
    dut : 
        The Device Under Test.

    val_fill : int
        Valid input data for RAM controller (val & 0x3F != 0).
        We will fill the RAM with this value.

    val_overflow : int
        Valid input data for RAM controller (val & 0x3F != 0).
        We will try to overflow the the RAM with this value.
    
    ram_size : int
        The size of the RAM in bytes.

    clock_period_ns : int
        The period of the system clock in nanoseconds.
    """
    assert input_data_is_valid(val_fill), 'val_fill is not valid! exiting test...'
    assert input_data_is_valid(val_overflow), 'val_overflow is not valid! exiting test...'

    expected_lwa = calculate_expected_lwa(dut, [val_fill for i in range(ram_size)], log=False)
    dut._log.info('expected last written address: {}'.format(expected_lwa))

    dut._log.info('start filling RAM with 0x{:X}!'.format(val_fill))

    dut.i_ena.value = 1 # first enable the RAM to write

    dut.i_data.value = val_fill
    dut._log.info('waiting {:2.2f} ns until {} values are writed into RAM (clk period = {} ns)'.format(clock_period_ns*ram_size, ram_size, clock_period_ns))
    await Timer(clock_period_ns*ram_size, 'ns')

    # The RAM controller have a delay of 1 clock cycle processing the input data.
    # Also, there is one delay of 1 clock cycle in the setting of the 'last written address' port.
    dut.i_data.value = val_overflow
    for i in range(2):
        dut._log.info('waiting until ram controller finishes the writing, writing 0x{:X}...'.format(val_overflow))
        await RisingEdge(dut.i_clk)

    dut._log.info('RAM filled ({} times {:X})... checking mem_full flag, we and lwa!'.format(ram_size, val_fill))

    # this is the exact moment when the mem_full flag must be set! 
    assert dut.o_mem_full.value == 1, 'exact check: mem_full flag is not set after filling RAM!'
    dut._log.info('exact check: mem_full flag is set!')

    # The RAM controller have a delay of 1 clock cycle processing the input data.
    # Also, there is one delay of 1 clock cycle in the setting of the 'last written address' port.
    dut._log.info('waiting until ram controller finishes the writing...')
    await Timer(2*clock_period_ns, 'ns')
#    # we and lwa are set in the next clock cycle after mem full flag is set0
#    await RisingEdge(dut.i_clk)
    assert dut.ram.we.value == 0, 'exact check: write_enable signal was not deactivated after filling RAM!'
    dut._log.info('exact check: write_enable is deactivated!')
    assert dut.o_lwa.value == expected_lwa, 'exact check: last written address is incorrect! {:d} != {:d}'.format(dut.o_lwa.value.integer, expected_lwa)
    dut._log.info('exact check: last written address is {:d}!'.format(dut.o_lwa.value.integer))

    await Timer(clock_period_ns*10, 'ns')

    # check if after setting the flag the state is the same
    assert dut.o_mem_full.value == 1, 'xcheck: mem_full flag is not set after filling RAM!'
    dut._log.info('xcheck: mem_full flag is set!')
    assert dut.ram.we.value == 0, 'xcheck: write_enable signal was not deactivated after filling RAM!'
    dut._log.info('xcheck: write_enable is deactivated!')
    assert dut.o_lwa.value == expected_lwa, 'xcheck: last written address is incorrect! {:d} != {:d}'.format(dut.o_lwa.value.integer, expected_lwa)
    dut._log.info('xcheck: last written address is {:d}!'.format(dut.o_lwa.value.integer))

    dut.i_data.value = 0

    dut._log.info('overflow test in tdc ram finished successfully!')

    dut.i_ena.value = 0 # disable RAM
    await Timer(clock_period_ns, 'ns')

