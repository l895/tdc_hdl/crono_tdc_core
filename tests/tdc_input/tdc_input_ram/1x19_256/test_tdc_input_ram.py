from cocotb.triggers import Timer, RisingEdge
import cocotb
import os
import sys

# add parent directory to sys path (to import common python files!)
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

from tdc_input_ram_common import setup, fill_ram_with_pattern, verify_pattern_in_ram, fill_ram_with_val, test_ram_overflow
from tdc_input_ram_patterns import PATTERNS
from tb_tools.utils import reset_dut

RAM_SIZE=256
CLK_PERIOD_NS=10

@cocotb.test()
async def test_tdc_input_ram_fast_pulses(dut):
    await setup(dut,CLK_PERIOD_NS)
    await fill_ram_with_pattern(dut, PATTERNS['fast_pulses']['data'], CLK_PERIOD_NS)
    await verify_pattern_in_ram(dut, PATTERNS['fast_pulses']['data'], CLK_PERIOD_NS)


@cocotb.test()
async def test_tdc_input_ram_full_exact_size(dut):
    """
    Fill the RAM with exactly the size of the RAM and see if mem full flag is set.
    """
    await setup(dut, CLK_PERIOD_NS)

    await fill_ram_with_val(dut, 0x3F, RAM_SIZE, CLK_PERIOD_NS, 'ns')

    await Timer(CLK_PERIOD_NS*1.1, "ns")
    assert dut.o_mem_full.value == 1, f'mem full was not set after {RAM_SIZE} writes...'

    await reset_dut(dut.i_reset, CLK_PERIOD_NS*2, 'ns')

    assert dut.o_mem_full.value == 0, 'mem full was not reset after reset...'
    assert dut.o_lwa.value == 0, 'last written address was not reset after reset...'


@cocotb.test()
async def test_tdc_input_ram_overflow(dut):
    """
    Present as input more data than the size of the RAM and see if the mem_full flag is
    set and the write_enable of the RAM is deactivated.
    """
    await setup(dut, CLK_PERIOD_NS)
    await test_ram_overflow(dut, 0x3F, 0x0E, RAM_SIZE, CLK_PERIOD_NS)


@cocotb.test()
async def test_tdc_input_ram_lwa_reset(dut):
    """ Test if o_lwa of tdc_input_ram is reset between two different measures. """
    await setup(dut,CLK_PERIOD_NS)

    dut._log.info(f"starting first measurement!")
    await fill_ram_with_pattern(dut, PATTERNS['fast_pulses']['data'], CLK_PERIOD_NS)
    await verify_pattern_in_ram(dut, PATTERNS['fast_pulses']['data'], CLK_PERIOD_NS)


    await Timer(CLK_PERIOD_NS*0.5,'ns')
    #await RisingEdge(dut.i_clk)

    dut._log.info(f"starting second measurement!")
    await fill_ram_with_pattern(dut, PATTERNS['fast_pulses']['data'], CLK_PERIOD_NS)
    await verify_pattern_in_ram(dut, PATTERNS['fast_pulses']['data'], CLK_PERIOD_NS)