import cocotb
import os
import sys

# add parent directory to sys path (to import common python files!)
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

from tdc_input_ram_common import setup, fill_ram_with_pattern, verify_pattern_in_ram
from tdc_input_ram_patterns import PATTERNS

CLK_PERIOD_NS=10

@cocotb.test()
async def test_tdc_input_ram_fast_pulses(dut):

    await setup(dut, CLK_PERIOD_NS)

    await fill_ram_with_pattern(dut, PATTERNS['fast_pulses']['data'], CLK_PERIOD_NS)
    await verify_pattern_in_ram(dut, PATTERNS['fast_pulses']['data'], CLK_PERIOD_NS)
