import cocotb
from cocotb.triggers import Timer, RisingEdge

from tb_tools.tdc_input import generate_multiphase_clocks, simulate_input_signal
from tb_tools.utils import reset_dut
from tb_tools.fake_tdc_patterns import TEST_PATTERNS

RAM_SIZE = 256
CLOCK_PERIOD_NS = 10


async def wait_until_dready_is_set(dut):
    """ Wait until data ready signal of the dut is set """
    while dut.o_dready != 1:
        dut._log.info('waiting until data ready signal is set...')
        await RisingEdge(dut.i_clk)


async def verify_pattern_in_ram(dut, test_pattern:dict):
    """
    Verify if RAM have a given pattern.
    Checks from address 0 to len(values)-1 if the RAM have a 
    given pattern. 

    Parameters
    ----------
    dut : 
        The Device Under Test.

    test_pattern : dict
        The directory of the pattern used.
    """
    dut._log.info('start checking the values of the RAM!')

    dut.i_ena.value = 0 # disable RAM to read
    for i in range(2):
        await RisingEdge(dut.i_clk)

    assert dut.o_lwa.value.integer >= len(test_pattern['expected_ram_data'])-1, f"not enough data stored in RAM {dut.o_lwa.value.integer} >= {len(test_pattern['expected_ram_data'])-1}"

    addr_to_read = 0
    for exp in test_pattern['expected_ram_data']:
        dat = 0
        dat = ((exp['clock_cycle_count'] << 7) & 0xFFF80) | ((exp['data'] << 3) & 0b1111000) | (exp['tag'] & 0b111)

        if (dat & 0x3F) == 0:
            dut._log.info('value 0x{:02X} of the pattern is not valid, skipping...'.format(dat))
        else:
            dut._log.info(f"checking if RAM addr {addr_to_read} value is equal to: 0b{dat:020b} (0x{dat:X}) (tag: {exp['tag']})")

            dut.i_read_addr.value = addr_to_read
            await RisingEdge(dut.i_clk); await Timer(1, 'ps')

            assert dut.o_data.value == dat, f'value not corresponds to pattern, 0b{dut.o_data.value.integer:020b} (0x{dut.o_data.value.integer:04X}) != 0x{dat:020b} (0x{dat:04X})'

            dut._log.info(
                f'addr {dut.i_read_addr.value.integer:3}:'
                f'value 0b{dut.o_data.value.integer:020b} (0x{dut.o_data.value.integer:04X}) == 0b{dat:020b} (0x{dat:X}) is valid!')
            addr_to_read += 1


async def test_with_input(dut, test_pattern: dict):
    """
    Test tdc input with an input signal and check if the data 
    stored in RAM is the expected.

    Parameters
    ----------
    dut :
        The device under test (tdc_input).

    test_pattern : dict
        The pattern with which we will test the DUT.
    """

    # Set input signal initial value to 0
    dut.i_ena.value = 0
    dut.i_data.value = 0b00000

    await cocotb.start(generate_multiphase_clocks(dut, CLOCK_PERIOD_NS))
    clock_cycle_count = 0

    await reset_dut(dut.i_sync_reset, round(CLOCK_PERIOD_NS*1.1), 'ns')
    await RisingEdge(dut.i_clk)
    clock_cycle_count += 1

    dut.i_ena.value = 1

    dut._log.info('Generating input signal of {} MHz ({}ns)...'.format(
            round(1000/test_pattern['input_period_ns']),
            test_pattern['input_period_ns']
        )
    )

    ## generate input signal
    await simulate_input_signal(
            dut_signal=dut.i_data,
            values=test_pattern['input'],
            clock_period_ns = test_pattern['input_period_ns']
        )

    dut.i_ena.value = 0
    await wait_until_dready_is_set(dut)
    await verify_pattern_in_ram(dut, test_pattern)


@cocotb.test()
async def test_only_fast_small_pulses_pattern(dut):
  await test_with_input(dut=dut, test_pattern=TEST_PATTERNS['fast_small_pulses'])


@cocotb.test()
async def test_only_slow_small_pulses_pattern(dut):
   await test_with_input(dut=dut, test_pattern=TEST_PATTERNS['small_pulses'])


@cocotb.test()
async def test_only_fast_long_pulses_pattern(dut):
    await test_with_input(dut=dut, test_pattern=TEST_PATTERNS['fast_long_pulses'])


@cocotb.test()
async def test_only_slow_long_pulses_pattern(dut):
    await test_with_input(dut=dut, test_pattern=TEST_PATTERNS['long_pulses'])


@cocotb.test()
async def test_fast_intercalated_pulses_pattern(dut):
    await test_with_input(dut=dut, test_pattern=TEST_PATTERNS['fast_intercalated_pulses'])


@cocotb.test()
async def test_fake_tdc_mem_full(dut):
    """
    Fill the RAM of the TDC input and see if the correspondent flags
    are set and no more data is writed into the RAM.
    """
    clk_period_ns = 10
    dut.i_data.value = 0b00000

    dut._log.info('initializing mem_full test with TDC input with a RAM of {} registers'.format(RAM_SIZE))

    await cocotb.start(generate_multiphase_clocks(dut, clk_period_ns))

    await reset_dut(dut.i_sync_reset, clk_period_ns, 'ns')
    await RisingEdge(dut.i_clk)

    dut.i_ena.value = 1

    # set input to 1 and start filling with data
    dut.i_data.value = 0
    dut._log.info('waiting {:2.2f} ns until RAM is full (clk period = {} ns)'.format(RAM_SIZE*(clk_period_ns*4), clk_period_ns))
    for i in range(RAM_SIZE):
        await Timer(clk_period_ns/4, 'ns')
        dut.i_data.value = 1
        await Timer(clk_period_ns/4, 'ns')
        dut.i_data.value = 1
        await Timer(clk_period_ns/4, 'ns')
        dut.i_data.value = 0
        await Timer(clk_period_ns/4, 'ns')

    # The RAM Controller have a delay of 2 clock cycles processing and storing the data.
    # For this reason we wait 2*4 clock cycles (two periods, 4 bits per period).
    # We changed the pattern to a pattern of intercalated ones and zeros.
    dut.i_data.value = 0
    for i in range(2*4):
        dut._log.info('waiting until ram controller finishes the writing, writing 0x{:X}...'.format(0xFF))
        await RisingEdge(dut.i_clk)
        dut.i_data.value = 1 if (dut.i_data.value == 0) else 0

    dut._log.info('RAM filled ({} times {:X})... checking mem_full flag and lwa!'.format(RAM_SIZE, 0x3F))

    # this is the exact moment when the mem_full flag must be set! 
    assert dut.o_mem_full.value == 1, 'mem_full flag is not set after filling RAM!'
    dut._log.info('mem_full flag is set!')
    assert dut.o_lwa.value == RAM_SIZE-1, 'last written address is incorrect! {:d} != {:d}'.format(dut.o_lwa.value.integer, RAM_SIZE-1)
    dut._log.info('last written address is correct: {:d}!'.format(dut.o_lwa.value.integer))

    dut.i_data.value = 0
    await Timer(clk_period_ns*10, 'ns')

    dut.i_ena.value = 0

    dut.i_data.value = 1
    await Timer(clk_period_ns*10, 'ns')
