
import cocotb
from cocotb.triggers import Timer, RisingEdge, FallingEdge

async def reset_dut(sync_reset, duration, time_unit):
    """
    Put in reset state the DUT for a certain time interval 
    and unreset it then.

    Parameters
    ----------
    sync_reset : cocotb.handle.ModifiableObject
        The reset signal of the DUT.

    duration : int
        The width of the reset pulse.

    time_unit: str
        The time unit of the duration parameter (ns, us, etc...)
    """
    sync_reset._log.info("Touching the reset button of the DUT! ({}{})".format(duration, time_unit))
    sync_reset.value = 1
    await Timer(duration, units=time_unit)
    sync_reset.value = 0
    sync_reset._log.info("The reset was completed!")


async def generate_multiphase_clocks(dut, freq_mhz):
    """ Generates 4 clocks with 45 grades of phase shift between them """
    # pre-construct triggers for performance
    period_us = 1/freq_mhz
    phase_shifts = period_us/4
    dut._log.info('generating clocks of {}MHz ({}us) with phase shifts of {}us'.format(freq_mhz, period_us, phase_shifts))
    while True:
        dut.CLK.value = 1
        dut.OCLK.value = 1
        dut.CLKB.value = 0
        dut.OCLKB.value = 0
        await Timer(phase_shifts, units='us')
        dut.CLK.value = 0
        dut.OCLK.value = 1
        dut.CLKB.value = 1
        dut.OCLKB.value = 0
        await Timer(phase_shifts, units='us')
        dut.CLK.value = 0
        dut.OCLK.value = 0
        dut.CLKB.value = 1
        dut.OCLKB.value = 1
        await Timer(phase_shifts, units='us')
        dut.CLK.value = 1
        dut.OCLK.value = 0
        dut.CLKB.value = 0
        dut.OCLKB.value = 1
        await Timer(phase_shifts, units='us')


@cocotb.test()
async def test_fake_iserdese2_ov(dut):
    """
        Test that the input serial pattern is deserialized at the output
        of the ISERDESE2.
    """
    test_cases = {
        0: {"input_nibble": 0x0},
        1: {"input_nibble": 0x1},
        2: {"input_nibble": 0x2},
        3: {"input_nibble": 0x3},

        4: {"input_nibble": 0x4},
        5: {"input_nibble": 0x5},
        6: {"input_nibble": 0x6},
        7: {"input_nibble": 0x7},

        8: {"input_nibble": 0x8},
        9: {"input_nibble": 0x9},
        10: {"input_nibble": 0xA},
        11: {"input_nibble": 0xB},

        12: {"input_nibble": 0xC},
        13: {"input_nibble": 0xD},
        14: {"input_nibble": 0xE},
        15: {"input_nibble": 0xF},
    }

    clk_freq_mhz = 100
    await cocotb.start(generate_multiphase_clocks(dut, clk_freq_mhz))

    ## if we not set the input to zero at the beginning is set as high-z
    dut.D.value = 0
    await reset_dut(dut.RST, 10, "ns")
    await FallingEdge(dut.CLK)
    await Timer(3/(8 * clk_freq_mhz), 'us')

    for case in test_cases:
        dut._log.info('(test case {}): feeding SerDes with nibble {:04b}...'.format(
            case,
            test_cases[case]['input_nibble']
            )
        )
        dut.D.value = (test_cases[case]['input_nibble'] & 0x8) >> 3
        await Timer(1/(4 * clk_freq_mhz), 'us')

        dut.D.value = (test_cases[case]['input_nibble'] & 0x4) >> 2
        await Timer(1/(4 * clk_freq_mhz), 'us')

        dut.D.value = (test_cases[case]['input_nibble'] & 0x2) >> 1
        await Timer(1/(4 * clk_freq_mhz), 'us')

        dut.D.value = (test_cases[case]['input_nibble'] & 0x1) >> 0
        await Timer(1/(4 * clk_freq_mhz), 'us')
        #await FallingEdge(dut.CLK)

        if case >= 2: # from third test nibble test if the p
            dut._log.info('(test case {}): checking if previous nibble {:04b} is now at output...'.format(case-1, test_cases[case-1]['input_nibble'])) 
            assert (dut.Q.value.integer > 3) & 0x1 == (test_cases[case-2]['input_nibble'] > 3) & 0x1
            assert (dut.Q.value.integer > 2) & 0x1 == (test_cases[case-2]['input_nibble'] > 2) & 0x1
            assert (dut.Q.value.integer > 1) & 0x1 == (test_cases[case-2]['input_nibble'] > 1) & 0x1
            assert (dut.Q.value.integer > 0) & 0x1 == (test_cases[case-2]['input_nibble'] > 0) & 0x1
            dut._log.info('(test case {}): parallelized data was the expected ({:04b} == {:04b})'.format(case-1, dut.Q.value.integer, test_cases[case]['input_nibble']))

    await Timer(1/clk_freq_mhz, 'us')
    dut._log.info('(test case {}): checking if nibble {:04b} is now at output...'.format(max(test_cases.keys())-1, test_cases[max(test_cases.keys())-1]['input_nibble'])) 
    assert (dut.Q.value.integer > 3) & 0x1 == (test_cases[max(test_cases.keys())-1]['input_nibble'] > 3) & 0x1
    assert (dut.Q.value.integer > 2) & 0x1 == (test_cases[max(test_cases.keys())-1]['input_nibble'] > 2) & 0x1
    assert (dut.Q.value.integer > 1) & 0x1 == (test_cases[max(test_cases.keys())-1]['input_nibble'] > 1) & 0x1
    assert (dut.Q.value.integer > 0) & 0x1 == (test_cases[max(test_cases.keys())-1]['input_nibble'] > 0) & 0x1
    dut._log.info('(test case {}): parallelized data was the expected ({:04b} == {:04b})'.format(max(test_cases.keys())-1, dut.Q.value.integer, test_cases[max(test_cases.keys())-1]['input_nibble']))

    await Timer(1/clk_freq_mhz, 'us')
    dut._log.info('checking if last nibble {:04b} is now at output...'.format(test_cases[max(test_cases.keys())]['input_nibble'])) 
    assert (dut.Q.value.integer > 3) & 0x1 == (test_cases[max(test_cases.keys())]['input_nibble'] > 3) & 0x1
    assert (dut.Q.value.integer > 2) & 0x1 == (test_cases[max(test_cases.keys())]['input_nibble'] > 2) & 0x1
    assert (dut.Q.value.integer > 1) & 0x1 == (test_cases[max(test_cases.keys())]['input_nibble'] > 1) & 0x1
    assert (dut.Q.value.integer > 0) & 0x1 == (test_cases[max(test_cases.keys())]['input_nibble'] > 0) & 0x1
    dut._log.info('(test case {}): parallelized data was the expected ({:04b} == {:04b})'.format(max(test_cases.keys()), dut.Q.value.integer, test_cases[max(test_cases.keys())]['input_nibble']))

    ## just to see more time the waveform
    await Timer(10, units="ns")