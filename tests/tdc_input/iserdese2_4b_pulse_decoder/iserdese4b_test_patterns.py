# This are some common patterns that could be procuded by an ISERDESE2 core
# This pattern present some event of interest 
# (small pulses, long pulses, pulses of different width intercalated, etc...)

ONLY_SMALL_PULSES_PATTERN = {
    "input_data": [
        0b0001, 0b0010, 0b0011, 0b0100, 0b0101, 0b0110, 0b0111, 0b0100, 0b1001, 0b0000, 0b1110, 0b1010, 0b1010
    ],
    "output_data": [
        # Clock Cycle | ISERDE2 OUT   | TAG
        (1 << 7)      | (0b0001 << 3) | 0b001,
        (2 << 7)      | (0b0010 << 3) | 0b001,
        (3 << 7)      | (0b0011 << 3) | 0b001,
        (4 << 7)      | (0b0100 << 3) | 0b001,
        (5 << 7)      | (0b0101 << 3) | 0b001,
        (6 << 7)      | (0b0110 << 3) | 0b001,
        (7 << 7)      | (0b0111 << 3) | 0b001,
        (8 << 7)      | (0b0100 << 3) | 0b001,
        (9 << 7)      | (0b1001 << 3) | 0b001,
        (11 << 7)     | (0b1110 << 3) | 0b001,
        (12 << 7)     | (0b1010 << 3) | 0b001,
        (13 << 7)     | (0b1010 << 3) | 0b001
    ]
}


ONLY_LONG_PULSES_PATTERN = {
    "input_data": [
        0b0001, 0b1000,
        0b0011, 0b1000,
        0b0111, 0b1000,
        0b1111, 0b1000,

        0b0001, 0b1100,
        0b0011, 0b1100,
        0b0111, 0b1100,
        0b1111, 0b1100,

        0b0001, 0b1110,
        0b0011, 0b1110,
        0b0111, 0b1110,
        0b1111, 0b1110,

        0b0001, 0b1111, 0b1000,
        0b0011, 0b1111, 0b1000,
        0b0111, 0b1111, 0b1000,
        0b1111, 0b1111, 0b1000,

        0b0001, 0b1111, 0b1100,
        0b0011, 0b1111, 0b1100,
        0b0111, 0b1111, 0b1100,
        0b1111, 0b1111, 0b1100,

        0b0001, 0b1111, 0b1110,
        0b0011, 0b1111, 0b1110,
        0b0111, 0b1111, 0b1110,
        0b1111, 0b1111, 0b1110,
    ],

    "output_data": [
        # Clock Cycle | ISERDE2 OUT   | TAG
        (1 << 7)      | (0b0001 << 3) | 0b010, # long pulse starting
        (2 << 7)      | (0b1000 << 3) | 0b100, # long pulse ending
        (3 << 7)      | (0b0011 << 3) | 0b010,
        (4 << 7)      | (0b1000 << 3) | 0b100,
        (5 << 7)      | (0b0111 << 3) | 0b010,
        (6 << 7)      | (0b1000 << 3) | 0b100,
        (7 << 7)      | (0b1111 << 3) | 0b010,
        (8 << 7)      | (0b1000 << 3) | 0b100,

        (9 << 7)      | (0b0001 << 3) | 0b010, # long pulse starting
        (10 << 7)     | (0b1100 << 3) | 0b100, # long pulse ending
        (11 << 7)     | (0b0011 << 3) | 0b010,
        (12 << 7)     | (0b1100 << 3) | 0b100,
        (13 << 7)     | (0b0111 << 3) | 0b010,
        (14 << 7)     | (0b1100 << 3) | 0b100,
        (15 << 7)     | (0b1111 << 3) | 0b010,
        (16 << 7)     | (0b1100 << 3) | 0b100,

        (17 << 7)     | (0b0001 << 3) | 0b010, # long pulse starting
        (18 << 7)     | (0b1110 << 3) | 0b100, # long pulse ending
        (19 << 7)     | (0b0011 << 3) | 0b010,
        (20 << 7)     | (0b1110 << 3) | 0b100,
        (21 << 7)     | (0b0111 << 3) | 0b010,
        (22 << 7)     | (0b1110 << 3) | 0b100,
        (23 << 7)     | (0b1111 << 3) | 0b010,
        (24 << 7)     | (0b1110 << 3) | 0b100,

        (25 << 7)     | (0b0001 << 3) | 0b010, # long pulse starting
        (27 << 7)     | (0b1000 << 3) | 0b100, # long pulse ending
        (28 << 7)     | (0b0011 << 3) | 0b010,
        (30 << 7)     | (0b1000 << 3) | 0b100,
        (31 << 7)     | (0b0111 << 3) | 0b010,
        (33 << 7)     | (0b1000 << 3) | 0b100,
        (34 << 7)     | (0b1111 << 3) | 0b010,
        (36 << 7)     | (0b1000 << 3) | 0b100,

        (37 << 7)     | (0b0001 << 3) | 0b010, # long pulse starting
        (39 << 7)     | (0b1100 << 3) | 0b100, # long pulse ending
        (40 << 7)     | (0b0011 << 3) | 0b010,
        (42 << 7)     | (0b1100 << 3) | 0b100,
        (43 << 7)     | (0b0111 << 3) | 0b010,
        (45 << 7)     | (0b1100 << 3) | 0b100,
        (46 << 7)     | (0b1111 << 3) | 0b010,
        (48 << 7)     | (0b1100 << 3) | 0b100,

        (49 << 7)     | (0b0001 << 3) | 0b010, # long pulse starting
        (51 << 7)     | (0b1110 << 3) | 0b100, # long pulse ending
        (52 << 7)     | (0b0011 << 3) | 0b010,
        (54 << 7)     | (0b1110 << 3) | 0b100,
        (55 << 7)     | (0b0111 << 3) | 0b010,
        (57 << 7)     | (0b1110 << 3) | 0b100,
        (58 << 7)     | (0b1111 << 3) | 0b010,
        (60 << 7)     | (0b1110 << 3) | 0b100,

    ]
}


LONG_PULSES_SMALL_INTERCALATED_PATTERN = {
    "input_data": [
        0b0001,
        0b0001, 0b1000,

        0b0010,
        0b0011, 0b1000,

        0b0011,
        0b0111, 0b1000,

        0b0100,
        0b1111, 0b1000,

        0b0101,
        0b0001, 0b1100,

        0b0110,
        0b0011, 0b1100,

        0b0111,
        0b0111, 0b1100,

        0b1000,
        0b0111, 0b1100,

        0b1001,
        0b0111, 0b1100,

        0b1010,
        0b0001, 0b1110,

        0b1011,
        0b0011, 0b1110,

    ],

    "output_data": [
        # Clock Cycle | ISERDE2 OUT   | TAG
        (1 << 7)      | (0b0001 << 3) | 0b001,
        (2 << 7)      | (0b0001 << 3) | 0b010, # long pulse starting
        (3 << 7)      | (0b1000 << 3) | 0b100, # long pulse ending

        (4 << 7)      | (0b0010 << 3) | 0b001,
        (5 << 7)      | (0b0011 << 3) | 0b010, # long pulse starting
        (6 << 7)      | (0b1000 << 3) | 0b100, # long pulse ending

        (7 << 7)      | (0b0011 << 3) | 0b001,
        (8 << 7)      | (0b0111 << 3) | 0b010, # long pulse starting
        (9 << 7)      | (0b1000 << 3) | 0b100, # long pulse ending

        (10 << 7)      | (0b0100 << 3) | 0b001,
        (11 << 7)      | (0b1111 << 3) | 0b010, # long pulse starting
        (12 << 7)      | (0b1000 << 3) | 0b100, # long pulse ending

        (13 << 7)      | (0b0101 << 3) | 0b001,
        (14 << 7)      | (0b0001 << 3) | 0b010, # long pulse starting
        (15 << 7)      | (0b1100 << 3) | 0b100, # long pulse ending

        (16 << 7)      | (0b0110 << 3) | 0b001,
        (17 << 7)      | (0b0011 << 3) | 0b010, # long pulse starting
        (18 << 7)      | (0b1100 << 3) | 0b100, # long pulse ending

        (19 << 7)      | (0b0111 << 3) | 0b001,
        (20 << 7)      | (0b0111 << 3) | 0b010, # long pulse starting
        (21 << 7)      | (0b1100 << 3) | 0b100, # long pulse ending

        (22 << 7)      | (0b1000 << 3) | 0b001,
        (23 << 7)      | (0b0111 << 3) | 0b010, # long pulse starting
        (24 << 7)      | (0b1100 << 3) | 0b100, # long pulse ending

        (25 << 7)      | (0b1001 << 3) | 0b001,
        (26 << 7)      | (0b0111 << 3) | 0b010, # long pulse starting
        (27 << 7)      | (0b1100 << 3) | 0b100, # long pulse ending

        (28 << 7)      | (0b1010 << 3) | 0b001,
        (29 << 7)      | (0b0001 << 3) | 0b010, # long pulse starting
        (30 << 7)      | (0b1110 << 3) | 0b100, # long pulse ending

        (31 << 7)      | (0b1011 << 3) | 0b001,
        (32 << 7)      | (0b0011 << 3) | 0b010, # long pulse starting
        (33 << 7)      | (0b1110 << 3) | 0b100, # long pulse ending

    ]
}


VERY_LONG_PULSE_SMALL_INTERCALATED_PATTERN = {
    "input_data": [
        0b0001, 0b1111, 0b1111, 0b1111, 0b1111, 0b1111, 0b1111, 0b1111, 0b1111, 0b1111, 0b1111, 0b1111, 0b1111, 0b1000, 0b0001,
        0b0011, 0b1111, 0b1111, 0b1111, 0b1111, 0b1111, 0b1111, 0b1111, 0b1111, 0b1111, 0b1111, 0b1111, 0b1111, 0b1100, 0b0010,
        0b0111, 0b1111, 0b1111, 0b1111, 0b1111, 0b1111, 0b1110, 0b0010,
        0b1111, 0b1111, 0b1111, 0b1111, 0b1110, 0b0100,
        0b1111, 0b1111, 0b1111, 0b1111, 0b1111, 0b0101,
        0b0111, 0b1111, 0b1111, 0b1111, 0b1110, 0b0110,
        0b0111, 0b1111, 0b1111, 0b1110, 0b0111,
        0b0011, 0b1111, 0b1111, 0b1110, 0b1000,
        0b0011, 0b1111, 0b1111, 0b1110, 0b1001,
        0b0011, 0b1111, 0b1111, 0b1110, 0b1010,
    ],

    "output_data": [
        # Clock Cycle | ISERDE2 OUT   | TAG
        (1 << 7)      | (0b0001 << 3) | 0b010, # long pulse starting
        (14 << 7)     | (0b1000 << 3) | 0b100, # long pulse ending
        (15 << 7)     | (0b0001 << 3) | 0b001, 

        (16 << 7)     | (0b0011 << 3) | 0b010, # long pulse starting
        (29 << 7)     | (0b1100 << 3) | 0b100, # long pulse ending
        (30 << 7)     | (0b0010 << 3) | 0b001, 

        (31 << 7)     | (0b0111 << 3) | 0b010, # long pulse starting
        (37 << 7)     | (0b1110 << 3) | 0b100, # long pulse ending
        (38 << 7)     | (0b0010 << 3) | 0b001, 

        (39 << 7)     | (0b1111 << 3) | 0b010, # long pulse starting
        (43 << 7)     | (0b1110 << 3) | 0b100, # long pulse ending
        (44 << 7)     | (0b0100 << 3) | 0b001, 

        (45 << 7)     | (0b1111 << 3) | 0b010, # long pulse starting
        (49 << 7)     | (0b1111 << 3) | 0b100, # long pulse ending
        (50 << 7)     | (0b0101 << 3) | 0b001, 

        (51 << 7)     | (0b0111 << 3) | 0b010, # long pulse starting
        (55 << 7)     | (0b1110 << 3) | 0b100, # long pulse ending
        (56 << 7)     | (0b0110 << 3) | 0b001, 

        (57 << 7)     | (0b0111 << 3) | 0b010, # long pulse starting
        (60 << 7)     | (0b1110 << 3) | 0b100, # long pulse ending
        (61 << 7)     | (0b0111 << 3) | 0b001, 

        (62 << 7)     | (0b0011 << 3) | 0b010, # long pulse starting
        (65 << 7)     | (0b1110 << 3) | 0b100, # long pulse ending
        (66 << 7)     | (0b1000 << 3) | 0b001, 

        (67 << 7)     | (0b0011 << 3) | 0b010, # long pulse starting
        (70 << 7)     | (0b1110 << 3) | 0b100, # long pulse ending
        (71 << 7)     | (0b1001 << 3) | 0b001, 

        (72 << 7)     | (0b0011 << 3) | 0b010, # long pulse starting
        (75 << 7)     | (0b1110 << 3) | 0b100, # long pulse ending
        (76 << 7)     | (0b1010 << 3) | 0b001, 

    ]
}


PULSE_STARTS_SMALL_APPEARS_PATTERN = {
    "input_data": [
        0b0000,
        0b0101, 0b1111, 0b1000,
        0b1001, 0b1111, 0b1100,
        0b1101, 0b1111, 0b1111, 0b1111, 0b1110,
        0b1011, 0b1111, 0b1111, 0b1111, 0b1110,
    ],

    "output_data": [
        # Clock Cycle | ISERDE2 OUT   | TAG
        (2 << 7)      | (0b0101 << 3) | 0b011, # long pulse starting
        (4 << 7)      | (0b1000 << 3) | 0b100,

        (5 << 7)      | (0b1001 << 3) | 0b011, # long pulse starting
        (7 << 7)      | (0b1100 << 3) | 0b100,

        (8 << 7)      | (0b1101 << 3) | 0b011, # long pulse starting
        (12 << 7)     | (0b1110 << 3) | 0b100,

        (13 << 7)     | (0b1011 << 3) | 0b011, # long pulse starting
        (17 << 7)     | (0b1110 << 3) | 0b100,
    ]
}


LONG_PULSE_ENDS_SMALL_APPEARS_PATTERN = {
    "input_data": [
        0b0001, 0b1111, 0b1001,
        0b0001, 0b1111, 0b1010,
        0b0001, 0b1111, 0b1011,
        0b0001, 0b1111, 0b1101,
        0b0000
    ],

    "output_data": [
        # Clock Cycle | ISERDE2 OUT   | TAG
        (1 << 7)      | (0b0001 << 3) | 0b010, # long pulse starting
        (3 << 7)      | (0b1001 << 3) | 0b101,

        (4 << 7)      | (0b0001 << 3) | 0b010, # long pulse starting
        (6 << 7)      | (0b1010 << 3) | 0b101,

        (7 << 7)      | (0b0001 << 3) | 0b010, # long pulse starting
        (9 << 7)      | (0b1011 << 3) | 0b101,

        (10 << 7)     | (0b0001 << 3) | 0b010, # long pulse starting
        (12 << 7)     | (0b1101 << 3) | 0b101,
    ]
}


LONG_PULSE_ENDS_LONG_STARTS_PATTERN = {
    "input_data": [
        0b0001, 0b1111, 0b1001, 0b1111, 0b1111, 0b1000,
        0b0001, 0b1111, 0b1011, 0b1111, 0b1111, 0b1000,
        0b0001, 0b1111, 0b1101, 0b1111, 0b1111, 0b1000,
    ],

    "output_data": [
        # Clock Cycle | ISERDE2 OUT   | TAG
        (1 << 7)      | (0b0001 << 3) | 0b010, # long pulse starting
        (3 << 7)      | (0b1001 << 3) | 0b110,
        (6 << 7)      | (0b1000 << 3) | 0b100,

        (7 << 7)      | (0b0001 << 3) | 0b010, # long pulse starting
        (9 << 7)      | (0b1011 << 3) | 0b110,
        (12 << 7)     | (0b1000 << 3) | 0b100,

        (13 << 7)     | (0b0001 << 3) | 0b010, # long pulse starting
        (15 << 7)     | (0b1101 << 3) | 0b110,
        (18 << 7)     | (0b1000 << 3) | 0b100,
    ]
}