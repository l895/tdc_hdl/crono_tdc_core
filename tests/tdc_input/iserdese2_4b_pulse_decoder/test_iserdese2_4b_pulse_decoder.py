from enum import IntEnum
import cocotb
from cocotb.clock import Clock
from cocotb.triggers import Timer

from tb_tools.utils import reset_dut
from tb_tools.tdc_input import simulate_serdes_output
import iserdese4b_test_patterns

CLOCK_PERIOD_NS=10

class measure_tag(IntEnum):
    SMALL_PULSE                                 = 0b001
    LONG_START_PULSE                            = 0b010
    LONG_PULSE_STARTS_SMALL_APPEARS             = 0b011
    LONG_PULSE_ENDS                             = 0b100
    LONG_PULSE_ENDS_SMALL_APPEARS               = 0b101
    LONG_PULSE_ENDS_LONG_STARTS                 = 0b110
    LONG_PULSE_ENDS_SMALL_APPEARS_LONG_STARTS   = 0b111


async def output_data_validator(dut, expected_values: list):
    """
    Compare DUT output with a list of expected values and assert if the
    sequence is not the expected one.

    Parameters
    ----------
    dut : 
        The DUT that will generate the output.

    expected_values : list
        A list with the expected output data (in order).
    """
    counter = 0

    while counter < len(expected_values):
        while dut.o_data.value.integer == 0:
            await Timer(CLOCK_PERIOD_NS,'ns')
        
        assert dut.o_data.value.integer == expected_values[counter], f"output data is not the expected: 0b{dut.o_data.value.integer:020b} != 0b{expected_values[counter]:020b}"
        dut._log.info(f"output_data_validator: data {counter:3d}: 0x{expected_values[counter]:04X} (0b{expected_values[counter]:020b}) is valid!")
        counter += 1
        await Timer(CLOCK_PERIOD_NS,'ns')


async def test_with_inputs(dut, test_pattern):
    """ Feed the DUT with a certain test pattern and verify if the output is correct. """
    dut.i_start.value = 0b0
    dut.i_stop.value = 0b0

    dut.i_serdes_out.value = 0b0000
    
    clock = Clock(dut.i_clk, CLOCK_PERIOD_NS, units="ns")
    cocotb.start_soon(clock.start())  # Start the clock

    await reset_dut(sync_reset=dut.i_reset, duration=CLOCK_PERIOD_NS*2, time_unit='ns')

    dut.i_start.value = 0b1
    await Timer(CLOCK_PERIOD_NS*2, 'ns')
    dut.i_start.value = 0b0

    cocotb.start_soon(simulate_serdes_output(dut_signal=dut.i_serdes_out, values=test_pattern['input_data'], clock_period_ns=CLOCK_PERIOD_NS))

#    await Timer(CLOCK_PERIOD_NS*100, 'ns')
    await output_data_validator(dut=dut, expected_values=test_pattern['output_data'])

    dut.i_stop.value = 0b1
    await Timer(CLOCK_PERIOD_NS*2, 'ns')
    dut.i_stop.value = 0b0



@cocotb.test()
async def test_only_small_pulses_pattern(dut):
    await test_with_inputs(dut, iserdese4b_test_patterns.ONLY_SMALL_PULSES_PATTERN)

@cocotb.test()
async def test_only_long_pulses_pattern(dut):
    await test_with_inputs(dut, iserdese4b_test_patterns.ONLY_LONG_PULSES_PATTERN)

@cocotb.test()
async def test_long_pulses_small_intercalated_pattern(dut):
    await test_with_inputs(dut, iserdese4b_test_patterns.LONG_PULSES_SMALL_INTERCALATED_PATTERN)

@cocotb.test()
async def test_very_long_pulses_small_intercalated_pattern(dut):
    await test_with_inputs(dut, iserdese4b_test_patterns.VERY_LONG_PULSE_SMALL_INTERCALATED_PATTERN) 

@cocotb.test()
async def test_pulse_starts_small_pattern(dut):
    await test_with_inputs(dut, iserdese4b_test_patterns.PULSE_STARTS_SMALL_APPEARS_PATTERN)

@cocotb.test()
async def test_long_pulse_ends_small_pattern(dut):
    await test_with_inputs(dut, iserdese4b_test_patterns.LONG_PULSE_ENDS_SMALL_APPEARS_PATTERN)

@cocotb.test()
async def test_long_pulse_ends_long_starts_pattern(dut):
    await test_with_inputs(dut, iserdese4b_test_patterns.LONG_PULSE_ENDS_LONG_STARTS_PATTERN)