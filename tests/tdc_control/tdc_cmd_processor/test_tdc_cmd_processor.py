from cocotb.clock import Clock
from cocotb.triggers import Timer
import cocotb

from tb_tools.utils import reset_dut


async def simulate_input(dut, i_rx_data: int, active_time_ns: int):
    """
    Simulate `i_rx_dv` and `i_rx_data` signals putting some val and having them high
    some value.
    """
    dut.i_rx_dv.value = i_rx_data.bit_length()
    dut.i_rx_data.value = i_rx_data
    await Timer(active_time_ns, 'ns')
    dut.i_rx_dv.value = 0
    dut.i_rx_data.value = 0


@cocotb.test()
async def test_with_input_data(dut):
    """
    Test if the command processor can distinguish between known and unknown commands.
    Test must be re-checked after adding commands to the list in `tdc_commands.v`.
    """
    clock_period_ns = 10
    test_cases = [ 
#        {'input_data': 0x555555000001,  'expected_data': 0x555555, 'expected_cmd_exec': 0x1, 'input_signal_active_time_ns': clock_period_ns},
        {'input_data': 0x555555000001,  'expected_data': 0x555555, 'expected_cmd_exec': 0x1, 'input_signal_active_time_ns': clock_period_ns*2},
        {'input_data': 0x555555000001,  'expected_data': 0x555555, 'expected_cmd_exec': 0x1, 'input_signal_active_time_ns': clock_period_ns*3},
        {'input_data': 0x555555000001,  'expected_data': 0x555555, 'expected_cmd_exec': 0x1, 'input_signal_active_time_ns': clock_period_ns*4},
        {'input_data': 0x555555000001,  'expected_data': 0x555555, 'expected_cmd_exec': 0x1, 'input_signal_active_time_ns': clock_period_ns*5},
#        {'input_data': 0x5F495FA50002,  'expected_data': 0x5F495F, 'expected_cmd_exec': 0x0, 'input_signal_active_time_ns': clock_period_ns},
        {'input_data': 0x5F495FA50002,  'expected_data': 0x5F495F, 'expected_cmd_exec': 0x0, 'input_signal_active_time_ns': clock_period_ns*2},
#        {'input_data': 0x5F495F000003,  'expected_data': 0x5F495F, 'expected_cmd_exec': 0x4, 'input_signal_active_time_ns': clock_period_ns},
        {'input_data': 0x5F495F000003,  'expected_data': 0x5F495F, 'expected_cmd_exec': 0x4, 'input_signal_active_time_ns': clock_period_ns*2},
        {'input_data': 0x5F495F000003,  'expected_data': 0x5F495F, 'expected_cmd_exec': 0x4, 'input_signal_active_time_ns': clock_period_ns*3},
        {'input_data': 0x5F495F000003,  'expected_data': 0x5F495F, 'expected_cmd_exec': 0x4, 'input_signal_active_time_ns': clock_period_ns*4},
        {'input_data': 0x5F495F000003,  'expected_data': 0x5F495F, 'expected_cmd_exec': 0x4, 'input_signal_active_time_ns': clock_period_ns*5},
#        {'input_data': 0x00123245,      'expected_data': 0x00,     'expected_cmd_exec': 0x0, 'input_signal_active_time_ns': clock_period_ns},
        {'input_data': 0x00123245,      'expected_data': 0x00,     'expected_cmd_exec': 0x0, 'input_signal_active_time_ns': clock_period_ns*2},
#        {'input_data': 0x00000001,      'expected_data': 0x00,     'expected_cmd_exec': 0x1, 'input_signal_active_time_ns': clock_period_ns},
        {'input_data': 0x00000001,      'expected_data': 0x00,     'expected_cmd_exec': 0x1, 'input_signal_active_time_ns': clock_period_ns*2},
        {'input_data': 0x00000001,      'expected_data': 0x00,     'expected_cmd_exec': 0x1, 'input_signal_active_time_ns': clock_period_ns*3},
#        {'input_data': 0x00000002,      'expected_data': 0x00,     'expected_cmd_exec': 0x2, 'input_signal_active_time_ns': clock_period_ns},
        {'input_data': 0x00000002,      'expected_data': 0x00,     'expected_cmd_exec': 0x2, 'input_signal_active_time_ns': clock_period_ns*2},
        {'input_data': 0x00000002,      'expected_data': 0x00,     'expected_cmd_exec': 0x2, 'input_signal_active_time_ns': clock_period_ns*3},
#        {'input_data': 0x00000008,      'expected_data': 0x00,     'expected_cmd_exec': 0x80, 'input_signal_active_time_ns': clock_period_ns},
        {'input_data': 0x00000008,      'expected_data': 0x00,     'expected_cmd_exec': 0x80, 'input_signal_active_time_ns': clock_period_ns*2},
        {'input_data': 0x00000008,      'expected_data': 0x00,     'expected_cmd_exec': 0x80, 'input_signal_active_time_ns': clock_period_ns*3},
        ]


    clock = Clock(dut.i_clk, clock_period_ns, units="ns")  # Create a 10us period clock on port clk
    cocotb.start_soon(clock.start())  # Start the clock

    await reset_dut(dut.i_reset, 2*clock_period_ns, 'ns')

    dut.i_rx_dv.value = 0
    dut.i_rx_data.value = 0
    await Timer(clock_period_ns*2, units='ns') ## to differentiate different test cases

    case_counter = 0
    for case in test_cases:
        dut._log.info(f"{case_counter}: testing with input data: 0x{case['input_data']:0X} maintaining it active for {case['input_signal_active_time_ns']} ns")
        cocotb.start_soon(simulate_input(dut=dut, i_rx_data=case['input_data'], active_time_ns=case['input_signal_active_time_ns']))

        # The circuit takes 2 clock cycles to process its input
        await Timer(3*clock_period_ns, units='ns')
        assert dut.o_rx_cmd_data.value == case['expected_data'], f"data was not the expected {case['expected_data']} != {dut.o_rx_cmd_data.value.integer}"
        assert dut.o_cmd_exec_start.value == case['expected_cmd_exec'], f"decoded command was not detected! {case['expected_cmd_exec']:0X} != {dut.o_cmd_exec_start.value.integer:0X}"
        dut._log.info(f"{case_counter}: o_rx_cmd_data (0x{dut.o_rx_cmd_data.value.integer:X}) & o_cmd_exec_start (0x{dut.o_cmd_exec_start.value.integer:X}) ok!")

        # Also, the outputs must be high during 2 clock cycles, check if the next clock cycle the output is high
        await Timer(clock_period_ns, units='ns')
        if case['expected_cmd_exec'] != 0:
            assert dut.o_rx_cmd_data.value == case['expected_data'], f"output data do not long 2 clock cycles! {case['expected_data']} != {dut.o_rx_cmd_data.value.integer}"
            assert dut.o_cmd_exec_start.value == case['expected_cmd_exec'], f"output command do not long 2 clock cycles! {case['expected_cmd_exec']:0X} != {dut.o_cmd_exec_start.value.integer:0X}"
            dut._log.info(f"{case_counter}: o_rx_cmd_data (0x{dut.o_rx_cmd_data.value.integer:X}) & o_cmd_exec_start (0x{dut.o_cmd_exec_start.value.integer:X}) ok after 1 clock cycle!")

            # But, the `o_cmd_exec_start` must be high *only* during 2 clock cycles, because it can trigger more unexpected things
            # if its active more time
            await Timer(clock_period_ns, units='ns')
            assert dut.o_cmd_exec_start.value == 0, f"output command was not cleared after 2 clock cycles! {0} != {dut.o_cmd_exec_start.value.integer:0X}"
            dut._log.info(f"{case_counter}: o_cmd_exec_start cleared after 2 clock cycles!")

        case_counter += 1

        await Timer(clock_period_ns, units='ns') ## to differentiate different test cases
    
    await Timer(10*clock_period_ns, units='ns') ## to have time to analyze the waveform after the last case