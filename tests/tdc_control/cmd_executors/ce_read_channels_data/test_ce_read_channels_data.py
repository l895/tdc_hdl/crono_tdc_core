from cmath import exp
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, Join, with_timeout
import cocotb
from cocotb.utils import get_sim_time
import math

from tb_tools.utils import reset_dut, block_until_signal
from tb_tools.ce_read_channels_data_utils import data_buffers_validator, simulate_channels_ram

# parameters used in this simulation
TDC_CHANNEL_DATA_REG_WIDTH=20
CHANNELS_AVAILABLE_QTY=3

#TX_BUFFER_BYTES_WIDTH=16 # available TX buffer
TX_BUFFER_BYTES_WIDTH=13 # available TX buffer
DATA_BUFFER_SENT_O_TX_VALID=TX_BUFFER_BYTES_WIDTH*8#-1 # value set in o_tx_data_valid when a data buffer is sent

CLOCK_PERIOD_NS=10

async def simulate_arbiter(dut, allowed_delay_time_ns):
    """ The arbiter is the module that will do a handshake with o_tx_active and i_tx_allowed signals."""
    dut._log.info('arbiter simulator starting...')
    while dut.o_tx_active.value != 1:
        await Timer(CLOCK_PERIOD_NS, 'ns')
    dut._log.info(f"arbiter: dut set o_tx_active flag!")

    await Timer(allowed_delay_time_ns, 'ns')
    dut.i_tx_allowed.value = 1
    dut._log.info(f"arbiter: access to tx buffer was granted to dut!")

    while dut.o_tx_active.value == 1:
        await Timer(CLOCK_PERIOD_NS, 'ns')

    dut._log.info(f"arbiter: dut released tx buffer!")
    dut.i_tx_allowed.value = 0


async def simulate_start_signal(dut, active_time_ns):
    """ Simulate that some external logic is controlling the start signal, activating a given time."""
    dut._log.info('start signal simulator starting...')
    dut.i_start.value = 1 
    await Timer(active_time_ns, 'ns')
    dut.i_start.value = 0


async def test_cmd_execution(dut, available_channels, channels_data):
    """
    Test READ_CHANNELS_DATA command execution with a given set of available
    channels and channels data.
    This routine tests all the possible combination of enabled channels
    with the given inputs, validating the output data of the DUT.

    Parameters
    ----------
    available_channels : int
        Bit-mask that indicates which channels are available to the DUT.

    channels_data : dict
        Dictionary with the data stored in each channel.
        {
            0: <channel 0 array with the values stored in RAM>,
            1: <channel 1 array with the values stored in RAM>,
            ...
        }
    """
    clock = Clock(dut.i_clk, CLOCK_PERIOD_NS, units="ns")  # Create a 10us period clock on port clk
    cocotb.start_soon(clock.start())  # Start the clock

    await reset_dut(dut.i_reset, 2*CLOCK_PERIOD_NS, 'ns')
    dut.i_start.value = 0
    await Timer(CLOCK_PERIOD_NS*2, 'ns')
    await RisingEdge(dut.i_clk)

    # start with this inputs clean
    dut.i_tx_data_done.value = 0
    dut.i_tx_allowed.value = 0
    dut.i_dready.value = 0
    dut.i_read_chn_data.value = 0x0
    dut.i_read_chn_lwa.value = 0
    
    cocotb.start_soon(simulate_channels_ram(dut, channels_data))

    dut.i_channels_available.value = available_channels

    case_counter = 0
    # For each time windows that will be tested, test each combination of channels enabled
    for channels_enabled in range(2**CHANNELS_AVAILABLE_QTY):
        if channels_enabled & available_channels != 0: ## only cases where there are enabled channels available
            dut._log.info(f"\n/*---------- STARTING CASE {case_counter} ----------*/")
            # check which channels are enabled and build a dictionary with the RAM data of each enabled and available channel
            channels_ram = dict()
            for ch in range(CHANNELS_AVAILABLE_QTY):
                if (((1 << ch) & channels_enabled & available_channels) != 0) and (ch in list(channels_data.keys())):
                    channels_ram[ch] = channels_data[ch]

            data_buffer_validator_task = cocotb.start_soon(data_buffers_validator(dut=dut, channels_ram=channels_ram, clock_period_ns=CLOCK_PERIOD_NS, channel_data_reg_width=TDC_CHANNEL_DATA_REG_WIDTH, tx_buffer_width_bytes=TX_BUFFER_BYTES_WIDTH, expected_o_tx_data_valid=DATA_BUFFER_SENT_O_TX_VALID))
            dut._log.info(f"{case_counter}: dut: i_enabled_channels => 0b{channels_enabled:03b}, i_channels_available => 0b{available_channels:03b}, channels_ram => {channels_ram}")

            ## STATE_IDLE
            # set input values (channels enabled) and active start signal
            dut.i_channels_enabled.value = channels_enabled
            cocotb.start_soon(simulate_start_signal(dut, 2*CLOCK_PERIOD_NS))

            ## STATE_WAIT_UNTIL_CHANNELS_ARE_READY: here the DUT waits until all enabled channels are ready (can happen that the system is making a measure)
            await Timer(CLOCK_PERIOD_NS*3, 'ns')
            dut.i_dready.value = available_channels & channels_enabled
            await Timer(CLOCK_PERIOD_NS, 'ns')

            cocotb.start_soon(simulate_arbiter(dut, allowed_delay_time_ns=CLOCK_PERIOD_NS))
            await Timer(CLOCK_PERIOD_NS, 'ns')
            assert dut.o_tx_active.value == 1, f"{case_counter}: o_tx_active was not set by executor!"

            await with_timeout(Join(data_buffer_validator_task), timeout_time=100, timeout_unit='us') # wait until data buffer validator task ends

            wait_until_o_tx_active_falls = cocotb.start_soon(block_until_signal(dut.o_tx_active, 0, CLOCK_PERIOD_NS))
            await with_timeout(Join(wait_until_o_tx_active_falls), timeout_time=10, timeout_unit='us')

            dut._log.info(f"ending case {case_counter}...\n")
            case_counter += 1



@cocotb.test()
async def test_cmd_execution_one_buffer_all_channels_available(dut):
    """
    Test if the executor sends the correct data when is activated. 
    """

    available_channels = 0b111

    channels_data = {
        0: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
        1: [6, 5, 9, 1, 3],
        2: [9, 1, 10, 100, 1000, 10000, 100000],
    }
    
    await test_cmd_execution(dut, available_channels, channels_data)


@cocotb.test()
async def test_cmd_execution_one_buffer_some_channels_available(dut):
    """
    Test if the executor sends the correct data when is activated. 
    """

    available_channels = 0b101

    channels_data = {
        0: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
        1: [8, 5, 9, 1, 3, 3, 2, 1, 35, 64, 14],
        2: [9, 1, 10, 100, 1000, 10000, 100000],
    }
    
    await test_cmd_execution(dut, available_channels, channels_data)


@cocotb.test()
async def test_cmd_execution_one_buffer_one_channel_available(dut):
    """
    Test if the executor sends the correct data when is activated. 
    """

    available_channels = 0b10

    channels_data = {
        0: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
        1: [8, 5, 9, 1, 3, 3, 2, 1, 35, 64, 14],
        2: [9, 1, 10, 100, 1000, 10000, 100000],
    }
    
    await test_cmd_execution(dut, available_channels, channels_data)


@cocotb.test()
async def test_cmd_execution_multiple_buffers_all_channels_available(dut):
    """
    Test if the executor sends the correct data when is activated. 
    """

    available_channels = 0b111

    channels_data = {
        0: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21],
        1: [8, 5, 9, 1, 3, 3, 2, 1, 35, 64, 14, 1251, 124, 6661, 123, 51, 123],
        2: [9, 1, 10, 100, 1000, 10000, 100000, 123, 511, 12521, 519, 5151, 144, 9191, 12, 21415, 5151, 519, 5141, 1313, 151, 1551],
    }
    
    await test_cmd_execution(dut, available_channels, channels_data)


@cocotb.test()
async def test_cmd_execution_multiple_buffer_some_channels_available(dut):
    """
    Test if the executor sends the correct data when is activated. 
    """

    available_channels = 0b101

    channels_data = {
        0: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21],
        1: [8, 5, 9, 1, 3, 3, 2, 1, 35, 64, 14, 1251, 124, 6661, 123, 51, 123],
        2: [9, 1, 10, 100, 1000, 10000, 100000, 123, 511, 12521, 519, 5151, 144, 9191, 12, 21415, 5151, 519, 5141, 1313, 151, 1551],
    }
    
    await test_cmd_execution(dut, available_channels, channels_data)


@cocotb.test()
async def test_cmd_execution_multiple_buffer_one_channel_available(dut):
    """
    Test if the executor sends the correct data when is activated. 
    """

    available_channels = 0b10

    channels_data = {
        0: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21],
        1: [8, 5, 9, 1, 3, 3, 2, 1, 35, 64, 14, 1251, 124, 6661, 123, 51, 123],
        2: [9, 1, 10, 100, 1000, 10000, 100000, 123, 511, 12521, 519, 5151, 144, 9191, 12, 21415, 5151, 519, 5141, 1313, 151, 1551],
    }
    
    await test_cmd_execution(dut, available_channels, channels_data)