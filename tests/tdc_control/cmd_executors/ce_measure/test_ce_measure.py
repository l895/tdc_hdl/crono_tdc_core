from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge
import cocotb
from cocotb.utils import get_sim_time

from tb_tools.utils import reset_dut

MAX_CHANNELS_QTY=32
CHANNELS_AVAILABLE_QTY=3
CLOCK_PERIOD_NS = 10

async def simulate_arbiter(dut, allowed_delay_time_ns):
    """ The arbiter is the module that will do a handshake with o_tx_active and i_tx_allowed signals."""
    while dut.o_tx_active.value != 1:
        await Timer(CLOCK_PERIOD_NS, 'ns')
    dut._log.info(f"arbiter: dut set o_tx_active flag!")

    await Timer(allowed_delay_time_ns, 'ns')
    dut.i_tx_allowed.value = 1
    dut._log.info(f"arbiter: access to tx buffer was granted to dut!")

    while dut.o_tx_active.value == 1:
        await Timer(CLOCK_PERIOD_NS, 'ns')

    dut._log.info(f"arbiter: dut released tx buffer!")
    dut.i_tx_allowed.value = 0


async def simulate_start_signal(dut, active_time_ns):
    """ Simulate that some external logic is controlling the start signal, activating a given time."""
    dut.i_start.value = 1 
    await Timer(active_time_ns, 'ns')
    dut.i_start.value = 0


@cocotb.test()
async def test_cmd_execution_no_enabled_channels(dut):
    """
    Test if the executor sends the correct data when is activated and no available channel
    is enabled.
    """

    test_cases = [
        {"time_windows_us": 10,     "available_channels": 0b111},
        {"time_windows_us": 100,   "available_channels": 0b101},
    ]

    clock = Clock(dut.i_clk, CLOCK_PERIOD_NS, units="ns")  # Create a 10us period clock on port clk
    cocotb.start_soon(clock.start())  # Start the clock

    await reset_dut(dut.i_reset, 2*CLOCK_PERIOD_NS, 'ns')
    dut.i_start.value = 0
    await Timer(CLOCK_PERIOD_NS*2, 'ns')
    await RisingEdge(dut.i_clk)

    # start with this inputs clean
    dut.i_tx_data_done.value = 0
    dut.i_tx_allowed.value = 0

    for case in test_cases:
        dut.i_channels_available.value = case['available_channels']
        # for each time windows that will be tested, test each combination of channels enabled
        for channels_enabled in range(2**CHANNELS_AVAILABLE_QTY):
            if channels_enabled & case['available_channels'] == 0: ## only cases where there are not enabled channels available
                dut._log.info(f"dut: i_time_to_count_us: {case['time_windows_us']:10} us, i_enabled_channels => 0b{channels_enabled:03b}, i_channels_available => 0b{case['available_channels']:03b}")
                ## STATE_IDLE
                # set input values (channels enabled and time windows) and active start signal
                dut.i_channels_enabled.value = channels_enabled
                dut.i_time_to_count_us.value = case['time_windows_us'] 
                cocotb.start_soon(simulate_start_signal(dut, 2*CLOCK_PERIOD_NS))
                await Timer(CLOCK_PERIOD_NS, 'ns')

                ## STATE_NO_CHANNELS_ENABLED_MSG: here the DUT send a msg saying that there are not enabled channels available
                cocotb.start_soon(simulate_arbiter(dut, allowed_delay_time_ns=CLOCK_PERIOD_NS))
                await Timer(2*CLOCK_PERIOD_NS+round(CLOCK_PERIOD_NS/5), 'ns')
                assert dut.o_tx_active.value == 1, "o_tx_active was not set by executor!"
                
                while dut.i_tx_allowed.value.integer != 1:
                    await Timer(CLOCK_PERIOD_NS, 'ns')
                    dut._log.info(f"dut: waiting until arbiter gives him control of tx buffer (fsm: {dut.fsm_state.value})")

                dut._log.info(f"dut: arbiter granted access to buffer to dut! (fsm: {dut.fsm_state.value})")

                assert dut.o_tx_data_valid.value.integer == 40, f"data valid was not correct! {dut.o_tx_data_valid.value.integer} != 40"

                expected_confirmation_msg = b"noena"
                assert dut.o_tx_data.value.buff[-5:] == expected_confirmation_msg, f"confirmation message was not correct! {dut.o_tx_data.value.buff[dut.o_tx_data_valid.value.integer-1:0]} != {expected_confirmation_msg}"
                dut._log.info(f"dut: message received from dut: {dut.o_tx_data.value.buff}")

                await Timer(CLOCK_PERIOD_NS, 'ns')
                dut.i_tx_data_done.value = 1
                await Timer(CLOCK_PERIOD_NS, 'ns')
                dut.i_tx_data_done.value = 0


@cocotb.test()
async def test_cmd_execution_enabled_channels(dut):
    """
    Test if the executor sends the correct data when is activated. 
    Also check that he output 
    """
    test_cases = [
        {"time_windows_us": 10,     "available_channels": 0b111},
        {"time_windows_us": 100,   "available_channels": 0b101},
    ]

    clock = Clock(dut.i_clk, CLOCK_PERIOD_NS, units="ns")  # Create a 10us period clock on port clk
    cocotb.start_soon(clock.start())  # Start the clock

    await reset_dut(dut.i_reset, 2*CLOCK_PERIOD_NS, 'ns')
    dut.i_start.value = 0
    await Timer(CLOCK_PERIOD_NS*2, 'ns')
    await RisingEdge(dut.i_clk)

    # start with this inputs clean
    dut.i_tx_data_done.value = 0
    dut.i_tx_allowed.value = 0

    for case in test_cases:
        dut.i_channels_available.value = case['available_channels']
        # for each time windows that will be tested, test each combination of channels enabled
        for channels_enabled in range(2**CHANNELS_AVAILABLE_QTY):
            if channels_enabled & case['available_channels'] != 0: ## only cases where there are enabled channels available
                dut._log.info(f"dut: i_time_to_count_us: {case['time_windows_us']:10} us, i_enabled_channels => 0b{channels_enabled:03b}, i_channels_available => 0b{case['available_channels']:03b}")
                ## STATE_IDLE
                # set input values (channels enabled and time windows) and active start signal
                dut.i_channels_enabled.value = channels_enabled
                dut.i_time_to_count_us.value = case['time_windows_us'] 
                cocotb.start_soon(simulate_start_signal(dut, 2*CLOCK_PERIOD_NS))

                ## STATE_ENABLE_CHANNELS: here the DUT enable the channels that must start the measuring process
                await Timer(CLOCK_PERIOD_NS*3, 'ns')
                possible_channels = channels_enabled & case['available_channels']
                assert dut.o_chn_ena.value.integer == channels_enabled & case['available_channels'], f"channels were not enabled by DUT! {dut.o_chn_ena.value.integer} != {possible_channels}"

                ## STATE_WAIT_UNTIL_TIME_OVER: wait until timer is done and check if the time that passed is correct
                init_time_us = get_sim_time('us')
                await RisingEdge(dut.w_timer_done)
                time_windows_calculated_us = get_sim_time('us')-init_time_us
                assert case['time_windows_us']*1.02 > time_windows_calculated_us, f"calculated time windows is beyond error, calculated: {time_windows_calculated_us} us, max expected: {case['time_windows_us']*1.02} us"
                assert case['time_windows_us']*0.98 < time_windows_calculated_us, f"calculated time windows is beyond error, calculated: {time_windows_calculated_us} us, min expected: {case['time_windows_us']*0.98} us"
                dut._log.info(f'dut: time windows accomplished! approx {time_windows_calculated_us} us of measuring (fsm: {dut.fsm_state.value})')

                ## STATE_DISABLE_CHANNELS: now the fsm is waiting until the channels are ready, wait one clock cycle and set i_dready to high
                await Timer(CLOCK_PERIOD_NS, 'ns')
                dut.i_dready.value = channels_enabled

                ## STATE_STORE_DATA_IN_TX_BUFF: now the fsm is waiting until the tx buffer is gived to him, wait one clock cycle 
                ## and set i_tx_allowed to high to simulate that some logic (arbiter) is giving the control of the buffer to him
                cocotb.start_soon(simulate_arbiter(dut, allowed_delay_time_ns=CLOCK_PERIOD_NS))
                await Timer(2*CLOCK_PERIOD_NS+round(CLOCK_PERIOD_NS/5), 'ns')
                assert dut.o_tx_active.value == 1, "o_tx_active was not set by executor!"
                
                while dut.i_tx_allowed.value.integer != 1:
                    await Timer(CLOCK_PERIOD_NS, 'ns')
                    dut._log.info(f"dut: waiting until arbiter gives him control of tx buffer (fsm: {dut.fsm_state.value})")

                dut._log.info(f"dut: arbiter granted access to buffer to dut! (fsm: {dut.fsm_state.value})")
                while dut.o_tx_data_valid.value.integer == 0:
                    await Timer(CLOCK_PERIOD_NS, 'ns')

                ## STATE_SEND_CONFIRMATION_MSG: now the fsm will store a confirmation msg in output buffer.
                ## Simulate that the transmission takes only 1 clock cycle.
                assert dut.o_tx_data_valid.value.integer == 88, f"data valid was not correct! {dut.o_tx_data_valid.value.integer} != 88"
                expected_enabled_mask = case['available_channels'] & channels_enabled

                # convert the mask (in number) to ascii characters
                ascii_enabled_mask = b''
                for i in range(7,-1,-1):
                    ascii_val = ((expected_enabled_mask & (0xF << i*8)) >> i*8)+48
                    ascii_enabled_mask = ascii_enabled_mask + ascii_val.to_bytes(1,'big')

                expected_confirmation_msg = b"ena"+ ascii_enabled_mask
                assert dut.o_tx_data.value.buff[-11:] == expected_confirmation_msg, f"confirmation message was not correct! {dut.o_tx_data.value.buff[dut.o_tx_data_valid.value.integer-1:0]} != {expected_confirmation_msg}"
                dut._log.info(f"dut: message received from dut: {dut.o_tx_data.value.buff}")

                await Timer(CLOCK_PERIOD_NS, 'ns')
                dut.i_tx_data_done.value = 1
                await Timer(CLOCK_PERIOD_NS, 'ns')
                dut.i_tx_data_done.value = 0
