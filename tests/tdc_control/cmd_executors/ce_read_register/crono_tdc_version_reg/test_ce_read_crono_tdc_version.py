from cocotb.clock import Clock
from cocotb.triggers import Timer
import cocotb

from tb_tools.tdc_commands import read_crono_tdc_version
from tb_tools.utils import reset_dut

CRONO_TDC_CORE_VERSION_PKG_REL_PATH='../../../../../src/crono_tdc_core_version_pkg.sv'

@cocotb.test()
async def test_cmd_execution(dut):
    """
    Test if the executor sends the correct data when is activated. 
    Also check that he output 
    """
    clock_period_ns = 10
    test_cases = [  {'i_tx_allowed_delay' : 0,                 'i_tx_data_done_time_ns': clock_period_ns*5},
                    {'i_tx_allowed_delay' : clock_period_ns,   'i_tx_data_done_time_ns': clock_period_ns*5},
                    {'i_tx_allowed_delay' : 2*clock_period_ns, 'i_tx_data_done_time_ns': clock_period_ns*5},
                    {'i_tx_allowed_delay' : 10*clock_period_ns,'i_tx_data_done_time_ns': clock_period_ns*5},
                    {'i_tx_allowed_delay' : 0,                 'i_tx_data_done_time_ns': clock_period_ns*50},
                    {'i_tx_allowed_delay' : clock_period_ns,   'i_tx_data_done_time_ns': clock_period_ns*50},
                    {'i_tx_allowed_delay' : 2*clock_period_ns, 'i_tx_data_done_time_ns': clock_period_ns*50},
                    {'i_tx_allowed_delay' : 10*clock_period_ns,'i_tx_data_done_time_ns': clock_period_ns*250},
                    {'i_tx_allowed_delay' : 0,                 'i_tx_data_done_time_ns': clock_period_ns*250},
                    {'i_tx_allowed_delay' : clock_period_ns,   'i_tx_data_done_time_ns': clock_period_ns*250},
                    {'i_tx_allowed_delay' : 2*clock_period_ns, 'i_tx_data_done_time_ns': clock_period_ns*250},
                    {'i_tx_allowed_delay' : 10*clock_period_ns,'i_tx_data_done_time_ns': clock_period_ns*250},
    ]
    crono_tdc_version = read_crono_tdc_version(CRONO_TDC_CORE_VERSION_PKG_REL_PATH)
    dut._log.info('Crono TDC version: {}'.format(crono_tdc_version))
    clock = Clock(dut.i_clk, clock_period_ns, units="ns")  # Create a 10us period clock on port clk
    cocotb.start_soon(clock.start())  # Start the clock

    await reset_dut(dut.i_reset, 2*clock_period_ns, 'ns')

    dut.i_reg_val.value = int.from_bytes(bytes(crono_tdc_version, 'ascii'), 'big') # mok register with crono tdc version
    dut.i_tx_data_done.value = 0

    for case in test_cases:
        dut.i_start.value = 0
        await Timer(clock_period_ns, units='ns')

        dut.i_start.value = 1
        await Timer(clock_period_ns, units='ns')
        dut.i_start.value = 0

        while dut.o_tx_active.value != 1:
            await Timer(clock_period_ns)
            dut._log.info('waiting until o_tx_active is set...')

        ## here we are in fill_data_buffer state
        await Timer(case['i_tx_allowed_delay'], units='ns')
        dut.i_tx_allowed.value = 1
        await Timer(clock_period_ns, units='ns')
        dut.i_tx_allowed.value = 0

        await Timer(case['i_tx_data_done_time_ns'], units='ns')

        assert dut.o_tx_active.value == 1, 'o_tx_active is not 1 when dut is using tx buffer'
        assert dut.o_tx_data_valid.value == len(crono_tdc_version)*8, 'o_tx_data_valid is not correct! {} != {}'.format(dut.o_tx_data_valid.value.integer,len(crono_tdc_version)*8)

        # transform binary number to string
        tx_data = dut.o_tx_data.value.integer.to_bytes(int(dut.o_tx_data.value.integer.bit_length()/8+1),'big').decode('utf-8')
        assert tx_data == crono_tdc_version, 'o_tx_data is not correct! {} != {}'.format(tx_data, crono_tdc_version)

        dut.i_tx_data_done.value = 1
        await Timer(clock_period_ns, units='ns')
        dut.i_tx_data_done.value = 0
        assert dut.o_tx_active.value == 0, 'o_tx_active is not reset after i_tx_data_done is reset'

        await Timer(clock_period_ns, units='ns') ## to differentiate different test cases