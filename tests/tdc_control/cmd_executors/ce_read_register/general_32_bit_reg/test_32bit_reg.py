from cocotb.clock import Clock
from cocotb.triggers import Timer
import cocotb

from tb_tools.utils import reset_dut

@cocotb.test()
async def test_cmd_execution(dut):
    """
    Test if the executor sends the correct data when is activated. 
    Also check that he output 
    """
    clock_period_ns = 10
    test_cases = [  {'reg_val': 0xFFFFFFFF, 'i_tx_allowed_delay' : 0,                 'i_tx_data_done_time_ns': clock_period_ns*5},
                    {'reg_val': 0xF2F0F45F, 'i_tx_allowed_delay' : clock_period_ns,   'i_tx_data_done_time_ns': clock_period_ns*5},
                    {'reg_val': 0x55555555, 'i_tx_allowed_delay' : 2*clock_period_ns, 'i_tx_data_done_time_ns': clock_period_ns*5},
                    {'reg_val': 0x200F00F1, 'i_tx_allowed_delay' : 10*clock_period_ns,'i_tx_data_done_time_ns': clock_period_ns*5},
                    {'reg_val': 0x1F004C0F, 'i_tx_allowed_delay' : 0,                 'i_tx_data_done_time_ns': clock_period_ns*50},
                    {'reg_val': 0x640CFB10, 'i_tx_allowed_delay' : clock_period_ns,   'i_tx_data_done_time_ns': clock_period_ns*50},
                    {'reg_val': 0x80F040F8, 'i_tx_allowed_delay' : 2*clock_period_ns, 'i_tx_data_done_time_ns': clock_period_ns*50},
                    {'reg_val': 0x51FC1021, 'i_tx_allowed_delay' : 10*clock_period_ns,'i_tx_data_done_time_ns': clock_period_ns*250},
                    {'reg_val': 0x59F8F4F2, 'i_tx_allowed_delay' : 0,                 'i_tx_data_done_time_ns': clock_period_ns*250},
                    {'reg_val': 0x11110000, 'i_tx_allowed_delay' : clock_period_ns,   'i_tx_data_done_time_ns': clock_period_ns*250},
                    {'reg_val': 0x00001111, 'i_tx_allowed_delay' : 2*clock_period_ns, 'i_tx_data_done_time_ns': clock_period_ns*250},
                    {'reg_val': 0x000FF000, 'i_tx_allowed_delay' : 10*clock_period_ns,'i_tx_data_done_time_ns': clock_period_ns*250},
    ]

    clock = Clock(dut.i_clk, clock_period_ns, units="ns")  # Create a 10us period clock on port clk
    cocotb.start_soon(clock.start())  # Start the clock

    await reset_dut(dut.i_reset, 2*clock_period_ns, 'ns')

    dut.i_tx_data_done.value = 0

    for case in test_cases:
        dut.i_reg_val.value = case['reg_val']
        dut.i_start.value = 0
        await Timer(clock_period_ns, units='ns')

        dut.i_start.value = 1
        await Timer(clock_period_ns, units='ns')
        dut.i_start.value = 0

        while dut.o_tx_active.value != 1:
            await Timer(clock_period_ns)
            dut._log.info('waiting until o_tx_active is set...')

        ## here we are in fill_data_buffer state
        await Timer(case['i_tx_allowed_delay'], units='ns')
        dut.i_tx_allowed.value = 1
        await Timer(clock_period_ns, units='ns')
        dut.i_tx_allowed.value = 0

        await Timer(case['i_tx_data_done_time_ns'], units='ns')

        assert dut.o_tx_active.value == 1, 'o_tx_active is not 1 when dut is using tx buffer'
        assert dut.o_tx_data_valid.value == 32, 'o_tx_data_valid is not correct! {} != {}'.format(dut.o_tx_data_valid.value.integer,32)
        assert dut.o_tx_data.value.integer == case['reg_val'], 'o_tx_data is not correct! {} != {}'.format(dut.o_tx_data.value.integer,case['reg_val'] )

        dut.i_tx_data_done.value = 1
        await Timer(clock_period_ns, units='ns')
        dut.i_tx_data_done.value = 0
        assert dut.o_tx_active.value == 0, 'o_tx_active is not reset after i_tx_data_done is reset'

        await Timer(clock_period_ns, units='ns') ## to differentiate different test cases