from cocotb.clock import Clock
from cocotb.triggers import Timer
import cocotb

from tb_tools.utils import reset_dut

RESET_VALUE=155
CLOCK_PERIOD_NS = 10

TEST_DATA = [   {'i_rx_cmd_data': 0x41, 'expected_o_reg_val' : 0x41,},
                {'i_rx_cmd_data': 0x42, 'expected_o_reg_val' : 0x42,},
                {'i_rx_cmd_data': 0x43, 'expected_o_reg_val' : 0x43,},
                {'i_rx_cmd_data': 0x44, 'expected_o_reg_val' : 0x44,},
                {'i_rx_cmd_data': 0x45, 'expected_o_reg_val' : 0x45,},
                {'i_rx_cmd_data': 0x46, 'expected_o_reg_val' : 0x46,},
                {'i_rx_cmd_data': 0x47, 'expected_o_reg_val' : 0x47,},
                {'i_rx_cmd_data': 0x48, 'expected_o_reg_val' : 0x48,},
                {'i_rx_cmd_data': 0x49, 'expected_o_reg_val' : 0x49,},
                {'i_rx_cmd_data': 0x4A, 'expected_o_reg_val' : 0x4A,},
                {'i_rx_cmd_data': 0x4B, 'expected_o_reg_val' : 0x4B,},
                {'i_rx_cmd_data': 0x4C, 'expected_o_reg_val' : 0x4C,},
                {'i_rx_cmd_data': 0x4D, 'expected_o_reg_val' : 0x4D,},
                {'i_rx_cmd_data': 0x4E, 'expected_o_reg_val' : 0x4E,},
                {'i_rx_cmd_data': 0x4F, 'expected_o_reg_val' : 0x4F,},
                {'i_rx_cmd_data': 0x50, 'expected_o_reg_val' : 0x50,},
                {'i_rx_cmd_data': 0x51, 'expected_o_reg_val' : 0x51,},
                {'i_rx_cmd_data': 0x52, 'expected_o_reg_val' : 0x52,},
                {'i_rx_cmd_data': 0x53, 'expected_o_reg_val' : 0x53,},
                {'i_rx_cmd_data': 0x54, 'expected_o_reg_val' : 0x54,},
                {'i_rx_cmd_data': 0x55, 'expected_o_reg_val' : 0x55,},
                {'i_rx_cmd_data': 0x56, 'expected_o_reg_val' : 0x56,},
                {'i_rx_cmd_data': 0x57, 'expected_o_reg_val' : 0x57,},
                {'i_rx_cmd_data': 0x58, 'expected_o_reg_val' : 0x58,},
                {'i_rx_cmd_data': 0x59, 'expected_o_reg_val' : 0x59,},
                {'i_rx_cmd_data': 0x5A, 'expected_o_reg_val' : 0x5A,},
                {'i_rx_cmd_data': 0x5B, 'expected_o_reg_val' : 0x5B,},
                {'i_rx_cmd_data': 0x5C, 'expected_o_reg_val' : 0x5C,},
                {'i_rx_cmd_data': 0x5D, 'expected_o_reg_val' : 0x5D,},
                {'i_rx_cmd_data': 0x5E, 'expected_o_reg_val' : 0x5E,},
                {'i_rx_cmd_data': 0x1015845E, 'expected_o_reg_val' : 0x1015845E,},
                {'i_rx_cmd_data': 0x99011245, 'expected_o_reg_val' : 0x99011245,},
    ]


@cocotb.test()
async def test_cmd_execution(dut):
    """ Test if the executor sends the correct data when is activated. """

    test_cases = [
        {"start_active_time_ns": CLOCK_PERIOD_NS*2}, ## start signal will need to be pressed at least 2 clock cycles to work!
        {"start_active_time_ns": CLOCK_PERIOD_NS*3},
        ]

    clock = Clock(dut.i_clk, CLOCK_PERIOD_NS, units="ns")  # Create a 10us period clock on port clk
    cocotb.start_soon(clock.start())  # Start the clock

    await reset_dut(dut.i_reset, 2*CLOCK_PERIOD_NS, 'ns')
    dut.i_start.value = 0
    dut.i_rx_cmd_data.value = 0
    await Timer(CLOCK_PERIOD_NS*2, 'ns')

    for case in test_cases:
        for data in TEST_DATA:
            dut._log.info(f"start_active_time_ns: {case['start_active_time_ns']}, i_rx_cmd_data => {data['i_rx_cmd_data']}")

            dut.i_start.value = 1
            dut.i_rx_cmd_data.value = data['i_rx_cmd_data']
            await Timer(case['start_active_time_ns'], 'ns')
            dut.i_start.value = 0
            dut.i_rx_cmd_data.value = 0

            dut._log.info(f"checking if reg val {dut.o_reg_val.value.integer} is correct")
            assert dut.o_reg_val.value.integer == data['expected_o_reg_val'], f"o_reg_val is different from expected {dut.o_reg_val.value.integer:X} != {data['expected_o_reg_val']}"
            await Timer(CLOCK_PERIOD_NS*2, 'ns')


@cocotb.test()
async def test_reset_value(dut):
    """ Test if the register have the reset value after a reset. """

    clock = Clock(dut.i_clk, CLOCK_PERIOD_NS, units="ns")  # Create a 10us period clock on port clk
    cocotb.start_soon(clock.start())  # Start the clock

    await reset_dut(dut.i_reset, 2*CLOCK_PERIOD_NS, 'ns')
    dut.i_start.value = 0
    dut.i_rx_cmd_data.value = 0

    await Timer(CLOCK_PERIOD_NS*2, 'ns')
    assert dut.o_reg_val.value.integer == RESET_VALUE, f"register do not have the reset value! {dut.o_reg_val.value.integer} != {RESET_VALUE}"
