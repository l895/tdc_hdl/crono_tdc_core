from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, Join, with_timeout
import cocotb
from math import floor

from tb_tools.tdc_commands import CMD_ID, read_crono_tdc_version, build_tdc_command
from tb_tools.utils import reset_dut
from tb_tools.ce_read_channels_data_utils import data_buffers_validator, simulate_channels_ram

# DUT Parameters for this testbench
CHANNELS_AVAILABLE_QTY = 3
AVAILABLE_CHANNELS=0b111
CFG_REG_INIT_VAL_ENABLED_CHN = 0b111

CFG_REG_INIT_VAL_TIME_WINDOWS_US = 10

TDC_CHANNEL_CLK_CYCLE_COUNT_WIDTH=13
TX_BUFFER_BYTES_WIDTH=16
TDC_DATA_REG_WIDTH=TDC_CHANNEL_CLK_CYCLE_COUNT_WIDTH+7
MAX_DATA_REGS_PER_FRAME=floor((TX_BUFFER_BYTES_WIDTH*8)/TDC_DATA_REG_WIDTH)
DATA_BUFFER_SENT_O_TX_VALID=TX_BUFFER_BYTES_WIDTH*8 # value set in o_tx_data_valid when a data buffer is sent

CLOCK_PERIOD_NS = 10

CRONO_TDC_CORE_VERSION_PKG_REL_PATH='../../../src/crono_tdc_core_version_pkg.sv'

async def simulate_input(dut, i_rx_data: int, active_time_ns: int):
    """
    Simulate `i_rx_dv` and `i_rx_data` signals putting some val and having them high
    some value.
    """
    dut.i_rx_dv.value = i_rx_data.bit_length()
    dut.i_rx_data.value = i_rx_data
    await Timer(active_time_ns, 'ns')
    dut.i_rx_dv.value = 0
    dut.i_rx_data.value = 0


async def test_read_reg_cmd(    dut, clock_period_ns: int, cmd_to_send: int,
                                expected_data_from_tdc: int, expected_data_from_tdc_width: int,
                                fake_transmision_time_ns: int):
    """
    Co-routine used to test a command that reads a register.

    Parameters
    ----------
    clock_period_ns : int
        System clock period.

    cmd_to_send : int
        The command that must be send to the dut. Must be in binary 
        format (because this, the type is integer).

    expected_data_from_tdc : int
        Is the output data that we expect from the TDC. Must be in binary 
        format (because this, the type is integer).

    expected_data_from_tdc_width : int
        Is the output width, in bits, tha the tdc_core_logic must present.

    fake_transmission_time_ns : int
        The routine will fake a transmission time for command that the dut is processing.
        Simulating that there is an external device trying to send the data. This is the
        time it task to the "fake device" send the data. 

    """
    dut.i_rx_dv.value = 0
    dut.i_rx_data.value = 0
    await Timer(clock_period_ns, units='ns') ## to differentiate different test cases

    dut.i_rx_data.value = cmd_to_send
    dut.i_rx_dv.value = cmd_to_send.bit_length()

    # The core tdc logic takes 4 clock cycles to process the command
    # we leave a little of "changui" here to be sure that the simulation is in the clock cycle
    # in which the write was done
    await Timer((clock_period_ns*4)+round(clock_period_ns/5), units='ns')
    assert dut.o_tx_data.value.integer == expected_data_from_tdc, 'command response is not the expected one! {} != {}'.format(dut.o_tx_data.value.integer, expected_data_from_tdc)
    assert dut.o_tx_data_valid.value.integer == expected_data_from_tdc_width, 'data valid field is not the expected one! {} != {}'.format(dut.o_tx_data_valid.value.integer, expected_data_from_tdc_width)

    # simulate a determined "transmission time" and then notify to the core logic that the
    # data has been transmitted successfully
    await RisingEdge(dut.i_clk) ## Sync with clock
    await Timer(fake_transmision_time_ns, units='ns') ## to differentiate different test cases
    dut.i_tx_data_done.value = 1
    await Timer(clock_period_ns, units='ns')
    dut.i_tx_data_done.value = 0
    
    # Note: sometimes the output could not be cleared until next two clock cycles after setting data done flag low
    await Timer(2*clock_period_ns, units='ns')
    
    # check if data has been cleared!
    assert dut.o_tx_data.value.integer == 0, 'data field is not cleared after notifying that send is done! {} != 0'.format(dut.o_tx_data.value.integer)
    assert dut.o_tx_data_valid.value.integer == 0, 'data valid field is not cleared after notifying that send is done! {} != 0'.format(dut.o_tx_dv.value.integer)

    dut._log.info('read command tested successfully!')


@cocotb.test()
async def test_read_crono_tdc_core_version_cmd(dut):
    """
    Test if the dut can decode the command `READ_CRONO_TDC_CORE_VERSION` and send the current
    version of the core as a response.
    """
    cmd_to_send = build_tdc_command(cmd_id = CMD_ID.READ_CRONO_TDC_CORE_VERSION, cmd_data = 0)
    crono_tdc_version = read_crono_tdc_version(CRONO_TDC_CORE_VERSION_PKG_REL_PATH)
    expected_data_from_tdc = int.from_bytes(bytes(crono_tdc_version, 'ascii'), 'big') 

    dut._log.info(  'Testing READ_CRONO_TDC_CORE_VERSION command \n' + \
                    'Crono TDC Core version: {}\n'.format(crono_tdc_version) + \
                    'Command to send: {:X}\n'.format(cmd_to_send) + \
                    'Bit-width of the command: {}\n'.format(cmd_to_send.bit_length()) + \
                    'Expected data from TDC: {:X}\n'.format(expected_data_from_tdc))

    clock_period_ns = 10
    test_cases = [
        {'transmission_time_ns': clock_period_ns,       'rest_time_ns': clock_period_ns },
        {'transmission_time_ns': clock_period_ns*2,     'rest_time_ns': clock_period_ns },
        {'transmission_time_ns': clock_period_ns*3,     'rest_time_ns': clock_period_ns },
        {'transmission_time_ns': clock_period_ns*5,     'rest_time_ns': clock_period_ns },
        {'transmission_time_ns': clock_period_ns*10,    'rest_time_ns': clock_period_ns },
        {'transmission_time_ns': clock_period_ns*50,    'rest_time_ns': clock_period_ns },
        {'transmission_time_ns': clock_period_ns*100,   'rest_time_ns': clock_period_ns },

        {'transmission_time_ns': clock_period_ns,       'rest_time_ns': clock_period_ns*2 },
        {'transmission_time_ns': clock_period_ns*2,     'rest_time_ns': clock_period_ns*2 },
        {'transmission_time_ns': clock_period_ns*3,     'rest_time_ns': clock_period_ns*2 },
        {'transmission_time_ns': clock_period_ns*5,     'rest_time_ns': clock_period_ns*2 },
        {'transmission_time_ns': clock_period_ns*10,    'rest_time_ns': clock_period_ns*2 },
        {'transmission_time_ns': clock_period_ns*50,    'rest_time_ns': clock_period_ns*2 },
        {'transmission_time_ns': clock_period_ns*100,   'rest_time_ns': clock_period_ns*2 },

        {'transmission_time_ns': clock_period_ns,       'rest_time_ns': clock_period_ns*10 },
        {'transmission_time_ns': clock_period_ns*2,     'rest_time_ns': clock_period_ns*10 },
        {'transmission_time_ns': clock_period_ns*3,     'rest_time_ns': clock_period_ns*10 },
        {'transmission_time_ns': clock_period_ns*5,     'rest_time_ns': clock_period_ns*10 },
        {'transmission_time_ns': clock_period_ns*10,    'rest_time_ns': clock_period_ns*10 },
        {'transmission_time_ns': clock_period_ns*50,    'rest_time_ns': clock_period_ns*10 },
        {'transmission_time_ns': clock_period_ns*100,   'rest_time_ns': clock_period_ns*10 }

    ]

    clock = Clock(dut.i_clk, clock_period_ns, units="ns")  # Create a 10ns period clock on port clk
    cocotb.start_soon(clock.start())  # Start the clock

    await reset_dut(dut.i_reset, 2*clock_period_ns, 'ns')

    for case in test_cases:
        dut._log.info('testing READ_CRONO_TDC_CORE_VERSION command with a transmission time of {} ns'.format(case['transmission_time_ns']))
        await test_read_reg_cmd(    dut = dut, clock_period_ns = clock_period_ns,
                                    cmd_to_send = cmd_to_send, 
                                    expected_data_from_tdc = expected_data_from_tdc,
                                    expected_data_from_tdc_width = len(crono_tdc_version)*8,
                                    fake_transmision_time_ns = case['transmission_time_ns']
            )
        dut._log.info('leaving a rest time of {} ns before next test case...'.format(case['rest_time_ns']))
        await Timer(case['rest_time_ns'], 'ns')


@cocotb.test()
async def test_read_time_windows_cmd(dut):
    """
    Test if the dut can decode the command `READ_TIME_WINDOWS_US` and send the current
    current value of this window as a response..
    Note: this assumes that the current value is the initial one.
    """
    cmd_to_send = build_tdc_command(cmd_id = CMD_ID.READ_TIME_WINDOWS_US, cmd_data = 0)
    expected_data_from_tdc = CFG_REG_INIT_VAL_TIME_WINDOWS_US

    dut._log.info(  'Testing READ_TIME_WINDOWS_US command \n' + \
                    'Command to send: {:X}\n'.format(cmd_to_send) + \
                    'Bit-width of the command: {}\n'.format(cmd_to_send.bit_length()) + \
                    'Expected data from TDC: {:X}\n'.format(expected_data_from_tdc) + \
                    'Expected data bit-width from TDC: 32')

    clock_period_ns = 10
    test_cases = [
        {'transmission_time_ns': clock_period_ns,       'rest_time_ns': clock_period_ns },
        {'transmission_time_ns': clock_period_ns*2,     'rest_time_ns': clock_period_ns },
        {'transmission_time_ns': clock_period_ns*3,     'rest_time_ns': clock_period_ns },
        {'transmission_time_ns': clock_period_ns*5,     'rest_time_ns': clock_period_ns },
        {'transmission_time_ns': clock_period_ns*10,    'rest_time_ns': clock_period_ns },
        {'transmission_time_ns': clock_period_ns*50,    'rest_time_ns': clock_period_ns },
        {'transmission_time_ns': clock_period_ns*100,   'rest_time_ns': clock_period_ns },

        {'transmission_time_ns': clock_period_ns,       'rest_time_ns': clock_period_ns*2 },
        {'transmission_time_ns': clock_period_ns*2,     'rest_time_ns': clock_period_ns*2 },
        {'transmission_time_ns': clock_period_ns*3,     'rest_time_ns': clock_period_ns*2 },
        {'transmission_time_ns': clock_period_ns*5,     'rest_time_ns': clock_period_ns*2 },
        {'transmission_time_ns': clock_period_ns*10,    'rest_time_ns': clock_period_ns*2 },
        {'transmission_time_ns': clock_period_ns*50,    'rest_time_ns': clock_period_ns*2 },
        {'transmission_time_ns': clock_period_ns*100,   'rest_time_ns': clock_period_ns*2 },

        {'transmission_time_ns': clock_period_ns,       'rest_time_ns': clock_period_ns*10 },
        {'transmission_time_ns': clock_period_ns*2,     'rest_time_ns': clock_period_ns*10 },
        {'transmission_time_ns': clock_period_ns*3,     'rest_time_ns': clock_period_ns*10 },
        {'transmission_time_ns': clock_period_ns*5,     'rest_time_ns': clock_period_ns*10 },
        {'transmission_time_ns': clock_period_ns*10,    'rest_time_ns': clock_period_ns*10 },
        {'transmission_time_ns': clock_period_ns*50,    'rest_time_ns': clock_period_ns*10 },
        {'transmission_time_ns': clock_period_ns*100,   'rest_time_ns': clock_period_ns*10 }

    ]

    clock = Clock(dut.i_clk, clock_period_ns, units="ns")  # Create a 10ns period clock on port clk
    cocotb.start_soon(clock.start())  # Start the clock

    await reset_dut(dut.i_reset, 2*clock_period_ns, 'ns')

    for case in test_cases:
        dut._log.info('testing READ_CRONO_TDC_CORE command with a transmission time of {} ns'.format(case['transmission_time_ns']))
        await test_read_reg_cmd(    dut = dut, clock_period_ns = clock_period_ns,
                                    cmd_to_send = cmd_to_send, 
                                    expected_data_from_tdc = expected_data_from_tdc,
                                    expected_data_from_tdc_width = 32,
                                    fake_transmision_time_ns = case['transmission_time_ns']
            )
        dut._log.info('leaving a rest time of {} ns before next test case...'.format(case['rest_time_ns']))
        await Timer(case['rest_time_ns'], 'ns')


async def test_write_reg_cmd(dut, register, value_to_write: int, clock_period_ns: int, cmd_id: int):
    """
    Co-routine used to test a command that writes a register.

    Parameters
    ----------
    register:
        The register to be written. In the DUT, we have a wire that 
        exposes the register per se.

    value_to_write : int
        Value that will be written into the register.

    clock_period_ns : int
        System clock period.

    cmd_id : int
        The command ID of the command that will be tested.

    """
    cmd_to_send = build_tdc_command(cmd_id = cmd_id, cmd_data = value_to_write)
    dut._log.info(  f'Testing command ID {cmd_id} \n' + \
                    f'Command to send: 0x{cmd_to_send:X}\n' + \
                    f'Bit-width of the command: {cmd_to_send.bit_length()}' )

    cocotb.start_soon(simulate_input(dut, i_rx_data=cmd_to_send, active_time_ns=2*clock_period_ns))

    # The core tdc logic takes 4 clock cycles to process the command and effectivize the write
    # we leave a little of "changui" here to be sure that the simulation is in the clock cycle
    # in which the write was done
    await Timer(clock_period_ns*4+round(clock_period_ns/5), units='ns') 
    assert register.value.integer == value_to_write, f'register value is not the expected one! {register.value.integer} != {value_to_write}'

    dut.i_rx_dv.value = 0
    dut.i_rx_data.value = 0

    await RisingEdge(dut.i_clk)


@cocotb.test()
async def test_write_time_windows_cmd(dut):
    """
    Test if the dut can decode the command `WRITE_TIME_WINDOWS_US` and store the value in
    the TIME_WINDOWS_US register.
    """

    clock_period_ns = 10
    test_cases = [
        {'value_to_write': 0x00       ,'rest_time_ns': clock_period_ns  },
        {'value_to_write': 0x01       ,'rest_time_ns': clock_period_ns  },
        {'value_to_write': 0x02       ,'rest_time_ns': clock_period_ns  },
        {'value_to_write': 0x53       ,'rest_time_ns': clock_period_ns  },
        {'value_to_write': 0x86       ,'rest_time_ns': clock_period_ns  },
        {'value_to_write': 0x10F      ,'rest_time_ns': clock_period_ns  },
        {'value_to_write': 0xFFF8     ,'rest_time_ns': clock_period_ns  },
        {'value_to_write': 0x2320     ,'rest_time_ns': clock_period_ns  },
        {'value_to_write': 0xA012D2   ,'rest_time_ns': clock_period_ns  },
        {'value_to_write': 0x10A012D2 ,'rest_time_ns': clock_period_ns  },
        {'value_to_write': 0x95A01202 ,'rest_time_ns': clock_period_ns  },
        {'value_to_write': 0x21A70254 ,'rest_time_ns': clock_period_ns  },
        {'value_to_write': 0x555555   ,'rest_time_ns': clock_period_ns  },
        {'value_to_write': 0x512505   ,'rest_time_ns': clock_period_ns  },
        {'value_to_write': 0x521076   ,'rest_time_ns': clock_period_ns  },

        {'value_to_write': 0x00       ,'rest_time_ns': round(clock_period_ns/2)  },
        {'value_to_write': 0x01       ,'rest_time_ns': round(clock_period_ns/2)  },
        {'value_to_write': 0x02       ,'rest_time_ns': round(clock_period_ns/2)  },
        {'value_to_write': 0x53       ,'rest_time_ns': round(clock_period_ns/2)  },
        {'value_to_write': 0x86       ,'rest_time_ns': round(clock_period_ns/2)  },
        {'value_to_write': 0x10F      ,'rest_time_ns': round(clock_period_ns/2)  },
        {'value_to_write': 0xFFF8     ,'rest_time_ns': round(clock_period_ns/2)  },
        {'value_to_write': 0x2320     ,'rest_time_ns': round(clock_period_ns/2)  },
        {'value_to_write': 0xA012D2   ,'rest_time_ns': round(clock_period_ns/2)  },
        {'value_to_write': 0x10A012D2 ,'rest_time_ns': round(clock_period_ns/2)  },
        {'value_to_write': 0x95A01202 ,'rest_time_ns': round(clock_period_ns/2)  },
        {'value_to_write': 0x21A70254 ,'rest_time_ns': round(clock_period_ns/2)  },
        {'value_to_write': 0x555555   ,'rest_time_ns': round(clock_period_ns/2)  },
        {'value_to_write': 0x512505   ,'rest_time_ns': round(clock_period_ns/2)  },
        {'value_to_write': 0x521076   ,'rest_time_ns': round(clock_period_ns/2)  },
    ]

    clock = Clock(dut.i_clk, clock_period_ns, units="ns")  # Create a 10ns period clock on port clk
    cocotb.start_soon(clock.start())  # Start the clock

    await reset_dut(dut.i_reset, 2*clock_period_ns, 'ns')

    dut.i_rx_dv.value = 0
    dut.i_rx_data.value = 0
    await Timer(clock_period_ns*2, units='ns')

    for case in test_cases:
        await test_write_reg_cmd(   dut = dut, register = dut.cmd_executor.w_time_windows_us, 
                                    clock_period_ns = clock_period_ns,
                                    cmd_id = CMD_ID.WRITE_TIME_WINDOWS_US,
                                    value_to_write = case['value_to_write']
            )
        dut._log.info('leaving a rest time of {} ns before next test case...'.format(case['rest_time_ns']))
        await Timer(case['rest_time_ns'], 'ns')


@cocotb.test()
async def test_write_channels_enabled_cmd(dut):
    """
    Test if the dut can decode the command `WRITE_CHANNELS_ENABLED` and store the value in
    the CHANNELS_ENABLED register.
    """

    clock_period_ns = 10
    test_cases = [
        {'value_to_write': 0b000, 'rest_time_ns': clock_period_ns  },
        {'value_to_write': 0b001, 'rest_time_ns': clock_period_ns  },
        {'value_to_write': 0b010, 'rest_time_ns': clock_period_ns  },
        {'value_to_write': 0b011, 'rest_time_ns': clock_period_ns  },
        {'value_to_write': 0b100, 'rest_time_ns': clock_period_ns  },
        {'value_to_write': 0b101, 'rest_time_ns': clock_period_ns  },
        {'value_to_write': 0b110, 'rest_time_ns': clock_period_ns  },
        {'value_to_write': 0b111, 'rest_time_ns': clock_period_ns  },

        {'value_to_write': 0b000, 'rest_time_ns': round(clock_period_ns/2)  },
        {'value_to_write': 0b001, 'rest_time_ns': round(clock_period_ns/2)  },
        {'value_to_write': 0b010, 'rest_time_ns': round(clock_period_ns/2)  },
        {'value_to_write': 0b011, 'rest_time_ns': round(clock_period_ns/2)  },
        {'value_to_write': 0b100, 'rest_time_ns': round(clock_period_ns/2)  },
        {'value_to_write': 0b101, 'rest_time_ns': round(clock_period_ns/2)  },
        {'value_to_write': 0b110, 'rest_time_ns': round(clock_period_ns/2)  },
        {'value_to_write': 0b111, 'rest_time_ns': round(clock_period_ns/2)  },
    ]

    clock = Clock(dut.i_clk, clock_period_ns, units="ns")  # Create a 10ns period clock on port clk
    cocotb.start_soon(clock.start())  # Start the clock

    await reset_dut(dut.i_reset, 2*clock_period_ns, 'ns')

    dut.i_rx_dv.value = 0
    dut.i_rx_data.value = 0
    await Timer(clock_period_ns, units='ns')

    for case in test_cases:
        await test_write_reg_cmd(   dut = dut, register = dut.cmd_executor.w_channels_enabled, 
                                    clock_period_ns = clock_period_ns,
                                    cmd_id = CMD_ID.WRITE_CHANNELS_ENABLED,
                                    value_to_write = case['value_to_write']
            )
        dut._log.info('leaving a rest time of {} ns before next test case...'.format(case['rest_time_ns']))
        await Timer(case['rest_time_ns'], 'ns')


async def simulate_tx_data_done_signal(dut, active_time_ns):
    """ Simulate i_tx_data_done port of the DUT meanwhile o_tx_active port is valid. """
    while True:
        if dut.o_tx_data_valid.value.integer != 0:
            await Timer(active_time_ns, 'ns')
            dut.i_tx_data_done.value = 1

            await Timer(CLOCK_PERIOD_NS, 'ns')
            dut.i_tx_data_done.value = 0
        else:
            await Timer(CLOCK_PERIOD_NS, 'ns')


async def test_cmd_read_channels_data(dut, channels_data):
    """
    Test READ_CHANNELS_DATA command with a given set of data stored in each channel.
    """
    clock = Clock(dut.i_clk, CLOCK_PERIOD_NS, units="ns")
    cocotb.start_soon(clock.start())  # Start the clock

    # input ports initial value
    dut.i_tx_data_done.value = 0
    dut.i_chn_dready.value = 0
    dut.i_read_chn_data.value = 0x0
    dut.i_read_chn_lwa.value = 0

    await reset_dut(dut.i_reset, 2*CLOCK_PERIOD_NS, 'ns')
    cocotb.start_soon(simulate_channels_ram(dut, channels_data))

    assert (CFG_REG_INIT_VAL_ENABLED_CHN & AVAILABLE_CHANNELS) != 0, "no channel available is enabled by default? Is the test bad programmed?"

    # check which channels are enabled and build a dictionary with the RAM data of each enabled and available channel
    channels_ram = dict()
    for ch in range(CHANNELS_AVAILABLE_QTY):
        if (((1 << ch) & CFG_REG_INIT_VAL_ENABLED_CHN & AVAILABLE_CHANNELS) != 0) and (ch in list(channels_data.keys())):
            channels_ram[ch] = channels_data[ch]

    data_buffer_validator_task = cocotb.start_soon(data_buffers_validator(dut=dut, channels_ram=channels_ram, clock_period_ns=CLOCK_PERIOD_NS, channel_data_reg_width=TDC_DATA_REG_WIDTH, tx_buffer_width_bytes=TX_BUFFER_BYTES_WIDTH, expected_o_tx_data_valid=DATA_BUFFER_SENT_O_TX_VALID))
    dut._log.info(f" channels_ram => {channels_ram}")

    # build and send command
    cmd_to_send = build_tdc_command(cmd_id = CMD_ID.READ_CHANNELS_DATA, cmd_data = 0)
    dut._log.info(  f'Testing command ID {CMD_ID.READ_CHANNELS_DATA} \n' + \
                    f'Command to send: 0x{cmd_to_send:X}\n' + \
                    f'Bit-width of the command: {cmd_to_send.bit_length()}' )
    cocotb.start_soon(simulate_input(dut, i_rx_data=cmd_to_send, active_time_ns=CLOCK_PERIOD_NS))

    # The core tdc logic takes 4 clock cycles to process the command
    # we leave a little of "changui" here to be sure that the simulation is in the clock cycle
    # in which the write was done
    await Timer((CLOCK_PERIOD_NS*4)+round(CLOCK_PERIOD_NS/5), units='ns')

    ## Set te appropiate value to i_dready signal to indicate to DUT that all the data is ready
    dut.i_chn_dready.value = AVAILABLE_CHANNELS & CFG_REG_INIT_VAL_ENABLED_CHN
    await Timer(CLOCK_PERIOD_NS, 'ns')

    dut._log.info("waiting until data buffer validator task ends...")
    await with_timeout(Join(data_buffer_validator_task), timeout_time=10000, timeout_unit='ns') # wait until data buffer validator task ends



@cocotb.test()
async def test_cmd_read_channels_data_all_channels_available(dut):
    """
    Test READ_CHANNELS_DATA command execution with a given set of available
    channels and channels data.
    This routine tests all the possible combination of enabled channels
    with the given inputs, validating the output data of the DUT.
    """
    dut._log.info('Testing the command where TDC reads all the data of enabled channels RAMs...')

    channels_data = {
        0: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21],
        1: [8, 5, 9, 51, 123],
        2: [9, 1, 10, 100, 1000, 10000, 100000, 123, 511, 12521, 519, 5151, 144, 9191, 12, 21415, 5151, 519, 5141, 1313, 151, 1551],
    }

    await test_cmd_read_channels_data(dut=dut, channels_data=channels_data)