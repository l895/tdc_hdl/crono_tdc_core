from cocotb.clock import Clock
from cocotb.triggers import Timer, Join, with_timeout
import cocotb
from cocotb.utils import get_sim_time
from math import ceil, log2


from tb_tools.tdc_commands import CMD_EXECUTOR_ID, read_crono_tdc_version
from tb_tools.utils import reset_dut
from tb_tools.ce_read_channels_data_utils import data_buffers_validator, simulate_channels_ram

CRONO_TDC_CORE_VERSION_PKG_REL_PATH='../../../src/crono_tdc_core_version_pkg.sv'

# DUT Parameters for this testbench
CHANNELS_AVAILABLE_QTY = 3
AVAILABLE_CHANNELS = 0b111
CFG_REG_INIT_VAL_ENABLED_CHN = AVAILABLE_CHANNELS
CFG_REG_INIT_VAL_TIME_WINDOWS_US = 10

TDC_CHANNEL_DATA_REG_WIDTH=20
TX_BUFFER_BYTES_WIDTH=16
DATA_BUFFER_SENT_O_TX_VALID=TX_BUFFER_BYTES_WIDTH*8 # value set in o_tx_data_valid when a data buffer is sent

CLOCK_PERIOD_NS = 10

async def test_read_reg_command(dut, executor_id: int, expected_output_data: int, expected_output_data_valid: int):
    """
    Test if a command that reads a register works well.
    This is tested starting the command executor and checking if the data that
    is at the output is correct.
    """
    clock = Clock(dut.i_clk, CLOCK_PERIOD_NS, units="ns")  # Create a 10us period clock on port clk
    cocotb.start_soon(clock.start())  # Start the clock

    dut.i_cmd_exec_start.value = 0
    dut.i_rx_cmd_data.value = 0
    dut.i_tx_data_done.value = 0
    await reset_dut(dut.i_reset, 2*CLOCK_PERIOD_NS, 'ns')

    # start command setting its start signal one clock cycle
    dut._log.info('starting command...')
    dut.i_cmd_exec_start.value = executor_id
    await Timer(CLOCK_PERIOD_NS, 'ns')
    dut.i_cmd_exec_start.value = 0

    await Timer(CLOCK_PERIOD_NS*2, 'ns')
    # after two clock cycles the data must be in the output buffer, check if it is there
    assert dut.o_tx_data_valid.value.integer == expected_output_data_valid, 'o_tx_data_valid is not correct! {} != {}'.format(dut.o_tx_data_valid.value.integer,expected_output_data_valid)
    assert dut.o_tx_data.value.integer == expected_output_data, 'o_tx_data is not correct! {} != {}'.format(dut.o_tx_data.value, expected_output_data)
    dut._log.info(f'correct data is present at output: 0x{dut.o_tx_data.value.integer:0X}!')

    # simulate that there is some transceiver sending the data that will set the
    # i_tx_done port high during one clock cycle in some moment
    await Timer(CLOCK_PERIOD_NS*100, 'ns')
    dut.i_tx_data_done.value = 1
    await Timer(CLOCK_PERIOD_NS, 'ns')
    dut.i_tx_data_done.value = 0
    await Timer(CLOCK_PERIOD_NS*5, 'ns')


@cocotb.test()
async def test_cmd_read_crono_tdc_version(dut):
    """
    Test the execution of the command that reads the Crono TDC Version
    from a register and sends it.
    """
    crono_tdc_version = read_crono_tdc_version(CRONO_TDC_CORE_VERSION_PKG_REL_PATH)
    dut._log.info('Crono TDC version: {}'.format(crono_tdc_version))
    dut._log.info('Testing the command that reads the Crono TDC version...')
    await test_read_reg_command(
        dut,
        executor_id = CMD_EXECUTOR_ID.READ_CRONO_TDC_CORE_VERSION,
        expected_output_data = int.from_bytes(bytes(crono_tdc_version, 'ascii'), 'big'),
        expected_output_data_valid = len(crono_tdc_version)*8
    )
    dut._log.info('the command that reads Crono TDC Core version from register works!!')


@cocotb.test()
async def test_cmd_read_time_windows_us(dut):
    """
    Test the execution of the command that reads time windows 
    from a register and sends it.
    """
    dut._log.info('Testing the command that reads the time windows reg...')
    await test_read_reg_command(
        dut,
        executor_id = CMD_EXECUTOR_ID.READ_TIME_WINDOWS_US,
        expected_output_data = CFG_REG_INIT_VAL_TIME_WINDOWS_US,
        expected_output_data_valid = 32
    )
    dut._log.info('the command that reads the time windows works!!')


@cocotb.test()
async def test_cmd_read_channels_available(dut):
    """
    Test the execution of the command that reads available channels 
    from a register and sends it.
    """
    dut._log.info('Testing the command that reads the channels available reg...')
    await test_read_reg_command(
        dut,
        executor_id = CMD_EXECUTOR_ID.READ_CHANNELS_AVAILABLE,
        expected_output_data = AVAILABLE_CHANNELS, # this is attached to the quantities of tdc channels (in this case, 3)
        expected_output_data_valid = 32
    )
    dut._log.info('the command that reads the enabled channels works!!')


@cocotb.test()
async def test_cmd_read_channels_enabled(dut):
    """
    Test the execution of the command that reads channels enabled 
    from a register and sends it.
    """
    dut._log.info('Testing the command that reads the time windows reg...')

    await test_read_reg_command(
        dut,
        executor_id = CMD_EXECUTOR_ID.READ_CHANNELS_ENABLED,
        expected_output_data = CFG_REG_INIT_VAL_ENABLED_CHN,
        expected_output_data_valid = 32
    )
    dut._log.info('the command that reads the time windows works!!')


async def test_write_reg_command(dut, register, executor_id: int, value_to_write: int):
    """
    Test if a command that writes a register works well.
    This is tested starting the command executor, giving some value as input 
    and checking if that data is writed in the register.

    Parameters
    -----------
    register:
        The register to be written. In the DUT, we have a wire that 
        exposes the register per se.
    
    executor_id: int
        The id of the executor that executes the write command.

    value_to_write: int
        The value to be written in the register.
    """
    clock = Clock(dut.i_clk, CLOCK_PERIOD_NS, units="ns")  # Create a 10us period clock on port clk
    cocotb.start_soon(clock.start())  # Start the clock

    dut.i_cmd_exec_start.value = 0
    dut.i_rx_cmd_data.value = 0
    await reset_dut(dut.i_reset, 2*CLOCK_PERIOD_NS, 'ns')

    # start command setting its start signal one clock cycle
    dut._log.info('starting command...')
    dut.i_rx_cmd_data.value = value_to_write
    dut.i_cmd_exec_start.value = executor_id
    await Timer(CLOCK_PERIOD_NS, 'ns')
    dut.i_cmd_exec_start.value = 0

    # wait one clock cycle and a little more
    await Timer(round(CLOCK_PERIOD_NS*1.2), 'ns')
    dut._log.info('checking if the value was writed in register...')
    assert register.value.integer == value_to_write, f'register value is not correct! {register.value.integer} != {value_to_write}'

    await Timer(CLOCK_PERIOD_NS, 'ns')


@cocotb.test()
async def test_cmd_write_time_windows_us(dut):
    """ Test the execution of the command that writes the time windows register."""
    dut._log.info('Testing the command that writes the time windows reg...')

    test_cases = [
        {'value_to_write': 0x12},
        {'value_to_write': 0x84},
        {'value_to_write': 0x109},
        {'value_to_write': 0x259},
        {'value_to_write': 0x854},
        {'value_to_write': 0x2687},
        {'value_to_write': 0x68794},
        {'value_to_write': 0x980154},
        {'value_to_write': 0x1545364},
        {'value_to_write': 0x8545364},
        {'value_to_write': 0x18545364},
        ]

    for case in test_cases:
        await test_write_reg_command(
            dut, dut.w_time_windows_us,
            executor_id = CMD_EXECUTOR_ID.WRITE_TIME_WINDOWS_US,
            value_to_write = case['value_to_write'],
        )
    dut._log.info('the command that writes the time windows works!!')

@cocotb.test()
async def test_cmd_write_channels_enabled(dut):
    """ Test the execution of the command that writes the channels enabled register."""
    dut._log.info('Testing the command that writes the channels enabled reg...')

    test_cases = [
        {'value_to_write': 0b000},
        {'value_to_write': 0b001},
        {'value_to_write': 0b010},
        {'value_to_write': 0b011},
        {'value_to_write': 0b100},
        {'value_to_write': 0b101},
        {'value_to_write': 0b110},
        {'value_to_write': 0b111}
        ]

    for case in test_cases:
        await test_write_reg_command(
            dut, dut.w_channels_enabled,
            executor_id = CMD_EXECUTOR_ID.WRITE_CHANNELS_ENABLED,
            value_to_write = case['value_to_write'],
        )
    dut._log.info('the command that configures the channels enabled works!!')

@cocotb.test()
async def test_cmd_measure(dut):
    """ Test the execution of the command where the TDC measure with all the enabled channels. """
    dut._log.info('Testing the command where TDC measures with all the enabled channels...')

    clock = Clock(dut.i_clk, CLOCK_PERIOD_NS, units="ns")  # Create a 10us period clock on port clk
    cocotb.start_soon(clock.start())  # Start the clock

    # input ports initial value
    dut.i_cmd_exec_start.value = 0
    dut.i_rx_cmd_data.value = 0
    await reset_dut(dut.i_reset, 2*CLOCK_PERIOD_NS, 'ns')

    # start command setting its start signal one clock cycle
    dut._log.info('starting command...')
    dut.i_rx_cmd_data.value = 0
    dut.i_cmd_exec_start.value = CMD_EXECUTOR_ID.MEASURE
    await Timer(CLOCK_PERIOD_NS, 'ns')
    dut.i_cmd_exec_start.value = 0

    init_time_us = get_sim_time('us')
    while dut.o_chn_ena.value == 0:
        elapsed_time_us = get_sim_time('us') - init_time_us
        if (elapsed_time_us > 0.1) and (elapsed_time_us % 5 == 0):
            dut._log.info(f"waiting until o_chn_ena is set... (elapsed time: {elapsed_time_us} us)")
        await Timer(CLOCK_PERIOD_NS, 'ns')

    dut._log.info(f"o_chn_ena was set!")

    init_time_us = get_sim_time('us')
    while dut.o_chn_ena.value != 0:
        elapsed_time_us = get_sim_time('us') - init_time_us
        if (elapsed_time_us > 0.1) and (elapsed_time_us % 5 == 0):
            dut._log.info(f"elapsed time: {elapsed_time_us} us...")
        assert elapsed_time_us < CFG_REG_INIT_VAL_TIME_WINDOWS_US*1.2, f"time elapsed is bigger than windows! error in the command!"
        await Timer(CLOCK_PERIOD_NS, 'ns')

    dut._log.info(f"o_chn_ena was unset!")
    await Timer(CLOCK_PERIOD_NS,'ns')

    # we simulate that the channels stored all the data in their rams
    # and assert dready signal 
    dut.i_dready.value = CFG_REG_INIT_VAL_ENABLED_CHN
    await Timer(CLOCK_PERIOD_NS, 'ns')
    dut.i_dready.value = 0

    # here we wait until the tdc_cmd_executor gives access to the tx buffer
    # to this particular command executor and the data of this executor
    # is present in tx buffer
    while dut.o_tx_data_valid.value == 0:
        await Timer(CLOCK_PERIOD_NS, 'ns')
        dut._log.info('waiting until tdc_cmd_executor data valid is different from 1')

    # convert the mask (in number) to ascii characters
    expected_enabled_mask = CFG_REG_INIT_VAL_ENABLED_CHN
    ascii_enabled_mask = b''
    for i in range(7,-1,-1):
        ascii_val = ((expected_enabled_mask & (0xF << i*8)) >> i*8)+48
        ascii_enabled_mask = ascii_enabled_mask + ascii_val.to_bytes(1,'big')
    expected_confirmation_msg = b"ena"+ ascii_enabled_mask

    assert dut.o_tx_data.value.buff[-11:] == expected_confirmation_msg, f"data in tx buffer is not the expected one {dut.o_tx_data_valid.value.buff} != {expected_confirmation_msg}"
    dut._log.info(f'expected data in tx buffer: {dut.o_tx_data.value.buff}!')

    # simulate that data was transmitted
    dut.i_tx_data_done.value = 1
    await Timer(CLOCK_PERIOD_NS,'ns')
    dut.i_tx_data_done.value = 0

    while dut.o_tx_data_valid.value != 0:
        await Timer(CLOCK_PERIOD_NS, 'ns')
        dut._log.info('waiting until o_tx_data_valid is cleared!')
    
    dut._log.info('o_tx_data_valid was cleared!')


async def simulate_tx_data_done_signal(dut, active_time_ns):
    """ Simulate i_tx_data_done port of the DUT meanwhile o_tx_active port is valid. """
    while True:
        if dut.o_tx_data_valid.value.integer != 0:
            await Timer(active_time_ns, 'ns')
            dut.i_tx_data_done.value = 1

            await Timer(CLOCK_PERIOD_NS, 'ns')
            dut.i_tx_data_done.value = 0
        else:
            await Timer(CLOCK_PERIOD_NS, 'ns')


async def test_cmd_read_channels_data(dut, channels_data):
    """
    Test READ_CHANNELS_DATA command with a given set of data stored in each channel.
    """
    clock = Clock(dut.i_clk, CLOCK_PERIOD_NS, units="ns")
    cocotb.start_soon(clock.start())  # Start the clock

    # input ports initial value
    dut.i_cmd_exec_start.value = 0
    dut.i_rx_cmd_data.value = 0
    await reset_dut(dut.i_reset, 2*CLOCK_PERIOD_NS, 'ns')
    
    cocotb.start_soon(simulate_channels_ram(dut, channels_data))

    assert (CFG_REG_INIT_VAL_ENABLED_CHN & AVAILABLE_CHANNELS) != 0, "no channel available is enabled by default? Is the test bad programmed?"

    # check which channels are enabled and build a dictionary with the RAM data of each enabled and available channel
    channels_ram = dict()
    for ch in range(CHANNELS_AVAILABLE_QTY):
        if (((1 << ch) & CFG_REG_INIT_VAL_ENABLED_CHN & AVAILABLE_CHANNELS) != 0) and (ch in list(channels_data.keys())):
            channels_ram[ch] = channels_data[ch]

    dut._log.info(f"launching data buffers validator...")
    data_buffer_validator_task = cocotb.start_soon(data_buffers_validator(dut=dut, channels_ram=channels_ram, clock_period_ns=CLOCK_PERIOD_NS, channel_data_reg_width=TDC_CHANNEL_DATA_REG_WIDTH, tx_buffer_width_bytes=TX_BUFFER_BYTES_WIDTH, expected_o_tx_data_valid=DATA_BUFFER_SENT_O_TX_VALID))

    ## start command setting its start signal one clock cycle
    dut._log.info('starting command execution...')
    dut.i_rx_cmd_data.value = 0
    dut.i_cmd_exec_start.value = CMD_EXECUTOR_ID.READ_CHANNELS_DATA
    await Timer(CLOCK_PERIOD_NS*2, 'ns')
    dut.i_cmd_exec_start.value = 0

    ## Here the DUT waits until all enabled channels are ready (can happen that the system is making a measure)
    await Timer(CLOCK_PERIOD_NS, 'ns')
    dut.i_dready.value = AVAILABLE_CHANNELS & CFG_REG_INIT_VAL_ENABLED_CHN
    await Timer(CLOCK_PERIOD_NS, 'ns')

    dut._log.info("waiting until data buffer validator task ends...")
    await with_timeout(Join(data_buffer_validator_task), timeout_time=10000, timeout_unit='ns') # wait until data buffer validator task ends


@cocotb.test()
async def test_cmd_read_channels_data_all_channels_available(dut):
    """
    Test READ_CHANNELS_DATA command execution with a given set of available
    channels and channels data.
    This routine tests all the possible combination of enabled channels
    with the given inputs, validating the output data of the DUT.
    """
    dut._log.info('Testing the command where TDC reads all the data of enabled channels RAMs...')

    channels_data = {
        0: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21],
        1: [8, 5, 9, 51, 123],
        2: [9, 1, 10, 100, 1000, 10000, 100000, 123, 511, 12521, 519, 5151, 144, 9191, 12, 21415, 5151, 519, 5141, 1313, 151, 1551],
    }

    await test_cmd_read_channels_data(dut=dut, channels_data=channels_data)