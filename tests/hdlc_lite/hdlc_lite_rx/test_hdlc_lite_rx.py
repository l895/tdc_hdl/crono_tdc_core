from pickle import FRAME
import cocotb
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, with_timeout, Join

from tb_tools.utils import block_until_signal_is_different_from_value, reset_dut

FRAME_DELIMITER_CHAR = 0x7E
SCAPE_CHAR = 0x7D
CLK_PERIOD_NS = 10

async def send_byte(dut, data:int):
    dut.i_rx_dv.value = 1
    dut.i_rx_byte.value = data
    await RisingEdge(dut.i_clk)
    await Timer(CLK_PERIOD_NS, 'ns')
    dut.i_rx_dv.value = 0


async def send_hdlc_lite_frame(dut, data: int, crc: int, bytes_qty: int):
    """
    Send a frame to the hdlc_lite_rx receiver. Here we use two signals 
    to introduce data to the CRC:
      * i_rx_byte: here we stores the byte that we want to send 
      * i_rx_dv: to notify to the hdlc_rx module that data is valid at port i_rx_byte

    The sequence is the following:
    1) Send frame delimiter byte
    2) Send data byte by byte the data
    3) Send the high byte of the CRC
    4) Send the low byte of the CRC
    5) Send frame delimiter byte

    Parameters
    ----------
    dut : cocotb.handle.ModifiableObject
        The hdlc_lite_rx module.

    data : int
        The data to send in the frame.

    crc: int
        The CRC16-CCITT of the data to send.

    bytes_qty : int
        Quantity of bytes needed to send the data.

    """
    dut._log.info('input to dut: 0x{:X}{:X} (bytes qty = {})'.format(data, crc, bytes_qty))

    dut._log.info('starting frame... sending frame delimiter byte {:X}'.format(FRAME_DELIMITER_CHAR))
    cocotb.start_soon(send_byte(dut,FRAME_DELIMITER_CHAR))
#    await Timer(1, 'us') # simulate a baudrate of 1Mhz, one byte per 1us
    await Timer(CLK_PERIOD_NS*3, 'ns') 


    for i in range(bytes_qty-1, -1, -1):
        # enter byte by byte to receiver, starting from MSB
        input_byte = (data >> (i*8)) & 0xFF
        dut._log.info('{}: entering with data byte 0x{:X}'.format(i, input_byte))
        cocotb.start_soon(send_byte(dut,input_byte))
#        await Timer(1, 'us') # simulate a baudrate of 1Mhz, one byte per 1us
        await Timer(CLK_PERIOD_NS*3, 'ns') 

    for i in range(1, -1, -1):
        # enter CRC to receiver, starting from MSB
        input_byte = (crc >> (i*8)) & 0xFF
        dut._log.info('{}: entering with crc byte 0x{:X}'.format(i, input_byte))
        cocotb.start_soon(send_byte(dut,input_byte))
#        await Timer(1, 'us') # simulate a baudrate of 1Mhz, one byte per 1us
        await Timer(CLK_PERIOD_NS*3, 'ns') 

    dut._log.info('ending frame... sending frame delimiter byte {:X}'.format(FRAME_DELIMITER_CHAR))
    cocotb.start_soon(send_byte(dut,FRAME_DELIMITER_CHAR))
    await Timer(CLK_PERIOD_NS*2, 'ns') 
    dut.i_rx_dv.value = 0
    dut.i_rx_byte.value = 0


@cocotb.test()
async def test_sending_frames_with_correct_crc(dut):
    """ 
    Test if hdlc_lite receiver decodes some correct frames with a 
    correct CRC as expected. The port `w_crc_encoder_output` must be 1 
    because the CRC of the frames are right.
    """
    data = [ 
        {'data': 0x123456789A, 'crc': 0xE9AC, 'bytes_qty': 5},
        {'data': 0x5F5F, 'crc': 0xB59B, 'bytes_qty': 2},
        {'data': 0x5F5F5F, 'crc': 0xC764, 'bytes_qty': 3},
        {'data': 0x12, 'crc': 0x3273, 'bytes_qty': 1},
        {'data': 0x120055, 'crc': 0x2753, 'bytes_qty': 3},
        {'data': 0x00, 'crc': 0x00, 'bytes_qty': 1},
        ]

    dut.i_rx_dv.value = 0
    dut.i_rx_byte.value = 0
    clock = Clock(dut.i_clk, CLK_PERIOD_NS, units="ns")  # Create a 10us period clock on port clk
    cocotb.start_soon(clock.start())  # Start the clock

    await Timer(CLK_PERIOD_NS*3, units='ns') ## to differentiate different test cases
    
    await reset_dut(dut.i_rst, round(CLK_PERIOD_NS*1.1), 'ns')
    await RisingEdge(dut.i_clk)

    case_counter = 0
    for case in data:
        dut._log.info(f"#---- TESTING CASE {case_counter} ----#")
        await send_hdlc_lite_frame(dut, data=case['data'], crc=case['crc'], bytes_qty=case['bytes_qty'])

        wait_o_rx_dv_task = cocotb.start_soon(block_until_signal_is_different_from_value(dut.o_rx_dv, 0, CLK_PERIOD_NS))
        await with_timeout(Join(wait_o_rx_dv_task), 100, 'us')

        assert dut.o_rx_dv.value.integer == case['bytes_qty']*8, 'bits quantity is not correct! {} != {}'.format(dut.o_rx_dv.value.integer, case['bytes_qty']*8)
        assert dut.o_rx_data.value.integer == case['data'], 'data at output is not correct! {:0X} != {:0X}'.format(dut.o_rx_data.value.integer, case['data'])
        assert dut.o_correct_crc.value == 1, 'crc is marked as not correct, but in this test all are correct!'

        dut._log.info('success! data at output: 0x{:0X}, correct crc!'.format(dut.o_rx_data.value.integer))

        await Timer(CLK_PERIOD_NS*10, units='ns') ## to differentiate different test cases
        case_counter +=1

    dut.i_rx_dv.value = 0
    dut.i_rx_byte.value = 0


@cocotb.test()
async def test_sending_frames_with_incorrect_crc(dut):
    """ 
    Test if hdlc_lite receiver decodes some correct frames with a 
    incorrect CRC as expected. The port `w_crc_encoder_output` must be 0 
    because the CRC of the frames are wrong.
    """
    data = [ 
        {'data': 0x123456789A, 'crc_to_send': 0xABCD, 'correct_crc': 0xE9AC, 'bytes_qty': 5},
        {'data': 0x5F5F5F5F5F, 'crc_to_send': 0x0123, 'correct_crc': 0x167A, 'bytes_qty': 5},
        {'data': 0x12, 'crc_to_send': 0x5678, 'correct_crc': 0x3273, 'bytes_qty': 1},
        {'data': 0x120055, 'crc_to_send': 0x8554, 'correct_crc': 0x2753, 'bytes_qty': 3},
        ]

    dut.i_rx_dv.value = 0
    dut.i_rx_byte.value = 0
    clock = Clock(dut.i_clk, CLK_PERIOD_NS, units="ns")  # Create a 10us period clock on port clk
    cocotb.start_soon(clock.start())  # Start the clock

    await Timer(CLK_PERIOD_NS*3, units='ns') ## to differentiate different test cases
    
    await reset_dut(dut.i_rst, round(CLK_PERIOD_NS*1.1), 'ns')
    await RisingEdge(dut.i_clk)

    dut.i_rx_dv.value = 0
    dut.i_rx_byte.value = 0

    case_counter = 0
    for case in data:
        await send_hdlc_lite_frame(dut, data=case['data'], crc=case['crc_to_send'], bytes_qty=case['bytes_qty'])

        wait_o_rx_dv_task = cocotb.start_soon(block_until_signal_is_different_from_value(dut.o_rx_dv, 0, CLK_PERIOD_NS))
        await with_timeout(Join(wait_o_rx_dv_task), 100, 'us')

        assert dut.o_rx_dv.value.integer == case['bytes_qty']*8, 'bits quantity is not correct! {} != {}'.format(dut.o_rx_dv.value.integer, case['bytes_qty']*8)
        assert dut.o_rx_data.value.integer == case['data'], 'data at output is not correct! {:0X} != {:0X}'.format(dut.o_rx_data.value.integer, case['data'])
        assert dut.o_correct_crc.value == 0, 'crc is marked as correct, but in this test all are incorrect!'

        dut._log.info('success! data at output: 0x{:0X}, correct crc!'.format(dut.o_rx_data.value.integer))
        await Timer(CLK_PERIOD_NS*10, units='ns') ## to differentiate different test cases
        case_counter += 1

    dut.i_rx_dv.value = 0
    dut.i_rx_byte.value = 0


@cocotb.test()
async def test_sending_frames_with_escaped_chars(dut):
    """ 
    Test if hdlc_lite receiver decodes some correct frames with scaped chars. 
    The port `w_crc_encoder_output` must be 1 because the CRC of the frames are right.
    Here, the number of bytes sent is not equal to the number of bytes of the data,
    because the scaped char is not taken in count when the module process the frame.
    """
    data = [ 
        {'data': 0x7D5D,                    'expected_data': 0x7D,              'crc': 0xAF3A, 'bytes_qty': 2,  'expected_data_bytes_qty': 1},
        {'data': 0x7D5E,                    'expected_data': 0x7E,              'crc': 0x9F59, 'bytes_qty': 2,  'expected_data_bytes_qty': 1},
        {'data': 0x7D5D12,                  'expected_data': 0x7D12,            'crc': 0x4C76, 'bytes_qty': 3,  'expected_data_bytes_qty': 2},
        {'data': 0x7D5E12,                  'expected_data': 0x7E12,            'crc': 0x1925, 'bytes_qty': 3,  'expected_data_bytes_qty': 2},
        {'data': 0x127D5D,                  'expected_data': 0x127D,            'crc': 0xCA2B, 'bytes_qty': 3,  'expected_data_bytes_qty': 2},
        {'data': 0x127D5E,                  'expected_data': 0x127E,            'crc': 0xFA48, 'bytes_qty': 3,  'expected_data_bytes_qty': 2},
        {'data': 0x12347D5E56789A,          'expected_data': 0x12347E56789A,    'crc': 0xBB6E, 'bytes_qty': 7,  'expected_data_bytes_qty': 6},
        {'data': 0x12347D5D56789A,          'expected_data': 0x12347D56789A,    'crc': 0x20B2, 'bytes_qty': 7,  'expected_data_bytes_qty': 6},
        {'data': 0x577D5D7D5D7D5D000012,    'expected_data': 0x577D7D7D000012,  'crc': 0x8D70, 'bytes_qty': 10, 'expected_data_bytes_qty': 7},
        {'data': 0x577D5E7D5D7D5D000012,    'expected_data': 0x577E7D7D000012,  'crc': 0x4390, 'bytes_qty': 10, 'expected_data_bytes_qty': 7},
        {'data': 0x577D5D7D5E7D5D000012,    'expected_data': 0x577D7E7D000012,  'crc': 0x63A2, 'bytes_qty': 10, 'expected_data_bytes_qty': 7},
        {'data': 0x577D5D7D5D7D5E000012,    'expected_data': 0x577D7D7E000012,  'crc': 0x16AC, 'bytes_qty': 10, 'expected_data_bytes_qty': 7},
        {'data': 0x577D5E7D5D7D5E000012,    'expected_data': 0x577E7D7E000012,  'crc': 0xD84C, 'bytes_qty': 10, 'expected_data_bytes_qty': 7},
        {'data': 0x577D5E7D5E7D5E000012,    'expected_data': 0x577E7E7E000012,  'crc': 0x369E, 'bytes_qty': 10, 'expected_data_bytes_qty': 7},
        {'data': 0x577D5E007D5E000012,      'expected_data': 0x577E007E000012,  'crc': 0xE4B0, 'bytes_qty': 9,  'expected_data_bytes_qty': 7},
        {'data': 0x127D5E,                  'expected_data': 0x127E,            'crc': 0xFA48, 'bytes_qty': 3,  'expected_data_bytes_qty': 2},
        ]

    dut.i_rx_dv.value = 0
    dut.i_rx_byte.value = 0
    clock = Clock(dut.i_clk, CLK_PERIOD_NS, units="ns")  # Create a 10us period clock on port clk
    cocotb.start_soon(clock.start())  # Start the clock

    await Timer(CLK_PERIOD_NS*3, units='ns') ## to differentiate different test cases
    
    await reset_dut(dut.i_rst, round(CLK_PERIOD_NS*1.1), 'ns')
    await RisingEdge(dut.i_clk)

    dut.i_rx_dv.value = 0
    dut.i_rx_byte.value = 0

    case_counter = 0
    for case in data:
        dut._log.info(f"#---- TESTING CASE {case_counter} ----#")
        await send_hdlc_lite_frame(dut, data=case['data'], crc=case['crc'], bytes_qty=case['bytes_qty'])

        wait_o_rx_dv_task = cocotb.start_soon(block_until_signal_is_different_from_value(dut.o_rx_dv, 0, CLK_PERIOD_NS))
        await with_timeout(Join(wait_o_rx_dv_task), 100, 'us')

        assert dut.o_rx_dv.value.integer == case['expected_data_bytes_qty']*8, 'bits quantity is not correct! {} != {}'.format(dut.o_rx_dv.value.integer, case['expected_data_bytes_qty']*8)
        assert dut.o_rx_data.value == case['expected_data'], 'data at output is not correct! {:0X} != {:0X}'.format(dut.o_rx_data.value.integer, case['expected_data'])
        assert dut.o_correct_crc.value == 1, 'crc is marked as not correct, but in this test all are correct!'

        dut._log.info('success! data at output: 0x{:0X}, correct crc!'.format(dut.o_rx_data.value.integer))
        await Timer(CLK_PERIOD_NS*10, units='ns') ## to differentiate different test cases
        case_counter += 1

    dut.i_rx_dv.value = 0
    dut.i_rx_byte.value = 0
