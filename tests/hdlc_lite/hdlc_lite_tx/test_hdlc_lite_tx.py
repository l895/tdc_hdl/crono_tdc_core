from pickle import FRAME
import cocotb
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, with_timeout, Join, FallingEdge

from tb_tools.utils import reset_dut

FRAME_DELIMITER_CHAR = 0x7E
ESCAPE_CHAR = 0x7D

async def simulate_uart_transceiver(dut, char_send_wait_time_ns: int, clock_period_ns: int):
    '''
    Launch this co-routine to simulate a uart transceiver connected to the output
    of the hdlc-lite transceiver.
    This co-routine will handle the `i_tx_done` input port of the hdlc-lite transceiver
    in response of the o_tx_dv output port.
    '''
    while True:
        if( dut.o_tx_dv.value == 1 ):
            await Timer(char_send_wait_time_ns, units='ns')
            dut.i_tx_done.value = 1
            await Timer(clock_period_ns*2, units='ns')
            dut.i_tx_done.value = 0
        else:
            await Timer(clock_period_ns, units='ns')


async def run_black_box_test(dut, case: dict, clock_period_ns: int):
    """
    Run a black-box test fo the hdlc-lite transceiver.
    This test will insert data to the transceiver and will wait until it finish sending it.
    This coroutine checks if:
    1) the MSB and LSB of the output frame are frame delimiters
    2) the CRC of the frame is correct comparing to the expected CRC located in `case` dictionary
    3) the data of the frame is correct comparing to the expected data located in `case` dictionary.
    
    Parameters
    ----------
    case : dict
        A dictionary with the data of the test case that will be executed in the black-box test.
        The shape of the dict must be the following:
        {'data': 0x123456789A, 'expected_crc': 0xE9AC}

    clock_period_ns : int
        The period of the clock running in nanoseconds.

    """
    dut.i_tx_dv.value = 0
    dut.i_tx_data.value = 0
    dut.i_tx_done.value = 0
    await Timer(clock_period_ns*5, 'ns')
    await RisingEdge(dut.i_clk)

    received_frame = 0
    received_bytes = 0

    dut._log.info('inserting data 0x{:X} to hdlc lite tx...'.format(case['data']))
    dut.i_tx_data.value = case['data']
    dut.i_tx_dv.value = case['data'].bit_length()
    received_frame = 0
    received_bytes = 0

    await RisingEdge(dut.o_tx_active)

    while dut.o_tx_active.value.integer == 1:
        if( dut.o_tx_dv.value != 0 ):
            if (received_bytes == 0):   # check if MSB its a frame delimiter
                assert dut.o_tx_byte.value.integer == FRAME_DELIMITER_CHAR, 'MSB is not the frame delimiter! 0x{:02X} != 0x{:02X}'.format(dut.o_tx_byte.value.integer, FRAME_DELIMITER_CHAR)
                dut._log.info('OK! MSB is the frame delimiter! 0x{:02X}'.format(FRAME_DELIMITER_CHAR))
            received_frame = (received_frame << 8) | dut.o_tx_byte.value.integer
            received_bytes += 1
            dut._log.info('{}: o_tx_dv != 0... the byte at the output is 0x{:02X}.. frame until the moment: 0x{:X} (received bytes {})'.format(received_bytes, dut.o_tx_byte.value.integer, received_frame, received_bytes))
            await Timer(clock_period_ns*3, units='ns')
        await Timer(clock_period_ns, units='ns')

    # 1) check if LSB its a frame delimiter
    assert (received_frame & 0xFF) == FRAME_DELIMITER_CHAR, 'LSB is not the frame delimiter! 0x{:02X} != 0x{:02X}'.format((received_frame & 0xFF), FRAME_DELIMITER_CHAR) 
    dut._log.info('OK! LSB is the frame delimiter! 0x{:02X}'.format(FRAME_DELIMITER_CHAR))

    received_frame = received_frame >> 8 # delete END frame delimiter
    received_bytes = received_bytes - 2 # do not count frame delimiters
    received_frame = received_frame & ~(0xFF << received_bytes*8) # delete frame delimiter

    dut._log.info(f"analyzing escaped frame (0x{received_frame:X}) without frame delimiters...")
    # 2) unescape all the data
    unescaped_frame = 0
    unescaped_frame_pointer = received_bytes
    received_data_bytes = 0
    escaped_char = False
    for i in range(received_bytes-1,-1,-1):
        analyzed_char = (received_frame >> (i*8)) & 0xFF
        dut._log.info(f"analyzing byte {i} (0x{analyzed_char:02X})...")

        if escaped_char:
            dut._log.info(f"byte {i} (0x{analyzed_char:02X}) is escaped! unescaping it...")
            escaped_char = False
            unescaped_frame |= ((analyzed_char ^ 0x20) << (unescaped_frame_pointer*8))
            unescaped_frame_pointer -= 1
            received_data_bytes += 1
        elif analyzed_char == 0x7D:
            dut._log.info(f"byte {i} (0x{analyzed_char:02X}) is a control byte (0x7D)")
            escaped_char = True
        else:
            dut._log.info(f"byte {i} (0x{analyzed_char:02X}) is not a control byte and do not must be escaped!")
            unescaped_frame |= (analyzed_char << (unescaped_frame_pointer*8))
            unescaped_frame_pointer -= 1
            received_data_bytes += 1
        dut._log.info(f"unescaped frame until the moment 0x{unescaped_frame:X}! (received data bytes: {received_data_bytes})")

    unescaped_frame = unescaped_frame >> ((received_bytes-received_data_bytes)*8) # shift data if one character is escaped

    dut._log.info(f"unescaped frame: 0x{unescaped_frame:X}")

    # 3) check if the CRC is the expected
    incoming_frame_crc = (unescaped_frame >> 8) & 0xFFFF 
    assert (incoming_frame_crc) == case['expected_crc'], 'CRC16 computed by the hdlc lite is not the expected one! 0x{:04X} != 0x{:04X}'.format(incoming_frame_crc, case['expected_crc'])
    dut._log.info('OK! CRC of the incoming frame is correct! 0x{:04X} != 0x{:04X}'.format(incoming_frame_crc, case['expected_crc']))

    # 4) check if the data is the expected
    incoming_frame_data = (unescaped_frame >> 24)
    assert (incoming_frame_data) == case['data'], 'data of the frame does not corresponds with the input data! 0x{:X} != 0x{:X}'.format(incoming_frame_data, case['data'])
    dut._log.info('OK! data of the incoming frame is correct! 0x{:04X} != 0x{:04X}'.format(incoming_frame_data, case['data']))

    dut.i_tx_dv.value = 0
    dut.i_tx_data.value = 0
    # Note: wait one clock cycle until the hdlc-lite TX fsm entry to idle state
    await Timer(clock_period_ns, units='ns')


@cocotb.test()
async def test_sending_data_without_escaping(dut):
    """ 
    Test if hdlc_lite transmitter can handle input data and 
    transforms this data into an HDLC-lite frame correctly.
    """
    test_cases = [ 
        {'data': 0x123456789A, 'expected_crc': 0xE9AC},
        {'data': 0x5510000052, 'expected_crc': 0x5775},
        {'data': 0x01,         'expected_crc': 0x1021},
        {'data': 0x83,         'expected_crc': 0xA1EB},
        {'data': 0x59A023C0,   'expected_crc': 0xB129},
        {
           'data': 0x353337653965343230,
           'expected_crc': 0x5C73
        },
        {
           'data': 0x35333765396534323122,
           'expected_crc': 0xDF68
        },
        {
           'data': 0x9033376539653432312201,
           'expected_crc': 0xE86D
        }
    ]

    dut.i_tx_data.value = 0
    dut.i_tx_done.value = 0
    dut.i_tx_dv.value = 0

    clock_period_ns = 10
    clock = Clock(dut.i_clk, clock_period_ns, units="ns")  # Create a 10us period clock on port clk
    cocotb.start_soon(clock.start())  # Start the clock
    await Timer(clock_period_ns*3, units='ns') ## to differentiate different test cases

    await reset_dut(dut.i_rst, round(clock_period_ns*1.1), 'ns')
    await RisingEdge(dut.i_clk)

    # Start a co-routine that will simulate the uart transceiver
    cocotb.start_soon(simulate_uart_transceiver(dut, 
        char_send_wait_time_ns=clock_period_ns*100,
        clock_period_ns=clock_period_ns
        )
    )

    case_counter = 0
    for case in test_cases:
        dut._log.info(f"#---- TESTING CASE {case_counter} ----#")
        test_case_task = cocotb.start_soon(run_black_box_test(dut, case, clock_period_ns))
        await with_timeout(Join(test_case_task), 100, 'us')
        case_counter += 1

    await Timer(clock_period_ns*100, units='ns')


@cocotb.test()
async def test_sending_data_with_escaping(dut):
    """ 
    Test if hdlc_lite transmitter can handle input data and 
    transforms this data into an HDLC-lite frame correctly.
    This input data will need escape some characters.
    
    Take in count that the escaping process takes in count the CRC. In fact,
    when a transmitter endpoint wants to build a HDLC-lite frame:
    1) computes CRC of data
    2) escapes the data and the CRC
    3) adds at the beginning and at the end the frame delimiter.
    """
    test_cases = [ 
        {'data': 0x127E3456789A,  'expected_crc': 0xEA9F},
        {'data': 0x7D,            'expected_crc': 0xAF3A},
        {'data': 0x7E,            'expected_crc': 0x9F59},
        {'data': 0x7D007D,        'expected_crc': 0x3563},
        {'data': 0x7D007E,        'expected_crc': 0x0500},
        {'data': 0x7E007E,        'expected_crc': 0x5C50},
        {'data': 0x7E007D,        'expected_crc': 0x6C33},
        {'data': 0x497E117D,      'expected_crc': 0xC19A},
        {'data': 0x01F517D0,      'expected_crc': 0x2F8F},
        ]

    dut.i_tx_data.value = 0
    dut.i_tx_done.value = 0
    dut.i_tx_dv.value = 0

    clock_period_ns = 10
    clock = Clock(dut.i_clk, clock_period_ns, units="ns")  # Create a 10us period clock on port clk
    cocotb.start_soon(clock.start())  # Start the clock
    await Timer(clock_period_ns*3, units='ns') ## to differentiate different test cases

    await reset_dut(dut.i_rst, round(clock_period_ns*1.1), 'ns')
    await RisingEdge(dut.i_clk)

    # Start a co-routine that will simulate the uart transceiver
    cocotb.start_soon(simulate_uart_transceiver(dut, 
        char_send_wait_time_ns=clock_period_ns*100,
        clock_period_ns=clock_period_ns
        )
    )
    
    case_counter = 0
    for case in test_cases:
        dut._log.info(f"#---- TESTING CASE {case_counter} ----#")
        test_case_task = cocotb.start_soon(run_black_box_test(dut, case, clock_period_ns))
        await with_timeout(Join(test_case_task), 1000, 'us')
        case_counter += 1

    await Timer(clock_period_ns*100, units='ns')