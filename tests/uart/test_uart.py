import cocotb
from cocotb.clock import Clock
from cocotb.triggers import FallingEdge, Timer, RisingEdge, with_timeout

from tb_tools.uart import send_bytes_through_uart, receive_bytes_through_uart
from tb_tools.utils import reset_dut

CLOCK_FREQ_HZ   = 100*1e6
CLOCK_PERIOD_NS = 1e9/CLOCK_FREQ_HZ
BAUDRATE        = 921600
CLKS_PER_BIT    = int(CLOCK_FREQ_HZ/BAUDRATE)


@cocotb.test()
async def test_uart_rx_depth(dut):
    """ 
    Test that if we send data, the buffer full flag is 
    set and the data is stored in the reception buffer.
    This test check step by step the routine followed by 
    the rx module of the uart
    """
    test_bytes = [
            0x55, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, \
            0xA, 0xB, 0xC, 0xD, 0xE, 0xF, 0x11, 0x12, 0x13, 0x14, \
            0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, \
            0x1F, 0x20, 0x21, 0x22, 0x23, 0x24
        ]

    dut._log.info('Starting UART RX depth testbench!')
    clock = Clock(dut.i_clk, CLOCK_PERIOD_NS, units="ns")  # Create a 10us period clock on port clk
    cocotb.start_soon(clock.start())  # Start the clock
    
    await reset_dut(dut.i_reset, CLOCK_PERIOD_NS, 'ns')

    dut.i_rx_serial.value = 1 # inactive is high
    await Timer(10*CLOCK_PERIOD_NS, units="ns")
    await FallingEdge(dut.i_clk) # wait to synchronize with the start of a clock cycle

    bytes_tested = 0
    for tbyte in test_bytes:
        dut._log.info('{}: sending byte {} to uart receptor'.format(bytes_tested, tbyte))

        # send start bit
        dut.i_rx_serial.value = 0
        for i in range(CLKS_PER_BIT): await FallingEdge(dut.i_clk)
        dut._log.info('{}: start bit sent!'.format(bytes_tested))

        # send the 8 bits
        for i in range(8):
            # sending one bit a time
            dut.i_rx_serial.value = (tbyte >> i) & 0x1
            for i in range(CLKS_PER_BIT): await FallingEdge(dut.i_clk)

        # send stop bit
        dut.i_rx_serial.value = 1
        # wait until reception buffer full is set
        await RisingEdge(dut.o_rx_dv)

        assert dut.o_rx_dv.value == 1, "the rx buffer full flag was not set after the stop bit!"
        assert dut.o_rx_byte.value == tbyte, "in byte {} the data in the rx buffer is different to the expected one!".format(bytes_tested)
        dut._log.info('{}: byte {} received correctly!'.format(bytes_tested, tbyte))
        bytes_tested += 1


@cocotb.test()
async def test_uart_tx_depth(dut):
    """ 
    Test that if we send data, it is sent in a serial way and 
    the transference done flag is set. This test check step by step the 
    routine followed by the tx module of the uart
    """
    test_bytes = [
            0x55, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, \
            0xA, 0xB, 0xC, 0xD, 0xE, 0xF, 0x11, 0x12, 0x13, 0x14, \
            0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, \
            0x1F, 0x20, 0x21, 0x22, 0x23, 0x24
        ]

    dut._log.info('Starting UART TX depth testbench!')
    clock = Clock(dut.i_clk, CLOCK_PERIOD_NS, units="ns")  # Create a 10us period clock on port clk
    cocotb.start_soon(clock.start())  # Start the clock

    await reset_dut(dut.i_reset, CLOCK_PERIOD_NS, 'ns')

    await Timer(10*CLOCK_PERIOD_NS, units="ns")
    await FallingEdge(dut.i_clk) # wait to synchronize with the start of a clock cycle

    bytes_tested = 0
    for tbyte in test_bytes:
        dut._log.info('{}: putting byte {} in input register'.format(bytes_tested, tbyte))
        dut.i_tx_byte.value = tbyte
        
        dut.i_tx_dv.value = 1 # set enable transference signal

        await with_timeout(FallingEdge(dut.o_tx_serial), 1000, 'us') ## wait for start bit
        dut.i_tx_dv.value = 0 # reset enable transference signal

        await Timer(CLOCK_PERIOD_NS/2, units='ns')

        for i in range(8):
            expected_bit = (tbyte >> i) & 0x1
            await Timer(CLKS_PER_BIT*CLOCK_PERIOD_NS, units='ns')
            assert dut.o_tx_serial.value == expected_bit, "the bit {} is not the expected".format(i)

        # wait until transference done flag is set
        while dut.o_tx_done.value == 0:
            await Timer(CLOCK_PERIOD_NS, units='ns')

        bytes_tested += 1
    
    await Timer(CLOCK_PERIOD_NS*2, 'ns')


@cocotb.test()
async def test_uart_rx_black_box(dut):
    """ 
    Test the UART receptor module as a black box, sending bytes with a 'fake' uart
    and checking if at the output the data is the expected one.
    """
    test_inputs = [
            b'hola!', b'como va?', b'123456789ABCDEF',
            b'naruto', b'goku', b'!!duck',
        ]

    dut._log.info('Starting UART RX black box testbench!')
    clock = Clock(dut.i_clk, CLOCK_PERIOD_NS, units="ns")
    cocotb.start_soon(clock.start())  # Start the clock

    await reset_dut(dut.i_reset, CLOCK_PERIOD_NS, 'ns')

    dut.i_rx_serial.value = 1
    await Timer(CLOCK_PERIOD_NS, 'ns')

    for input in test_inputs:
        bytes_counter = 0
        total_data_received = 0

        ## send bytes to the uart module with an emulated uart
        cocotb.start_soon( send_bytes_through_uart( dut.i_rx_serial, dut._log,
                                                    baudrate=BAUDRATE, data_to_send=input,
                                                    stop_bits=1)
                                                    )

        # check if the received bytes are the expected ones
        for b in input:
            while( dut.o_rx_dv.value == 0 ):
                await Timer(CLOCK_PERIOD_NS, 'ns')

            received_data = dut.o_rx_byte.value.integer
            assert received_data == b, f'byte received by the UART is not the expected one! 0x{b:02X} != 0x{received_data:02X}'
            await Timer(CLOCK_PERIOD_NS, 'ns')
            total_data_received = (total_data_received << 8) | received_data
            bytes_counter += 1

        total_bytes_received = total_data_received.to_bytes(bytes_counter,'big')
        assert input == total_bytes_received, f'bytes received were not as expected'
        dut._log.info(f'bytes received are ok! {total_bytes_received} == {input}')


@cocotb.test()
async def test_uart_tx_black_box(dut):
    """ 
    Test the UART transmitter module as a black box, sending bytes with the module
    and checking if we can receive the sent bytes with a `fake` uart.
    """
    test_inputs = [
            b'hola!', b'como va?', b'123456789ABCDEF',
            b'naruto', b'goku', b'!!duck',
        ]

    dut._log.info('Starting UART TX black box testbench!')
    dut._log.info(f'Clocks cycles per bit: {CLKS_PER_BIT}')
    clock = Clock(dut.i_clk, CLOCK_PERIOD_NS, units="ns")
    cocotb.start_soon(clock.start())  # Start the clock

    await reset_dut(dut.i_reset, CLOCK_PERIOD_NS, 'ns')

    dut.i_rx_serial.value = 1
    await Timer(CLOCK_PERIOD_NS, 'ns')

    for input in test_inputs:
        bytes_counter = 0
        total_data_received = 0
        for b in input:
            dut._log.info(f'requesting to UART module to send byte 0x{b:02X}...')
            dut.i_tx_byte.value = b
            dut.i_tx_dv.value = 1
            await Timer(CLOCK_PERIOD_NS, 'ns')
            dut.i_tx_dv.value = 0

            rx_byte = await receive_bytes_through_uart(dut.o_tx_serial, dut._log,
                baudrate=BAUDRATE, bytes_qty=1, stop_bits=1
                )

            received_data = int.from_bytes(rx_byte, 'big')
            assert b == received_data, f'the received byte {received_data} is different from the sent one {b}'

            total_data_received = (total_data_received << 8) | received_data
            bytes_counter += 1

        total_bytes_received = total_data_received.to_bytes(bytes_counter,'big')
        assert input == total_bytes_received, f'bytes received were not as expected'
        dut._log.info(f'bytes received are ok! {total_bytes_received} == {input}')
