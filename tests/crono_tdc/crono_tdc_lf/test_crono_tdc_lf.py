from resource import RLIMIT_SIGPENDING
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge
import cocotb
from cocotb.utils import get_sim_time
from crc import CrcCalculator, Crc16
import enum
from math import floor

from tb_tools.tdc_commands import CMD_ID, read_crono_tdc_version, build_tdc_command
from tb_tools.utils import reset_dut
from tb_tools.hdlc_lite import build_hdlc_lite_frame, receive_frame_through_uart, send_frame_through_uart
from tb_tools.uart import send_bytes_through_uart
from tb_tools.tdc_input import simulate_input_signal, generate_multiphase_clocks
from tb_tools.ce_read_channels_data_utils import calculate_expected_tx_buffers
from tb_tools.fake_tdc_patterns import TEST_PATTERNS


CRONO_TDC_CORE_VERSION_PKG_REL_PATH='../../../src/crono_tdc_core_version_pkg.sv'

CLOCK_FREQ_HZ   = 100*1e6
CLOCK_PERIOD_NS = 1e9/CLOCK_FREQ_HZ
BAUDRATE        = 921600
CLKS_PER_BIT    = int(CLOCK_FREQ_HZ/BAUDRATE)
TDC_CHANNEL_CLK_CYCLE_COUNT_WIDTH = 13
TDC_DATA_REG_WIDTH=TDC_CHANNEL_CLK_CYCLE_COUNT_WIDTH+7

TDC_CHANNEL_QTY = 3
CFG_REG_INIT_VAL_ENABLED_CHN = 0b111

CFG_REG_INIT_VAL_TIME_WINDOWS_US = 100

HDLC_LITE_FRAME_MAX_WIDTH_BITS=256 # including frame delimiters and CRC
TDC_CMD_EXECUTOR_TX_BUFFER_WIDTH=int((HDLC_LITE_FRAME_MAX_WIDTH_BITS-16)/2-16)
MAX_DATA_REGS_PER_FRAME=floor((HDLC_LITE_FRAME_MAX_WIDTH_BITS-4*8)/TDC_DATA_REG_WIDTH) # the -4*8 is because frame's data also have the channel id and data regs qty


async def test_read_reg_cmd(   dut, cmd_id: enum.EnumMeta, cmd_data: int,
                               expected_data_from_tdc: int):
    """
    Co-routine used to test a command that reads a register, sending and HDLC-lite frame
    byte by byte simulating a UART.

    Parameters
    ----------
    cmd_id : enum.EnumMeta
        The ID of the command that will be tested.

    cmd_data : int
        The data that must be included in the command.

    expected_data_from_tdc : int
        Is the output data that we expect from the TDC. Must be in binary 
        format (because this, the type is integer).
    """
    cmd_to_send = build_tdc_command(cmd_id = cmd_id, cmd_data = cmd_data, bytes_rep=True)
    hdlc_frame_to_send = build_hdlc_lite_frame(cmd_to_send)

    crc_calculator = CrcCalculator(Crc16.CCITT)
    expected_crc_data_from_tdc = crc_calculator.calculate_checksum(expected_data_from_tdc).to_bytes(2,'big')

    dut._log.info(  "Testing read register command \n"
                    f'Command to send: {cmd_id}\n'
                    f'Byte-width of the command: {len(cmd_to_send)}\n'
                    f'Expected data from TDC: {expected_data_from_tdc}\n'
                    f'HDLC-lite frame to send: {hdlc_frame_to_send}\n'
                    f'Byte-width of the hdlc-lite frame: {len(hdlc_frame_to_send)}\n'
    )

    cocotb.start_soon(
        send_bytes_through_uart(dut.i_rx_serial, dut._log,
            baudrate=BAUDRATE, data_to_send=hdlc_frame_to_send
            )
        )
    frame = await receive_frame_through_uart(dut.o_tx_serial, dut._log, baudrate=BAUDRATE, stop_bits=1)

    assert frame['data'] == expected_data_from_tdc, f"the data received from the TDC was not the expected {frame['data']} != {expected_data_from_tdc}"
    assert frame['crc'] == expected_crc_data_from_tdc, f"the crc received from the TDC was not the expected {frame['crc']} != {expected_crc_data_from_tdc}"

    dut._log.info(f'received frame: {frame} is the expected one!')

    return frame['data']


@cocotb.test()
async def test_read_crono_tdc_core_cmd(dut):
    """
    Test if the dut can decode the command `READ_CRONO_TDC_CORE_VERSION` and send the current
    version of the core as a response.
    """
    crono_tdc_version = read_crono_tdc_version(CRONO_TDC_CORE_VERSION_PKG_REL_PATH)
    expected_data_from_tdc = bytes(crono_tdc_version, 'ascii')

    dut._log.info(  "Testing READ_CRONO_TDC_CORE_VERSION command \n"
                    f"Crono TDC Core version: {crono_tdc_version}\n"
        )

    clock = Clock(dut.i_clk, CLOCK_PERIOD_NS, units="ns")
    cocotb.start_soon(clock.start())  # Start the clock

    await Timer(CLOCK_PERIOD_NS*10, 'ns')
    await reset_dut(dut.i_reset, CLOCK_PERIOD_NS, 'ns')

    frame_data = await test_read_reg_cmd(dut, CMD_ID.READ_CRONO_TDC_CORE_VERSION,0,expected_data_from_tdc)

    dut._log.info(f"Crono TDC Core version readed from TDC: {frame_data} => {frame_data.decode('ascii')}!!")


@cocotb.test()
async def test_read_time_windows_us_cmd(dut):
    """
    Test if the dut can decode the command `READ_TIME_WINDOWS_US` and send the 
    default value of this register as a response.
    """
    time_windows_init_val = CFG_REG_INIT_VAL_TIME_WINDOWS_US
    expected_data_from_tdc = time_windows_init_val.to_bytes(4,'big')

    dut._log.info(  "Testing READ_TIME_WINDOWS_US command \n"
                    f"Time windows us initial val: {time_windows_init_val}\n"
        )

    clock = Clock(dut.i_clk, CLOCK_PERIOD_NS, units="ns")
    cocotb.start_soon(clock.start())  # Start the clock

    await Timer(CLOCK_PERIOD_NS*10, 'ns')
    await reset_dut(dut.i_reset, CLOCK_PERIOD_NS, 'ns')

    frame_data = await test_read_reg_cmd(dut, CMD_ID.READ_TIME_WINDOWS_US,0,expected_data_from_tdc)

    dut._log.info(f"time windows us readed from TDC: {frame_data} => {int.from_bytes(frame_data,'big')} us!!")


async def test_write_read_reg(dut,  read_cmd_id: enum.EnumMeta, reg_width_bytes: int,
                                    write_cmd_id: enum.EnumMeta, write_cmd_data: bytes):
    """
    Co-routine used to test a command that writes a register sending and HDLC-lite frame
    byte by byte simulating a UART. Then, we read the value of the register with a read
    register command and checks if the value that stores the register is the value set
    before.

    Parameters
    ----------
    read_cmd_id : enum.EnumMeta
        The ID of the command that reads the register that will be tested.

    reg_width_bytes : int
        The width in bytes of the register.

    write_cmd_id : int
        The ID of the command that writes the register.

    write_cmd_data : int
        The data of the command that writes the register (value to be written in register).
    """
    # prepare and send write register cmd
    write_cmd = build_tdc_command(cmd_id = write_cmd_id, cmd_data = write_cmd_data, bytes_rep=True)
    write_hdlc_frame = build_hdlc_lite_frame(write_cmd)
    dut._log.info(  f"Sending write register command, trying to write the value 0x{int.from_bytes(write_cmd_data,'big')} on the register\n"
                    f'Command to send: {write_cmd}\n'
                    f'Byte-width of the command: {len(write_cmd)}\n'
                    f'HDLC-lite frame to send: {write_hdlc_frame}\n'
                    f'Byte-width of the hdlc-lite frame: {len(write_hdlc_frame)}\n'
    )
    await send_frame_through_uart(
            dut_rx_serial = dut.i_rx_serial, logger = dut._log, 
            frame = write_hdlc_frame, baudrate = BAUDRATE)

    # prepare and send read register cmd
    crc_calculator = CrcCalculator(Crc16.CCITT)
    read_cmd = build_tdc_command(cmd_id = read_cmd_id, cmd_data = 0x00, bytes_rep=True)
    read_hdlc_frame = build_hdlc_lite_frame(read_cmd)
    expected_crc = crc_calculator.calculate_checksum(write_cmd_data).to_bytes(2,'big')
    
    # compute expected value of register if the write value is smaller or bigger 
    # than the register width
    write_val_bytes_diff = reg_width_bytes - len(write_cmd_data)
    if write_val_bytes_diff > 0:
        expected_data = write_cmd_data
        for i in range(write_val_bytes_diff):
            expected_data = b'\x00' + expected_data
    elif write_val_bytes_diff < 0:
        expected_data = write_cmd_data[4:]
    else:
        expected_data = write_cmd_data

    dut._log.info(  "Sending read register command \n"
                    f'Command to send: {read_cmd}\n'
                    f'Byte-width of the command: {len(read_cmd)}\n'
                    f'Expected data from TDC: 0x{expected_data}\n'
                    f'HDLC-lite frame to send: {read_hdlc_frame}\n'
                    f'Byte-width of the hdlc-lite frame: {len(read_hdlc_frame)}\n'
    )

    cocotb.start_soon(
        send_frame_through_uart(
                dut_rx_serial = dut.i_rx_serial, logger = dut._log, 
                frame = read_hdlc_frame, baudrate = BAUDRATE)
    )

    frame = await receive_frame_through_uart(dut.o_tx_serial, dut._log, baudrate=BAUDRATE, stop_bits=1)

    assert int.from_bytes(frame['data'], 'big') == int.from_bytes(expected_data, 'big'), f"the data received from the TDC was not the expected {frame['data']} != {expected_data}"
    assert frame['crc'] == expected_crc, f"the crc received from the TDC was not the expected {frame['crc']} != {expected_crc}"

    dut._log.info(f"received frame: {frame} is the expected one! The value of the register was changed to 0x{int.from_bytes(expected_data, 'big')}")

    return frame['data']


@cocotb.test()
async def test_write_and_read_time_windows_us_reg(dut):
    """
    Test if we can set the register `TIME_WINDOWS_US` with a command 
    and then read the new value with another command.
    """
    dut._log.info("Testing READ_TIME_WINDOWS_US & WRITE_TIME_WINDOWS_US command")

    clock = Clock(dut.i_clk, CLOCK_PERIOD_NS, units="ns")
    cocotb.start_soon(clock.start())  # Start the clock

    await Timer(CLOCK_PERIOD_NS*10, 'ns')
    await reset_dut(dut.i_reset, CLOCK_PERIOD_NS, 'ns')

    await test_write_read_reg(dut,
        reg_width_bytes=4,
        read_cmd_id = CMD_ID.READ_TIME_WINDOWS_US,
        write_cmd_id = CMD_ID.WRITE_TIME_WINDOWS_US,
        write_cmd_data = b'\x26\x25\xA0')

    dut._log.info(f"We could write and read time windows us register correctly!!")


@cocotb.test()
async def test_write_and_read_channels_enabled_reg(dut):
    """
    Test if we can set the register `CHANNELS_ENABLED` with a command 
    and then read the new value with another command.
    """
    dut._log.info("Testing WRITE_CHANNELS_ENABLED & READ_CHANNELS_ENABLED command")

    clock = Clock(dut.i_clk, CLOCK_PERIOD_NS, units="ns")
    cocotb.start_soon(clock.start())  # Start the clock

    await Timer(CLOCK_PERIOD_NS*10, 'ns')
    await reset_dut(dut.i_reset, CLOCK_PERIOD_NS, 'ns')

    await test_write_read_reg(dut,
        reg_width_bytes=4,
        read_cmd_id = CMD_ID.READ_CHANNELS_ENABLED,
        write_cmd_id = CMD_ID.WRITE_CHANNELS_ENABLED,
        write_cmd_data = b'\x01')

    dut._log.info(f"We could write and read channels enabled register correctly!!")


async def send_measure_cmd(dut):
    """
    Send a MEASURE command.
    
    Parameters
    ----------
    dut:
        The device under test.
    """
    # prepare and send write register cmd
    meas_cmd = build_tdc_command(cmd_id = CMD_ID.MEASURE, cmd_data = b'', bytes_rep=True)
    meas_hdlc_frame = build_hdlc_lite_frame(meas_cmd)
    dut._log.info(  f"Sending MEASURE command\n"
                    f'Command to send: {meas_cmd}\n'
                    f'Byte-width of the command: {len(meas_cmd)}\n'
                    f'HDLC-lite frame to send: {meas_hdlc_frame}\n'
                    f'Byte-width of the hdlc-lite frame: {len(meas_hdlc_frame)}\n'
    )

    init_time_us = get_sim_time('us')
    await send_frame_through_uart(
            dut_rx_serial = dut.i_rx_serial, logger = dut._log, 
            frame = meas_hdlc_frame, baudrate = BAUDRATE)

    frame = await receive_frame_through_uart(dut.o_tx_serial, dut._log, baudrate=BAUDRATE, stop_bits=1)
    assert frame['data'] == bytes(f"ena{CFG_REG_INIT_VAL_ENABLED_CHN:08X}", 'ascii')

    dut._log.info(f"confirmation message received successfully: {frame['data'].decode('ascii')}")
    total_time_us = get_sim_time('us') - init_time_us
    dut._log.info(f"measuring + confirmation msg total time: {total_time_us} us")


async def read_channels_data(dut) -> set:
    """ Execute READ_CHANNELS_DATA command. """
    received_frames = set()
    meas_cmd = build_tdc_command(cmd_id = CMD_ID.READ_CHANNELS_DATA, cmd_data = b'', bytes_rep=True)
    meas_hdlc_frame = build_hdlc_lite_frame(meas_cmd)
    dut._log.info(  f"Sending READ_CHANNELS_DATA command\n"
                    f'Command to send: {meas_cmd}\n'
                    f'Byte-width of the command: {len(meas_cmd)}\n'
                    f'HDLC-lite frame to send: {meas_hdlc_frame}\n'
                    f'Byte-width of the hdlc-lite frame: {len(meas_hdlc_frame)}\n'
    )

    await send_frame_through_uart(
            dut_rx_serial = dut.i_rx_serial, logger = dut._log, 
            frame = meas_hdlc_frame, baudrate = BAUDRATE)

    init_time_us = get_sim_time('us')
    end_frame_received = False
    while end_frame_received == False:
        frame = await receive_frame_through_uart(dut.o_tx_serial, dut._log, baudrate=BAUDRATE, stop_bits=1)
        end_frame_received = True if (frame['data'] == bytes("enaread", 'ascii')) else False
        if end_frame_received:
            break
        else:
            channel = (int.from_bytes(frame['data'], 'big') >> 240) & 0xFFFF 
            data_regs_qty = (int.from_bytes(frame['data'], 'big') >> 232) & 0xFF
            dut._log.info(f"info from frame:\nchannel: {channel}\ndata_regs_qty: {data_regs_qty}\nframe: 0x{int.from_bytes(frame['data'], 'big'):032X}")
            received_frames.add(int.from_bytes(frame['data'], 'big'))
            elapsed_time_us = get_sim_time('us') - init_time_us
            assert elapsed_time_us < 10000, "timeout error!"

    return received_frames


@cocotb.test()
async def test_measure_cmd_isolated(dut):
    """
    Test if measure command works as expected. The tdc must send a confirmation message
    after the time windows time saying that the channels are fully measured.
    """
    dut._log.info("Testing MEASURE command!")

    clock = Clock(dut.i_clk, CLOCK_PERIOD_NS, units="ns")
    cocotb.start_soon(clock.start())  # Start the clock

    await Timer(CLOCK_PERIOD_NS*10, 'ns')
    await reset_dut(dut.i_reset, CLOCK_PERIOD_NS, 'ns')

    await send_measure_cmd(dut)


def build_expected_frames_from_channels_data(channels_input_data:dict) -> list:
    """ Build the expected HDLC frame data from channels inputs. """
    print(f"building expected frames from channels data:\nHDLC_LITE_FRAME_MAX_WIDTH_BITS = {HDLC_LITE_FRAME_MAX_WIDTH_BITS}\nMAX_DATA_REGS_PER_FRAME = {MAX_DATA_REGS_PER_FRAME}\n")
    expected_frames_data = list()
    for chn in channels_input_data:
        print(f"\nbuilding expected tx buffers for chn {chn} test pattern ")
        data_regs_from_chn = list()

        for data_reg_info in channels_input_data[chn]['expected_ram_data']:
            print(f"### building expected frames for the following ram data {data_reg_info} ###")
            data_reg = (data_reg_info['clock_cycle_count'] << 7) \
                        | (data_reg_info['data'] << 3) \
                        | data_reg_info['tag']
            data_regs_from_chn.append(data_reg)

        tx_buffers_from_chn = calculate_expected_tx_buffers(
            channels_ram={chn:data_regs_from_chn},
            channel_data_reg_width=TDC_DATA_REG_WIDTH,
            tx_buffer_width_bytes=int(TDC_CMD_EXECUTOR_TX_BUFFER_WIDTH/8)
        )[chn]
        
        print(tx_buffers_from_chn)
        
        expected_frames_data.extend(tx_buffers_from_chn)

    return expected_frames_data


async def simulate_channels_inputs(dut, channels_input_data:dict):
    """ Simulate channels inputs. """
    ## This hook is not pretty, but is the only way where we can inject signals in the channels
    await RisingEdge(dut.core_logic.cmd_executor.ce_measure_channels.o_measure_ongoing)
       
    dut._log.info(f"channel_inputs_simulator: TDC is measuring, generating input signals...")
    for chn in channels_input_data:
        cocotb.start_soon(simulate_input_signal(
            dut_signal=dut.channel[chn].tdc_input.i_data,
            values=channels_input_data[chn]['input'],
            clock_period_ns=channels_input_data[chn]['input_period_ns']
            )
        )


@cocotb.test()
async def test_complete_pipeline(dut):
    """
    Measure with Crono TDC and read channels data. Then check if the received data is equal to expected.
    """
    dut._log.info("Testing complete pipeline (measure and then read channels data)...")
    dut.i_channels_inputs.value = 0

    channels_input_data = {
        0: TEST_PATTERNS['fast_small_pulses'],
        1: TEST_PATTERNS['few_fast_small_pulses'],
        2: TEST_PATTERNS['fast_long_pulses']
    }

    clock = Clock(dut.i_clk, CLOCK_PERIOD_NS, units="ns")
    cocotb.start_soon(clock.start())  # Start the clock

    await Timer(1, 'us')
    expected_frames_data = build_expected_frames_from_channels_data(channels_input_data)
    dut._log.info(f"expected frames data:")
    for fd in expected_frames_data:
        dut._log.info(f"{fd:X}")

    cocotb.start_soon(generate_multiphase_clocks(dut, CLOCK_PERIOD_NS))

    await Timer(CLOCK_PERIOD_NS*10, 'ns')
    await reset_dut(dut.i_reset, CLOCK_PERIOD_NS*3, 'ns')

    cocotb.start_soon(simulate_channels_inputs(dut, channels_input_data))

    await send_measure_cmd(dut)

    received_frames_data = await read_channels_data(dut)
    dut._log.info(f"received frames data:")
    for f in received_frames_data:
        dut._log.info(f"{f:0X}")

    for frame in received_frames_data:
        is_expected = False
        dut._log.info(f"checking if frame 0x{frame:0X} is expected...")

        # here we check if received frame is equal to any expected frame
        # note: we use `startswith` because there are trailing zeros in expected frames
        # that are deleted in the transmission chain
        for i in range(len(expected_frames_data)):
            expected_frame = expected_frames_data[i]
            is_expected = True if frame == expected_frame else False
            if is_expected:
                dut._log.info(f"frame {frame:0X} is expected! pop-ing element {i} from list...")
                expected_frames_data.pop(i)
                break

        expected_frames_left_str = ""
        for ef in expected_frames_data:
            expected_frames_left_str = expected_frames_left_str + f"{ef:0X}\n"
        if not expected_frames_left_str:
            dut._log.info(f"expected frames left: {expected_frames_left_str}")

        assert is_expected == True, f"frame {frame:0X} is not expected!"
    
    dut._log.info(f"EXITO! all expected frames were received and no un-expected frame was received!!") 
